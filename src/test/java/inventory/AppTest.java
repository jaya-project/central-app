package inventory;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.xml.xpath.XPathExpressionException;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.ui.Launcher;
import com.valorti.jaya.ui.Main;
import com.valorti.utils.PathUtils;
import com.valorti.utils.WinCEUtils;

public class AppTest {
	private final Logger	log	= LoggerFactory.getLogger(AppTest.class);

	public AppTest() throws Exception {
		Launcher.main(null);
		// Main.configure();
	}

	public void testRapi() {
		WinCEUtils wince = new WinCEUtils(Main.configuration().getString(
				"/Application/WinCE/RapiWrapper/Executable"));

		try {
			final String deviceDataFolder = Main.configuration().getString(
					"/Application/WinCE/DeviceDataFolder");
			final String entriesFileName = Main.configuration().getString(
					"/Application/WinCE/Files/File[@id='entries']");
			final String desktopDataFolder = Main.configuration().getString(
					"/Application/WinCE/TmpDesktopFolder");

			File entriesFile = new File(PathUtils.combine(deviceDataFolder,
					entriesFileName));
			int response = wince.copyFile(entriesFile, desktopDataFolder,
					WinCEUtils.OPTION_REMOVE_AFTER_COPY);
			log.debug("Response code: {}", response);

			if (response != 1) {
				log.error("Failed to copy entries file: {}");
			}
		} catch (Exception ex) {
			log.error(
					"An exception was caught while copying the entries file to the desktop.",
					ex);
		}
	}

	@Test
	public void test() throws DocumentException, XPathExpressionException {
		ln("--- TEST START ---");
		// String num = "1.0";
		//
		// try {
		// ln("Num: " + Integer.parseInt(num));
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
		//
		// num = "1.0";
		//
		// try {
		// ln("Num: " + Double.parseDouble(num));
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }

		// listPrinters();
		/*
		 * XMLConfiguration config; try { config = new XMLConfiguration(new
		 * File("conf/app-new.xconf")); ln("Images folder: " +
		 * config.getString("/Application/Barcode/ApiCalls/ApiCall"
		 * ));//config.getString("//FolderStructure/ImagesFolder")); /*boolean
		 * flc =
		 * config.getBoolean("/Application/SpecificConfiguration/FirstLoadComplete"
		 * ); ln("First load complete: " + flc);
		 * 
		 * List<String> list =
		 * config.getStringList("/Application/Barcode/ApiCalls");
		 * 
		 * if(list != null && list.size() > 0) { ln("List is not empty.");
		 * 
		 * for(String s: list) { ln("List value: " + s); } } else {
		 * ln("List empty."); } } catch (FileNotFoundException e) {
		 * e.printStackTrace(); }
		 */
		// traverse(doc.node(0).selectNodes(doc.node(0).getPath()));
	}

	private void listPrinters() {
		PrintService[] services = PrintServiceLookup.lookupPrintServices(null,
				null);

		if (services != null) {
			for (PrintService service : services) {
				log.debug("Service: {}", service.getName());
				// log.debug("Name: {}",
				// service.getAttribute(PrinterName.class));
				listPrinterData(service);
			}
		}
	}

	private void listPrinterData(PrintService printer) {
		Class<? extends Attribute>[] categories = ((Class<? extends Attribute>[]) printer
				.getSupportedAttributeCategories());
		DocFlavor[] flavors = printer.getSupportedDocFlavors();
		AttributeSet attributes = printer.getAttributes();

		// get all the avaliable attributes
		for (Class<? extends Attribute> category : categories) {
			for (DocFlavor flavor : flavors) {
				// get the value
				Object value = printer.getSupportedAttributeValues(category,
						flavor, attributes);
				listPrinterAttributes(value);
			}
		}
	}

	private void listPrinterAttributes(Object value) {
		// check if it's something
		if (value != null) {
			// if it's a SINGLE attribute...
			if (value instanceof Attribute) {
				Attribute attr = (Attribute) value;
				log.debug("Attribute: {}", attr);
			}
			// if it's a SET of attributes...
			else if (value instanceof Attribute[]) {
				ln("Attribute list:");
				Attribute[] list = (Attribute[]) value;

				for (Attribute attr : list) {
					log.debug("Attribute: {}", attr.toString());
				}
			}
		}
	}

	private void traverse(List<?> list) {
		Iterator<?> it = list.iterator();

		for (; it.hasNext();) {
			Element element = (Element) it.next();
			List<?> nodeList = element.elements();
			l("Current node: [" + element.getName() + ", ");

			if (nodeList != null && nodeList.size() > 0) {
				traverse(nodeList);
				ln("]");
			} else {
				ln("Value: " + element.getText() + "]");
			}
		}
	}

	private void l(String s) {
		System.out.print(s);
	}

	private void ln(String s) {
		l(s + "\n");
	}
}
