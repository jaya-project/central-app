/**
 * 
 */
package com.valorti.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.InvalidXPathException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.io.SAXWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * Specialized class that loads a XML file for later use.
 * 
 * @author Nicolas Mancilla Vergara
 *
 */
public class XMLConfiguration {

	protected final Logger	log					= LoggerFactory
														.getLogger(XMLConfiguration.class);

	/**
	 * XML configuration file.
	 */
	protected File			xmlFile				= null;

	/**
	 * 
	 */
	protected Document		XDoc				= null;

	/**
	 * String to split tag values (Default: <b>,</b>)
	 */
	private String			listSeparator		= ",";

	/**
	 * Used to filter out variables inside tag values.
	 */
	private final Pattern	varPattern			= Pattern
														.compile("(\\$\\{[A-Za-z0-9_\\.\\-()\\[\\]'@=]+\\})");

	private final Pattern	xpathPattern		= Pattern
														.compile("^((/{1,2})?([A-Za-z_][A-Za-z_\\-\\.()]*))+$");

	/**
	 * Root path to the root tag.
	 */
	protected String		rootPath			= "";

	/**
	 * Enables or disables the search for nodes that contains text only.
	 */
	protected boolean		textOnlyNodeMode	= true;

	/**
	 * Enables or disables the auto save function.
	 */
	protected boolean		autoSave			= true;

	/**
	 * Instantiates this class.
	 * 
	 * @param xmlFile
	 *            The configuration file path.
	 * @throws FileNotFoundException
	 *             If the file could not be found and the defaults.xconf isn't
	 *             present either.
	 */
	public XMLConfiguration(File xmlFile) throws FileNotFoundException {
		if (xmlFile.exists()) {
			this.xmlFile = xmlFile;
		} else {
			log.warn(
					"User Configuration file does not exist! Loading defaults instead.\n{}",
					"This program needs a file named app.xconf to be in the configuration directory (configuration/app.xconf) in order to work.\n{}",
					"In the future, edit the file configuration/app.xconf with an XML editor to configure.");
			InputStream input = null;
			BufferedReader reader = null;

			input = getClass().getResourceAsStream("defaults.xconf");
			reader = new BufferedReader(new InputStreamReader(input));
			OutputStreamWriter out = null;
			String line = "";
			this.xmlFile = new File("configuration/app.xconf");

			if (!this.xmlFile.getParentFile().exists())
				this.xmlFile.getParentFile().mkdirs();

			try {
				out = new FileWriter(xmlFile);

				while ((line = reader.readLine()) != null) {
					out.write(line);
				}
			} catch (IOException ex) {
				log.error(
						"An error ocurred reading the defaults configuration file: {}.",
						line, ex);
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException ex) {
						log.error("Failed to close input stream.", ex);
					}
					input = null;
				}

				if (reader != null) {
					try {
						reader.close();
					} catch (IOException ex) {
						log.error("Failed to close buffered reader.", ex);
					}

					reader = null;
				}

				if (out != null) {
					try {
						out.close();
					} catch (IOException ex) {
						log.error("Failed to close output writter.", ex);
					}

					out = null;
				}
			}

			if (!this.xmlFile.exists()) {
				throw new FileNotFoundException(
						"Could not find the [defaults.xconf] file (it should be on the resources folder).");
			}
		}

		loadXML();
	}

	/**
	 * Expands the node text value.
	 * 
	 * @param text
	 *            The string value of the node (assuming it's a node and not a
	 *            node list).
	 * @return The expanded text if there was something to expand or just the
	 *         text if there is nothing to parse.
	 */
	protected String expandText(String text) {
		log.trace("Expanding XML variables in text [{}].", text);
		String r = "";
		Matcher matcher = varPattern.matcher(text);

		if (matcher != null) {
			log.debug("Variables found: {}", matcher.groupCount());

			while (matcher.find()) {
				String group = matcher.group();
				String clean = group.replaceAll("[\\{\\}\\$]{1}", "").replace(
						".", "/");
				StringBuilder xpath = new StringBuilder(rootPath);
				xpath.append('/').append(clean.replace(".", "/"));
				log.trace("Current group: [group={}, clean={}, xpath={}].",
						group, clean, xpath);

				try {
					Node node = XDoc.selectSingleNode(xpath.toString());

					if (node != null) {
						r = (expandText(node.getText()));
						text = (text.replace(group, r));
						r = text;
						log.trace("Parsed text [{}]", r);
					}
				} catch (Exception ex) {
					log.error("Node selection failed.", ex);
				}

				group = null;
				clean = null;
				xpath = null;
			}
		}

		if (r.length() <= 0) {
			r = text;
		}

		matcher = null;

		return r;
	}

	/**
	 * Retrieves a boolean value.
	 * 
	 * @param xpath
	 *            Path to retrieve.
	 * @return The boolean value from the xml document. Returns false if:
	 *         <ul>
	 *         <li>The text could not be retrieved or</li>
	 *         <li>Value is not correct, thus cannot be parsed.</li>
	 *         </ul>
	 */
	public boolean getBoolean(String xpath) {
		boolean r = false;
		String s = getString(xpath);

		try {
			r = Boolean.parseBoolean(s);
		} catch (Exception ex) {
			log.error("Failed to parse [{}] to boolean.", s, ex);
		}

		return r;
	}

	/**
	 * Retrieves a double value from XML Document.
	 * 
	 * @param xpath
	 *            Expression to retrieve.
	 * @return The value on the document or -1.0 if the value cannot be parsed.
	 */
	public double getDouble(String xpath) {
		double r = -1.0;
		String s = getString(xpath);

		try {
			r = Double.parseDouble(s);
		} catch (Exception ex) {
			log.error("Failed to parse [{}] to double.", s, ex);
		}

		return r;
	}

	/**
	 * Retrieves an integer value from the xml document.
	 * 
	 * @param xpath
	 *            Expression to find.
	 * @return The integer value in the XML document or -1 if the value couldn't
	 *         be parsed.
	 */
	public int getInteger(String xpath) {
		int r = -1;
		String s = getString(xpath);

		try {
			r = Integer.parseInt(s);
		} catch (Exception ex) {
			log.error("Failed to parse [{}] to integer.", s, ex);
		}

		return r;
	}

	/**
	 * Retrieves the currently used list separator.
	 * 
	 * @return Returns the list separator.
	 */
	public String getListSeparator() {
		return listSeparator;
	}

	/**
	 * Retrieves a long value from XML Document.
	 * 
	 * @param xpath
	 *            Expression to retrieve.
	 * @return The value on the document or -1 if the value cannot be parsed.
	 */
	public long getLong(String xpath) {
		long r = -1L;
		String s = getString(xpath);

		try {
			r = Long.parseLong(s);
		} catch (Exception ex) {
			log.error("Failed to parse [{}] to long.", s, ex);
		}

		return r;
	}

	/**
	 * Retrieves the string value for the XPath argument.
	 * 
	 * @param xpath
	 *            The XPath selector.
	 * @return The selected value or empty if the node does not exists. If the
	 *         node is not a text-only node, then this function returns the
	 *         first node that contains only text. To disable this behavior,
	 *         before calling this function, call
	 *         {@link XMLConfiguration#setTextOnlyNodeMode} with false.
	 */
	public String getString(String xpath) {
		String s = "";
		Element element = read(xpath);

		if (element != null) {
			if (element.isTextOnly()) {
				s = expandText(element.getText());
			} else {
				log.warn("The path [{}] is not a text only value.", xpath);

				if (isTextOnlyNodeModeEnabled())
					s = selectFirstTextNode(element).getText();
			}
		}

		log.trace("Retrieved value: {}", s);

		return s;
	}

	/**
	 * Retrieves a string array from the xpath selector. If the xpath node has
	 * children nodes, then this function returns the values of every node as a
	 * string array. On the other hand, if the value
	 * 
	 * @param xpath
	 *            The xpath selector.
	 * @return
	 */
	public String[] getStringArray(String xpath) {
		String[] list = null;
		Element element = read(xpath);

		if (element != null) {
			if (element.isTextOnly()) {
				list = element.getText().split(listSeparator);
			} else {
				int nodeCount = element.nodeCount();
				list = new String[nodeCount];

				for (int i = 0; i < nodeCount; i++) {
					try {
						list[i] = element.node(i).getText();
					} catch (Exception ex) {
						log.error(
								"Failed to retrieve element text from expression [{}] (Node number: {}).",
								xpath, i, ex);
					}
				}
			}
		}

		return list;
	}

	/**
	 * Retrieves a String list from the XML Document.
	 * 
	 * @param xpath
	 *            The XPath selector.
	 * @return A String list.
	 */
	public List<String> getStringList(String xpath) {
		return Arrays.asList(getStringArray(xpath));
	}

	/**
	 * Retrieves the current XML document.
	 * 
	 * @return {@link org.dom4j.Document} The current XML Document.
	 */
	public Document getXDoc() {
		return XDoc;
	}

	/**
	 * Checks if the auto save option is enabled or not.
	 * 
	 * @return true if enabled, false if not.
	 */
	public boolean isAutoSaveEnabled() {
		return autoSave;
	}

	/**
	 * Checks if the text-only node mode is enabled or not.
	 * 
	 * @see {@link #setTextOnlyNodeMode}
	 * 
	 * @return true if enabled, false otherwise.
	 */
	public boolean isTextOnlyNodeModeEnabled() {
		return textOnlyNodeMode;
	}

	/**
	 * Loads the XML document into the XDoc variable.
	 */
	private void loadXML() {
		SAXReader reader = new SAXReader();
		InputStream input = null;

		try {
			input = new FileInputStream(xmlFile);
			XDoc = reader.read(input);
			rootPath = XDoc.node(0).getPath();
		} catch (DocumentException ex) {
			log.error("Failed to read XML configuration file [{}].",
					xmlFile.getAbsolutePath(), ex);
		} catch (FileNotFoundException ex) {
			log.error("", ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException ex) {
					log.error("", ex);
				}
				input = null;
			}
		}
	}

	/**
	 * Retrieves an Element from the document.
	 * 
	 * @param xpath
	 *            The epression to retrieve.
	 * @return An Element instance or null if the element does not exist.
	 */
	private Element read(String xpath) {
		log.info("Retrieving node: [xpath={}].", xpath);
		try {
			Object o = XDoc.selectSingleNode(xpath);

			if (o != null) {
				return (Element) o;
			} else {
				log.warn("Failed to retrieve element for path [{}].", xpath);
			}
		} catch (InvalidXPathException ex) {
			log.error("The XPath [expression={}] is not valid.", xpath, ex);
		}

		return null;
	}

	/**
	 * Overwrites the current configuration.
	 * 
	 * @throws SAXException
	 */
	public void save() throws SAXException {
		SAXWriter writer = new SAXWriter();
		writer.write(XDoc);
		writer = null;
	}

	/**
	 * Selects the first node that has only text.
	 * 
	 * @param e
	 *            The element to iterate.
	 * @return A string with the first node value or empty if there is none.
	 */
	private Element selectFirstTextNode(Element element) {
		Element r = null;

		if (!element.isTextOnly()) {
			log.trace("Iterating elements to select first text-only node.");
			Iterator<?> it = element.elementIterator();

			for (; it.hasNext();) {
				Element el = (Element) it.next();

				if (el.isTextOnly()) {
					r = el;
					break;
				} else {
					if (isTextOnlyNodeModeEnabled())
						selectFirstTextNode(el);
				}
			}
		}

		return r;
	}

	/**
	 * Set the attribute value.
	 * 
	 * @param xpath
	 *            Path to the node.
	 * @param attribute
	 *            Attribute name.
	 * @param value
	 *            Attribute value.
	 * @return True if the value was set, false if the node wasn't found.
	 * @throws SAXException
	 *             See {@link #save()}.
	 */
	public boolean setAttribute(String xpath, String attribute, String value)
			throws SAXException {
		Element e = read(xpath);

		if (e != null) {
			if (e.addAttribute(attribute, value) != null) {
				if (isAutoSaveEnabled())
					save();

				return true;
			} else {
				log.warn(
						"Failed to set attribute to node: [path={}, attribute name={}, value={}]",
						xpath, attribute, value);
			}
		}

		return false;
	}

	/**
	 * Changes the state of the auto-save function.
	 * 
	 * @param state
	 *            true to enable, false to disable. <b>Default: true</b>.
	 */
	public void setAutoSave(boolean state) {
		autoSave = state;
	}

	/**
	 * Changes the list separator to use when retrieving a list or array of
	 * values (by calling getStringArray or getStringList).
	 * 
	 * @param listSeparator
	 *            The list separator to use.
	 */
	public void setListSeparator(String listSeparator) {
		this.listSeparator = listSeparator;
	}

	/**
	 * Sets whether or not the first text-only node should be retrieved when the
	 * XPath points to an element that has children. <b>Default: true</b>.
	 * 
	 * @param textOnly
	 *            True to enable, false to disable.
	 */
	public void setTextOnlyNodeMode(boolean textOnly) {
		this.textOnlyNodeMode = textOnly;
	}

	/**
	 * Sets a node value.
	 * 
	 * @param xpath
	 *            XPath selector.
	 * @param o
	 *            The object to set.
	 * @return true if the value was set, false otherwise.
	 * @throws SAXException
	 *             See {@link #save()}.
	 */
	public boolean setValue(String xpath, Object o) throws SAXException {
		if (validateXPath(xpath)) {
			if (o == null)
				o = "null";

			Element e = read(xpath);

			if (e != null) {
				if (e.isTextOnly()) {
					e.setText(o.toString());

					if (isAutoSaveEnabled())
						save();

					return true;
				} else {
					if (isTextOnlyNodeModeEnabled()) {
						selectFirstTextNode(e).setText(o.toString());

						if (isAutoSaveEnabled())
							save();

						return true;
					}
				}
			}
		} else {
			log.error("Invalid XPath expression: {}", xpath);
		}

		return false;
	}

	/**
	 * @see {@link #setValue(String, Object)}
	 */
	public boolean setValue(String xpath, String value) throws SAXException {
		return setValue(xpath, value);
	}

	protected boolean validateXPath(String xpath) {
		if (xpath == null || (xpath != null && xpath.length() <= 0))
			return false;

		return xpathPattern.matcher(xpath).matches();
	}
}
