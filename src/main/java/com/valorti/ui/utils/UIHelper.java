package com.valorti.ui.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Combo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.WarehouseCode;
import com.valorti.jaya.model.manager.WarehouseCodeManager;
import com.valorti.jaya.model.manager.WarehouseManager;
import com.valorti.jaya.ui.Main;
import com.valorti.utils.PathUtils;
import com.valorti.utils.StringUtils;

public class UIHelper {
	private static final Logger					log				= LoggerFactory
																		.getLogger(UIHelper.class);

	private static final WarehouseManager		warehouseMgr	= new WarehouseManager();

	private static final WarehouseCodeManager	whcodeMgr		= new WarehouseCodeManager();

	public UIHelper() {
	}

	/**
	 * Fills a combo with items.
	 * 
	 * @param combo
	 *            The combo that will be filled.
	 * @param items
	 *            An array of string that will fill the combo.
	 * @param defaultSelection
	 *            The default selected string.
	 * @param clearFirst
	 *            If the combo has elements, they will be removed if this option
	 *            is true.
	 */
	public static void fillCombo(Combo combo, Object[] items,
			String firstElement, Object selected, boolean clearFirst) {
		log.debug("Filling combo box...");

		if (clearFirst) {
			if (combo.getItemCount() > 0) {
				combo.removeAll();
			}
		}

		int select = -1, i = 0;

		if (firstElement != null && firstElement.length() > 0) {
			combo.add(firstElement);
			i++;
		}

		for (Object o : items) {
			if (selected != null) {
				log.debug("Current item | selected: {} == {}", o, selected);

				if (selected.equals(o)) {
					log.debug("Item matches selected param: {} == {}", o,
							selected);
					select = i;
				}
			}

			combo.add(o.toString());
			i++;
		}

		log.debug("Select combo index {}", select);
		combo.select(select);
	}

	public static void fillRowCombo(Combo combo, boolean clearFirst) {
		UIHelper.fillRowCombo(combo, null, clearFirst);
	}

	/**
	 * Fills the combo with the locationRows data from the configuration file.
	 * 
	 * @param combo
	 *            The combo that will be filled.
	 * @param clearFirst
	 *            If the combo has elements, this option removes all elements
	 *            before adding the new ones.
	 */
	public static void fillRowCombo(Combo combo, String currentRow,
			boolean clearFirst) {
		log.debug("Filling row combo...");
		try {
			String[] locationRows = Main.configuration().getStringArray(
					"/Application/SpecificConfiguration/LocationRows");
			fillCombo(combo, locationRows, "Seleccione un Rack", currentRow,
					clearFirst);
		} catch (Exception ex) {
			log.error("Exception caught while filling row combo.", ex);
		}
	}

	public static void fillWarehouseCombo(Combo combo, boolean clearFirst) {
		UIHelper.fillWarehouseCombo(combo, null, clearFirst);
	}

	/**
	 * Fills the warehouse combo.
	 */
	public static void fillWarehouseCombo(Combo combo, Warehouse warehouse,
			boolean clearFirst) {
		log.debug("Filling Warehouse combo.");
		List<Warehouse> whlist = warehouseMgr.list(Warehouse.class);

		if (whlist != null && whlist.size() > 0) {
			List<String> items = new ArrayList<String>();

			for (Warehouse wh : whlist) {
				items.add(String.format("%s - %s", wh.getWhName(), wh
						.getWarehouseCode().getWhCode()));
			}

			String selected = null;

			if (warehouse != null) {
				selected = String.format("%s - %s", warehouse.getWhName(),
						warehouse.getWarehouseCode().getWhCode());
			}

			UIHelper.fillCombo(combo, items.toArray(), "Seleccione una Bodega",
					selected, clearFirst);

			items = null;
		} else {
			log.error("The warehouse list was null or empty!");
		}

		whlist = null;
	}
	
	/**
	 * Retrieves the currently selected warehouse from the combo.
	 * 
	 * @return The selected warehouse or null if the selected index is 0 or
	 *         less.
	 */
	public static Warehouse getSelectedWarehouse(Combo combo) {
		Warehouse wh = null;

		if (combo.getSelectionIndex() > 0) {
			String selected = combo.getItem(combo.getSelectionIndex());
			WarehouseCode whcode = whcodeMgr.findByCode(StringUtils
					.getValueFromSplit(selected));

			if (whcode != null) {
				String name = StringUtils.getValueFromSplit(selected, 0);

				for (Iterator<Warehouse> it = whcode.getWarehouses().iterator(); it
						.hasNext();) {
					Warehouse tmp = it.next();

					if (tmp.getWhName().compareTo(name) == 0) {
						wh = tmp;
						break;
					}
				}
			}
		} else {
			log.error("The selected index is not greater than 0.");
		}

		return wh;
	}
	
	public static WarehouseCodeManager getWarehouseCodeManager() {
		return UIHelper.whcodeMgr;
	}

	public static WarehouseManager getWarehouseManager() {
		return UIHelper.warehouseMgr;
	}

	public static Image loadExcelIcon(Device device) {
		return loadImage(device, "excel_icon.png");
	}

	/**
	 * Loads an image from the images folder. Reads the configuration to get the
	 * image folder path.
	 * 
	 * @param fileName
	 *            Name of the file to load.
	 * @return The loaded image or null on error or if the file doesn't exist.
	 */
	public static Image loadImage(Device device, String fileName) {
		Image img = null;

		try {
			String path = Main.configuration().getString(
					"/Application/FolderStructure/ImagesFolder");

			if (path != null && path.length() > 0) {
				img = new Image(device, PathUtils.combine(path, fileName));
			}
		} catch (Exception ex) {
			log.error("Failed to load image resource [{}].", fileName, ex);
		}

		return img;
	}

	public static Image loadTitleIcon(Device device) {
		return loadImage(device, "title_icon.jpg");
	}

	/**
	 * Compares item by item to the Object passed and selects the index of the
	 * match (if any).
	 * 
	 * @param combo
	 *            The combo which item needs to be selected.
	 * @param compareTo
	 *            The object tha will be compared.
	 */
	public static void select(Combo combo, Object compareTo) {
		select(combo, compareTo, false);
	}

	/**
	 * Compares item by item to the Object passed and selects the index of the
	 * match (if any).
	 * 
	 * @param combo
	 *            The Combo control which item needs to be selected.
	 * @param compareTo
	 *            The object to which every item will be compared.
	 * @param backwards
	 *            If set to true, this function starts counting down, whereas if
	 *            set to false it starts at 0.
	 */
	public static void select(Combo combo, Object compareTo, boolean backwards) {
		if (combo != null) {
			int itemCount = combo.getItemCount();

			if (itemCount > 0) {
				boolean stop = false;
				int startAt = backwards ? (itemCount - 1) : 0, i = startAt;
				log.debug("Starting at {} (i = {}). Backwards? {}", startAt, i,
						backwards);

				while (!stop) {
					Object item = combo.getItem(i);
					boolean equals = item.equals(compareTo);
					log.debug("Item | CompareTo: ({} == {}) = {}", item,
							compareTo, equals);

					if (equals) {
						log.debug("Selecting index {} and stopping.", i);
						combo.select(i);
						stop = true;
					}

					if (!stop) {
						if (backwards) {
							i--;
							stop = i <= 0;
						} else {
							i++;
							stop = i == (itemCount - 1);
						}
						log.debug("i = {} - stop = {}", i, stop);
					}
				}
			}
		}
	}
}
