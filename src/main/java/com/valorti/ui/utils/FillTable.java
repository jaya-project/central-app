/**
 * 
 */
package com.valorti.ui.utils;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.ui.events.FillCompleteListener;

/**
 * @author Nicolás
 * 
 */
public class FillTable<E> implements Runnable {
	private List<FillCompleteListener>	fillCompleteListeners	= new ArrayList<FillCompleteListener>();
	private final Logger				log						= LoggerFactory
																		.getLogger(FillTable.class);
	private List<E>						list					= null;
	private Table						table					= null;
	private List<String>				columns					= new ArrayList<String>();
	private String						itemDataName			= "Id";
	protected String[]					expSequences			= null;
	protected int						currentSequenceIndex;
	protected String					separator				= " ";

	/***
	 * Instantiates a fill table class. This class uses the
	 * column.getData().toString() to call a method from the object list to
	 * populate the data. If this values are empty or null then the toString()
	 * method is called on every object. If the column data is a string
	 * separated by dots, then a succession of calls is made, for example:
	 * getObject.getName, would call Object.getName(), where Object is the
	 * object represented by the generic type E, so, make sure that the method
	 * exists.
	 * 
	 * @param list
	 *            - The list of objects to fill the table with.
	 * @param table
	 *            - Table to populate.
	 */
	public FillTable(List<E> list, Table table) {
		this.list = list;
		this.table = table;
		readColumns();
	}

	/***
	 * Instantiates a fill table class.
	 * 
	 * @param list
	 *            - The list of objects to fill the table with.
	 * @param table
	 *            - Table to populate.
	 * @param itemDataName
	 *            - Name of the data field on the table. By the default is Id.
	 */
	public FillTable(List<E> list, Table table, String itemDataName) {
		this.list = list;
		this.table = table;
		this.itemDataName = itemDataName;
		readColumns();
	}

	public boolean addListener(FillCompleteListener listener) {
		if (!fillCompleteListeners.contains(listener))
			return fillCompleteListeners.add(listener);

		return false;
	}

	private Object exec(Class<? extends Object> clazz, Object o, String method,
			Object... args) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException {
		// log.debug("Calling method {} of class {}.",
		// method, clazz.getSimpleName());
		return clazz.getDeclaredMethod(method.trim()).invoke(o, args);
	}

	private void fireOnComplete() {
		for (FillCompleteListener listener : fillCompleteListeners) {
			listener.complete();
		}
	}

	/**
	 * @return the columns
	 */
	public List<String> getColumns() {
		return columns;
	}

	/**
	 * @return the itemDataName
	 */
	public String getItemDataName() {
		return itemDataName;
	}

	/**
	 * @return the list
	 */
	public List<E> getList() {
		return list;
	}

	/**
	 * @return the separator
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * @return the table
	 */
	public Table getTable() {
		return table;
	}

	/**
	 * Retrieves the value from the current call.
	 * 
	 * @param e
	 *            - Generic type to invoke the methods on.
	 * @param calls
	 *            - The current calls to the generic object.
	 * @return Returns null if the call was empty or an Object with the data
	 *         required.
	 */
	protected Object getValueFromCall(E e, String[] split) {
		Object ret = null;

		if (split != null && split.length > 0) {
			try {
				Class<? extends Object> clazz = e.getClass();
				// log.debug("Starting class: {}.", clazz.getSimpleName());
				Object instance = null;

				if (split != null && split.length > 0) {
					instance = exec(clazz, e, split[0]);

					if (split.length > 1) {
						for (int i = 1; i < split.length; i++) {
							clazz = instance.getClass();
							ret = exec(clazz, instance, split[i]);
							instance = ret;
						}
					} else {
						// log.debug("split.length was LTE 1");
						ret = instance;
					}
				} else {
					ret = exec(clazz, e, split[0].trim());
				}
			} catch (IllegalAccessException ex) {
				log.error("Illegal Access: ", ex);
			} catch (IllegalArgumentException ex) {
				log.error("Illegal argument: ", ex);
			} catch (InvocationTargetException ex) {
				log.error("Invocation failed: ", ex);
			} catch (NoSuchMethodException ex) {
				log.error("Method not found: ", ex);
			} catch (SecurityException ex) {
				log.error("Security Exception: ", ex);
			}
		} else {
			log.debug("The call string was null or empty!");
		}

		return ret;
	}

	/**
	 * Checks if there are more sequences to parse or not.
	 * 
	 * @return True if the currentSequenceIndex is less than Sequences.length -
	 *         1; false otherwise.
	 */
	protected boolean hasMoreSequences() {
		if (expSequences.length <= 0)
			return false;
		return currentSequenceIndex < (expSequences.length - 1);
	}

	/**
	 * Fetches the next sequence.
	 * 
	 * @return A string array with the next call.
	 */
	protected String[] nextSequence() {
		return expSequences[++currentSequenceIndex].split("\\.");
	}

	/**
	 * Sets the initial data for the sequences. It will always have at least one
	 * element.
	 * 
	 * @param calls
	 */
	protected void prepareSequences(String calls) {
		// log.debug("Current calls: {}", calls);
		currentSequenceIndex = -1;

		if (calls.indexOf('+') > 0) {
			expSequences = calls.split("\\+");
		} else {
			expSequences = new String[] { calls };
		}

		// log.debug("Call sequences length: {}", expSequences.length);
	}

	/**
	 * Reads the column data (getData) to create a callable function based on
	 * this data.
	 */
	protected void readColumns() {
		TableColumn[] cols = table.getColumns();

		for (int i = 0; i < cols.length; i++) {
			Object o = cols[i].getData();

			if (o != null) {
				String s = o.toString();

				if (s.length() > 0)
					columns.add(o.toString());
				else
					columns.add("toString");
			} else {
				columns.add("toString");
			}
		}
	}

	public boolean removeListener(FillCompleteListener listener) {
		try {
			return fillCompleteListeners.remove(listener);
		} catch (Exception ex) {
			log.error("Failed to remove fill complete listener.", ex);
		}

		return false;
	}

	/***
	 * Fills the table asynchronously.
	 */
	@Override
	public void run() {
		String[] values = null;
		TableItem ti = null;

		try {
			for (E e : list) {
				ti = new TableItem(table, SWT.BORDER_SOLID);
				values = new String[columns.size()];

				for (int i = 0; i < columns.size(); i++) {
					prepareSequences(columns.get(i));

					while (hasMoreSequences()) {
						Object o = getValueFromCall(e, nextSequence());

						if (o != null) {
							if (values[i] != null && values[i].length() > 0) {
								String tmp = "";

								if (o instanceof Date) {
									tmp = new SimpleDateFormat(
											"dd-MM-yyyy HH:mm:ss")
											.format((Date) o);
								} else {
									tmp = o.toString();
								}

								values[i] = String.format("%s%s%s", values[i],
										separator, tmp);
							} else {
								values[i] = o.toString();
							}
						} else {
							values[i] = "";
						}

						// log.debug("values[{}] = {}", i, values[i]);
					}

					values[i] = values[i].replaceAll(separator + "$", "");
				}

				ti.setData(itemDataName, e);
				ti.setText(values);
			}

			fireOnComplete();
		} catch (Exception ex) {
			log.error("Failed to fill table: ", ex);
		} finally {
			ti = null;
			values = null;
		}
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}

	/**
	 * @param itemDataName
	 *            the itemDataName to set
	 */
	public void setItemDataName(String itemDataName) {
		this.itemDataName = itemDataName;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(List<E> list) {
		this.list = list;
	}

	/**
	 * @param separator
	 *            the separator to set
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTable(Table table) {
		this.table = table;
	}
}
