package com.valorti.data.formatter;

import java.io.File;

public interface IFormatter {
	/**
	 * Formats the document contents.
	 */
	public void format();

	/**
	 * The file object that was used to store the contents to disk.
	 * 
	 * @return A file object if it was set or null if not.
	 */
	public File getFile();

	/**
	 * Retrieves the file extension name.
	 * 
	 * @return A string representing the file extension to be appended to the
	 *         file.
	 */
	public String getFileExtension();

	/**
	 * Checks whether the file was saved to disk or not.
	 * 
	 * @return true if it's saved or false if not.
	 */
	public boolean isSaved();

	/**
	 * Saves the file to disk using the outputFolder field.
	 * 
	 * @return True if the file was saved or false if something failed.
	 */
	public boolean save();

	/**
	 * Saves the contents to disk as the specified file.
	 * 
	 * @param file
	 *            The file where to save the contents.
	 * @return true if it was saved or false if not.
	 */
	public boolean saveAs(File file);

	/**
	 * Saves the contents to disk using the path and file name specified.
	 * 
	 * @param path
	 *            Path and file name where to save the contents.
	 * @return True if the file was saved or false if not.
	 */
	public boolean saveAs(String path);

	/**
	 * Sets the file where the contents will be stored.
	 * 
	 * @param file
	 *            A file to store the contents. Doesn't need to exist.
	 */
	public void setFile(File file);
}
