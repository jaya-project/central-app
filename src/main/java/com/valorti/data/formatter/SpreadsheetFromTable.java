package com.valorti.data.formatter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

/**
 * 
 * @author Nicolás
 *
 *         Formats and generates a Spreadsheet based on the contents of an
 *         org.eclipse.swt.widgets.Table.
 */
public class SpreadsheetFromTable extends WorkbookBuilder {
	protected Table	table	= null;

	/**
	 * 
	 * @param table
	 *            The table with the contents.
	 * @throws IllegalArgumentException
	 *             If the table parameter is null.
	 */
	public SpreadsheetFromTable(Table table) throws IllegalArgumentException {
		this(table, "", "");
	}

	/**
	 * 
	 * @param table
	 *            The table with the contents.
	 * @param sheetName
	 *            Optional name of the sheet.
	 * @param fileExt
	 *            The name of the file extension to use.
	 * @throws IllegalArgumentException
	 *             If the table is null.
	 */
	public SpreadsheetFromTable(Table table, String sheetName, String fileExt)
			throws NullPointerException {
		super(sheetName, fileExt);

		if (table == null)
			throw new IllegalArgumentException("The 'Table' is not optional.");

		this.table = table;
	}

	/**
	 * Adds the content to the sheet.
	 * 
	 * @param items
	 *            The items to be processed.
	 * @param oddCellStyle
	 *            A style that will be applied to every odd row cell.
	 * @param evenCellStyle
	 *            A style to apply to every even row cell.
	 */
	protected void addContent(TableItem[] items, CellStyle oddRowStyle,
			CellStyle evenRowStyle) {
		int colCount = table.getColumnCount();

		for (TableItem ti : items) {
			int rowNum = sheet.getLastRowNum() + 1;
			Cell cell = null;

			for (int i = 0; i < colCount; i++) {
				String s = ti.getText(i);
				log.trace("Adding {} to cell.", s);
				cell = addCell(rowNum, s);

				if (cell != null) {
					if ((rowNum % 2) == 0 && evenRowStyle != null) {
						cell.setCellStyle(evenRowStyle);
					} else {
						if (oddRowStyle != null)
							cell.setCellStyle(oddRowStyle);
					}

					cell = null;
				}

				s = null;
			}
		}
	}

	protected void addContentColumnNames(TableColumn[] columns,
			CellStyle style, float rowHeight) {
		if (columns != null && columns.length > 0) {
			int row = sheet.getLastRowNum() + 1;

			for (TableColumn c : columns) {
				Cell cell = addCell(row, c.getText());

				if (style != null)
					cell.setCellStyle(style);
			}

			sheet.getRow(row).setHeightInPoints(rowHeight);
		}
	}

	@Override
	public void format() {
		super.createWorkbook(sheetName);
		TableItem[] items = this.table.getItems();

		if (items != null && items.length > 0) {
			addContentColumnNames(table.getColumns(), null, 18);
			addContent(items, null, null);
		}
	}
}
