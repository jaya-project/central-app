package com.valorti.data.formatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.swt.widgets.Table;

import com.valorti.jaya.model.Inventory;

public class InventorySpreadsheet extends SpreadsheetFromTable {
	private Inventory		inventory;
	private DateFormat		dateFormat	= new SimpleDateFormat("dd-MM-yyyy");
	private XSSFWorkbook	wb			= null;
	private XSSFSheet		sh			= null;

	public InventorySpreadsheet(Table table, String sheetName,
			Inventory inventory) {
		super(table, sheetName, WorkbookBuilder.EXT_MS_XLSX);
		this.inventory = inventory;
		wb = (XSSFWorkbook) workbook;
		sh = (XSSFSheet) sheet;
	}

	@Override
	public void format() {
		this.createWorkbook(sheetName);
		wb = (XSSFWorkbook) workbook;
		sh = (XSSFSheet) sheet;
		int colCnt = table.getColumnCount();

		XSSFColor brown = new XSSFColor(new byte[] { (byte) 222, (byte) 193,
				120 });
		XSSFColor textBrown = new XSSFColor(new byte[] { (byte) 151, 123, 77 });
		XSSFColor borderBrown = new XSSFColor(new byte[] { (byte) 144, 94, 21 });
		XSSFColor bgYellow = new XSSFColor(new byte[] { (byte) 244, (byte) 234,
				(byte) 210 });

		XSSFFont titleFont = wb.createFont();
		titleFont.setFontHeightInPoints((short) 18);
		titleFont.setColor(IndexedColors.WHITE.getIndex());

		XSSFCellStyle titleStyle = wb.createCellStyle();
		titleStyle.setAlignment(CellStyle.ALIGN_LEFT);
		titleStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		titleStyle.setFillForegroundColor(brown);
		titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		titleStyle.setFont(titleFont);

		XSSFCell cell = (XSSFCell) addCell(
				0,
				String.format("INVENTARIO %s - %s",
						dateFormat.format(inventory.getStartDate()),
						dateFormat.format(inventory.getEndDate())));
		cell.getRow().setHeightInPoints(48);
		cell.setCellStyle(titleStyle);
		sh.addMergedRegion(new CellRangeAddress(0, 0, 0, colCnt - 1));

		XSSFFont headFont = wb.createFont();
		headFont.setFontHeightInPoints((short) 11);
		headFont.setColor(IndexedColors.WHITE.getIndex());

		XSSFCellStyle headStyle = wb.createCellStyle();
		headStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		headStyle.setFillForegroundColor(brown);
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headStyle.setBorderRight(CellStyle.BORDER_THIN);
		headStyle.setBorderTop(CellStyle.BORDER_THIN);
		headStyle.setTopBorderColor(borderBrown);
		headStyle.setBottomBorderColor(borderBrown);
		headStyle.setLeftBorderColor(borderBrown);
		headStyle.setRightBorderColor(borderBrown);
		headStyle.setWrapText(true);
		headStyle.setFont(headFont);

		XSSFFont evenFont = wb.createFont();
		evenFont.setFontHeightInPoints((short) 11);
		// evenFont.setColor(black);

		XSSFCellStyle evenCellStyle = wb.createCellStyle();
		evenCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		evenCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		evenCellStyle.setWrapText(true);
		evenCellStyle.setFont(evenFont);
		evenCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		evenCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		evenCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		evenCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		evenCellStyle.setTopBorderColor(borderBrown);
		evenCellStyle.setBottomBorderColor(borderBrown);
		evenCellStyle.setLeftBorderColor(borderBrown);
		evenCellStyle.setRightBorderColor(borderBrown);

		XSSFFont oddFont = wb.createFont();
		oddFont.setFontHeightInPoints((short) 11);
		oddFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		oddFont.setColor(textBrown);

		XSSFCellStyle oddCellStyle = wb.createCellStyle();
		oddCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		oddCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		oddCellStyle.setWrapText(true);
		oddCellStyle.setFillForegroundColor(bgYellow);
		oddCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		oddCellStyle.setFont(oddFont);
		oddCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		oddCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		oddCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		oddCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		oddCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		oddCellStyle.setTopBorderColor(borderBrown);
		oddCellStyle.setBottomBorderColor(borderBrown);
		oddCellStyle.setLeftBorderColor(borderBrown);
		oddCellStyle.setRightBorderColor(borderBrown);

		addContentColumnNames(table.getColumns(), headStyle, 38);
		addContent(table.getItems(), oddCellStyle, evenCellStyle);
		autoSize(sh, true);

		brown = null;
		textBrown = null;
		borderBrown = null;
		bgYellow = null;

		titleFont = null;
		titleStyle = null;
		cell = null;

		headFont = null;
		headStyle = null;

		evenFont = null;
		evenCellStyle = null;

		oddFont = null;
		oddCellStyle = null;
	}
}