package com.valorti.data.formatter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.InletGuide;
import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.ItemProperties;

/**
 * 
 * @author Techwert This class is used to format, add rows and store the Excel
 *         file that will be uploaded to Softland.
 * 
 *         The user should first call setOutputFolder to tell where to store the
 *         file, and can call setFilePrefix to name the file with that prefix.
 *         The sufix will be the current date and time with format:
 *         dd-MM-YYYY_HHmmss.xls
 */
public class InletGuideFormatter extends ImplAbstractFormatter {
	private Container					container	= null;

	private final Logger				log			= LoggerFactory
															.getLogger(this
																	.getClass());

	/**
	 * Variables for the XLSX file.
	 */
	private Sheet						sheet;
	private Workbook					workbook;
	private int							sheetRowCount;
	private int							sheetColumnCount;
	// private String fileNameFormat = "{name}_{date:dd-MM-yyyy}";

	private static InletGuideFormatter	self		= null;

	private InletGuideFormatter() {
		this.sheet = null;
		this.workbook = null;
		this.sheetRowCount = -1;
		this.sheetColumnCount = 0;
		this.fileExtension = "xls";
	}

	/**
	 * Retrieves an InletGuideFormatter instance.
	 * 
	 * @return An instance of this class.
	 */
	public static InletGuideFormatter getInstance() {
		if (self == null) {
			self = new InletGuideFormatter();
		}

		return self;
	}

	private void addBlankCell(Row row) {
		addBlankCell(row, 1);
	}

	private void addBlankCell(Row row, int blankCellsToAdd) {
		for (int i = 0; i < blankCellsToAdd; i++) {
			Cell cell = nextCell(row);
			cell.setCellType(Cell.CELL_TYPE_BLANK);
		}
	}

	public boolean addRow(InletGuide inletGuide) {
		this.startNewSheet();

		try {
			String tmpStr;
			Long tmpLng;
			Double tmpDbl, totalLength = this.getTotalLenght(inletGuide
					.getContainers());

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Row row = nextRow();
			Cell cell = nextCell(row);

			// 1.- Código de Bodega
			cell.setCellValue(container.getLocation().getWarehouse()
					.getWarehouseCode().getWhCode());
			cell.setCellType(Cell.CELL_TYPE_STRING);
			autoSize();

			// 2.- Número de Folio (softland)
			cell = nextCell(row);
			cell.setCellValue(inletGuide.getFolioNumber());
			cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			autoSize();

			// 3.- Fecha Generación Guía Entrada
			cell = nextCell(row);
			cell.setCellValue(dateFormat.format(inletGuide.getGenerationDate()));
			cell.setCellType(Cell.CELL_TYPE_STRING);
			autoSize();

			// 4.- Concepto de Entrada a Bodega
			cell = nextCell(row);
			cell.setCellValue(inletGuide.getWhEntryConcept());
			cell.setCellType(Cell.CELL_TYPE_STRING);
			autoSize();

			// 5.- Descripción (no. pedido + guía?)
			cell = nextCell(row);
			cell.setCellType(Cell.CELL_TYPE_BLANK);

			// 6.- Código de Proveedor
			cell = nextCell(row);
			cell.setCellValue(inletGuide.getProvider().getProviderCode());
			cell.setCellType(Cell.CELL_TYPE_STRING);
			autoSize();

			tmpStr = inletGuide.getCostCenterCode();

			if (tmpStr != null && tmpStr.length() > 0) {
				// 7.- Código Centro Costos
				cell = nextCell(row);
				cell.setCellValue(tmpStr);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpStr = inletGuide.getSourceWarehouseCode();

			// 8.- Código Bodega Origen
			if (tmpStr.length() > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpStr);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = inletGuide.geteWaybillNumber();

			if (tmpLng != null && tmpLng > 0) {
				// 9.- Número guía despacho asociada (externo)
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();

				// 10.- Fecha guía despacho asociada (externa)
				cell = nextCell(row);
				cell.setCellValue(dateFormat.format(inletGuide
						.geteWaybillDate()));
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				// Si no hay número de guía de despacho, se añaden dos celdas en
				// blanco.
				addBlankCell(row, 2);
			}

			tmpLng = inletGuide.geteBillNumber();

			if (tmpLng != null && tmpLng > 0) {
				// 11.- Número de Factura / Nota de crédito asociada (externa)
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();

				// 12.- Subtipo de factura (externa)
				cell = nextCell(row);
				cell.setCellValue(inletGuide.geteBillSubtype().toString());
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();

				// 13.- Fecha factura o nota de crédito (externa)
				cell = nextCell(row);
				cell.setCellValue(dateFormat.format(inletGuide.geteBillDate()));
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row, 3);
			}

			tmpLng = inletGuide.getiWorkOrderNumber();

			// 14.- Número Orden de Trabajo (interno)
			if (tmpLng != null && tmpLng > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = inletGuide.getiProductionOrderNumber();

			// 15.- Número orden de Producción (interno)
			if (tmpLng != null && tmpLng > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = inletGuide.getiPurchaseOrderNumber();

			// 16.- Número de Orden de compra (interno)
			if (tmpLng != null && tmpLng > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = inletGuide.getiBillNumber();

			// 17.- Número factura asociada (interna)
			if (tmpLng != null && tmpLng > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = inletGuide.getiCreditNoteNumber();

			// 18.- Número nota crédito asociada (interna)
			if (tmpLng != null && tmpLng > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpStr = inletGuide.getPostingCostCenterCode();

			if (tmpStr != null && tmpStr.length() > 0) {
				// 19.- Código centro costo para contabilizar
				cell = nextCell(row);
				cell.setCellValue(tmpStr);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = inletGuide.getPurchaseTotal();

			if (tmpLng != null) {
				// 20.- Total Final
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			// 21.- Código de Producto
			cell = nextCell(row);
			cell.setCellValue(container.getItem().getItemCode());
			cell.setCellType(Cell.CELL_TYPE_STRING);
			autoSize();

			// 22.- Descripción
			tmpStr = getItemDescription(container.getItem());

			if (tmpStr != null && tmpStr.length() > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpStr);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			// 23.- Cantidad ingresada (metros totales del lote)
			cell = nextCell(row);
			cell.setCellValue(totalLength);
			cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			autoSize();

			tmpDbl = container.getUnitPrice();

			if (tmpDbl != null && tmpDbl > 0) {
				// 24.- Precio Unitario
				cell = nextCell(row);
				cell.setCellValue(tmpDbl);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			Date tmpDte = inletGuide.getPurchaseDate();

			if (tmpDte != null) {
				// 25.- Fecha de Compra
				cell = nextCell(row);
				cell.setCellValue(dateFormat.format(tmpDte));
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			// 26.- Lote, partida o talla.
			cell = nextCell(row);
			cell.setCellValue(container.getLotCode());
			cell.setCellType(Cell.CELL_TYPE_STRING);
			autoSize();

			// 27.- Pieza o Color
			addBlankCell(row);

			// 28.- Fecha de Vencimiento
			if (container.getExpireDate() != null) {
				cell = nextCell(row);
				cell.setCellValue(dateFormat.format(container.getExpireDate()));
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpStr = inletGuide.getSerialNumber();

			// 29.- Serie
			if (tmpStr.length() > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpStr);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpStr = inletGuide.getIntakeAccount();

			// 30.- Cuenta de Consumo
			if (tmpStr != null && tmpStr.length() > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpStr);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = inletGuide.getBillType();

			// 31.- Tipo de factura (esto debería estar junto a los datos de
			// factura
			// ¬¬)
			if (tmpLng != null && tmpLng > 0) {
				cell = nextCell(row);
				cell.setCellValue(tmpLng);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				autoSize();
			} else {
				addBlankCell(row);
			}

			tmpLng = null;
			tmpStr = null;
			tmpDbl = null;
			tmpDte = null;

			return true;
		} catch (Exception ex) {
			log.error("Failed to add row. Error:", ex);
		}

		return false;
	}

	private void autoSize() {
		this.sheet.autoSizeColumn(sheetColumnCount);
	}

	private void createNewSheet() {
		sheetRowCount = 0;
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet("Guía de Entrada");
		isSaved = false;
	}

	@Override
	public void format() {
	}

	/**
	 * How many columns have been added.
	 * 
	 * @return Column count.
	 */
	public int getCurrentColumnCount() {
		return this.sheetColumnCount;
	}

	/**
	 * Returns the current row count.
	 * 
	 * @return How many rows actually are in the sheet.
	 */
	public int getCurrentRowCount() {
		return this.sheetRowCount;
	}

	private String getItemDescription(Item item) {
		try {
			for (Iterator<ItemProperties> it = item.getItemProperties()
					.iterator(); it.hasNext();) {
				ItemProperties ip = it.next();

				if (ip.getProperty().getPropertyName().equals("Descripción")) {
					return ip.getPropertyValue();
				}
			}
		} catch (Exception ex) {
			log.debug("Failed to retrieve item properties.", ex);
		}

		return "";
	}

	/**
	 * Retrieves the length of every container.
	 * 
	 * @param containers
	 *            The container set.
	 * @return The total length for the lot.
	 */
	private double getTotalLenght(Set<Container> containers) {
		Container c = null;
		double totalLength = 0.0;

		for (Iterator<Container> it = containers.iterator(); it.hasNext();) {
			c = it.next();

			if (c != null) {
				if (container == null)
					container = c;

				try {
					totalLength += c.getItemTotalLength();
				} catch (Exception ex) {
					log.error(
							"Failed to add container ({}) length to sum. Error: {}",
							c, ex);
				}
			} else {
				log.warn("A container is null!");
			}
		}

		return totalLength;
	}

	private Cell nextCell(Row row) {
		return row.createCell(sheetColumnCount++);
	}

	// GETTERS

	private Row nextRow() {
		return this.sheet.createRow(sheetRowCount++);
	}

	/**
	 * Saves the file to disk.
	 * 
	 * @return True if the file was saved to disk or false if something went
	 *         wrong.
	 */
	@Override
	public boolean save() {
		log.info("Saving inlet guide to {}", file.getAbsolutePath());
		FileOutputStream outputStream = null;

		try {
			if (!file.getParentFile().exists())
				file.getParentFile().mkdirs();

			if (!file.exists())
				file.createNewFile();

			outputStream = new FileOutputStream(file);
			workbook.write(outputStream);
			isSaved = true;
			log.info("Inlet guide file saved as {}", file.getAbsolutePath());
			return true;
		} catch (FileNotFoundException ex) {
			log.error("The file does not exist, so it could not be written.",
					ex);
		} catch (IOException ex) {
			log.error("Could not write contents of the workbook: {}", ex);
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException ex) {
					log.error("Failed to close FileOutputStream: {}", ex);
				}
			}
			outputStream = null;
		}

		return false;
	}

	/**
	 * Starts a new sheet by reseting the column and row count.
	 */
	public void startNewSheet() {
		sheetColumnCount = 0;
		container = null;

		if (sheetRowCount < 0 || sheetRowCount >= 29) {
			createNewSheet();
		}
	}

	/*
	 * public void setFileNameFormat(String format) { this.fileNameFormat =
	 * format; }
	 * 
	 * private String formatFileName() { StringBuilder sb = new StringBuilder();
	 * sb.append(this.fileNameFormat.replaceAll("\\{name\\}", this.fileName));
	 * fileName = ""; return ""; }
	 */
}
