package com.valorti.data.formatter;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ImplAbstractFormatter implements IFormatter {
	protected final Logger	log				= LoggerFactory.getLogger(this
													.getClass());
	protected File			file			= null;
	protected boolean		isSaved			= false;
	protected String		fileExtension	= "txt";

	public ImplAbstractFormatter() {
	}

	@Override
	public File getFile() {
		return file;
	}

	@Override
	public String getFileExtension() {
		return fileExtension;
	}

	@Override
	public boolean isSaved() {
		return isSaved;
	}

	@Override
	public boolean saveAs(File file) {
		this.file = file;
		return save();
	}

	@Override
	public boolean saveAs(String path) {
		return saveAs(new File(path));
	}

	@Override
	public void setFile(File file) {
		if (file != null)
			this.file = file;
	}

	/**
	 * Sets the file extension.
	 * 
	 * @param fileExtension
	 *            The file extension.
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
}
