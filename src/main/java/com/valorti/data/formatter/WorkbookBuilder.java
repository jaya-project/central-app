package com.valorti.data.formatter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.valorti.utils.PathUtils;

/**
 * Generates an workbook to be saved later.
 * 
 * @author Nicolás
 *
 */
public abstract class WorkbookBuilder extends ImplAbstractFormatter {
	public static final String		EXT_MS_XLS	= "xls";
	public static final String		EXT_MS_XLSX	= "xlsx";

	protected Sheet					sheet;
	protected Workbook				workbook;
	protected String				sheetName;
	protected Map<String, String>	fileExtensions;
	protected HSSFPalette			palette;

	public WorkbookBuilder() {
		this(null, WorkbookBuilder.EXT_MS_XLSX);
	}

	/**
	 * @param sheetName
	 *            The name of the first sheet.
	 * @param fileExt
	 *            One of the EXT_ constants.
	 */
	public WorkbookBuilder(String sheetName, String fileExt) {
		this.sheet = null;
		this.workbook = null;
		this.sheetName = "Sheet 1";
		this.fileExtensions = new HashMap<String, String>();
		this.fileExtensions.put("Excel 2003", WorkbookBuilder.EXT_MS_XLS);
		this.fileExtensions.put("Excel 2007", WorkbookBuilder.EXT_MS_XLSX);

		if (sheetName != null && sheetName.length() > 0)
			this.sheetName = WorkbookUtil.createSafeSheetName(sheetName);

		if (fileExt != null && fileExt.length() > 0)
			fileExtension = fileExt;
		else
			fileExtension = WorkbookBuilder.EXT_MS_XLSX;
	}

	/**
	 * Adds a cell in the specified location with the provided content.
	 * 
	 * @param rowNumber
	 *            A cell row.
	 * @param content
	 *            The cell content.
	 * @return The generated cell.
	 */
	public Cell addCell(int rowNumber, Object content) {
		log.debug("Adding cell at row {}", rowNumber);
		Cell cell = null;
		Row row = this.sheet.getRow(rowNumber);

		if (row == null) {
			row = this.sheet.createRow(rowNumber);
			cell = row.createCell(0);
		} else {
			cell = row.createCell(row.getLastCellNum());
		}

		if (cell != null) {
			cell.setCellValue(content.toString());
		}

		return cell;
	}

	/**
	 * Creates a new sheet where to add the contents.
	 */
	public void addSheet() {
		this.addSheet(null);
	}

	/**
	 * Creates a new sheet where to add the contents.
	 * 
	 * @param sheetName
	 *            The name of the sheet.
	 */
	public void addSheet(String sheetName) {
		if (sheetName != null && sheetName.length() > 0)
			this.sheetName = WorkbookUtil.createSafeSheetName(sheetName);

		this.sheet = this.workbook.createSheet(sheetName);
	}

	/**
	 * Resizes all columns to fit its content.
	 * 
	 * @param sheet
	 *            The sheet to auto-size.
	 */
	public void autoSize(Sheet sheet, boolean merged) {
		Row row = sheet.getRow(0);

		for (Iterator<Cell> it = row.cellIterator(); it.hasNext();) {
			Cell c = it.next();

			if (c != null) {
				sheet.autoSizeColumn(c.getColumnIndex(), merged);
			}
		}
	}

	/**
	 * Creates a new spreadsheet workbook and sheet with the specified name.
	 * 
	 * @param sheetName
	 *            The optional sheet name.
	 */
	protected void createWorkbook(String sheetName) {
		switch (this.fileExtension) {

			case WorkbookBuilder.EXT_MS_XLS:
				this.workbook = new HSSFWorkbook();
				palette = ((HSSFWorkbook) workbook).getCustomPalette();
				break;

			case WorkbookBuilder.EXT_MS_XLSX:
			default:
				this.workbook = new XSSFWorkbook();
				break;
		}

		this.addSheet(sheetName);
		this.file = null;
	}

	/**
	 * Fetches the cell at the specified row and column number.
	 * 
	 * @param rowNumber
	 *            The row of the cell you're looking.
	 * @param cellNumber
	 *            The column number of the cell you want.
	 * @return The needed Cell or null if the row or cell does not exists.
	 */
	public Cell getCellAt(int rowNumber, int cellNumber) {
		log.debug("Retrieving cell [{},{}].", rowNumber, cellNumber);
		Cell c = null;
		Row r = sheet.getRow(rowNumber);

		if (r != null) {
			c = r.getCell(cellNumber);
		}

		return c;
	}

	/**
	 * Retrieves a custom color from the underlying workbook.
	 * 
	 * @param color
	 *            {@link java.awt.Color} of to retrieve.
	 * @param replaceIndex
	 *            The color index to be replaced with this color.
	 * @return The index of the color. If it doesn't find the color, a 0 is
	 *         returned.
	 */
	public short getColor(byte red, byte green, byte blue, short replaceIndex) {
		log.trace("Retrieving color [red={}, green={}, blue={}].", red, green,
				blue);
		short r = 0;

		switch (this.fileExtension) {
			case WorkbookBuilder.EXT_MS_XLS:
				if (palette == null)
					palette = ((HSSFWorkbook) workbook).getCustomPalette();

				HSSFColor hssfcolor = null;

				try {
					hssfcolor = palette.findColor(red, green, blue);
				} catch (Exception ex) {
					log.error("Failed to find color.", ex);
				}

				if (hssfcolor == null) {
					try {
						palette.setColorAtIndex(replaceIndex, red, green, blue);
						r = replaceIndex;
					} catch (RuntimeException ex) {
						log.error("Failed to add color [r={},g={},b={}].", red,
								green, blue, ex);
						hssfcolor = new HSSFColor.BLACK();
						r = hssfcolor.getIndex();
					}
				} else {
					r = hssfcolor.getIndex();
				}

				log.debug("HSSFColor: {}", hssfcolor);
				hssfcolor = null;
				break;

			case WorkbookBuilder.EXT_MS_XLSX:
			default:
				XSSFColor xssfcolor = null;

				try {
					xssfcolor = new XSSFColor(new byte[] { red, green, blue });
					xssfcolor.setIndexed(replaceIndex);
					r = replaceIndex;
				} catch (Exception ex) {
					log.error("Failed to create XSSF color.", ex);
					r = IndexedColors.BLACK.getIndex();
				}

				xssfcolor = null;
				break;
		}

		log.trace("Retrieved color index: {}", r);
		return r;
	}

	/**
	 * Returns the current, active, sheet.
	 * 
	 * @return The sheet.
	 */
	public Sheet getSheet() {
		return this.workbook.getSheetAt(this.workbook.getActiveSheetIndex());
	}

	/**
	 * Retrieves a Map with the supported file extensions of this class.
	 * 
	 * @return A HashMap with the supported file extensions.
	 */
	public Map<String, String> getSupportedFileExtensions() {
		return this.fileExtensions;
	}

	/**
	 * Retrieves the current workbook.
	 * 
	 * @return The Workbook.
	 */
	public Workbook getWorkbook() {
		return this.workbook;
	}

	/**
	 * Modifies the cell contents only if the cell exists.
	 * 
	 * @param rowNumber
	 *            A row number for the cell.
	 * @param colNumber
	 *            The column number of the cell.
	 * @param newContent
	 *            The content to add.
	 * @return The modified cell (or created, if createIfNull is set to true) or
	 *         null if the cell does not exist (or failed to create it).
	 * 
	 * @see FormatTableAsXLS#modCell(int, int, Object, boolean)
	 */
	public Cell modCell(int rowNumber, int colNumber, Object newContent) {
		return modCell(rowNumber, colNumber, newContent, false);
	}

	/**
	 * Modifies the cell contents if the cell exists. If it does not exists, it
	 * will not be created unless createIfNull is set to true.
	 * 
	 * @param rowNumber
	 *            A row number for the cell.
	 * @param colNumber
	 *            The column number of the cell.
	 * @param newContent
	 *            The content to add.
	 * @param createIfNull
	 *            If set to true and the cell does not exist, it will be
	 *            created.
	 * @return The modified cell (or created, if createIfNull is set to true) or
	 *         null if the cell does not exist (or failed to create it).
	 */
	public Cell modCell(int rowNumber, int colNumber, Object newContent,
			boolean createIfNull) {
		Cell cell = null;
		Row row = this.sheet.getRow(rowNumber);
		log.debug("Trying to modify cell at [{},{}] - Create if null? {}",
				rowNumber, colNumber, createIfNull);

		if (row != null) {
			cell = row.getCell(colNumber);

			if (cell == null && createIfNull)
				cell = row.createCell(colNumber);

			if (cell != null) {
				cell.setCellValue(newContent.toString());
			}
		}

		return cell;
	}

	@Override
	public boolean save() {
		log.info("Saving workbook...");
		OutputStream ostream = null;

		try {
			String fname = "";

			if (this.file == null) {
				fname = PathUtils.generateFileName(null, "wb_", "",
						this.fileExtension);
				this.file = new File(fname);
			}

			log.trace("Save().file {}", this.file);

			if (!this.file.getParentFile().exists()) {
				log.info("Creating folder structure...");

				if (!this.file.getParentFile().mkdirs()) {
					log.info("Failed!");
				} else {
					log.info("Success.");
				}
			}

			if (this.file.getParentFile().exists()) {
				if (!this.file.exists()) {
					log.info("File does not exists, trying to create it...");

					if (!file.createNewFile()) {
						log.info("Failed to create file.");
					} else {
						log.info("Success.");
					}
				}

				ostream = new FileOutputStream(file);
				this.workbook.write(ostream);
				this.isSaved = true;
				log.info("Workbook saved as [{}].", file.getAbsolutePath());
				return true;
			}
		} catch (Exception ex) {
			log.error("Failed to store workbook.", ex);
		} finally {
			if (ostream != null) {
				try {
					ostream.close();
				} catch (IOException ex) {
					log.error("Failed to close output stream.", ex);
				}

				ostream = null;
			}
		}

		return false;
	}
}