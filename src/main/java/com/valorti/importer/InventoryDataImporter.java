package com.valorti.importer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Inventory;
import com.valorti.jaya.model.InventoryContainer;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.InventoryManager;
import com.valorti.utils.XmlUtils;

public class InventoryDataImporter extends AbstractXmlImporter<Inventory> {
	private final InventoryManager	imgr					= new InventoryManager();
	private Inventory				inventory;
	private List<Element>			unfinishedInventories	= new ArrayList<Element>();

	private DateFormat				dateFormat				= null;

	@SuppressWarnings("unchecked")
	public InventoryDataImporter(File xmlFile) {
		super(xmlFile);
		this.rootNode = xmlDoc.selectNodes("/Content/InventoryItems");
		this.rootIterator = this.rootNode.iterator();
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	}

	private Date getDate(String source, DateFormat df) {
		if (source != null && source.length() > 0) {
			try {
				return df.parse(source);
			} catch (ParseException ex) {
				log.error("Failed to parse date string: ", ex);
			}
		}

		return null;
	}

	@Override
	public Inventory next() {
		if (hasNext()) {
			super.next();
			traverseItemNodes();
		}

		return inventory;
	}

	private void traverseItemNodes() {
		log.debug("Parsing inventory...");
		inventory = null;

		Element node = (Element) rootNode.get(currentElement);
		log.debug("Parsing node: {}", node.asXML());

		if (node.hasContent()) {
			// Iterates through "Inventory" nodes.
			for (Iterator<?> inventoryNodeIt = node.elementIterator(); inventoryNodeIt
					.hasNext();) {
				Element element = (Element) inventoryNodeIt.next();
				Serializable s = null;
				Object tmp = element.attributeValue("StartDate");
				Date startDate = tmp != null ? getDate(tmp.toString(),
						dateFormat) : Calendar.getInstance().getTime();
				tmp = element.attributeValue("EndDate");
				Date endDate = tmp != null ? getDate(tmp.toString(), dateFormat)
						: null;
				inventory = imgr.findByDate(startDate);

				// There is an inventory with this start date.
				if (inventory != null) {
					// Check if the inventory hasn't been closed.
					if (inventory.getEndDate() == null) {
						log.info("There is an inventory in progress; using it instead of creating a new one.");
						s = inventory.getId();
					} else {
						// The inventory is closed, so we can't add more
						// data to it.
						log.error("Adding container data to a finished inventory is forbidden.");
						s = null;
						inventory = null;
					}
				} else { // There is no inventory with this start date.
					// If endDate is null, that means the inventory is
					// not closed yet.
					// We will not do anything except store this element
					// back, so it can
					// be saved to the XML file.
					if (endDate == null) {
						log.info("The inventory hasn't finished yet; adding to unfinished list.");

						if (unfinishedInventories == null)
							unfinishedInventories = new ArrayList<Element>();

						unfinishedInventories.add(element);
						inventory = null;
					} else { // End date is not null, thus the inventory
								// is finished.
						log.info("Creating a new, finished, inventory.");
						inventory = new Inventory();
						inventory.setDataUpdated((byte) 0);
						inventory.setEndDate(endDate);
						inventory.setStartDate(startDate);
						s = imgr.add(inventory);
					}
				}

				// if (inventory == null) {
				//
				//
				// } else {
				// // The inventory hasn't finished yet, so we use it to
				// // store data.
				// log.info("Using existing, unfinished, inventory.");
				// s = inventory.getId();
				// }

				log.trace("Inventory serializable value: {}", s);

				if (s != null) {
					// Add the containers to the inventory.
					traverseItemNodes(element);
				} else {
					log.warn("No inventory to add data. This might be an error.");
				}

				inventory = null;
				s = null;
			}
		}
	}

	private void parseItem(Element e) {
		Long containerId = XmlUtils.getLong(e, "ContainerId");
		ContainerManager cmgr = new ContainerManager();
		Container container = (Container) cmgr.getById(Container.class,
				containerId);

		if (container != null) {
			Serializable s = null;
			s = imgr.linkToContainer(container, inventory, XmlUtils.getString(
					e, "Comments"), InventoryContainer.Difference
					.valueOf(XmlUtils.getString(e, "DifferenceName")), XmlUtils
					.getInt(e, "DifferenceCount"));

			if (s != null) {
				log.info("Inventory {} successfully linked to container {}.",
						inventory.getId(), container.getId());
			}

			s = null;
		}

		containerId = null;
		container = null;
	}

	/**
	 * Saves the unfinished inventory back to the XML file, if there is one.
	 * 
	 * @param path
	 *            The path and file name of the XML file.
	 * @return If there is an inventory to store back and the file is saved to
	 *         disk, it will return 1, if there is an error, we will return -1
	 *         and if there was nothing to do, then we will return 0.
	 */
	public short storeDocument(String path) {
		short result = 0;

		if (unfinishedInventories != null && unfinishedInventories.size() > 0) {
			Document doc = DocumentHelper.createDocument();
			Element root = doc.addElement("Content");
			root.addElement("Lots");
			root.addElement("PalletMoves");
			root.addElement("PalletModifications");
			root.addElement("InventoryItems");
			XMLWriter writer = null;

			try {
				File tmp = new File(path);

				if (tmp.exists()) {
					try {
						tmp.delete();
					} catch (SecurityException ex) {
						log.error(
								"Could not remove the file {}. Do you have permission to delete it? Make sure the application has control over the directory.",
								path, ex);
					} catch (Exception ex) {
						log.error("Failed to delete existing file {}.", path,
								ex);
					}
				}

				// Append unfinished elements.
				for (Element element : unfinishedInventories) {
					root.element("InventoryItems").add(element.detach());
				}

				OutputFormat format = OutputFormat.createPrettyPrint();
				writer = new XMLWriter(new FileWriter(path), format);
				writer.write(doc);

				if (tmp.exists()) {
					result = 1;
				}
			} catch (IOException ex) {
				log.error("Failed to write xml data back.", ex);
				result = -1;
			} finally {
				if (writer != null) {
					try {
						writer.close();
					} catch (IOException ex) {
						log.error("Failed to close XML writer object.", ex);
					}
				}

				writer = null;
			}
		}

		return result;
	}

	private void traverseItemNodes(Element e) {
		log.info("Traversing 'Item' nodes...");
		for (Iterator<?> it = e.elementIterator(); it.hasNext();) {
			Element itemElement = (Element) it.next();
			parseItem(itemElement);
		}
	}

	/**
	 * Sets the date format that will be read from the XML file.
	 * 
	 * @param df
	 */
	public void setDateFormat(DateFormat df) {
		this.dateFormat = df;
	}
}
