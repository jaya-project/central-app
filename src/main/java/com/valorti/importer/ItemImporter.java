package com.valorti.importer;

import java.util.List;
import java.util.Map;

import com.valorti.jaya.ui.Main;
import com.valorti.utils.HibernateUtils;

public class ItemImporter extends AbstractSqlImporter {
	private String	condition	= "";

	public ItemImporter(String condition) throws Exception {
		this.condition = condition;
	}

	private String getFProviderQuery(String code) {
		StringBuilder query = new StringBuilder(
				"SELECT foreignProviderId FROM ForeignProvider t WHERE t.code = '")
				.append(code).append('\'');

		return query.toString();
	}

	private Long getPropertyId(String propertyName) {
		Long r = -1L;

		try {
			List<Map<String, Object>> list = sqlite
					.query("SELECT propertyId FROM Property WHERE propertyName = ? LIMIT 1 OFFSET 0",
							propertyName);

			if (list != null && list.size() == 1) {
				log.debug("Property id list size: {}", list.size());
				Map<String, Object> map = list.get(list.size() - 1);
				r = Long.parseLong(map.get("propertyId").toString());
			} else {
				log.error("Result was either null or empty when retrieving property id.");
			}
		} catch (Exception ex) {
			log.error("Failed to retrieve property {} id: ", propertyName, ex);
		}

		return r;
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return false;
	}

	public void importData() {
		log.info("*** Item data import start ***");
		HibernateUtils.close();
		int offset = 0;
		final int count = Main.configuration().getInteger(
				"/Application/SpecificConfiguration/Sql/RowLimit");
		boolean stop = false;
		long propertyId = getPropertyId("descripción");

		if (propertyId > 0) {
			while (!stop) {
				log.info("Retrieving {} rows, starting at row number {}.",
						count, (offset + 1));
				List<Map<String, Object>> result = queryMsSqlServer(offset,
						count);

				if (result != null && result.size() > 0) {
					int rowCount = 0;

					for (Map<String, Object> map : result) {
						if (map != null) {
							try {
								String iname = getString(map.get("itemName")), icode = getString(map
										.get("itemCode")), code = getString(map
										.get("code")), desc = getString(map
										.get("description"));

								if (iname.length() > 0 && icode.length() > 0) {
									long itemId = sqlite.exists("itemId",
											"Item",
											"itemName = ? AND itemCode = ?",
											iname, icode);

									if (itemId == 0) {
										log.debug("Item does not exist... trying to create it.");
										itemId = insertItem(iname, icode, code);
									} else if (itemId > 0) {
										log.debug("The item already exists... SKIPPED");
									}

									if (itemId > 0) {
										long itemPropId = sqlite
												.exists(null,
														"ItemProperties",
														"itemId = ? AND propertyId = ?",
														itemId, propertyId);

										if (itemPropId == 0)
											linkItemProperty(itemId,
													propertyId, desc);
									}
								} else {
									log.debug(
											"Could not retrieve column data: [itemName={}, itemCode={}, code={}, description={}]",
											iname, icode, code, desc);
								}
							} catch (Exception ex) {
								log.error("Failed to process row {}: {}",
										rowCount, ex);
							}

							rowCount++;
						} else {
							log.warn("The map at row [{}] was null.", rowCount);
						}
					}

					log.debug("Finished with [{}] rows.", rowCount);
				} else {
					stop = true;
				}

				result = null;
				offset += count;
			}
		}

		log.info("*** Item data import finished ***");
	}

	private long insertItem(String itemName, String itemCode, String code) {
		StringBuilder query = new StringBuilder("?, ?, (").append(
				getFProviderQuery(code)).append(')');
		long id = sqlite.insert("Item",
				"itemName, itemCode, foreignProviderId", query.toString(),
				itemName, itemCode);
		query = null;

		if (id > 0) {
			log.debug("Item [name = {}, code = {}] created!", itemName,
					itemCode);
		}

		return id;
	}

	private void linkItemProperty(Long itemId, Long propertyId, String value) {
		log.debug("Linking item [{}] to property [{}]", itemId, propertyId);

		if (value != null) {
			long r = -1L;
			r = sqlite.insert("ItemProperties",
					"itemId, propertyId, propertyValue", "?, ?, ?", itemId,
					propertyId, value);

			if (r > 0) {
				log.error(
						"Failed to link item (id = {}) to property (id = {})",
						itemId, propertyId);
			}
		} else {
			log.warn("Property value was null.");
		}
	}

	@Override
	public Object next() {
		// TODO Auto-generated method stub
		return null;
	}

	private List<Map<String, Object>> queryMsSqlServer(int offset, int count) {
		StringBuilder query = new StringBuilder(
				"SELECT CodProd AS itemCode, DesProd AS 'itemName', DesProd2 AS 'description', tgrupo.CodGrupo AS code ")
				.append("FROM softland.iw_tprod tprod ")
				.append("LEFT OUTER JOIN softland.iw_tgrupo tgrupo ON tgrupo.CodGrupo = tprod.CodGrupo ")
				.append(condition);

		return mssql.query(mssql.limit(query.toString().trim(), "CodProd",
				offset, count));
	}
}
