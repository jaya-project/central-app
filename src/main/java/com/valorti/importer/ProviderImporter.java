package com.valorti.importer;

import java.util.List;
import java.util.Map;

import com.valorti.jaya.model.Provider;
import com.valorti.jaya.model.manager.ProviderManager;

public class ProviderImporter extends AbstractSqlImporter implements Runnable {
	public ProviderImporter() throws Exception {
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return false;
	}

	public void importData() {
		log.info("Importing provider data...");
		try {
			List<Map<String, Object>> result = mssql
					.query("SELECT sprod.CodGrupo AS 'providerCode', sprod.DesGrupo AS 'providerName' FROM softland.iw_tgrupo sprod");

			if (result != null && result.size() > 0) {
				Provider p = null;
				ProviderManager pm = new ProviderManager();
				boolean success = true;

				for (Map<String, Object> map : result) {
					p = new Provider();
					p.setProviderCode(map.get("providerCode").toString());
					p.setProviderName(map.get("providerName").toString());

					if (pm.add(p) == null) {
						log.error("Failed to import provider: {}", p);
						success &= false;
					} else {
						success &= true;
					}
				}

				if (!success) {
					log.warn("There were errors importing provider data.");
				}

				pm = null;
			} else {
				log.info("No results returned.");
			}
		} catch (Exception e) {
			log.error("Provider data import failed: ", e);
		}
	}

	@Override
	public Object next() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run() {
		importData();
	}
}
