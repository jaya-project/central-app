package com.valorti.importer;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.EmployeeContainer;
import com.valorti.jaya.model.ItemMovement;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.EmployeeManager;
import com.valorti.jaya.model.manager.ItemMovementManager;
import com.valorti.jaya.model.manager.WarehouseManager;
import com.valorti.utils.XmlUtils;

public class ProductExtractionImporter extends AbstractXmlImporter<Boolean> {
	private Container			container		= null;
	private ItemMovement		itemMovement	= null;
	private EmployeeContainer	empc			= null;
	private SimpleDateFormat	simpledf		= null;

	// Managers
	private ContainerManager	contMgr			= new ContainerManager();

	@SuppressWarnings("unchecked")
	public ProductExtractionImporter(File file) {
		super(file);
		simpledf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		this.rootNode = this.xmlDoc
				.selectNodes("/Content/PalletModifications/Container");
		this.rootIterator = this.rootNode.iterator();
	}

	private boolean parseAction(Element e, Container container) {
		ItemMovementManager itemMoveMgr = new ItemMovementManager();
		Warehouse warehouse = (Warehouse) new WarehouseManager().getById(
				Warehouse.class, XmlUtils.getLong(e, "WarehouseId"));
		ContainerManager mgr = new ContainerManager();

		if (warehouse != null) {
			itemMovement = itemMoveMgr.findByContainerAndWarehouse(container,
					warehouse);

			if (itemMovement == null) {
				itemMovement = new ItemMovement();
				itemMovement.setContainer(container);
				itemMovement.setItemsMoved(XmlUtils.getInt(e, "ItemsMoved"));
				itemMovement.setWarehouse(warehouse);
				container.getItemMovements().add(itemMovement);
				Serializable s = itemMoveMgr.add(itemMovement);
				log.debug("ContainerManager.add() serializable: {}", s);

				if (s != null) {
					log.debug("Item movement saved!");
					container.getEmployeeContainers().add(empc);

					if (mgr.update(container)) {
						log.debug("EmployeeContainer object data updated!");
						return true;
					} else {
						log.error("Failed to update employee container relation.");
					}
				} else {
					log.error("Failed to store Item movement.");
				}
			} else {
				itemMovement.setItemsMoved(itemMovement.getItemsMoved()
						+ XmlUtils.getInt(e, "ItemsMoved"));

				if (itemMoveMgr.update(itemMovement)) {
					container.getEmployeeContainers().add(empc);

					if (mgr.update(container)) {
						log.debug("EmployeeContainer object data updated!");
						return true;
					} else {
						log.error("Failed to update employee container relation.");
					}
				} else {
					log.error("Could not update item movement.");
				}
			}
		} else {
			log.error("Failed to retrieve warehouse information.");
		}

		warehouse = null;
		container = null;
		itemMovement = null;
		empc = null;
		return false;
	}

	private boolean parseContainer(Element e) {
		Long id = XmlUtils.getLong(e, "ContainerId");
		container = (Container) contMgr.getById(Container.class, id);
		log.debug("parseContainer({})", e.asXML());

		if (container != null) {
			Element el = e.element("UserContainer");
			Employee emp = (Employee) new EmployeeManager().getById(
					Employee.class, XmlUtils.getLong(el, "UserId"));
			empc = new EmployeeContainer();
			empc.setActionDate(XmlUtils.getDate(el, "ActionDate", simpledf));
			empc.setActionName(EmployeeContainer.Action.Modify);
			empc.setContainer(container);
			empc.setEmployee(emp);
			container.setItemCount(XmlUtils.getInt(e, "ItemCount"));

			if (contMgr.update(container)) {
				log.debug("Updated {}!", container.toString());
				return traverseActions(e.selectNodes("Action"));
			} else {
				log.error("Failed to update container with id {}", id);
			}

		} else {
			log.error(
					"The container with id {} does not exist. Maybe the user forgot to copy the updated database to the device?",
					id);
		}

		container = null;

		return false;
	}

	@Override
	public Boolean next() {
		Boolean r = false;
		
		if (super.hasNext()) {
			super.next();
			Element containerEl = (Element) rootNode.get(currentElement);
			r = parseContainer(containerEl);
			
			if(r == null)
				r = false;
		}

		return r;
	}

	private boolean traverseActions(List<?> list) {
		int modCount = 0;
		boolean actionsParsed = true;

		for (Iterator<?> it = list.iterator(); it.hasNext();) {
			Element e = (Element) it.next();
			actionsParsed &= parseAction(e, container);
			modCount++;
		}

		if (actionsParsed) {
			container.setModCount(modCount);

			try {
				if (contMgr.update(container)) {
					log.info("Container mod count successfully updated");
					return true;
				}
			} catch (Exception ex) {
				log.error("Faild to update container mod count. Reason: ", ex);
			}
		}

		return false;
	}
}
