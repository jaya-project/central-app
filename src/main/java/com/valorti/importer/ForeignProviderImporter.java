package com.valorti.importer;

import java.util.List;
import java.util.Map;

import com.valorti.utils.HibernateUtils;

public class ForeignProviderImporter extends AbstractSqlImporter {
	public ForeignProviderImporter() throws Exception {
	}

	@Override
	public boolean hasNext() {
		return false;
	}

	public void importData() {
		log.info("Trying to import Foreign Provider Data:");
		HibernateUtils.getSessionFactory().close();
		StringBuilder query = new StringBuilder(
				"SELECT CodGrupo AS code, DesGrupo AS name FROM softland.iw_tgrupo");
		List<Map<String, Object>> result = this.mssql.query(query.toString());

		if (result != null && result.size() > 0) {
			for (Map<String, Object> map : result) {
				try {
					String name = getString(map.get("name")), code = getString(map
							.get("code"));

					if (name.length() > 0 && code.length() > 0) {
						long foreignProviderId = this.sqlite.exists(
								"foreignProviderId", "ForeignProvider",
								"name = ? AND code = ?", name, code);

						if (foreignProviderId == 0) {
							log.debug(
									"The provider [code={}, name={}] does not exist. Trying to create it.",
									code, name);
							foreignProviderId = this.sqlite.insert(
									"ForeignProvider", "code, name", "?, ?",
									code, name);

							if (foreignProviderId > 0) {
								log.info(
										"Foreign Provider created (new id: {})!",
										foreignProviderId);
							} else {
								log.error(
										"Failed to create the foreign provider with code = {}, and name = {}.",
										code, name);
							}
						}
					} else {
						log.error(
								"The foreign provider name and/or code was null or empty: [code={}, name={}].",
								code, name);
					}

					name = null;
					code = null;
				} catch (Exception ex) {
					log.error("Exception caught: ", ex);
				}
			}
		}

		query = null;
		result = null;
	}

	@Override
	public Object next() {
		// TODO Auto-generated method stub
		return null;
	}
}
