package com.valorti.importer;

import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.ui.Main;
import com.valorti.sql.SqlServerHelper;
import com.valorti.sql.SqliteHelper;

public abstract class AbstractSqlImporter implements IImporter<Object> {
	protected SqlServerHelper	mssql	= null;
	protected SqliteHelper		sqlite	= null;
	protected final Logger		log		= LoggerFactory
												.getLogger(AbstractSqlImporter.class);

	public AbstractSqlImporter() throws Exception {
		mssql = new SqlServerHelper(
				Main.configuration()
						.getString(
								"/Application/Databases/Database[@id='sqlserver']/ConnectionString"));
		sqlite = new SqliteHelper(
				Main.configuration()
						.getString(
								"/Application/Databases/Database[@id='sqlite']/ConnectionString"));
		testConnection();
	}

	protected String getString(Object o) {
		if (o != null) {
			return o.toString();
		}

		return "";
	}

	private void testConnection() throws Exception {
		Connection c = mssql.connect();

		if (c == null) {
			throw new Exception(
					"Failed to connect to MS SQL Server. Maybe you are using wrong authentication method or credentials.");
		} else {
			mssql.close(c);
		}

		c = sqlite.connect();

		if (c == null) {
			throw new Exception(
					"Failed to connect to SQLite Database. Make sure that the DB file path is right.");
		} else {
			sqlite.close(c);
		}

		c = null;
	}
}