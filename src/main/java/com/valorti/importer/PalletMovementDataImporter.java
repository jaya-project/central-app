package com.valorti.importer;

import java.io.File;

import org.dom4j.Element;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Location;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.LocationManager;
import com.valorti.jaya.model.manager.WarehouseManager;
import com.valorti.utils.XmlUtils;

public class PalletMovementDataImporter extends AbstractXmlImporter<Boolean> {
	private final ContainerManager	contMgr	= new ContainerManager();
	private final LocationManager	locMgr	= new LocationManager();
	private final WarehouseManager	whMgr	= new WarehouseManager();

	@SuppressWarnings("unchecked")
	public PalletMovementDataImporter(File file) {
		super(file);
		this.rootNode = this.xmlDoc
				.selectNodes("/Content/PalletMoves/PalletMove");
		this.rootIterator = this.rootNode.iterator();
	}

	@Override
	public Boolean next() {
		if (this.hasNext()) {
			super.next();
			Element current = (Element) rootNode.get(currentElement);
			return parse(current);
		}

		return false;
	}

	private boolean parse(Element e) {
		Element containerElement = e.element("Container");
		Element destLocation = (Element) e
				.selectSingleNode("DestLocation/Location");
		Long id = XmlUtils.getLong(containerElement, "ContainerId");

		if (id > 0) {
			Container container = (Container) contMgr.getById(Container.class,
					id);

			if (container != null) {
				Warehouse srcWarehouse = container.getLocation().getWarehouse();
				Warehouse dstWarehouse = (Warehouse) whMgr.getById(
						Warehouse.class,
						XmlUtils.getLong(destLocation, "WarehouseId"));
				String row = XmlUtils.getString(destLocation, "Row");
				int column = XmlUtils.getInt(destLocation, "Column");
				int depth = XmlUtils.getInt(destLocation, "Depth");
				log.info(
						"Trying to move container (lotcode = {}-{}) from (warehouse [{}], location [{}{}{})]) to (warehouse [{}], location [{}{}{}]).",
						container.getLotCode(), container.getIdentifier(),
						srcWarehouse.getWhName(), container.getLocation()
								.getRow(), container.getLocation().getColumn(),
						container.getLocation().getDepth(), dstWarehouse
								.getWhName(), row, column, depth);

				Location l = locMgr.getLocation(row, column, depth,
						dstWarehouse);

				if (l != null) {
					container.setLocation(l);

					if (contMgr.update(container)) {
						log.info("Container location updated!");
						return true;
					} else {
						log.error("Failed to update container location!");
					}
				} else {
					log.error("The {} does not exist and failed to create it.",
							l);
				}
			} else {
				log.error("The container doesn't exists.!");
			}
		} else {
			log.error("The container id was 0.");
		}

		return false;
	}
}