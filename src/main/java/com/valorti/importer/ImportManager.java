package com.valorti.importer;

import java.io.File;
import java.util.List;

import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.ui.Main;

public class ImportManager {
	public ImportManager() {
	}

	/**
	 * Imports data from a CSV file.
	 * 
	 * @param file
	 *            - The CSV file location.
	 * @param warehouse
	 *            - Warehouse where the items will be.
	 * @param employee
	 *            - Who is importing the data.
	 * @return A list of the lines that failed to be parsed or null if every
	 *         line was parsed.
	 */
	public List<String> firstImport(File file, Warehouse warehouse,
			Employee employee) {
		FirstImport fi = new FirstImport(file);
		fi.parseFile(warehouse, employee);
		return fi.getFailedLines();
	}

	/**
	 * Imports data from the remote database.
	 */
	public void importForeignProviders() throws Exception {
		ForeignProviderImporter fpi = new ForeignProviderImporter();
		fpi.importData();
	}

	/**
	 * Imports item data from remote database and associates it to their Foreign
	 * Providers. <b>Note<b>: This is done using pure SQLite queries for better
	 * performance.
	 */
	public void importItems() throws Exception {
		ItemImporter ii = new ItemImporter(Main.configuration().getString(
				"/Application/SpecificConfiguration/ItemImport/ItemCondition"));
		ii.importData();
	}
}