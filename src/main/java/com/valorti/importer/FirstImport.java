package com.valorti.importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.EmployeeContainer;
import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.Location;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.ItemManager;
import com.valorti.jaya.model.manager.LocationManager;

public class FirstImport {
	private final Logger				log			= LoggerFactory
															.getLogger(FirstImport.class);
	private File						importFile;
	private Warehouse					warehouse	= null;
	private Employee					employee	= null;
	private List<String>				failedLines	= null;
	private ContainerManager			cm;
	private Map<String, List<String>>	lots		= new LinkedHashMap<String, List<String>>();

	public FirstImport(File importFile) {
		this.importFile = importFile;
		cm = new ContainerManager();
	}

	private void free() {
		importFile = null;
		warehouse = null;
		employee = null;
		lots = null;
		cm = null;
	}

	/**
	 * Retrieves the list of lines that failed to be parsed (the ones that had
	 * thrown an exception).
	 * 
	 * @return A list of the failed lines or null if there were no failures.
	 */
	public List<String> getFailedLines() {
		return failedLines;
	}

	private Item getItem(String code) throws Exception {
		Item item = null;

		if (code.length() > 0) {
			item = new ItemManager().findByCode(code);

			if (item == null) {
				log.error(
						"Failed to retrieve item with code {}. Maybe it haven't been stored on the database?",
						code);
				throw new Exception(
						"Item "
								+ code
								+ " not found. Make sure that the item has been stored on the database.");
			}
		}

		return item;
	}

	private Location getLocation(String locationStr) throws Exception {
		log.debug("Location String: {}", locationStr);
		String regex = "^[A-Z][0-9]{4}$";
		Location location = null;

		if (locationStr.matches(regex)) {
			String row = locationStr.substring(0, 1);
			String column = locationStr.substring(1, 3);
			String depth = locationStr.substring(3, 5);

			if (row.length() > 0 && column.length() > 0 && depth.length() > 0) {
				log.info(
						"Trying to create location [row={}, column={}, depth={}].",
						row, column, depth);
				LocationManager lm = new LocationManager();
				location = lm.findByLocation(row, column, depth);

				if (location == null) {
					location = new Location();
					location.setColumn(column);
					location.setDepth(depth);
					location.setRow(row);
					location.setWarehouse(warehouse);
					warehouse.getLocations().add(location);
					Serializable s = lm.add(location);

					if (s == null) {
						log.error("Failed to create location.");
						throw new Exception(
								String.format(
										"Failed to create entity Location [row=%s, column=%s, depth=%s]",
										row, column, depth));
					}
				}

				lm = null;
			}
			row = column = depth = null;
		}

		regex = null;

		return location;
	}

	/**
	 * Parses a line that matches the following regular expression:
	 * [a-z0-9\s-_];\d{3}-\d+;\d+;\d+;\d+;\d+;[a-z0-9]
	 * 
	 * @param line
	 *            The line to be parsed.
	 * @throws EntityCreationFailedException
	 */
	private void parse(String lotCode, Long identifier, String line)
			throws Exception {
		String[] split = line.split(";");
		log.debug("line piece count: {}", split.length);

		if (split.length == 4) {
			identifier++;
			Container container = cm.findByLotCodeAndIdentifier(lotCode,
					identifier);

			if (container == null) {
				Location location = getLocation(split[3].trim());
				Item item = getItem(split[0].trim());

				container = new Container();
				container.setDimensions("");
				container.setIdentifier(identifier);
				container.setLotCode(lotCode);
				container.setLocation(location);
				container.setItemCount(Integer.parseInt(split[2]));
				container.setItemTotalLength(Double.parseDouble(split[1]));
				container.setPerItemAvgLength(0.0);
				container.setUnitPrice(0.0);
				container.setItem(item);
				location.getContainers().add(container);
				Serializable s = cm.add(container);

				if (s != null) {
					s = cm.linkToEmployee(container, employee, new Date(),
							EmployeeContainer.Action.Entry);

					if (s == null) {
						log.warn("Failed to link container to employee.");
					}
				} else {
					log.error("Failed to create container.");
					throw new Exception(
							String.format(
									"Failed to create entity Container [lotCode=%d, identifier=%d]",
									lotCode, identifier));
				}
			} else {
				log.info("Container found: {}", container);
			}

			container = null;
		}
	}

	/**
	 * Parses and imports data into the database. Blank lines are not parsed,
	 * but counted. When finished, a list of failed parsed lines can be
	 * retrieved using getFailedLines.
	 * 
	 * @param warehouse
	 *            - The warehouse where the containers are being moved to.
	 * @throws NullPointerException
	 *             - If the warehouse or employee are null.
	 */
	public void parseFile(Warehouse warehouse, Employee employee)
			throws NullPointerException {
		if (warehouse == null || employee == null)
			throw new NullPointerException(
					"The warehouse or employee object provided was null. Failed to parse import file.");

		this.warehouse = warehouse;
		this.employee = employee;

		if (importFile.exists()) {
			String line = "";
			int lineNo = 1;

			try {
				FileReader fr = new FileReader(importFile);
				BufferedReader br = new BufferedReader(fr);

				while ((line = br.readLine()) != null) {
					if (line != null && line.length() > 0) {
						remap(line.trim());
					}
					lineNo++;
				}

				for (Entry<String, List<String>> entry : lots.entrySet()) {
					List<String> lines = entry.getValue();
					String lotCode = entry.getKey();
					Long identifier = 0L;

					for (String tmp : lines) {
						try {
							parse(lotCode, identifier, tmp);
						} catch (Exception ex) {
							log.error("Failed to parse data: {}", tmp, ex);
							if (failedLines == null)
								failedLines = new ArrayList<String>();

							failedLines.add(tmp);
						}

						identifier++;
					}
				}

				br.close();
			} catch (FileNotFoundException e) {
				log.error("Could not find the file {}: ",
						importFile.getAbsoluteFile(), e);
			} catch (IOException e) {
				log.error("Failed reading line {}: {}", lineNo, line, e);
			}

			log.info("Finished parsing file. Last line parsed: {} - {}",
					lineNo, line);
			// logLots();
		} else {
			log.error("The file {} does not exist.",
					importFile.getAbsolutePath());
		}

		free();
	}

	private void remap(String line) {
		String regex = "^[\\w\\W\\s-_]+;\\d{3}-\\d+;\\d+;\\d+;\\d+;\\d+;[a-zA-Z]{1}[0-9]{4}$";

		if (line != null && line.matches(regex)) {
			String[] split = line.split(";");

			if (split.length == 7) {
				String lotCode = split[2];
				String data = String.format("%s;%s;%s;%s", split[1], split[3],
						split[4], split[6]);
				// System.out.printf("Remapping lot code: %s\n", lotCode);
				// System.out.printf("New data: %s\n", data);

				if (lots.containsKey(lotCode)) {
					lots.get(lotCode).add(data);
					// System.out.printf("lots contains key.\n");
				} else {
					// System.out.printf("Lots does not contains key.\n");
					List<String> list = new ArrayList<String>();
					list.add(data);
					lots.put(lotCode, list);
					list = null;
				}
			} else {
				log.error("The split length was more or less than 7.");
			}
		} else {
			log.error("The line {} did not match the regular expression: {}",
					line, regex);
		}
	}

	// private void logLots() {
	// for (Entry<Long, List<String>> entry : lots.entrySet()) {
	// System.out.printf("Current Lot: %d\n", entry.getKey());
	//
	// for (Iterator<String> it = entry.getValue().iterator(); it
	// .hasNext();) {
	// System.out.printf("Current line: %s\n", it.next());
	// }
	// }
	// }
}
