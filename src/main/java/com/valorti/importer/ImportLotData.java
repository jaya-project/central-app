package com.valorti.importer;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.dom4j.Element;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.EmployeeContainer;
import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.Location;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.EmployeeManager;
import com.valorti.jaya.model.manager.ItemManager;
import com.valorti.jaya.model.manager.LocationManager;
import com.valorti.jaya.model.manager.WarehouseManager;
import com.valorti.utils.XmlUtils;

public class ImportLotData extends AbstractXmlImporter<Set<Container>> {
	private String					lotCode;
	private final LocationManager	locationMgr		= new LocationManager();
	private final ContainerManager	containerMgr	= new ContainerManager();
	private final WarehouseManager	warehouseMgr	= new WarehouseManager();

	private Set<Container>			containerSet	= null;

	@SuppressWarnings("unchecked")
	public ImportLotData(File xmlFile) {
		super(xmlFile);
		this.lotCode = "";
		this.rootNode = this.xmlDoc.selectNodes("/Content/Lots/Lot");
		this.rootIterator = this.rootNode.iterator();
	}

	@Override
	public Set<Container> next() {
		this.containerSet = null;

		if (this.hasNext()) {
			super.next();
			this.containerSet = new HashSet<Container>();
			Element current = (Element) rootNode.get(currentElement);
			this.lotCode = current.attributeValue("code");
			log.info("Importing data for lot code {}", this.lotCode);
			this.traverseLocations(current.selectNodes("Locations/Location"));
		}

		return this.containerSet;
	}

	private Container parseContainer(Element e, Location location) {
		log.debug("Parsing container node: {}", e.asXML());
		Long identifier = XmlUtils.getLong(e, "Identifier");
		Container container = this.containerMgr.findByLotCodeAndIdentifier(
				this.lotCode, identifier);
		Serializable s = null;
		String v = "";

		if (container == null) {
			container = new Container();
			container.setDimensions(XmlUtils.getString(e, "Dimensions"));
			container.setExpireDate(null);
			container.setIdentifier(identifier);
			container.setItemCount(XmlUtils.getInt(e, "ItemCount"));
			container.setItemTotalLength(XmlUtils.getDouble(e, "ItemTotalLength"));
			container.setPerItemAvgLength(XmlUtils.getDouble(e,
					"PerItemAvgLength"));
			container.setUnitPrice(XmlUtils.getDouble(e, "UnitPrice"));
			container.setItem((Item) new ItemManager().getById(Item.class,
					XmlUtils.getLong(e, "ItemId")));
			container.setLotCode(this.lotCode);
			container.setLocation(location);
			log.debug("Trying to save container = {}", container.toString());

			s = containerMgr.add(container);
		} else {
			log.debug("Container found with identifier {} and lot code {}",
					identifier, this.lotCode);
			s = container.getId();
		}

		if (s != null) {
			List<?> list = e.selectNodes("UserContainer");
			log.debug("Parsing user container list...");
			Employee employee = null;

			for (Iterator<?> ucIt = list.iterator(); ucIt.hasNext();) {
				Element el = (Element) ucIt.next();
				EmployeeManager em = new EmployeeManager();
				v = XmlUtils.getString(el, "UserId");
				employee = (Employee) em.getById(Employee.class,
						Long.parseLong(v));

				if (employee != null) {
					Date date = XmlUtils.getDate(el, "ActionDate",
							new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"));
					if (date == null)
						date = Calendar.getInstance().getTime();

					s = em.linkToContainer(container, employee, date,
							EmployeeContainer.Action.valueOf(XmlUtils
									.getString(el, "ActionName")));

					if (s != null) {
						log.error(
								"Failed to link employee {} to container {}!",
								employee, container);
					}

					date = null;
				} else {
					log.error(
							"Failed to link Employee to Container because the Employee (id {}) doesn't exist. This may be due that the database versions doesn't match.",
							v);
				}

				em = null;
				employee = null;
				el = null;
				s = null;
			}
		} else {
			log.error("Failed to create container. Data: {}.", e.asXML());
		}

		s = null;

		return container;
	}

	private void parseLocation(Element e) throws Exception {
		String row = XmlUtils.getString(e, "Row");
		String column = XmlUtils.getString(e, "Column");
		String depth = XmlUtils.getString(e, "Depth");
		Location location = this.locationMgr.findByLocation(row, column, depth);
		Serializable s = null;
		Warehouse warehouse = null;

		if (location == null) {
			location = new Location();
			location.setColumn(column);
			location.setDepth(depth);
			location.setRow(row);
			location.setPosition(XmlUtils.getString(e, "Position"));
			location.setLocationName(XmlUtils.getString(e, "LocationName"));
			warehouse = (Warehouse) warehouseMgr.getById(Warehouse.class,
					XmlUtils.getLong(e, "WarehouseId"));

			if (warehouse != null)
				log.debug("Warehouse not null: {}", warehouse);

			location.setWarehouse(warehouse);
			s = this.locationMgr.add(location);
		} else {
			s = location.getId();
			warehouse = location.getWarehouse();
		}

		if (s != null) {
			traverseContainers(e.selectNodes("Containers/Container"), location);
		} else {
			log.error("Failed to create location {}!", location);
		}

		row = "";
		column = "";
		depth = "";
		location = null;
		s = null;
	}

	private void traverseContainers(List<?> list, Location location) {
		if (list != null && list.size() > 0) {
			for (Iterator<?> it = list.iterator(); it.hasNext();) {
				Element containerEl = (Element) it.next();
				this.containerSet.add(parseContainer(containerEl, location));
			}
		} else {
			log.info("Container list was empty or null.");
		}
	}

	private void traverseLocations(List<?> list) {
		log.debug("Traversing locations nodes.");

		if (list != null && list.size() > 0) {
			for (Iterator<?> it = list.iterator(); it.hasNext();) {
				Element e = (Element) it.next();
				log.debug("Current Location\n{}\n# Location End #", e.asXML());

				try {
					parseLocation(e);
				} catch (Exception ex) {
					log.error(
							"Failed to parse location. XML Data\n{}\nError\n{}",
							e.asXML(), ex);
				}

				/*
				 * for (Iterator<?> lit = e.elementIterator(); lit.hasNext();) {
				 * Element locationEl = (Element) lit.next(); try {
				 * parseLocation(locationEl); } catch (Exception ex) {
				 * log.error( "Failed to parse location. Data {}. Error: {} ",
				 * locationEl.asXML(), ex); } }
				 */
			}
		} else {
			log.info("Location node list was empty or null.");
		}
	}
}