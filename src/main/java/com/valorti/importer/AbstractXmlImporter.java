package com.valorti.importer;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractXmlImporter<T> implements IImporter<T> {

	// Xml file where the data is.
	protected File					xmlFile;
	// Dom4j Document.
	protected Document				xmlDoc;
	// SLF4j logger variable.
	protected final Logger			log	= LoggerFactory
												.getLogger(AbstractXmlImporter.class);
	protected int					currentElement;

	protected List<? extends Node>	rootNode;
	protected Iterator<?>			rootIterator;

	public AbstractXmlImporter(File xmlFile) {
		this.xmlFile = xmlFile;
		this.currentElement = -1;
		readFile();
	}

	/**
	 * Checks if there are more elements in the array.
	 */
	@Override
	public boolean hasNext() {
		boolean r = false;
		
		try {
			log.trace("Current element = {} : Item count = {}",
					this.currentElement, this.rootNode.size());
			r = this.currentElement < (this.rootNode.size() - 1);
		} catch (Exception ex) {
			log.error("Exception caught: {}", ex);
			r = false;
		}

		return r;
	}

	@Override
	public T next() {
		log.debug("AbstractXMLImporter.next()");
		this.currentElement++;
		return null;
	}

	protected void readFile() {
		if (xmlFile != null && this.xmlFile.exists()) {
			try {
				SAXReader reader = new SAXReader();
				this.xmlDoc = reader.read(this.xmlFile);
			} catch (DocumentException ex) {
				log.error("Failed to open XML document: ", ex);
			}
		}
	}
}
