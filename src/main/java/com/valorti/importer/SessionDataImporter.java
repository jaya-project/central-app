package com.valorti.importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Session;
import com.valorti.jaya.model.manager.EmployeeManager;

public class SessionDataImporter {
	private final Logger	log				= LoggerFactory
													.getLogger(SessionDataImporter.class);
	private File			sessionFile		= null;
	private List<String>	unparsedLines	= new ArrayList<String>();

	public SessionDataImporter(File sessionFile) {
		if (sessionFile != null)
			this.sessionFile = sessionFile;
	}

	public List<String> getUnparsedLines() {
		return unparsedLines;
	}

	private void parseSessionFile() {
		log.info("Parsing session file...");
		BufferedReader reader = null;
		InputStreamReader isr = null;

		try {
			isr = new InputStreamReader(new FileInputStream(sessionFile));
			reader = new BufferedReader(isr);
			String line = "";
			EmployeeManager emgr = new EmployeeManager();
			Session session = null;
			Serializable s = null;

			while ((line = reader.readLine()) != null) {
				if (line.matches("^\\s*\\d{2}\\-\\d{2}\\-\\d{4}\\s*\\d{2}:\\d{2}:\\d{2}\\s*,\\s*\\w+\\s*,\\s*\\d+\\s*.+$")) {
					// eventTime, actionName, employeeId
					String[] split = line.split(",");
					Long eid = Long.parseLong(split[2].trim());
					Date eventTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
							.parse(split[0].trim());
					Session.EventName actionName = Session.EventName
							.valueOf(split[1].trim());
					session = new Session();
					session.setEventName(actionName);
					session.setEventTime(eventTime);

					if (split.length >= 5) {
						if (split[3] != null && split[3].length() > 0)
							session.setLogonDeviceId(split[3].trim());

						if (split[4] != null && split[4].length() > 0)
							session.setLogonDeviceName(split[4].trim());
					}

					s = emgr.addSession(eid, session);

					if (s == null) {
						log.error("Failed to add session {} to employee {}!",
								session, eid);
						unparsedLines.add(line);
					} else {
						log.info("Session {} linked to employee {}.",
								session.getId(), eid);
					}

					s = null;
					eventTime = null;
					actionName = null;
					session = null;
					eid = null;
					split = null;
				}
			}

			emgr = null;
			line = null;
		} catch (FileNotFoundException e) {
			log.error("The file {} was not found: ",
					sessionFile.getAbsolutePath(), e);
		} catch (IOException e) {
			log.error("Failed to read line: ", e);
		} catch (ParseException e) {
			log.error("Failed to parse timestamp: ", e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
					reader = null;
				}

				if (isr != null) {
					isr.close();
					isr = null;
				}
			} catch (IOException e) {
				log.error("Failed to close input streams: ", e);
			}
		}
	}

	public void run() {
		if (sessionFile.exists()) {
			parseSessionFile();
		}
	}
}
