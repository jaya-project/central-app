package com.valorti.importer;

public interface IImporter<E> {
	// public void parseData();

	public boolean hasNext();

	public E next();
}