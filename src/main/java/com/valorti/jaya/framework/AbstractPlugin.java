package com.valorti.jaya.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPlugin implements IPlugin {
	protected final Logger	log			= LoggerFactory
												.getLogger(AbstractPlugin.class);
	protected boolean		configured	= false;
	protected boolean		disposed	= false;

	public AbstractPlugin() {
	}
	
	@Override
	public void dispose() {
		disposed = true;
	}

	@Override
	public boolean isConfigured() {
		return configured;
	}

	@Override
	public boolean isDisposed() {
		return disposed;
	}
}
