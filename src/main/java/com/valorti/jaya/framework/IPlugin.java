package com.valorti.jaya.framework;

import org.eclipse.swt.widgets.MenuItem;

import com.valorti.configuration.XMLConfiguration;

public interface IPlugin {
	/**
	 * Configures the plugin using the XML configuration data.
	 */
	public void configure(XMLConfiguration config);

	/**
	 * Cleans up the resources.
	 */
	public void dispose();

	public void execute(Object... params);

	// /**
	// * Executes a plugin that receives the parameters depending on the action
	// it
	// * does.
	// *
	// * @param param
	// * The data expected by the plugin.
	// */
	// public void execute(Object param);
	//
	// /**
	// * Executes a plugin that receives an array of parameters.
	// *
	// * @param params
	// * The parameters expected by the plugin.
	// */
	// public void execute(Object... params);

	/**
	 * Adds a sub menu to the parent menu item.
	 * 
	 * @param parent
	 *            The parent menu item.
	 */
	public void addMenu(MenuItem parent);

	/**
	 * Checks if the plugin is already configured.
	 * 
	 * @return True if it's configured, false otherwise.
	 */
	public boolean isConfigured();

	/**
	 * Checks if the plugin has been disposed or not.
	 * 
	 * @return True if it's disposed, false otherwise.
	 */
	public boolean isDisposed();
}
