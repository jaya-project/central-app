package com.valorti.jaya.framework;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.ui.Main;
import com.valorti.jaya.ui.events.ProgressEvent;

/**
 * Manages the application plugins.
 * 
 * @author Nicolas Mancilla Vergara
 *
 */
public class PluginManager {
	private Logger					log			= LoggerFactory
														.getLogger(PluginManager.class);

	// Main Class Fully qualified name, plugin object.
	private Map<String, IPlugin>	pluginList	= null;
	private String					basePath	= "";
	private boolean					configured	= false;
	private static PluginManager	self		= null;

	private PluginManager() {
	}

	public static PluginManager instance() {
		if (self == null) {
			self = new PluginManager();
		}

		return self;
	}

	/**
	 * Configures the base path.
	 * 
	 * @param pluginsFolderPath
	 *            Path to the plugins folder.
	 */
	public void configure(String pluginsFolderPath) {
		if (!configured) {
			if (pluginsFolderPath != null && pluginsFolderPath.length() > 0) {
				basePath = pluginsFolderPath;
				configured = true;
			} else {
				basePath = "plugins";
				File tmp = new File(basePath);

				try {
					tmp.mkdirs();
					configured = true;
				} catch (Exception ex) {
					log.error("Failed to create plugin directory.");
				}
			}
		}
	}

	/**
	 * Loads the plugins for this application.
	 * 
	 * @param pe
	 */
	public void loadPlugins(ProgressEvent pe) {
		File pluginFolder = new File(basePath);
		pluginList = new HashMap<String, IPlugin>();

		if (pluginFolder.exists()) {
			if (pluginFolder.isDirectory()) {
				File[] fileList = pluginFolder.listFiles();

				if (fileList != null && fileList.length > 0) {
					for (File pluginFile : fileList) {
						log.trace("Loading plugin [name={}, file={}].",
								pluginFile.getName(),
								pluginFile.getAbsolutePath());

						if (pluginFile.getAbsolutePath().endsWith(".jar")) {
							JarFile jarfile = null;

							try {
								jarfile = new JarFile(pluginFile);
								Manifest mf = jarfile.getManifest();

								if (mf != null) {
									Attributes att = mf.getMainAttributes();
									String mainClass = att
											.getValue("Main-Class");
									IPlugin plugin = null;
									URLClassLoader classLoader = null;

									log.info(
											"Loading plugin [file={}, fqn={}].",
											pluginFile.getAbsolutePath(),
											mainClass);
									try {
										classLoader = new URLClassLoader(
												new URL[] { pluginFile.toURI()
														.toURL() });
										Class<?> clazz = classLoader
												.loadClass(mainClass);
										plugin = (IPlugin) clazz.newInstance();
										log.info("Success!");
									} catch (MalformedURLException ex) {
										log.error("Malformed URL.", ex);
									} catch (ClassNotFoundException ex) {
										log.error(
												"Could not find the class {}.",
												mainClass, ex);
									} catch (InstantiationException ex) {
										log.error(
												"Failed to instantiate class.",
												ex);
									} catch (IllegalAccessException ex) {
										log.error("Invalid access.", ex);
									} finally {
										if (classLoader != null) {
											try {
												classLoader.close();
											} catch (IOException ex) {
												log.error(
														"Failed to close class loader resource.",
														ex);
											}

											classLoader = null;
										}
									}

									pluginList.put(mainClass, plugin);

									if (log.isTraceEnabled())
										logMainAttributes(mf);
								}
							} catch (IOException ex) {
								log.error(
										"Could not read jar file manifest [file={}].",
										pluginFile.getAbsolutePath(), ex);
							} catch (SecurityException ex) {
								log.error(
										"No access to the plugin file. Maybe you need administrator rights (on Windows -> right click the application -> Run as Administrator).",
										ex);
							} finally {
								if (jarfile != null) {
									try {
										jarfile.close();
									} catch (IOException ex) {
										log.error(
												"Failed to close plugin jar file.",
												ex);
									}
									jarfile = null;
								}
							}
						} else {
							log.warn(
									"Tried to add a file that is not a plugin: [{}]",
									pluginFile.getAbsolutePath());
						}
					}
				}
			} else {
				log.error("The plugins folder [{}] is not a directory.",
						pluginFolder.getAbsolutePath());
			}
		} else {
			log.info("Plugins folder [{}] does not exist.",
					pluginFolder.getAbsolutePath());
		}
	}

	/**
	 * Only works if trace is enabled.
	 * 
	 * @param mf
	 */
	private void logMainAttributes(Manifest mf) {
		Attributes att = mf.getMainAttributes();

		if (att != null) {
			Set<Object> keys = att.keySet();
			for (Object key : keys) {
				try {
					log.trace("Manifest Attribute [{}={}]", key, att.get(key));
				} catch (Exception ex) {
					log.error("Exception...", ex);
				}
			}
		}
	}

	/**
	 * Retrieves the plugin by name.
	 * 
	 * @param name
	 *            Fully qualified main class name.
	 * @return An instance of IPlugin or null if the plugin does not exists.
	 */
	public IPlugin get(String name) {
		if (name != null && name.length() > 0) {
			if (pluginList.containsKey(name)) {
				IPlugin plugin = pluginList.get(name);

				if (!plugin.isConfigured())
					plugin.configure(Main.configuration());

				return plugin;
			} else {
				log.error("Plugin provider [{}] not found.", name);
			}
		}

		return null;
	}

	/**
	 * Disposes of all plugins (if they have not been disposed already).
	 */
	public void dispose() {
		Set<String> keys = pluginList.keySet();

		for (String key : keys) {
			IPlugin plugin = pluginList.get(key);

			if (!plugin.isDisposed()) {
				plugin.dispose();
			}
		}
	}
}
