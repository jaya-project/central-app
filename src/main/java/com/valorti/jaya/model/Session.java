package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Session", uniqueConstraints = { @UniqueConstraint(name = "SessionEventU", columnNames = {
		"eventTime", "eventName", "employeeId" }) })
public class Session implements IEntity {
	public enum EventName {
		Login, Logout
	}

	private static final long	serialVersionUID	= 7229479242804321885L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "sessionId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "eventTime", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date				eventTime;

	@Column(name = "eventName", nullable = false)
	private String				eventName;

	@Column(name = "logon_device_name", nullable = false)
	private String				logonDeviceName		= "device";

	@Column(name = "logon_device_id", nullable = false)
	private String				logonDeviceId		= "0";

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employeeId", nullable = false)
	private Employee			employee;

	@Version
	private Integer				version;

	public Session() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Session)) {
			return false;
		}
		Session other = (Session) obj;
		if (eventName == null) {
			if (other.eventName != null) {
				return false;
			}
		} else if (!eventName.equals(other.eventName)) {
			return false;
		}
		if (eventTime == null) {
			if (other.eventTime != null) {
				return false;
			}
		} else if (!eventTime.equals(other.eventTime)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the logonDeviceName
	 */
	public String getLogonDeviceName() {
		return logonDeviceName;
	}

	/**
	 * @param logonDeviceName
	 *            the logonDeviceName to set
	 */
	public void setLogonDeviceName(String logonDeviceName) {
		this.logonDeviceName = logonDeviceName;
	}

	/**
	 * @return the logonDeviceId
	 */
	public String getLogonDeviceId() {
		return logonDeviceId;
	}

	/**
	 * @param logonDeviceId
	 *            the logonDeviceId to set
	 */
	public void setLogonDeviceId(String logonDeviceId) {
		this.logonDeviceId = logonDeviceId;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public EventName getEventName() {
		return EventName.valueOf(eventName);
	}

	public Date getEventTime() {
		return eventTime;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventName == null) ? 0 : eventName.hashCode());
		result = prime * result
				+ ((eventTime == null) ? 0 : eventTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setEventName(EventName eventName) {
		this.eventName = eventName.toString();
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Session [id=").append(id).append(", eventTime=")
				.append(eventTime).append(", eventName=").append(eventName)
				.append(", employee=").append(employee).append(", version=")
				.append(version).append(']');
		return builder.toString();
	}
}
