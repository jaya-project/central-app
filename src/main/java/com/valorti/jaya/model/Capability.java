package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Capability", uniqueConstraints = { @UniqueConstraint(name = "CapabilityActionName_U", columnNames = "name") })
public class Capability implements IEntity {
	private static final long			serialVersionUID		= 7603876969398902239L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "capabilityId", unique = true, nullable = false)
	private Long						id;

	@Column(name = "name", unique = true, nullable = false)
	private String						name;

	@Column(name = "description", unique = false, nullable = false)
	private String						description;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.capability", cascade = CascadeType.ALL)
	private Set<EmployeeCapabilities>	employeeCapabilities	= new HashSet<EmployeeCapabilities>();

	@Version
	protected Integer					version;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Capability)) {
			return false;
		}
		Capability other = (Capability) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the employeeCapabilities
	 */
	public Set<EmployeeCapabilities> getEmployeeCapabilities() {
		if (employeeCapabilities == null)
			employeeCapabilities = new HashSet<EmployeeCapabilities>();

		return employeeCapabilities;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the version
	 */
	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param employeeCapabilities
	 *            the employeeCapabilities to set
	 */
	public void setEmployeeCapabilities(
			Set<EmployeeCapabilities> employeeCapabilities) {
		this.employeeCapabilities = employeeCapabilities;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Capability [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
