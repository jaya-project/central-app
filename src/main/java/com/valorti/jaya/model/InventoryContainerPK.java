package com.valorti.jaya.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class InventoryContainerPK implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@ManyToOne
	private Inventory			inventory;

	@ManyToOne
	private Container			container;

	public InventoryContainerPK() {
	}

	public InventoryContainerPK(Inventory inventory, Container container) {
		this.container = container;
		this.inventory = inventory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof InventoryContainerPK)) {
			return false;
		}
		InventoryContainerPK other = (InventoryContainerPK) obj;
		if (container == null) {
			if (other.container != null) {
				return false;
			}
		} else if (!container.equals(other.container)) {
			return false;
		}
		if (inventory == null) {
			if (other.inventory != null) {
				return false;
			}
		} else if (!inventory.equals(other.inventory)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the container
	 */
	public Container getContainer() {
		return container;
	}

	/**
	 * @return the inventory
	 */
	public Inventory getInventory() {
		return inventory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((container == null) ? 0 : container.hashCode());
		result = prime * result
				+ ((inventory == null) ? 0 : inventory.hashCode());
		return result;
	}

	/**
	 * @param container
	 *            the container to set
	 */
	public void setContainer(Container container) {
		this.container = container;
	}

	/**
	 * @param inventory
	 *            the inventory to set
	 */
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeCapabilitiesPK [Employee =")
				.append(inventory.getStartDate()).append(" - ")
				.append(inventory.getEndDate()).append(", Container =")
				.append(container.getLotCode()).append(']');
		return builder.toString();
	}
}