package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "ForeignProvider", uniqueConstraints = { @UniqueConstraint(name = "ForeignProviderCodeU", columnNames = "code") })
public class ForeignProvider implements IEntity {
	private static final long	serialVersionUID	= 5425910301156047371L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "foreignProviderId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "code", nullable = false)
	private String				code;

	@Column(name = "name", unique = false, nullable = false)
	private String				name;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "foreignProviderId", insertable = false, updatable = false)
	@OrderColumn(name = "index")
	private Set<Item>			items;

	@Version
	private Integer				version;

	public ForeignProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ForeignProvider)) {
			return false;
		}
		ForeignProvider other = (ForeignProvider) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @return the items
	 */
	public Set<Item> getItems() {
		if (items == null)
			items = new HashSet<Item>();

		return items;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the version
	 */
	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(Set<Item> items) {
		this.items = items;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ForeignProvider [id=").append(id).append(", code=")
				.append(code).append(", name=").append(name)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
