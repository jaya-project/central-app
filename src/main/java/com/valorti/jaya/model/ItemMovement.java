package com.valorti.jaya.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ItemMovement")
@AssociationOverrides({
		@AssociationOverride(name = "primaryKey.warehouse", joinColumns = @JoinColumn(name = "warehouseId")),
		@AssociationOverride(name = "primaryKey.container", joinColumns = @JoinColumn(name = "containerId")) })
public class ItemMovement {

	@EmbeddedId
	private ItemMovementPK	primaryKey	= null;

	@Column(name = "itemsMoved")
	private Integer			itemsMoved;

	@ManyToOne
	@JoinColumn(name = "locationId", nullable = true)
	private Location		location;

	public ItemMovement() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ItemMovement)) {
			return false;
		}
		ItemMovement other = (ItemMovement) obj;
		if (primaryKey == null) {
			if (other.primaryKey != null) {
				return false;
			}
		} else if (!primaryKey.equals(other.primaryKey)) {
			return false;
		}
		return true;
	}

	@Transient
	public Container getContainer() {
		return this.getPrimaryKey().getContainer();
	}

	/**
	 * @return the itemsMoved
	 */
	public Integer getItemsMoved() {
		return itemsMoved;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @return the primaryKey
	 */
	public ItemMovementPK getPrimaryKey() {
		if (primaryKey == null)
			primaryKey = new ItemMovementPK();

		return primaryKey;
	}

	public Warehouse getWarehouse() {
		return this.getPrimaryKey().getWarehouse();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((primaryKey == null) ? 0 : primaryKey.hashCode());
		return result;
	}

	public void setContainer(Container container) {
		this.getPrimaryKey().setContainer(container);
	}

	/**
	 * @param itemsMoved
	 *            the itemsMoved to set
	 */
	public void setItemsMoved(Integer itemsMoved) {
		this.itemsMoved = itemsMoved;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * @param primaryKey
	 *            the primaryKey to set
	 */
	public void setPrimaryKey(ItemMovementPK primaryKey) {
		this.primaryKey = primaryKey;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.getPrimaryKey().setWarehouse(warehouse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemMovement [primaryKey=").append(primaryKey)
				.append(", itemsMoved=").append(itemsMoved).append(']');
		return builder.toString();
	}
}
