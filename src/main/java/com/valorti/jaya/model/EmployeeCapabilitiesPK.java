package com.valorti.jaya.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class EmployeeCapabilitiesPK implements Serializable {
	private static final long	serialVersionUID	= -4191024078356002597L;

	@ManyToOne
	private Employee			employee;

	@ManyToOne
	private Capability			capability;

	public EmployeeCapabilitiesPK() {
	}

	public EmployeeCapabilitiesPK(Employee employee, Capability capability) {
		this.employee = employee;
		this.capability = capability;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EmployeeCapabilitiesPK)) {
			return false;
		}
		EmployeeCapabilitiesPK other = (EmployeeCapabilitiesPK) obj;
		if (capability == null) {
			if (other.capability != null) {
				return false;
			}
		} else if (!capability.equals(other.capability)) {
			return false;
		}
		if (employee == null) {
			if (other.employee != null) {
				return false;
			}
		} else if (!employee.equals(other.employee)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the capability
	 */
	public Capability getCapability() {
		return capability;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((capability == null) ? 0 : capability.hashCode());
		result = prime * result
				+ ((employee == null) ? 0 : employee.hashCode());
		return result;
	}

	/**
	 * @param capability
	 *            the capability to set
	 */
	public void setCapability(Capability capability) {
		this.capability = capability;
	}

	/**
	 * @param employee
	 *            the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeCapabilitiesPK [Employee =")
				.append(employee.getNickname()).append(", Capability =")
				.append(capability.getName()).append(']');
		return builder.toString();
	}
}