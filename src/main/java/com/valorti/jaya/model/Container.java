package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Container", uniqueConstraints = { @UniqueConstraint(name = "ContainerIdentifierU", columnNames = {
		"lotCode", "identifier" }) })
public class Container implements IEntity {
	private static final long		serialVersionUID	= 3369318059072744971L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "containerId", unique = true, nullable = false)
	private Long					id;

	@Column(name = "identifier", nullable = false)
	private Long					identifier;

	@Column(name = "lotCode", nullable = false)
	private String					lotCode;

	@Column(name = "dimensions", nullable = false)
	private String					dimensions;

	@Column(name = "modCount", nullable = true)
	private Integer					modCount;

	@Column(name = "expireDate", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date					expireDate;

	@Column(name = "itemCount", nullable = false)
	private Integer					itemCount;

	@Column(name = "currentItemLength", nullable = true)
	private Double					currentItemLength;

	@Column(name = "itemTotalLength", nullable = false)
	private Double					itemTotalLength;

	@Column(name = "perItemAvgLength", nullable = false)
	private Double					perItemAvgLength;

	@Column(name = "unitPrice", nullable = false)
	private Double					unitPrice;

	@ManyToOne
	@JoinColumn(name = "itemId", nullable = false)
	private Item					item;

	@ManyToOne
	@JoinColumn(name = "locationId", nullable = false)
	private Location				location;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.container", cascade = CascadeType.ALL)
	private Set<EmployeeContainer>	employeeContainers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.container", cascade = CascadeType.ALL)
	private Set<InventoryContainer>	inventoryContainers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.container", cascade = CascadeType.ALL)
	private Set<ItemMovement>		itemMovements;

	@Version
	protected Integer				version;

	public Container() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Container)) {
			return false;
		}
		Container other = (Container) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		if (lotCode == null) {
			if (other.lotCode != null) {
				return false;
			}
		} else if (!lotCode.equals(other.lotCode)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the currentItemLength
	 */
	public Double getCurrentItemLength() {
		return currentItemLength;
	}

	/**
	 * @return the dimensions
	 */
	public String getDimensions() {
		return dimensions;
	}

	/**
	 * @return the employeeContainers
	 */
	public Set<EmployeeContainer> getEmployeeContainers() {
		if (employeeContainers == null)
			employeeContainers = new HashSet<EmployeeContainer>();

		return employeeContainers;
	}

	/**
	 * @return the expireDate
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @return the identifier
	 */
	public Long getIdentifier() {
		return identifier;
	}

	/**
	 * @return the inventoryContainers
	 */
	public Set<InventoryContainer> getInventoryContainers() {
		if (inventoryContainers == null)
			inventoryContainers = new HashSet<InventoryContainer>();

		return inventoryContainers;
	}

	/**
	 * @return the item
	 */
	public Item getItem() {
		return item;
	}

	/**
	 * @return the itemCount
	 */
	public Integer getItemCount() {
		return itemCount;
	}

	/**
	 * @return the itemMovements
	 */
	public Set<ItemMovement> getItemMovements() {
		if (itemMovements == null)
			itemMovements = new HashSet<ItemMovement>();

		return itemMovements;
	}

	/**
	 * @return the itemTotalLength
	 */
	public Double getItemTotalLength() {
		return itemTotalLength;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @return the lotCode
	 */
	public String getLotCode() {
		return lotCode;
	}

	/**
	 * @return the modCount
	 */
	public Integer getModCount() {
		return modCount;
	}

	/**
	 * @return the perItemAvgLength
	 */
	public Double getPerItemAvgLength() {
		return perItemAvgLength;
	}

	/**
	 * @return the unitPrice
	 */
	public Double getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @return the version
	 */
	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((lotCode == null) ? 0 : lotCode.hashCode());
		return result;
	}

	/**
	 * @param currentItemLength
	 *            the currentItemLength to set
	 */
	public void setCurrentItemLength(Double currentItemLength) {
		this.currentItemLength = currentItemLength;
	}

	/**
	 * @param dimensions
	 *            the dimensions to set
	 */
	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	/**
	 * @param employeeContainers
	 *            the employeeContainers to set
	 */
	public void setEmployeeContainers(Set<EmployeeContainer> employeeContainers) {
		this.employeeContainers = employeeContainers;
	}

	/**
	 * @param expireDate
	 *            the expireDate to set
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param identifier
	 *            the identifier to set
	 */
	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	/**
	 * @param inventoryContainers
	 *            the inventoryContainers to set
	 */
	public void setInventoryContainers(
			Set<InventoryContainer> inventoryContainers) {
		this.inventoryContainers = inventoryContainers;
	}

	/**
	 * @param item
	 *            the item to set
	 */
	public void setItem(Item item) {
		this.item = item;
	}

	/**
	 * @param itemCount
	 *            the itemCount to set
	 */
	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}

	/**
	 * @param itemMovements
	 *            the itemMovements to set
	 */
	public void setItemMovements(Set<ItemMovement> itemMovements) {
		this.itemMovements = itemMovements;
	}

	/**
	 * @param itemTotalLength
	 *            the itemTotalLength to set
	 */
	public void setItemTotalLength(Double itemTotalLength) {
		this.itemTotalLength = itemTotalLength;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * @param lotCode
	 *            the lotCode to set
	 */
	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	/**
	 * @param modCount
	 *            the modCount to set
	 */
	public void setModCount(Integer modCount) {
		this.modCount = modCount;
	}

	/**
	 * @param perItemAvgLength
	 *            the perItemAvgLength to set
	 */
	public void setPerItemAvgLength(Double perItemAvgLength) {
		this.perItemAvgLength = perItemAvgLength;
	}

	/**
	 * @param unitPrice
	 *            the unitPrice to set
	 */
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Container [id=").append(id).append(", identifier=")
				.append(identifier).append(", lotCode=").append(lotCode)
				.append(", dimensions=").append(dimensions)
				.append(", expireDate=").append(expireDate)
				.append(", itemCount=").append(itemCount)
				.append(", itemTotalLength=").append(itemTotalLength)
				.append(", perItemAvgLength=").append(perItemAvgLength)
				.append(", unitPrice=").append(unitPrice).append(", item=")
				.append(item.getItemName()).append(", location=")
				.append(location.getId()).append(version).append(']');
		return builder.toString();
	}
}
