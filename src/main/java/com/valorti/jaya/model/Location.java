package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Location", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"row", "column", "depth", "warehouseId" }) })
public class Location implements IEntity {
	private static final long	serialVersionUID	= 2490159872954144967L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "locationId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "position", nullable = true)
	private String				position;

	@Column(name = "row", nullable = false)
	private String				row;

	@Column(name = "column", nullable = true)
	private String				column;

	@Column(name = "depth", nullable = true)
	private String				depth;

	@Column(name = "locationName", nullable = true)
	private String				locationName;

	@OneToMany(cascade = { CascadeType.ALL })
	@JoinColumn(name = "locationId", insertable = false, updatable = false)
	@OrderColumn(name = "index")
	private Set<Container>		containers			= new HashSet<Container>();

	@ManyToOne
	@JoinColumn(name = "warehouseId", nullable = false)
	private Warehouse			warehouse;

	@Version
	protected Integer	version;

	public Location() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Location)) {
			return false;
		}
		Location other = (Location) obj;
		if (column == null) {
			if (other.column != null) {
				return false;
			}
		} else if (!column.equals(other.column)) {
			return false;
		}
		if (depth == null) {
			if (other.depth != null) {
				return false;
			}
		} else if (!depth.equals(other.depth)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (row == null) {
			if (other.row != null) {
				return false;
			}
		} else if (!row.equals(other.row)) {
			return false;
		}
		return true;
	}

	public String getColumn() {
		return column;
	}

	public Set<Container> getContainers() {
		if (containers == null)
			containers = new HashSet<Container>();

		return containers;
	}

	public String getDepth() {
		return depth;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	public String getLocationName() {
		return locationName;
	}

	public String getPosition() {
		return position;
	}

	public String getRow() {
		return row;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	/**
	 * @return the warehouse
	 */
	public Warehouse getWarehouse() {
		return warehouse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((column == null) ? 0 : column.hashCode());
		result = prime * result + ((depth == null) ? 0 : depth.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((row == null) ? 0 : row.hashCode());
		return result;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public void setContainers(Set<Container> containers) {
		this.containers = containers;
	}

	public void setDepth(String depth) {
		this.depth = depth;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * @param warehouse
	 *            the warehouse to set
	 */
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Location [id=").append(id).append(", position=")
				.append(position).append(", row=").append(row)
				.append(", column=").append(column).append(", depth=")
				.append(depth).append(", locationName=").append(locationName)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
