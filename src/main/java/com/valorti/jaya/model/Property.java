package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Property", uniqueConstraints = { @UniqueConstraint(columnNames = "propertyName") })
public class Property implements IEntity {
	public enum DataType {
		String, Object, Integer, Float, Array, Date
	}

	private static final long	serialVersionUID	= -7264541709852149069L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "propertyId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "propertyName", nullable = false)
	private String				propertyName;

	@Column(name = "dataType", unique = false, nullable = false)
	private String				dataType;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.property", cascade = CascadeType.ALL)
	private Set<ItemProperties>	itemProperties	= new HashSet<ItemProperties>();

	@Version
	private Integer				version;

	public Property() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Property)) {
			return false;
		}
		Property other = (Property) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (propertyName == null) {
			if (other.propertyName != null) {
				return false;
			}
		} else if (!propertyName.equals(other.propertyName)) {
			return false;
		}
		return true;
	}

	public DataType getDataType() {
		return DataType.valueOf(dataType);
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	public Set<ItemProperties> getItemProperties() {
		if (itemProperties == null)
			itemProperties = new HashSet<ItemProperties>();

		return itemProperties;
	}

	public String getPropertyName() {
		return propertyName;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((propertyName == null) ? 0 : propertyName.hashCode());
		return result;
	}

	public void setDataType(DataType type) {
		this.dataType = type.toString();
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setItemProperties(Set<ItemProperties> itemProperties) {
		this.itemProperties = itemProperties;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Property [id=").append(id).append(", propertyName=")
				.append(propertyName).append(", dataType=").append(dataType)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
