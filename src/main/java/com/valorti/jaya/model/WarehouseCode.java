package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "WarehouseCode", uniqueConstraints = { @UniqueConstraint(name = "WarehouseCodeU", columnNames = "whCode") })
public class WarehouseCode implements IEntity {
	private static final long	serialVersionUID	= 5526270321816783119L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "warehouseCodeId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "whCode", nullable = false, length = 200)
	private String				whCode;

	@Column(name = "description", unique = false, nullable = true, length = 200)
	private String				description;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "warehouseCodeId")
	@OrderColumn(name = "index")
	private Set<Warehouse>		warehouses			= new HashSet<Warehouse>();

	@Version
	protected Integer	version;

	public WarehouseCode() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof WarehouseCode)) {
			return false;
		}
		WarehouseCode other = (WarehouseCode) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (whCode == null) {
			if (other.whCode != null) {
				return false;
			}
		} else if (!whCode.equals(other.whCode)) {
			return false;
		}
		return true;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	public Set<Warehouse> getWarehouses() {
		if (warehouses == null)
			warehouses = new HashSet<Warehouse>();

		return warehouses;
	}

	public String getWhCode() {
		return whCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((whCode == null) ? 0 : whCode.hashCode());
		return result;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	public void setWarehouses(Set<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}

	public void setWhCode(String whCode) {
		this.whCode = whCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WarehouseCode [id=").append(id).append(", whCode=")
				.append(whCode).append(", description=").append(description)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}