package com.valorti.jaya.model.manager;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.ItemProperties;
import com.valorti.jaya.model.Property;

public class PropertyManager extends AbstractManager {

	public PropertyManager() {
		// TODO Auto-generated constructor stub
	}

	private Property find(Criterion c) {
		return find(Property.class, c);
	}

	public Property findByName(String propertyName) {
		if (propertyName.length() > 0) {
			return find(Restrictions.eq("propertyName", propertyName));
		}

		return null;
	}

	@Override
	public void initialize(Object entity) {
		if (loadCollections) {
			Property p = (Property) entity;
			p.getItemProperties().size();
		}
	}

	public Serializable linkToItem(Item item, Property property,
			String propertyValue) {
		Serializable r = 0L;
		Session s = null;

		try {
			s = getSession();
			s.beginTransaction();
			ItemProperties ip = new ItemProperties();
			ip.setItem(item);
			ip.setProperty(property);
			ip.setPropertyValue(propertyValue);

			if (!property.getItemProperties().contains(ip)) {
				r = s.save(ip);
				s.getTransaction().commit();
			}

			ip = null;
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return r;
	}
}
