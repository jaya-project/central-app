package com.valorti.jaya.model.manager;

import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Inventory;
import com.valorti.jaya.model.InventoryContainer;

public class InventoryContainerManager extends AbstractManager {

	public InventoryContainerManager() {
	}

	public InventoryContainer findObject(Inventory inventory,
			Container container) {
		InventoryContainer ic = null;

		if (inventory != null && container != null) {
			ic = this.find(InventoryContainer.class, Restrictions.and(
					Restrictions.eq("inventory", inventory),
					Restrictions.eq("container", container)));
		}

		return ic;
	}

	@Override
	public void initialize(Object o) {
		// if(o != null) {
		// InventoryContainer ic = (InventoryContainer)o;
		// }
	}

	public boolean objectExists(Inventory inventory, Container container) {
		return findObject(inventory, container) != null;
	}

}
