package com.valorti.jaya.model.manager;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.WarehouseCode;

public class WarehouseCodeManager extends AbstractManager {

	public WarehouseCodeManager() {
		// TODO Auto-generated constructor stub
	}

	private WarehouseCode find(Criterion c) {
		return find(WarehouseCode.class, c);
	}

	public WarehouseCode findByCode(String code) {
		if (code.length() > 0) {
			return find(Restrictions.eq("whCode", code));
		}

		return null;
	}

	@Override
	public void initialize(Object entity) {
		if (loadCollections) {
			WarehouseCode w = (WarehouseCode) entity;
			w.getWarehouses().size();
		}
	}
}
