package com.valorti.jaya.model.manager;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.ItemMovement;
import com.valorti.jaya.model.Warehouse;

public class ItemMovementManager extends AbstractManager {

	public ItemMovementManager() {
	}

	private ItemMovement find(Criterion c) {
		return find(ItemMovement.class, c);
	}

	public ItemMovement findByContainerAndWarehouse(Container container,
			Warehouse warehouse) {
		ItemMovement itemMove = null;

		if (container != null && warehouse != null) {
			itemMove = find(Restrictions.and(
					Restrictions.eq("primaryKey.container", container),
					Restrictions.eq("primaryKey.warehouse", warehouse)));
		}

		return itemMove;
	}

	@Override
	public void initialize(Object o) {
	}

}
