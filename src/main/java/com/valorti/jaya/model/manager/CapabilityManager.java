package com.valorti.jaya.model.manager;

import java.io.Serializable;

import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Capability;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.EmployeeCapabilities;

public class CapabilityManager extends AbstractManager {
	public CapabilityManager() {
	}

	private Capability find(Criterion c) {
		return find(Capability.class, c);
	}

	public Capability findByName(String name) {

		if (name.length() > 0) {
			return find(Restrictions.eq("name", name));
		}

		return null;
	}

	@Override
	public void initialize(Object o) {
		if (o != null) {
			Capability c = (Capability) o;
			c.getEmployeeCapabilities().size();
		}
	}

	public Serializable linkToEmployee(Capability c, Employee e, Integer access) {
		Serializable r = null;

		if (c != null && e != null) {
			Session s = null;

			try {
				s = getSession();
				EmployeeCapabilities ec = new EmployeeCapabilities();
				ec.setAccess(access);
				ec.setCapability(c);
				ec.setEmployee(e);

				if (!c.getEmployeeCapabilities().contains(ec)) {
					s.beginTransaction();
					r = s.save(ec);
					s.getTransaction().commit();
				} else {
					log.warn("The employee capability link already exists!");
				}

				ec = null;
			} catch (NonUniqueResultException ex) {
				log.error("The result is not unique: ", ex);
			} finally {
				if (s != null)
					s.close();

				s = null;
			}
		} else {
			log.error(
					"Either the capability or the employee were null: {} - {}",
					c, e);
		}

		return r;
	}
}
