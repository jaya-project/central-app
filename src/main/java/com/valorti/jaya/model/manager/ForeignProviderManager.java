package com.valorti.jaya.model.manager;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.ForeignProvider;

public class ForeignProviderManager extends AbstractManager {

	public ForeignProviderManager() {
	}

	private ForeignProvider find(Criterion c) {
		return find(ForeignProvider.class, c);
	}

	public ForeignProvider findByCode(String code) {
		ForeignProvider provider = null;

		if (code != null && code.length() > 0) {
			return find(Restrictions.eq("code", code));
		}

		return provider;
	}

	public ForeignProvider findByName(String name) {
		ForeignProvider provider = null;

		if (name != null && name.length() > 0) {
			return find(Restrictions.eq("name", name));
		}

		return provider;
	}

	@Override
	public void initialize(Object o) {
		if (loadCollections) {
			ForeignProvider fp = (ForeignProvider) o;
			fp.getItems().size();
		}
	}

}
