package com.valorti.jaya.model.manager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Inventory;
import com.valorti.jaya.model.InventoryContainer;

public class InventoryManager extends AbstractManager {

	public InventoryManager() {
	}

	@SuppressWarnings("unchecked")
	public List<Inventory> filterByDate(Date calendar) {
		List<Inventory> filter = null;

		if (calendar != null) {
			Session s = null;
			try {
				s = getSession();
				s.beginTransaction();
				filter = s.createCriteria(Inventory.class)
						.add(Restrictions.eq("inventoryDate", calendar)).list();
				s.close();
			} finally {
				if (s != null)
					s.close();

				s = null;
			}
		}

		return filter;
	}

	private Inventory find(Criterion c) {
		return find(Inventory.class, c);
	}

	public Inventory findByDate(Date date) {
		Inventory i = null;

		if (date != null) {
			i = find(Restrictions.eq("startDate", date));
		}

		return i;
	}

	public Inventory findUnfinishedInventory() {
		Inventory inventory = null;

		try {
			inventory = find(Inventory.class, Restrictions.and(
					Restrictions.isNotNull("startDate"),
					Restrictions.isNull("endDate")));
		} catch (Exception ex) {
			log.error(
					"Failed to check if there are any unfinished inventories.",
					ex);
		}

		return inventory;
	}

	@Override
	public void initialize(Object entity) {
		if (loadCollections) {
			Inventory i = (Inventory) entity;
			i.getInventoryContainers().size();
		}
	}

	public Serializable linkToContainer(Container c, Inventory i,
			String comments, InventoryContainer.Difference diffname,
			Integer diffCount) {
		Serializable r = null;

		if (c != null && i != null) {
			Session s = null;
			try {
				s = getSession();
				s.beginTransaction();
				InventoryContainer ic = new InventoryContainer();
				ic.setComments(comments);
				ic.setContainer(c);
				ic.setDifferenceCount(diffCount);
				ic.setDifferenceName(diffname);
				ic.setInventory(i);

				if (!i.getInventoryContainers().contains(ic)) {
					r = s.save(ic);
					s.getTransaction().commit();
				}

				ic = null;
			} catch (Exception e) {
				log.error("Failed to link {} to {}: ", c, i, e);
			} finally {
				if (s != null)
					s.close();
				s = null;
			}
		}

		return r;
	}
}
