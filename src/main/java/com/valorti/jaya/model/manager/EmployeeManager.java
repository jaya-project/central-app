package com.valorti.jaya.model.manager;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Capability;
import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.EmployeeCapabilities;
import com.valorti.jaya.model.EmployeeContainer;

public class EmployeeManager extends AbstractManager {
	public EmployeeManager() {
	}

	public Serializable addSession(Long employeeId,
			com.valorti.jaya.model.Session session) {
		Serializable r = null;

		if (employeeId != null && employeeId > 0 && session != null) {
			Session s = null;

			try {
				s = getSession();
				s.beginTransaction();

				Employee employee = (Employee) s
						.get(Employee.class, employeeId);
				session.setEmployee(employee);
				employee.getSessions().add(session);

				r = s.save(session);
				s.getTransaction().commit();
			} catch (Exception ex) {
				log.error("Failed to add employee session: ", ex);
				s.getTransaction().rollback();
			} finally {
				if (s != null)
					s.close();

				s = null;
			}
		}

		return r;
	}

	private Employee find(Criterion c) {
		return find(Employee.class, c);
	}

	public Employee findByFirstAndLastName(String firstName, String lastName) {
		if (firstName.length() > 0 && lastName.length() > 0) {
			return find(Restrictions.and(
					Restrictions.eq("firstName", firstName),
					Restrictions.eq("lastName", lastName)));
		}

		return null;
	}

	public Employee findByNickname(String nickname) {
		if (nickname.length() > 0) {
			return find(Restrictions.eq("nickname", nickname));
		}

		return null;
	}

	public Employee findByRut(String rut) {
		if (rut.length() > 0) {
			return find(Restrictions.eq("rut", rut));
		}

		return null;
	}

	@Override
	public void initialize(Object entity) {
		if (loadCollections) {
			if (entity != null) {
				Employee e = (Employee) entity;
				e.getEmployeeCapabilities().size();
				e.getEmployeeContainers().size();
				e.getSessions().size();
			}
		}
	}

	public Serializable linkToCapability(Capability c, Employee e,
			Integer access) {
		Serializable r = null;
		Session s = null;

		try {
			s = getSession();
			EmployeeCapabilities ec = new EmployeeCapabilities();
			ec.setAccess(access);
			ec.setEmployee(e);
			ec.setCapability(c);

			if (!e.getEmployeeCapabilities().contains(ec)) {
				s.beginTransaction();
				r = s.save(ec);
				s.getTransaction().commit();
			} else {
				log.warn("The capability and employee are already linked!");
				r = 0;
			}

			ec = null;
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return r;
	}

	public Serializable linkToContainer(Container c, Employee e,
			Date actionDate, EmployeeContainer.Action actionName) {
		Serializable r = null;
		log.debug("Linking container {} to employee {}: {} - {}", c, e,
				actionDate, actionName);

		if (c != null && e != null && actionDate != null && actionName != null) {
			Session s = null;

			try {
				s = getSession();
				EmployeeContainer ec = new EmployeeContainer();
				ec.setContainer(c);
				ec.setEmployee(e);
				ec.setActionDate(actionDate);
				ec.setActionName(actionName);

				if (!e.getEmployeeContainers().contains(ec)) {
					s.beginTransaction();
					r = s.save(ec);
					s.getTransaction().commit();
				} else {
					log.warn("The container and employee are already linked!");
				}

				ec = null;
			} catch (Exception ex) {
				log.error("Failed to link employee to container: ", ex);
			} finally {
				if (s != null)
					s.close();

				s = null;
			}
		}

		return r;
	}
}
