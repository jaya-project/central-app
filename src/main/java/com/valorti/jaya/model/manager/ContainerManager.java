package com.valorti.jaya.model.manager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.EmployeeContainer;
import com.valorti.jaya.model.InletGuide;

public class ContainerManager extends AbstractManager {

	public ContainerManager() {
	}

	public Serializable addInletGuideAndContainer(Container container,
			InletGuide inletGuide) {
		Serializable r = null;
		Session s = null;

		try {
			s = getSession();
			s.beginTransaction();
			r = s.save(container);
			inletGuide.getContainers().add(container);
			s.saveOrUpdate(inletGuide);
			s.getTransaction().commit();
		} catch (Exception ex) {
			log.error("Failed to link Container to Inlet Guide: ", ex);
			s.getTransaction().rollback();
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return r;
	}

	private Container find(Criterion c) {
		return find(Container.class, c);
	}

	public Container findByLotCodeAndIdentifier(String lotCode, Long identifier) {
		if (lotCode != null && identifier != null) {
			return find(Restrictions.and(Restrictions.eq("lotCode", lotCode),
					Restrictions.eq("identifier", identifier)));
		}

		return null;
	}

	@Override
	public void initialize(Object o) {
		if (loadCollections) {
			Container c = (Container) o;
			c.getEmployeeContainers().size();
			c.getInventoryContainers().size();
			c.getItemMovements().size();
		}
	}

	public Serializable linkToEmployee(Container c, Employee e,
			Date actionDate, EmployeeContainer.Action actionName) {
		Serializable r = null;
		Session s = null;

		try {
			s = getSession();
			EmployeeContainer ec = new EmployeeContainer();
			ec.setContainer(c);
			ec.setEmployee(e);
			ec.setActionDate(actionDate);
			ec.setActionName(actionName);

			if (!c.getEmployeeContainers().contains(ec)) {
				s.beginTransaction();
				r = s.save(ec);
				s.getTransaction().commit();
			} else {
				log.warn("The container and employee are already linked!");
			}

			ec = null;
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return r;
	}

	public List<Container> listByItemCode(String itemCode, boolean excludeEmpty) {
		List<Container> list = null;

		if (itemCode != null && itemCode.length() > 0) {
			Session s = null;
			try {
				s = getSession();

				if (s != null) {
					Criteria c = s.createCriteria(Container.class).createAlias(
							"item", "i");

					if (excludeEmpty) {
						c.add(Restrictions.and(Restrictions.gt("itemCount", 0),
								Restrictions.eq("i.itemCode", itemCode)));
					} else {
						c.add(Restrictions.eq("i.itemCode", itemCode));
					}

					list = list(c);
				}
			} finally {
				if (s != null) {
					s.close();
				}

				s = null;
			}
		}

		return list;
	}

	public List<Container> listByItemName(String name, boolean excludeEmpty) {
		List<Container> list = null;

		if (name != null && name.length() > 0) {
			Session s = null;
			try {
				s = getSession();

				if (s != null) {
					Criteria c = s.createCriteria(Container.class);
					c.createAlias("item", "i");
					Conjunction conj = Restrictions.conjunction();

					if (excludeEmpty) {
						conj.add(Restrictions.and(Restrictions.gt("itemCount",
								0)));
					}

					conj.add(Restrictions.like("i.itemName", "%" + name + "%"));
					c.add(conj);
					list = list(c);
					conj = null;
					c = null;
				}
			} finally {
				if (s != null) {
					s.close();
				}

				s = null;
			}
		}

		return list;
	}

	public List<Container> listByLocation(char row, String column,
			String depth, boolean excludeEmpty) {
		List<Container> list = null;

		if (column != null && column.length() > 0 && depth != null
				&& depth.length() > 0) {
			Session s = null;

			try {
				s = getSession();

				if (s != null) {
					log.debug("Searching for location {} {} {}", row, column,
							depth);
					Criteria c = s.createCriteria(Container.class).createAlias(
							"location", "l");
					String tmp = String.valueOf(row).toUpperCase();

					if (excludeEmpty) {
						c.add(Restrictions.and(Restrictions.gt("itemCount", 0),
								Restrictions.eq("l.row", tmp),
								Restrictions.eq("l.column", column),
								Restrictions.eq("l.depth", depth)));
					} else {
						c.add(Restrictions.and(Restrictions.eq("l.row", tmp),
								Restrictions.eq("l.column", column),
								Restrictions.eq("l.depth", depth)));
					}

					tmp = null;
					list = list(c);
				}
			} finally {
				if (s != null) {
					s.close();
				}

				s = null;
			}
		}

		return list;
	}

	public List<Container> listByLotCode(String lotCode, boolean excludeEmpty) {
		List<Container> list = null;

		if (lotCode != null) {
			Conjunction conj = Restrictions.conjunction();
			conj.add(Restrictions.eq("lotCode", lotCode));

			if (excludeEmpty)
				conj.add(Restrictions.gt("itemCount", 0));

			list = list(Container.class, conj);
			conj = null;
		} else {
			log.warn("The lotcode {} was not found!", lotCode);
		}

		return list;
	}
}
