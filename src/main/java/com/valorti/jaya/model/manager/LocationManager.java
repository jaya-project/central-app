package com.valorti.jaya.model.manager;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Location;
import com.valorti.jaya.model.Warehouse;

public class LocationManager extends AbstractManager {

	public LocationManager() {
	}

	private Location find(Criterion c) {
		return find(Location.class, c);
	}

	public Location findByLocation(String row, String column, String depth) {
		if (row.length() > 0 && column.length() > 0 && depth.length() > 0) {
			log.debug("Row {}, Column {} and Depth {}", row, column, depth);
			return find(Restrictions.and(Restrictions.eq("row", row),
					Restrictions.eq("column", column),
					Restrictions.eq("depth", depth)));
		}

		return null;
	}

	public Location findByPosition(String position) {
		log.debug("Finding container by position {}", position);

		if (position.length() > 0) {
			return find(Restrictions.eq("position", position));
		}

		return null;
	}

	/**
	 * Retrieves a Location if it exists or creates a new one if it doesn't.
	 * 
	 * @param row
	 *            The location row.
	 * @param column
	 *            The location column.
	 * @param depth
	 *            The location depth.
	 * @param warehouse
	 *            A warehouse where the location is.
	 * @return A Location object if it exists or could be added to the database,
	 *         otherwise it will return null.
	 */
	public Location getLocation(String row, int column, int depth,
			Warehouse warehouse) {
		String c, d;

		if (column < 10)
			c = "0" + column;
		else
			c = "" + column;

		if (depth < 10)
			d = "0" + depth;
		else
			d = "" + depth;

		Location location = findByLocation(row, c, d);

		if (location == null) {
			location = new Location();
			location.setColumn(c);
			location.setRow(row);
			location.setDepth(d);
			location.setWarehouse(warehouse);

			if (add(location) == null) {
				location = null;
			}
		}

		return location;
	}

	@Override
	public void initialize(Object entity) {
		if (loadCollections) {
			Location l = (Location) entity;
			l.getContainers().size();
		}
	}
}
