package com.valorti.jaya.model.manager;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Provider;

public class ProviderManager extends AbstractManager {

	public ProviderManager() {
	}

	private Provider find(Criterion c) {
		return find(Provider.class, c);
	}

	public Provider findByCode(String code) {
		if (code.length() > 0) {
			return find(Restrictions.eq("providerCode", code));
		}

		return null;
	}

	public Provider findByName(String name) {
		if (name.length() > 0) {
			return find(Restrictions.eq("providerName", name));
		}

		return null;
	}

	@Override
	public void initialize(Object entity) {
	}
}
