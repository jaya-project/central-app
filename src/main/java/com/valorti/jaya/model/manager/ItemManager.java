package com.valorti.jaya.model.manager;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.ItemProperties;
import com.valorti.jaya.model.Property;

public class ItemManager extends AbstractManager {

	public ItemManager() {
	}

	private Item find(Criterion c) {
		return find(Item.class, c);
	}

	public Item findByCode(String itemCode) {
		if (itemCode.length() > 0) {
			return find(Restrictions.eq("itemCode", itemCode.trim()));
		}

		return null;
	}

	public Item findByName(String itemName) {
		if (itemName.length() > 0) {
			return find(Restrictions.eq("itemName", itemName));
		}

		return null;
	}

	@Override
	public void initialize(Object entity) {
		if (loadCollections) {
			Item i = (Item) entity;
			i.getContainers().size();
			i.getItemProperties().size();
		}
	}

	public Serializable linkToProperty(Item item, Property property,
			String propertyValue) {
		Serializable r = 0L;

		Session s = null;

		try {
			s = getSession();
			s.beginTransaction();
			ItemProperties ip = new ItemProperties();
			ip.setItem(item);
			ip.setProperty(property);
			ip.setPropertyValue(propertyValue);

			if (!item.getItemProperties().contains(ip)) {
				r = s.save(ip);
				s.getTransaction().commit();
			}

			ip = null;
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return r;
	}
}
