package com.valorti.jaya.model.manager;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.valorti.jaya.model.Warehouse;

public class WarehouseManager extends AbstractManager {

	public WarehouseManager() {
	}

	private Warehouse find(Criterion c) {
		return find(Warehouse.class, c);
	}

	public Warehouse findByName(String name) {
		if (name.length() > 0) {
			return find(Restrictions.eq("whName", name));
		}

		return null;
	}

	@Override
	public void initialize(Object entity) {
		if (loadCollections) {
			Warehouse w = (Warehouse) entity;
			w.getLocations().size();
		}
	}
}
