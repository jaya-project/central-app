package com.valorti.jaya.model.manager;

import java.io.Serializable;
import java.util.List;

import com.valorti.jaya.model.IEntity;

public interface IManager {

	/**
	 * Creates new entities.
	 * 
	 * @param <T>
	 * @param entities
	 *            - The list of entities to add.
	 * @return An array of @Serializable objects corresponding to the created
	 *         entity (if it was created).
	 */
	public <T> Serializable[] add(List<T> entities);

	public Serializable add(Object o);

	/**
	 * Deletes a list of entities.
	 * 
	 * @param entities
	 *            - A list of entities to delete.
	 */
	public <T> boolean delete(List<T> entities);

	public boolean delete(Object o);

	/**
	 * DO NOT USE IT, as it will always return false! Checks if the entity
	 * exists or not.
	 * 
	 * @param entity
	 *            - The entity to check.
	 * @return Returns true if the entity exists or false if it doesn't.
	 */
	public boolean exists(IEntity entity);

	/**
	 * Initializes the Object collections.
	 * 
	 * @param o
	 *            Object wich
	 */
	public void initialize(Object o);

	/**
	 * Updates a list of entities.
	 * 
	 * @param <T>
	 * @param entities
	 *            - A list of entities to update.
	 */
	public <T> boolean update(List<T> entities);

	public boolean update(Object o);
}