package com.valorti.jaya.model.manager;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.IEntity;
import com.valorti.utils.HibernateUtils;

public abstract class AbstractManager implements IManager {
	protected boolean		loadCollections	= true;

	protected final Logger	log				= LoggerFactory
													.getLogger(AbstractManager.class);

	public AbstractManager() {
	}

	/**
	 * Adds a list of entities to the database.
	 * 
	 * @param entities
	 *            The list of entities to add.
	 * 
	 * @return An array of serializable objects of the entities that were added
	 *         or null if none were added.
	 */
	@Override
	public <T> Serializable[] add(List<T> entities) {
		Serializable[] r = null;
		log.info("Adding new entities...");

		if (entities.size() > 0) {
			log.debug("Entities length: {}", entities.size());
			r = new Serializable[entities.size()];

			for (int i = 0; i < entities.size(); i++) {
				Object o = entities.get(i);
				r[i] = add(o);
			}
		}

		log.info("... finished adding entities.");
		return r;
	}

	/**
	 * Adds an entity to the database.
	 * 
	 * @param o
	 *            The entity to add.
	 * 
	 * @return A serializable object if the entity was added or null if failed
	 *         to be added.
	 */
	@Override
	public Serializable add(Object o) {
		Serializable r = null;
		Session s = null;

		try {
			s = getSession();
			s.beginTransaction();
			r = s.save(o);

			if (o != null)
				initialize(o);

			s.getTransaction().commit();
		} catch (Exception e) {
			log.warn("Failed to add entity {}: ", o.toString(), e);
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return r;
	}

	/**
	 * Deletes a list of entities. A successive call to delete(entity).
	 * 
	 * @param entities
	 *            A list of entities to delete.
	 * 
	 * @return True if all entities were deleted, false otherwise.
	 */
	@Override
	public <T> boolean delete(List<T> entities) {
		log.info("Deleting entities...");
		boolean updated = true;

		if (entities.size() > 0) {
			log.debug("Entities size: {}", entities.size());
			Iterator<T> i = entities.iterator();

			while (i.hasNext()) {
				updated &= delete(i.next());
			}
		}

		log.info("... finished deleting entities.");

		return updated;
	}

	/**
	 * Deletes the specified entity.
	 * 
	 * @param entity
	 *            The entity to delete.
	 * 
	 * @return true if the entity was deleted or false if not.
	 */
	@Override
	public boolean delete(Object entity) {
		Session s = null;
		boolean updated = true;
		try {
			s = getSession();
			s.beginTransaction();
			s.delete(entity);
			s.getTransaction().commit();
		} catch (Exception e) {
			log.warn(
					"The entity {} does not exist, so it could not be deleted.",
					entity.toString(), e);
			updated = false;
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return updated;
	}

	/**
	 * Checks wether the entity exists or not.
	 * 
	 * @param entity
	 *            The entity to check its existence.
	 * 
	 * @return true if it exists or false if it doesn't.
	 */
	@Override
	public boolean exists(IEntity entity) {
		Object o = null;
		Session s = null;

		try {
			s = getSession();
			o = s.load(entity.getClass(), entity.getId());

		} catch (Exception e) {
			log.error("Failed to check existence: ", e);
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return o != null;
	}

	/**
	 * 
	 * @param clazz
	 * @param restrictions
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> E find(Class<E> clazz, Criterion restrictions) {
		E o = null;
		Session s = null;

		try {
			s = getSession();
			o = (E) s.createCriteria(clazz).add(restrictions).uniqueResult();

			if (o != null)
				initialize(o);
		} catch (NonUniqueResultException ex) {
			log.error("The result is not unique: ", ex);
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return o;
	}

	@SuppressWarnings("unchecked")
	public <E> E find(Class<E> clazz, DetachedCriteria dc) {
		E out = null;
		Session s = null;

		try {
			s = getSession();

			if (s != null) {
				out = (E) dc.getExecutableCriteria(s).uniqueResult();
			}
		} catch (NonUniqueResultException ex) {
			log.error("The result is not unique: ", ex);
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return out;
	}

	/**
	 * Returns an entity by the id.
	 * 
	 * @param clazz
	 *            - The class to entity to get.
	 * @param id
	 *            - Id to the entity.
	 * @return An object that represents the entity.
	 */
	public Object getById(Class<?> clazz, Long id) {
		Session s = null;
		Object o = null;

		try {
			s = getSession();
			o = s.get(clazz, id);

			if (o != null)
				initialize(o);

		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return o;
	}

	/**
	 * Obtains an open hibernate session.
	 * 
	 * @return An hibernate session.
	 */
	protected Session getSession() {
		return HibernateUtils.getSessionFactory().openSession();
	}

	/**
	 * @return the loadCollections
	 */
	public boolean isLoadCollections() {
		return loadCollections;
	}

	/**
	 * 
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> List<E> list(Class<E> clazz) {
		Session s = null;
		List<E> list = null;

		try {
			s = getSession();
			list = s.createCriteria(clazz).list();
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return list;
	}

	@SuppressWarnings("unchecked")
	public <E> List<E> list(Class<E> clazz, Criterion criterion) {
		List<E> list = null;
		Session s = null;

		try {
			s = getSession();

			if (criterion == null) {
				list = s.createCriteria(clazz).list();
			} else {
				list = s.createCriteria(clazz).add(criterion).list();
			}
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return list;
	}

	@SuppressWarnings("unchecked")
	public <E> List<E> list(Class<E> clazz, DetachedCriteria dc) {
		List<E> list = null;
		Session s = null;

		try {
			s = getSession();

			if (s != null) {
				list = dc.getExecutableCriteria(s).list();
			}
		} catch (NonUniqueResultException ex) {
			log.error("The result is not unique: ", ex);
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return list;
	}

	/**
	 * 
	 * @param clazz
	 * @param projection
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> List<E> list(Class<E> clazz, Projection projection,
			Criterion criterion) {
		List<E> list = null;
		Session s = null;

		if (projection != null) {

			try {
				s = getSession();

				if (s != null) {
					Criteria criteria = s.createCriteria(clazz);

					if (criterion != null)
						criteria.add(criterion);

					if (projection != null)
						criteria.setProjection(projection);

					list = criteria.list();
				}
			} finally {
				if (s != null)
					s.close();

				s = null;
			}
		}

		return list;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> List<E> list(Criteria criteria) {
		if (criteria != null) {
			return criteria.list();
		}

		return null;
	}

	public Object project(Class<?> clazz, Projection projection,
			Criterion criterion) {
		Object o = null;
		Session s = null;

		try {
			s = getSession();

			if (s != null) {
				o = s.createCriteria(clazz).add(criterion)
						.setProjection(projection).uniqueResult();
			}
		} catch (Exception ex) {
			log.error("Projection failed.", ex);
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return o;
	}

	/**
	 * @param loadCollections
	 *            the loadCollections to set
	 */
	public void setLoadCollections(boolean loadCollections) {
		this.loadCollections = loadCollections;
	}

	/**
	 * Updates a list of entities.
	 * 
	 * @param entities
	 *            the list of entities to update.
	 * 
	 * @return true if all entities were updated, false otherwise.
	 */
	@Override
	public <T> boolean update(List<T> entities) {
		log.info("Updating entities...");
		boolean updated = true;

		if (entities.size() > 0) {
			log.debug("Entities size: {}", entities.size());
			Iterator<T> i = entities.iterator();

			while (i.hasNext()) {
				updated &= update(i.next());
			}
		}

		log.info("... finished updating entities.");

		return updated;
	}

	/**
	 * Updates an entity.
	 * 
	 * @param o
	 *            the entity to update.
	 * 
	 * @return true if the entity was updated or false if not.
	 */
	@Override
	public boolean update(Object o) {
		Session s = null;
		boolean updated = true;

		try {
			s = getSession();
			s.beginTransaction();
			s.update(o);

			if (o != null)
				initialize(o);

			s.getTransaction().commit();
		} catch (Exception e) {
			log.warn("Failed to update entity {}: ", o.toString(), e);
			updated = false;
		} finally {
			if (s != null)
				s.close();

			s = null;
		}

		return updated;
	}
}