package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Provider", uniqueConstraints = { @UniqueConstraint(name = "ProviderCodeNameU", columnNames = {
		"providerName", "providerCode" }) })
public class Provider implements IEntity {
	private static final long	serialVersionUID	= -6912500019468920461L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "providerId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "providerName", unique = true, nullable = false)
	private String				providerName;

	@Column(name = "providerCode", unique = true, nullable = false)
	private String				providerCode;

	@Version
	private Integer				version;

	public Provider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Provider)) {
			return false;
		}
		Provider other = (Provider) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (providerCode == null) {
			if (other.providerCode != null) {
				return false;
			}
		} else if (!providerCode.equals(other.providerCode)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public String getProviderName() {
		return providerName;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((providerCode == null) ? 0 : providerCode.hashCode());
		return result;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Provider [id=").append(id).append(", providerName=")
				.append(providerName).append(", providerCode=")
				.append(providerCode).append(", version=").append(version)
				.append(']');
		return builder.toString();
	}
}
