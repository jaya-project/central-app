package com.valorti.jaya.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

@Entity
@Table(name = "InventoryContainer")
@AssociationOverrides({
		@AssociationOverride(name = "primaryKey.inventory", joinColumns = @JoinColumn(name = "inventoryId")),
		@AssociationOverride(name = "primaryKey.container", joinColumns = @JoinColumn(name = "containerId")) })
public class InventoryContainer {
	public enum Difference {
		None, Surplus, Ullage
	}

	@EmbeddedId
	private InventoryContainerPK	primaryKey;

	@Column(name = "differenceCount")
	private Integer					differenceCount;

	@Column(name = "differenceName")
	private String					differenceName;

	@Column(name = "comments")
	private String					comments;

	@Version
	private Integer					version;

	public InventoryContainer() {
		primaryKey = new InventoryContainerPK();
		this.differenceCount = 0;
		this.differenceName = Difference.None.toString();
		this.comments = "";
	}

	public InventoryContainer(Inventory inventory, Container container,
			Integer differenceCount, Difference difference, String comments) {
		primaryKey = new InventoryContainerPK(inventory, container);
		this.differenceCount = differenceCount;
		this.differenceName = difference.toString();
		this.comments = comments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof InventoryContainer)) {
			return false;
		}
		InventoryContainer other = (InventoryContainer) obj;
		if (primaryKey == null) {
			if (other.primaryKey != null) {
				return false;
			}
		} else if (!primaryKey.equals(other.primaryKey)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	@Transient
	public Container getContainer() {
		return getPrimaryKey().getContainer();
	}

	/**
	 * @return the differenceCount
	 */
	public Integer getDifferenceCount() {
		return differenceCount;
	}

	/**
	 * @return the differenceName
	 */
	public Difference getDifferenceName() {
		return Difference.valueOf(differenceName);
	}

	@Transient
	public Inventory getInventory() {
		return getPrimaryKey().getInventory();
	}

	/**
	 * @return the primaryKey
	 */
	public InventoryContainerPK getPrimaryKey() {
		if (primaryKey == null)
			primaryKey = new InventoryContainerPK();

		return primaryKey;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((primaryKey == null) ? 0 : primaryKey.hashCode());
		return result;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setContainer(Container container) {
		getPrimaryKey().setContainer(container);
	}

	/**
	 * @param differenceCount
	 *            the differenceCount to set
	 */
	public void setDifferenceCount(Integer differenceCount) {
		this.differenceCount = differenceCount;
	}

	/**
	 * @param differenceName
	 *            the differenceName to set
	 */
	public void setDifferenceName(Difference differenceName) {
		this.differenceName = differenceName.toString();
	}

	public void setInventory(Inventory inventory) {
		getPrimaryKey().setInventory(inventory);
	}

	/**
	 * @param primaryKey
	 *            the primaryKey to set
	 */
	public void setPrimaryKey(InventoryContainerPK primaryKey) {
		this.primaryKey = primaryKey;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InventoryContainer [primaryKey=").append(primaryKey)
				.append(", differenceCount=").append(differenceCount)
				.append(", differenceName=").append(differenceName)
				.append(", comments=").append(comments).append(", version=")
				.append(version).append(']');
		return builder.toString();
	}
}
