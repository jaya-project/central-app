package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "Warehouse")
public class Warehouse implements IEntity {
	private static final long	serialVersionUID	= 931745459735177733L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "warehouseId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "whName", nullable = false, length = 100)
	private String				whName				= "";

	@Column(name = "whSize", unique = false, nullable = true, length = 100)
	private String				whSize				= "";

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "warehouseId")
	@OrderColumn(name = "index")
	private Set<Location>		locations			= new HashSet<Location>();

	@ManyToOne
	@JoinColumn(name = "warehouseCodeId", nullable = false)
	private WarehouseCode		warehouseCode		= null;

	@Version
	protected Integer	version;

	public Warehouse() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Warehouse)) {
			return false;
		}
		Warehouse other = (Warehouse) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (warehouseCode == null) {
			if (other.warehouseCode != null) {
				return false;
			}
		} else if (!warehouseCode.equals(other.warehouseCode)) {
			return false;
		}
		if (whName == null) {
			if (other.whName != null) {
				return false;
			}
		} else if (!whName.equals(other.whName)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	public Set<Location> getLocations() {
		if (locations == null)
			locations = new HashSet<Location>();

		return locations;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	public WarehouseCode getWarehouseCode() {
		return warehouseCode;
	}

	public String getWhName() {
		return whName;
	}

	public String getWhSize() {
		return whSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((warehouseCode == null) ? 0 : warehouseCode.hashCode());
		result = prime * result + ((whName == null) ? 0 : whName.hashCode());
		return result;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setLocations(Set<Location> locations) {
		this.locations = locations;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	public void setWarehouseCode(WarehouseCode warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public void setWhName(String whName) {
		this.whName = whName;
	}

	public void setWhSize(String whSize) {
		this.whSize = whSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Warehouse [id=").append(id).append(", whName=")
				.append(whName).append(", whSize=").append(whSize)
				.append(", warehouseCode=").append(warehouseCode)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
