package com.valorti.jaya.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "EmployeeCapabilities")
@AssociationOverrides({
		@AssociationOverride(name = "primaryKey.employee", joinColumns = @JoinColumn(name = "employeeId")),
		@AssociationOverride(name = "primaryKey.capability", joinColumns = @JoinColumn(name = "capabilityId")) })
public class EmployeeCapabilities {

	@EmbeddedId
	private EmployeeCapabilitiesPK	primaryKey;

	@Column(name = "access", nullable = false)
	private Integer					access;

	public EmployeeCapabilities() {
		this.primaryKey = new EmployeeCapabilitiesPK();
	}

	public EmployeeCapabilities(Employee employee, Capability capability,
			Integer access) {
		this.primaryKey = new EmployeeCapabilitiesPK(employee, capability);
		this.access = access;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EmployeeCapabilities)) {
			return false;
		}
		EmployeeCapabilities other = (EmployeeCapabilities) obj;
		if (primaryKey == null) {
			if (other.primaryKey != null) {
				return false;
			}
		} else if (!primaryKey.equals(other.primaryKey)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the access
	 */
	public Integer getAccess() {
		return access;
	}

	@Transient
	public Capability getCapability() {
		return getPrimaryKey().getCapability();
	}

	@Transient
	public Employee getEmployee() {
		return getPrimaryKey().getEmployee();
	}

	/**
	 * @return the primaryKey
	 */
	public EmployeeCapabilitiesPK getPrimaryKey() {
		if (primaryKey == null)
			primaryKey = new EmployeeCapabilitiesPK();

		return primaryKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((primaryKey == null) ? 0 : primaryKey.hashCode());
		return result;
	}

	/**
	 * @param access
	 *            the access to set
	 */
	public void setAccess(Integer access) {
		this.access = access;
	}

	public void setCapability(Capability capability) {
		getPrimaryKey().setCapability(capability);
	}

	public void setEmployee(Employee employee) {
		getPrimaryKey().setEmployee(employee);
	}

	/**
	 * @param primaryKey
	 *            the primaryKey to set
	 */
	public void setPrimaryKey(EmployeeCapabilitiesPK primaryKey) {
		this.primaryKey = primaryKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeCapabilities [Primary Key=").append(primaryKey)
				.append(", access=").append(access);
		return builder.toString();
	}
}
