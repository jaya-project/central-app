package com.valorti.jaya.model;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

@Entity
@Table(name = "ItemProperties")
@AssociationOverrides({
		@AssociationOverride(name = "primaryKey.item", joinColumns = @JoinColumn(name = "itemId")),
		@AssociationOverride(name = "primaryKey.property", joinColumns = @JoinColumn(name = "propertyId")) })
public class ItemProperties implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private ItemPropertiesPK	primaryKey;

	@Column(name = "propertyValue", nullable = false)
	private String				propertyValue;

	@Version
	protected Integer			version;

	public ItemProperties() {
		primaryKey = new ItemPropertiesPK();
	}

	public ItemProperties(Item item, Property property, String propertyValue) {
		primaryKey = new ItemPropertiesPK(item, property);
		this.propertyValue = propertyValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ItemProperties)) {
			return false;
		}
		ItemProperties other = (ItemProperties) obj;
		if (primaryKey == null) {
			if (other.primaryKey != null) {
				return false;
			}
		} else if (!primaryKey.equals(other.primaryKey)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @return The item ID.
	 */
	@Transient
	public Item getItem() {
		return getPrimaryKey().getItem();
	}

	/**
	 * @return the primaryKey
	 */
	public ItemPropertiesPK getPrimaryKey() {
		if (primaryKey == null)
			primaryKey = new ItemPropertiesPK();

		return primaryKey;
	}

	/**
	 * 
	 * @return The property ID
	 */
	@Transient
	public Property getProperty() {
		return getPrimaryKey().getProperty();
	}

	/**
	 * @return the propertyValue
	 */
	public String getPropertyValue() {
		return propertyValue;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((primaryKey == null) ? 0 : primaryKey.hashCode());
		return result;
	}

	/**
	 * 
	 * @param itemId
	 *            - The item ID to set.
	 */
	public void setItem(Item item) {
		getPrimaryKey().setItem(item);
	}

	/**
	 * @param primaryKey
	 *            the primaryKey to set
	 */
	public void setPrimaryKey(ItemPropertiesPK primaryKey) {
		this.primaryKey = primaryKey;
	}

	/**
	 * 
	 * @param propertyId
	 *            - The property ID to set.
	 */
	public void setProperty(Property property) {
		getPrimaryKey().setProperty(property);
	}

	/**
	 * @param propertyValue
	 *            the propertyValue to set
	 */
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemProperties [primaryKey=").append(primaryKey)
				.append(", propertyValue=").append(propertyValue)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
