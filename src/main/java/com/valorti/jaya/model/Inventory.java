package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "Inventory")
public class Inventory implements IEntity {
	private static final long		serialVersionUID	= -1950318980813752939L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "inventoryId", unique = true, nullable = false)
	private Long					id;

	@Column(name = "startDate", nullable = false)
	private Date					startDate;

	@Column(name = "endDate", nullable = true)
	private Date					endDate;

	@Column(name = "dataUpdated", nullable = true)
	private Byte					dataUpdated;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.inventory", cascade = CascadeType.ALL)
	private Set<InventoryContainer>	inventoryContainers;

	@Version
	private Integer					version;

	public Inventory() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Inventory)) {
			return false;
		}
		Inventory other = (Inventory) obj;
		if (endDate == null) {
			if (other.endDate != null) {
				return false;
			}
		} else if (!endDate.equals(other.endDate)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (startDate == null) {
			if (other.startDate != null) {
				return false;
			}
		} else if (!startDate.equals(other.startDate)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the dataUpdated
	 */
	public Byte getDataUpdated() {
		return dataUpdated;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @return the inventoryContainers
	 */
	public Set<InventoryContainer> getInventoryContainers() {
		if (inventoryContainers == null)
			inventoryContainers = new HashSet<InventoryContainer>();

		return inventoryContainers;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	/**
	 * @param dataUpdated
	 *            the dataUpdated to set
	 */
	public void setDataUpdated(Byte dataUpdated) {
		this.dataUpdated = dataUpdated;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param inventoryContainers
	 *            the inventoryContainers to set
	 */
	public void setInventoryContainers(
			Set<InventoryContainer> inventoryContainers) {
		this.inventoryContainers = inventoryContainers;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Inventory [id=").append(id).append(", inventoryDate=")
				.append(startDate).append(", version=").append(version)
				.append(']');
		return builder.toString();
	}
}