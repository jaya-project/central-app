package com.valorti.jaya.model;

import java.io.Serializable;

import javax.persistence.ManyToOne;

public class ItemMovementPK implements Serializable {
	private static final long	serialVersionUID	= -7402414141065733135L;

	@ManyToOne
	private Container			container;

	@ManyToOne
	private Warehouse			warehouse;

	public ItemMovementPK() {
	}

	public ItemMovementPK(Container container, Warehouse warehouse) {
		this.warehouse = warehouse;
		this.container = container;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ItemMovementPK)) {
			return false;
		}
		ItemMovementPK other = (ItemMovementPK) obj;
		if (container == null) {
			if (other.container != null) {
				return false;
			}
		} else if (!container.equals(other.container)) {
			return false;
		}
		if (warehouse == null) {
			if (other.warehouse != null) {
				return false;
			}
		} else if (!warehouse.equals(other.warehouse)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the container
	 */
	public Container getContainer() {
		return container;
	}

	/**
	 * @return the warehouse
	 */
	public Warehouse getWarehouse() {
		return warehouse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((container == null) ? 0 : container.hashCode());
		result = prime * result
				+ ((warehouse == null) ? 0 : warehouse.hashCode());
		return result;
	}

	/**
	 * @param container
	 *            the container to set
	 */
	public void setContainer(Container container) {
		this.container = container;
	}

	/**
	 * @param warehouse
	 *            the warehouse to set
	 */
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemMovementPK [container=")
				.append(container.getLotCode()).append(" - ")
				.append(container.getIdentifier()).append(", warehouse=")
				.append(warehouse.getWhName()).append(']');
		return builder.toString();
	}
}
