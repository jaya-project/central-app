package com.valorti.jaya.model;

import java.io.Serializable;

public interface IEntity extends Serializable {
	/**
	 * @return the id.
	 */
	public Long getId();

	/**
	 * 
	 * @return The version.
	 */
	public Integer getVersion();

	/**
	 * @param id
	 *            the id to set.
	 */
	public void setId(Long id);

	/**
	 * @param version
	 *            The version to set.
	 */
	public void setVersion(Integer version);
}
