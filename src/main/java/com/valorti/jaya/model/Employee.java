package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Employee", uniqueConstraints = {
		@UniqueConstraint(name = "EmployeeNameU", columnNames = { "firstName",
				"lastName" }),
		@UniqueConstraint(name = "EmployeeNicknameU", columnNames = "nickname") })
public class Employee implements IEntity {
	private static final long			serialVersionUID	= -508970772935318534L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "employeeId", unique = true, nullable = false)
	private Long						id;

	@Column(name = "firstName", nullable = false)
	private String						firstName;

	@Column(name = "lastName", nullable = false)
	private String						lastName;

	@Column(name = "nickname", nullable = false)
	private String						nickname;

	@Column(name = "rut", unique = false, nullable = true)
	private String						rut;

	@Column(name = "salt", unique = false, nullable = false)
	private byte[]						salt;

	@Column(name = "password", unique = false, nullable = false)
	private byte[]						password;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.employee", cascade = CascadeType.ALL)
	private Set<EmployeeContainer>		employeeContainers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.employee", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<EmployeeCapabilities>	employeeCapabilities;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "employeeId", insertable = false, updatable = false)
	@OrderColumn(name = "index")
	private Set<Session>				sessions			= new HashSet<Session>();

	@Version
	protected Integer					version;

	public Employee() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Employee)) {
			return false;
		}
		Employee other = (Employee) obj;
		if (firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (nickname == null) {
			if (other.nickname != null) {
				return false;
			}
		} else if (!nickname.equals(other.nickname)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the employeeCapabilities
	 */
	public Set<EmployeeCapabilities> getEmployeeCapabilities() {
		if (employeeCapabilities == null)
			employeeCapabilities = new HashSet<EmployeeCapabilities>();

		return employeeCapabilities;
	}

	/**
	 * @return the employeeContainers
	 */
	public Set<EmployeeContainer> getEmployeeContainers() {
		if (employeeContainers == null)
			employeeContainers = new HashSet<EmployeeContainer>();

		return employeeContainers;
	}

	/**
	 * @return the firsName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @return the password
	 */
	public byte[] getPassword() {
		return password;
	}

	/**
	 * @return the rut
	 */
	public String getRut() {
		return rut;
	}

	/**
	 * @return the salt
	 */
	public byte[] getSalt() {
		return salt;
	}

	/**
	 * @return the sessions
	 */
	public Set<Session> getSessions() {
		if (sessions == null)
			sessions = new HashSet<Session>();

		return sessions;
	}

	/**
	 * @return the version
	 */
	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((nickname == null) ? 0 : nickname.hashCode());
		return result;
	}

	/**
	 * @param employeeCapabilities
	 *            the employeeCapabilities to set
	 */
	public void setEmployeeCapabilities(
			Set<EmployeeCapabilities> employeeCapabilities) {
		this.employeeCapabilities = employeeCapabilities;
	}

	/**
	 * @param employeeContainers
	 *            the employeeContainers to set
	 */
	public void setEmployeeContainers(Set<EmployeeContainer> employeeContainers) {
		this.employeeContainers = employeeContainers;
	}

	/**
	 * @param firsName
	 *            the firsName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param nickname
	 *            the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(byte[] password) {
		this.password = password;
	}

	/**
	 * @param rut
	 *            the rut to set
	 */
	public void setRut(String rut) {
		this.rut = rut;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	/**
	 * @param sessions
	 *            the sessions to set
	 */
	public void setSessions(Set<Session> sessions) {
		this.sessions = sessions;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employee [id=").append(id).append(", firstName=")
				.append(firstName).append(", lastName=").append(lastName)
				.append(", nickname=").append(nickname).append(", rut=")
				.append(rut).append(", version=").append(version).append(']');
		return builder.toString();
	}
}
