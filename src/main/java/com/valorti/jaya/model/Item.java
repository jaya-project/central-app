package com.valorti.jaya.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "Item", uniqueConstraints = { @UniqueConstraint(name = "ItemNameCodeU", columnNames = {
		"itemName", "itemCode" }) })
public class Item implements IEntity {
	private static final long	serialVersionUID	= 841333116033120096L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "itemId", unique = true, nullable = false)
	private Long				id;

	@Column(name = "itemName", unique = true, nullable = false)
	private String				itemName;

	// This one is the fucking SOFTLAND CODE.
	@Column(name = "itemCode", unique = true, nullable = false)
	private String				itemCode;

	@OneToMany(cascade = { CascadeType.ALL })
	@JoinColumn(name = "itemId")
	@OrderColumn(name = "index")
	private Set<Container>		containers			= new HashSet<Container>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.item", cascade = CascadeType.ALL)
	private Set<ItemProperties>	itemProperties;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "foreignProviderId", nullable = true)
	private ForeignProvider		foreignProvider;

	@Version
	private Integer				version;

	public Item() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Item)) {
			return false;
		}
		Item other = (Item) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (itemCode == null) {
			if (other.itemCode != null) {
				return false;
			}
		} else if (!itemCode.equals(other.itemCode)) {
			return false;
		}
		if (itemName == null) {
			if (other.itemName != null) {
				return false;
			}
		} else if (!itemName.equals(other.itemName)) {
			return false;
		}
		return true;
	}

	public Set<Container> getContainers() {
		if (containers == null)
			containers = new HashSet<Container>();

		return containers;
	}

	/**
	 * @return the foreignProvider
	 */
	public ForeignProvider getForeignProvider() {
		return foreignProvider;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public Set<ItemProperties> getItemProperties() {
		if (itemProperties == null)
			itemProperties = new HashSet<ItemProperties>();

		return itemProperties;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((itemCode == null) ? 0 : itemCode.hashCode());
		result = prime * result
				+ ((itemName == null) ? 0 : itemName.hashCode());
		return result;
	}

	public void setContainers(Set<Container> containers) {
		this.containers = containers;
	}

	/**
	 * @param foreignProvider
	 *            the foreignProvider to set
	 */
	public void setForeignProvider(ForeignProvider foreignProvider) {
		this.foreignProvider = foreignProvider;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public void setItemProperties(Set<ItemProperties> itemProperties) {
		this.itemProperties = itemProperties;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Item [id=").append(id).append(", itemName=")
				.append(itemName).append(", itemCode=").append(itemCode)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
