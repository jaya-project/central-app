package com.valorti.jaya.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;

public class InletGuide {
	private Long			id;

	private String			lotCode	= "";

	private Long			folioNumber;

	private Date			generationDate;

	private String			whEntryConcept;

	private String			costCenterCode;

	private String			sourceWarehouseCode;

	private Long			eWaybillNumber;

	private Date			eWaybillDate;

	private Long			eBillNumber;

	private Character		eBillSubtype;

	private Date			eBillDate;

	private Long			iWorkOrderNumber;

	private Long			iProductionOrderNumber;

	private Long			iPurchaseOrderNumber;

	private Long			iBillNumber;

	private Long			iCreditNoteNumber;

	private String			postingCostCenterCode;

	private Date			purchaseDate;

	private String			serialNumber;

	private String			intakeAccount;

	private Long			billType;

	private Long			purchaseTotal;

	private Set<Container>	containers;

	private Provider		provider;

	public InletGuide() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InletGuide other = (InletGuide) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getBillType() {
		return billType;
	}

	/**
	 * @return the containers
	 */
	public Set<Container> getContainers() {
		if (containers == null)
			containers = new HashSet<Container>();

		return containers;
	}

	@Column(name = "costCenterCode", unique = false, nullable = false)
	public String getCostCenterCode() {
		return costCenterCode;
	}

	public Date geteBillDate() {
		return eBillDate;
	}

	public Long geteBillNumber() {
		return eBillNumber;
	}

	public Character geteBillSubtype() {
		return eBillSubtype;
	}

	public Date geteWaybillDate() {
		return eWaybillDate;
	}

	public Long geteWaybillNumber() {
		return eWaybillNumber;
	}

	@Column(name = "folioNumber", unique = false, nullable = false)
	public Long getFolioNumber() {
		return folioNumber;
	}

	@Column(name = "generationDate", unique = false, nullable = false)
	public Date getGenerationDate() {
		return generationDate;
	}

	public Long getiBillNumber() {
		return iBillNumber;
	}

	public Long getiCreditNoteNumber() {
		return iCreditNoteNumber;
	}

	public String getIntakeAccount() {
		return intakeAccount;
	}

	public Long getiProductionOrderNumber() {
		return iProductionOrderNumber;
	}

	public Long getiPurchaseOrderNumber() {
		return iPurchaseOrderNumber;
	}

	public Long getiWorkOrderNumber() {
		return iWorkOrderNumber;
	}

	/**
	 * @return the lotCode
	 */
	public String getLotCode() {
		return lotCode;
	}

	public String getPostingCostCenterCode() {
		return postingCostCenterCode;
	}

	/**
	 * @return the provider
	 */
	public Provider getProvider() {
		return provider;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public Long getPurchaseTotal() {
		return purchaseTotal;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public String getSourceWarehouseCode() {
		return sourceWarehouseCode;
	}

	@Column(name = "whEntryConcept", unique = false, nullable = false)
	public String getWhEntryConcept() {
		return whEntryConcept;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public void setBillType(Long billType) {
		this.billType = billType;
	}

	/**
	 * @param containers
	 *            the containers to set
	 */
	public void setContainers(Set<Container> containers) {
		this.containers = containers;
	}

	public void setCostCenterCode(String costCenterCode) {
		this.costCenterCode = costCenterCode;
	}

	public void seteBillDate(Date eBillDate) {
		this.eBillDate = eBillDate;
	}

	public void seteBillNumber(Long eBillNumber) {
		this.eBillNumber = eBillNumber;
	}

	public void seteBillSubtype(Character eBillSubtype) {
		this.eBillSubtype = eBillSubtype;
	}

	public void seteWaybillDate(Date eWaybillDate) {
		this.eWaybillDate = eWaybillDate;
	}

	public void seteWaybillNumber(Long eWaybillNumber) {
		this.eWaybillNumber = eWaybillNumber;
	}

	public void setFolioNumber(Long folioNumber) {
		this.folioNumber = folioNumber;
	}

	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}

	public void setiBillNumber(Long iBillNumber) {
		this.iBillNumber = iBillNumber;
	}

	public void setiCreditNoteNumber(Long iCreditNoteNumber) {
		this.iCreditNoteNumber = iCreditNoteNumber;
	}

	public void setIntakeAccount(String intakeAccount) {
		this.intakeAccount = intakeAccount;
	}

	public void setiProductionOrderNumber(Long iProductionOrderNumber) {
		this.iProductionOrderNumber = iProductionOrderNumber;
	}

	public void setiPurchaseOrderNumber(Long iPurchaseOrderNumber) {
		this.iPurchaseOrderNumber = iPurchaseOrderNumber;
	}

	public void setiWorkOrderNumber(Long iWorkOrderNumber) {
		this.iWorkOrderNumber = iWorkOrderNumber;
	}

	/**
	 * @param lotCode
	 *            the lotCode to set
	 */
	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public void setPostingCostCenterCode(String postingCostCenterCode) {
		this.postingCostCenterCode = postingCostCenterCode;
	}

	/**
	 * @param provider
	 *            the provider to set
	 */
	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public void setPurchaseTotal(Long purchaseTotal) {
		this.purchaseTotal = purchaseTotal;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setSourceWarehouseCode(String sourceWarehouseCode) {
		this.sourceWarehouseCode = sourceWarehouseCode;
	}

	public void setWhEntryConcept(String whEntryConcept) {
		this.whEntryConcept = whEntryConcept;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InletGuide [id=").append(id).append(", lotCode=")
				.append(lotCode).append(", folioNumber=").append(folioNumber)
				.append(", generationDate=").append(generationDate)
				.append(", whEntryConcept=").append(whEntryConcept)
				.append(", costCenterCode=").append(costCenterCode)
				.append(", sourceWarehouseCode=").append(sourceWarehouseCode)
				.append(", eWaybillNumber=").append(eWaybillNumber)
				.append(", eWaybillDate=").append(eWaybillDate)
				.append(", eBillNumber=").append(eBillNumber)
				.append(", eBillSubtype=").append(eBillSubtype)
				.append(", eBillDate=").append(eBillDate)
				.append(", iWorkOrderNumber=").append(iWorkOrderNumber)
				.append(", iProductionOrderNumber=")
				.append(iProductionOrderNumber)
				.append(", iPurchaseOrderNumber=").append(iPurchaseOrderNumber)
				.append(", iBillNumber=").append(iBillNumber)
				.append(", iCreditNoteNumber=").append(iCreditNoteNumber)
				.append(", postingCostCenterCode=")
				.append(postingCostCenterCode).append(", purchaseDate=")
				.append(purchaseDate).append(", serialNumber=")
				.append(serialNumber).append(", intakeAccount=")
				.append(intakeAccount).append(", billType=").append(billType)
				.append(", purchaseTotal=").append(purchaseTotal)
				.append(", provider=").append(provider.getProviderCode())
				.append(']');
		return builder.toString();
	}
}
