package com.valorti.jaya.model;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

@Entity
@Table(name = "EmployeeContainer")
@AssociationOverrides({
		@AssociationOverride(name = "primaryKey.container", joinColumns = @JoinColumn(name = "containerId")),
		@AssociationOverride(name = "primaryKey.employee", joinColumns = @JoinColumn(name = "employeeId")) })
public class EmployeeContainer {
	public enum Action {
		Entry, Modify, Move, Inventory
	}

	@EmbeddedId
	private EmployeeContainerPK	primaryKey;

	@Column(name = "actionDate", unique = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date				actionDate;

	@Column(name = "actionName", unique = false, nullable = false)
	private String				actionName;

	@Version
	protected Integer			version;

	public EmployeeContainer() {
		primaryKey = new EmployeeContainerPK();
	}

	public EmployeeContainer(Employee employee, Container container,
			Date actionDate, String actionName) {
		primaryKey = new EmployeeContainerPK(employee, container);
		this.actionDate = actionDate;
		this.actionName = actionName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EmployeeContainer)) {
			return false;
		}
		EmployeeContainer other = (EmployeeContainer) obj;

		if (primaryKey == null) {
			if (other.primaryKey != null) {
				return false;
			}
		} else if (!primaryKey.equals(other.primaryKey)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the actionDate
	 */
	public Date getActionDate() {
		return actionDate;
	}

	/**
	 * @return the actionName
	 */
	public Action getActionName() {
		return Action.valueOf(actionName);
	}

	@Transient
	public Container getContainer() {
		return getPrimaryKey().getContainer();
	}

	@Transient
	public Employee getEmployee() {
		return getPrimaryKey().getEmployee();
	}

	public EmployeeContainerPK getPrimaryKey() {
		if (this.primaryKey == null)
			primaryKey = new EmployeeContainerPK();

		return primaryKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((primaryKey == null) ? 0 : primaryKey.hashCode());
		return result;
	}

	/**
	 * @param actionDate
	 *            the actionDate to set
	 */
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * @param actionName
	 *            the actionName to set
	 */
	public void setActionName(Action action) {
		this.actionName = action.toString();
	}

	public void setContainer(Container container) {
		getPrimaryKey().setContainer(container);
	}

	public void setEmployee(Employee employee) {
		getPrimaryKey().setEmployee(employee);
	}

	public void setPrimaryKey(EmployeeContainerPK primaryKey) {
		this.primaryKey = primaryKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeContainer [primaryKey=").append(primaryKey)
				.append(", actionDate=").append(actionDate)
				.append(", actionName=").append(actionName)
				.append(", version=").append(version).append(']');
		return builder.toString();
	}
}
