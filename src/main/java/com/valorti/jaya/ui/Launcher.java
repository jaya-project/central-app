package com.valorti.jaya.ui;

import org.eclipse.swt.widgets.Display;

import com.valorti.utils.SingleInstance;

public class Launcher {
	public Launcher(String[] args) {
		final Display display = new Display();
		SplashView sv = new SplashView();
		sv.start(display, args);
		sv = null;

		while ((Display.getCurrent().getShells() != null && Display
				.getCurrent().getShells().length > 0)
				&& !Display.getCurrent().getShells()[0].isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		if (!display.isDisposed()) {
			display.dispose();
		}
	}

	public static void main(String[] args) {
		SingleInstance single = new SingleInstance("");

		if (!single.isActive()) {
			new Launcher(args);
		}

		single = null;
	}
}