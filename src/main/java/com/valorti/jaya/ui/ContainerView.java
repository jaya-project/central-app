package com.valorti.jaya.ui;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.ui.events.FillCompleteListener;
import com.valorti.ui.utils.FillTable;
import com.valorti.ui.utils.UIHelper;

public class ContainerView extends Dialog {
	private final Logger			log				= LoggerFactory
															.getLogger(ContainerView.class);
	private final ContainerManager	containerMgr	= new ContainerManager();

	protected Object				result;
	protected Shell					shell;
	private Table					tblContainer;
	private Text					tLotCode;
	private Text					tIdentifier;
	private Label					lblInfo;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public ContainerView(Shell parent, int style) {
		super(parent, style);
		setText("Gestión de Inventario - Listado de Pallets");
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setSize(800, 500);
		shell.setText("Gestión de Inventario - Listado de Pallets");
		shell.setLayout(new GridLayout(1, false));
		
		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if(image != null)
			shell.setImage(image);
		
		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});
		
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event e) {
				onClose();
			}
		});

		Group gSearch = new Group(shell, SWT.NONE);
		gSearch.setLayout(new GridLayout(3, false));
		GridData gd_gSearch = new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1);
		gd_gSearch.heightHint = 60;
		gSearch.setLayoutData(gd_gSearch);
		gSearch.setText("Búsqueda");

		Label lblNewLabel = new Label(gSearch, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel.setText("Lote:");

		tLotCode = new Text(gSearch, SWT.BORDER | SWT.SEARCH);
		GridData gd_tLotCode = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_tLotCode.widthHint = 85;
		tLotCode.setLayoutData(gd_tLotCode);
		new Label(gSearch, SWT.NONE);

		Label lblNewLabel_1 = new Label(gSearch, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_1.setText("Identificador:");

		tIdentifier = new Text(gSearch, SWT.BORDER | SWT.SEARCH);
		GridData gd_tIdentifier = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_tIdentifier.widthHint = 84;
		tIdentifier.setLayoutData(gd_tIdentifier);

		Button btnSearch = new Button(gSearch, SWT.NONE);
		btnSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				fillTable(filter());
			}
		});
		btnSearch.setText("Buscar");

		tblContainer = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.HIDE_SELECTION | SWT.VIRTUAL);
		tblContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
				1, 1));
		tblContainer.setHeaderVisible(true);
		tblContainer.setLinesVisible(true);

		TableColumn tblCol_Lot = new TableColumn(tblContainer, SWT.NONE);
		tblCol_Lot.setWidth(100);
		tblCol_Lot.setText("Lote");
		tblCol_Lot.setData("getLotCode");

		TableColumn tblCol_Identifier = new TableColumn(tblContainer, SWT.NONE);
		tblCol_Identifier.setWidth(100);
		tblCol_Identifier.setText("Identificador");
		tblCol_Identifier.setData("getIdentifier");

		TableColumn tblCol_Row = new TableColumn(tblContainer, SWT.NONE);
		tblCol_Row.setWidth(100);
		tblCol_Row.setText("Rack");
		tblCol_Row.setData("getLocation.getRow");

		TableColumn tblCol_Column = new TableColumn(tblContainer, SWT.NONE);
		tblCol_Column.setWidth(100);
		tblCol_Column.setText("Cuerpo");
		tblCol_Column.setData("getLocation.getColumn");

		TableColumn tblCol_Depth = new TableColumn(tblContainer, SWT.NONE);
		tblCol_Depth.setWidth(100);
		tblCol_Depth.setText("Estante");
		tblCol_Depth.setData("getLocation.getRow");

		lblInfo = new Label(shell, SWT.NONE);
		lblInfo.setText("label");
		lblInfo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		fillTable(filter());
	}

	private void fillTable(List<Container> list) {
		lblInfo.setText("");

		if (tblContainer.getItemCount() > 0)
			tblContainer.removeAll();

		if (list != null && list.size() > 0) {
			try {
				FillTable<Container> ft = new FillTable<Container>(list,
						tblContainer);
				ft.addListener(new FillCompleteListener() {
					@Override
					public void complete() {
						lblInfo.setText("Listado completo.");
						log.debug("Fill complete!");
					}
				});
				shell.getDisplay().asyncExec(ft);
			} catch (Exception ex) {
				log.error("Failed to fill container table.", ex);
			}
		} else {
			lblInfo.setText("No hubieron resultados.");
		}
	}

	private List<Container> filter() {
		List<Container> list = null;

		try {
			if (tLotCode.getText().length() > 0
					|| tIdentifier.getText().length() > 0) {
				Conjunction c = Restrictions.conjunction();

				if (tLotCode.getText().length() > 0)
					c.add(Restrictions.eq("lotCode", tLotCode.getText()));

				if (tIdentifier.getText().length() > 0)
					c.add(Restrictions.eq("identifier",
							Long.parseLong(tIdentifier.getText())));

				log.debug("Conjuction: {}", c.toString());
				list = containerMgr.list(Container.class, c);
			} else {
				list = containerMgr.list(Container.class);
			}
		} catch (Exception ex) {
			log.error("Failed to fill container list.", ex);
		}

		return list;
	}

	private void onClose() {
		setResult();
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/*
	 * private void onSelectContainer() { if (tblContainer.getSelectionCount() >
	 * 0) { TableItem[] ti = tblContainer.getItems(); Object o =
	 * ti[0].getData("Id"); log.debug("Object: {}", o);
	 * 
	 * if (o != null) { result = (Container) o; } else {
	 * log.debug("Result is null."); } } else { result = null; } }
	 */

	private void setResult() {
		try {
			if (tblContainer.getItemCount() > 0
					&& tblContainer.getSelectionCount() > 0) {
				TableItem[] items = tblContainer.getSelection();
				Object data = items[0].getData("Id");
				result = data;
			} else {
				result = null;
			}
		} catch (Exception ex) {
			log.error("Failed to set result object.", ex);
		}
	}
}
