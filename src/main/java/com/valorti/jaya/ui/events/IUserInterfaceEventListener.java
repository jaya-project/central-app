package com.valorti.jaya.ui.events;

public interface IUserInterfaceEventListener {
	/**
	 * Fires when any event on the UI is detected.
	 */
	public void eventDetected();
}
