package com.valorti.jaya.ui.events;

public class ProgressEvent {
	private int		currentValue;
	private int		maxValue;
	private int		initialValue;
	private String	message;

	public ProgressEvent() {
		this(100, 1, "");
	}

	public ProgressEvent(int maxValue, int initialValue, String message) {
		this.initialValue = initialValue;
		this.maxValue = maxValue;
		this.currentValue = initialValue;
		this.message = message;
	}

	/**
	 * @return the currentValue
	 */
	public int getCurrentValue() {
		return currentValue;
	}

	/**
	 * @return the initialValue
	 */
	public int getInitialValue() {
		return initialValue;
	}

	/**
	 * @return the maxValue
	 */
	public int getMaxValue() {
		return maxValue;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	public boolean isFinished() {
		return (currentValue >= maxValue);
	}

	/**
	 * @param currentValue
	 *            the currentValue to set
	 */
	public void setCurrentValue(int currentValue) {
		this.currentValue = currentValue;
	}

	/**
	 * @param initialValue
	 *            the initialValue to set
	 */
	public void setInitialValue(int initialValue) {
		this.initialValue = initialValue;
	}

	/**
	 * @param maxValue
	 *            the maxValue to set
	 */
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
