/**
 * 
 */
package com.valorti.jaya.ui.events;

/**
 * @author Nicolás
 *
 */
public interface IProgressEventListener {
	/**
	 * Fires after at the end of the event.
	 * 
	 * @param event
	 *            Event arguments.
	 */
	public void onFinish(ProgressEvent event);

	/**
	 * Fires at the start of the event.
	 * 
	 * @param event
	 *            Event arguments.
	 */
	public void onStart(ProgressEvent event);

	/**
	 * Fires on each update of the event.
	 * 
	 * @param event
	 *            Event arguments.
	 */
	public void onUpdate(ProgressEvent event);
}
