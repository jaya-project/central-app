package com.valorti.jaya.ui;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.ItemMovement;
import com.valorti.jaya.model.manager.ItemMovementManager;
import com.valorti.ui.utils.FillTable;
import com.valorti.ui.utils.UIHelper;

public class ItemMovementView extends Shell {
	private final Logger			log		= LoggerFactory
													.getLogger(ItemMovementView.class);

	private Table					table;
	private static ItemMovementView	shell	= null;

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public ItemMovementView(Display display) {
		super(display, SWT.SHELL_TRIM);
		setLayout(new GridLayout(1, false));

		final Image image = UIHelper.loadTitleIcon(display);
		if (image != null)
			this.setImage(image);

		this.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel
				.setText("Esta tabla muestra las materias primas que han sido movidas de una bodega (Bodega origen) a otra (Bodega Destino).");

		table = new Table(this, SWT.BORDER | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		GridData gd_table = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
		gd_table.heightHint = 367;
		gd_table.widthHint = 748;
		table.setLayoutData(gd_table);
		table.setLinesVisible(true);

		TableColumn tcMovedQty = new TableColumn(table, SWT.NONE);
		tcMovedQty.setToolTipText("Cantidad de rollos que se movieron");
		tcMovedQty.setWidth(105);
		tcMovedQty.setText("Cantidad Movida");
		tcMovedQty.setData("getItemsMoved");

		TableColumn tcSrcWarehouseCode = new TableColumn(table, SWT.NONE);
		tcSrcWarehouseCode.setWidth(100);
		tcSrcWarehouseCode.setText("Bodega Origen");
		tcSrcWarehouseCode
				.setData("getContainer.getLocation.getWarehouse.getWhName + getContainer.getLocation.getWarehouse.getWarehouseCode.getWhCode");

		TableColumn tcDstWarehouseCode = new TableColumn(table, SWT.NONE);
		tcDstWarehouseCode.setWidth(100);
		tcDstWarehouseCode.setText("Bodega Destino");
		tcDstWarehouseCode
				.setData("getWarehouse.getWhName + getWarehouse.getWarehouseCode.getWhCode");

		TableColumn tcSrcPalletLocation = new TableColumn(table, SWT.NONE);
		tcSrcPalletLocation.setWidth(153);
		tcSrcPalletLocation.setText("Pallet Origen (Ubicación)");
		tcSrcPalletLocation
				.setData("getContainer.getLocation.getRow + getContainer.getLocation.getColumn + getContainer.getLocation.getDepth");

		TableColumn tcItemCode = new TableColumn(table, SWT.NONE);
		tcItemCode.setWidth(100);
		tcItemCode.setText("Materia Prima");
		tcItemCode.setData("getContainer.getItem.getItemCode");

		Button btnClose = new Button(this, SWT.NONE);
		btnClose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				shell.close();
			}
		});
		btnClose.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		btnClose.setText("Cerrar");
		createContents();
		fillData(filter(null));
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			shell = new ItemMovementView(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}

			if (!shell.isDisposed())
				shell.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Movimiento entre Bodegas");
		setSize(800, 500);

	}

	private void fillData(List<ItemMovement> list) {
		try {
			if (list != null && list.size() > 0) {
				FillTable<ItemMovement> ft = new FillTable<ItemMovement>(list,
						table);
				ft.setSeparator(" - ");
				this.getDisplay().asyncExec(ft);
			}
		} catch (Exception ex) {
			log.error("Failed to fill table data: ", ex);
			MessageDialog
					.openError(
							shell,
							"Error",
							"No se pudo llenar la información. Para obtener más información revise el registro de errores.");
		}
	}

	private List<ItemMovement> filter(String search) {
		List<ItemMovement> list = null;

		try {
			if (search != null && search.length() > 0) {

			} else {
				list = new ItemMovementManager().list(ItemMovement.class);
			}
		} catch (Exception ex) {
			log.error("Failed to filter Item Movements: ", ex);
		}

		return list;
	}
}
