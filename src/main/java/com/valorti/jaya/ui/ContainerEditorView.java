package com.valorti.jaya.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.Location;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.LocationManager;
import com.valorti.ui.utils.UIHelper;

public class ContainerEditorView extends Dialog {
	private final Logger			log				= LoggerFactory
															.getLogger(ContainerEditorView.class);

	private final LocationManager	locationMgr		= new LocationManager();
	private final ContainerManager	containerMgr	= new ContainerManager();
	private Item					item			= null;
	private Container				container		= null;

	private String					lotCode			= null;

	protected Object				result;
	protected Shell					shell;
	private Text					tLotCode;
	private Group					gNewPallet;
	private Text					tItemCount;
	private Group					gItem;
	private Text					tItemName;
	private Text					tItemCode;
	private Spinner					spinIdentifier;
	private Combo					cbRow;
	private Combo					cbWarehouse;
	private Spinner					spinColumn;
	private Spinner					spinDepth;
	private Text					txtPalletLength;
	private Text					txtItemLength;
	private Group					gLocation;
	private Group					gControls;

	/**
	 * Creates the dialog.
	 * 
	 * @param parent
	 * @param style
	 * @param container
	 *            This dialog can edit or create a container, but in order to
	 *            edit it, we need the container data to edit. Pass null to
	 *            create a new container.
	 */
	public ContainerEditorView(Shell parent, int style, Container container) {
		super(parent, style);
		setText("Editor de Contenedores");
		this.container = container;
		result = null;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setSize(400, 450);

		if (container == null)
			shell.setText("Añadir Pallet");
		else
			shell.setText("Modificar Pallet");

		shell.setLayout(new GridLayout(1, false));

		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if (image != null)
			shell.setImage(image);

		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		ScrolledComposite scrolledComposite = new ScrolledComposite(shell,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1);
		gd_scrolledComposite.heightHint = 394;
		scrolledComposite.setLayoutData(gd_scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite form = new Composite(scrolledComposite, SWT.NONE);
		form.setLayout(new GridLayout(1, false));

		Group gLotSearch = new Group(form, SWT.NONE);
		gLotSearch.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		gLotSearch.setText("1.- Busque el lote al que añadirá el pallet");
		gLotSearch.setLayout(new GridLayout(3, false));

		Label lblNewLabel = new Label(gLotSearch, SWT.NONE);
		lblNewLabel.setText("Lote:");

		tLotCode = new Text(gLotSearch, SWT.BORDER);
		tLotCode.setToolTipText("Este es el número de lote que identifica a un grupo de Contenedores / Pallets");
		tLotCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Button btnLotSearch = new Button(gLotSearch, SWT.NONE);
		btnLotSearch
				.setToolTipText("Es necesario hacer clic aquí para poder seleccionar el identificador correcto para el lote");
		btnLotSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onLotSearch();
			}
		});
		btnLotSearch.setText("Listo");

		gItem = new Group(form, SWT.NONE);
		gItem.setEnabled(false);
		gItem.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));
		gItem.setText("2.- Información del Producto");
		gItem.setLayout(new GridLayout(2, false));

		Label lblNewLabel_6 = new Label(gItem, SWT.NONE);
		lblNewLabel_6.setText("Materia Prima:");

		Button btnSelectItem = new Button(gItem, SWT.NONE);
		btnSelectItem
				.setToolTipText("Abre una ventana que le permitirá seleccionar la materia prima que desea");
		btnSelectItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSelectItem();
			}
		});
		btnSelectItem.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
				false, 1, 1));
		btnSelectItem.setText("Seleccionar");

		Label lblNewLabel_7 = new Label(gItem, SWT.NONE);
		lblNewLabel_7.setText("Nombre Materia Prima:");

		tItemName = new Text(gItem, SWT.BORDER);
		tItemName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		tItemName.setEditable(false);

		Label lblNewLabel_8 = new Label(gItem, SWT.NONE);
		lblNewLabel_8.setText("Código de Materia Prima:");

		tItemCode = new Text(gItem, SWT.BORDER);
		tItemCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		tItemCode.setEditable(false);

		gNewPallet = new Group(form, SWT.NONE);
		gNewPallet.setEnabled(false);
		GridData gd_gNewPallet = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_gNewPallet.heightHint = 109;
		gNewPallet.setLayoutData(gd_gNewPallet);
		gNewPallet.setText("3.- Información del Pallet");
		gNewPallet.setLayout(new GridLayout(2, false));

		Label lblNewLabel_1 = new Label(gNewPallet, SWT.NONE);
		lblNewLabel_1.setText("Identificador:");

		spinIdentifier = new Spinner(gNewPallet, SWT.BORDER);
		spinIdentifier.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		spinIdentifier.setEnabled(false);
		spinIdentifier.setMaximum(999999999);
		spinIdentifier.setMinimum(1);
		spinIdentifier.setSelection(1);

		Label lblNewLabel_3 = new Label(gNewPallet, SWT.NONE);
		lblNewLabel_3.setText("Largo Total:");

		txtPalletLength = new Text(gNewPallet, SWT.BORDER);
		txtPalletLength
				.setToolTipText("La suma total del largo de cada rollo en el pallet");
		txtPalletLength.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				updateItemCount();
			}
		});
		txtPalletLength.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				onVerifyDouble(e);
			}
		});
		txtPalletLength.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		txtPalletLength.setText("1.0");

		Label lblNewLabel_2 = new Label(gNewPallet, SWT.NONE);
		lblNewLabel_2.setText("Cantidad de Rollos:");

		tItemCount = new Text(gNewPallet, SWT.BORDER);
		tItemCount.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				updateItemCount();
			}
		});
		tItemCount.setText("1");
		tItemCount.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_4 = new Label(gNewPallet, SWT.NONE);
		lblNewLabel_4.setText("Largo del Rollo:");

		txtItemLength = new Text(gNewPallet, SWT.BORDER);
		txtItemLength
				.setToolTipText("Cuantos metros/centímetros mide hay en cada rollo del pallet");
		txtItemLength.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		tItemCount.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				onVerifyInt(e);
			}
		});

		gLocation = new Group(form, SWT.NONE);
		gLocation.setEnabled(false);
		GridData gd_gLocation = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_gLocation.heightHint = 114;
		gLocation.setLayoutData(gd_gLocation);
		gLocation.setLayout(new GridLayout(2, false));
		gLocation.setText("4.- Información de Ubicación en Bodega");

		Label lblNewLabel_9 = new Label(gLocation, SWT.NONE);
		lblNewLabel_9.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_9.setText("Bodega:");

		cbWarehouse = new Combo(gLocation, SWT.READ_ONLY);
		cbWarehouse.setToolTipText("Bodega donde se guardará el pallet");
		cbWarehouse.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_10 = new Label(gLocation, SWT.NONE);
		lblNewLabel_10.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_10.setText("Rack:");

		cbRow = new Combo(gLocation, SWT.READ_ONLY);
		cbRow.setToolTipText("Rack de la ubicación del pallet");
		cbRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		Label lblNewLabel_11 = new Label(gLocation, SWT.NONE);
		lblNewLabel_11.setText("Cuerpo:");

		spinColumn = new Spinner(gLocation, SWT.BORDER);
		spinColumn.setToolTipText("Cuerpo de la ubicación del pallet");
		spinColumn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinColumn.setMaximum(99);

		Label lblNewLabel_12 = new Label(gLocation, SWT.NONE);
		lblNewLabel_12.setText("Estante:");

		spinDepth = new Spinner(gLocation, SWT.BORDER);
		spinDepth.setToolTipText("Estante de la ubicación del pallet");
		spinDepth.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinDepth.setMaximum(99);

		gControls = new Group(form, SWT.NONE);
		gControls.setEnabled(false);
		gControls.setLayout(new GridLayout(2, false));
		GridData gd_gControls = new GridData(SWT.FILL, SWT.FILL, false, false,
				1, 1);
		gd_gControls.heightHint = 31;
		gControls.setLayoutData(gd_gControls);
		gControls
				.setText("5.- Guarde la información o cierre la venta para cancelar");

		Button btnSave = new Button(gControls, SWT.NONE);
		btnSave.setToolTipText("Guarda los cambios y cierra la ventana.");
		GridData gd_btnSave = new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1);
		gd_btnSave.widthHint = 142;
		btnSave.setLayoutData(gd_btnSave);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSave();
			}
		});
		btnSave.setText("Guardar");

		Button btnNewButton = new Button(gControls, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onReset();
			}
		});
		btnNewButton.setToolTipText("Borra la información ingresada.");
		GridData gd_btnNewButton = new GridData(SWT.RIGHT, SWT.CENTER, true,
				false, 1, 1);
		gd_btnNewButton.widthHint = 145;
		btnNewButton.setLayoutData(gd_btnNewButton);
		btnNewButton.setText("Reiniciar");

		scrolledComposite.setContent(form);
		scrolledComposite
				.setMinSize(form.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		setup();
	}

	private Location getLocation(Warehouse warehouse) {
		int iCol = spinColumn.getSelection(), iDepth = spinDepth.getSelection();
		String row = cbRow.getItem(cbRow.getSelectionIndex());
		Location location = locationMgr.getLocation(row, iCol, iDepth,
				warehouse);

		if (location == null) {
			log.error("Failed to retrieve or create the new location.");
		}

		row = null;

		return location;
	}

	private void onLotSearch() {
		String search = tLotCode.getText();

		if (search.length() > 0) {
			Long tmp = (Long) containerMgr.project(Container.class,
					Projections.max("identifier"),
					Restrictions.eq("lotCode", search));

			if (tmp != null) {
				tmp += 1;
				lotCode = search;
				spinIdentifier.setSelection(tmp.intValue());
			} else {
				lotCode = search;
				spinIdentifier.setSelection(1);
			}

			tmp = null;
		}
	}

	private void onReset() {
		shell.setEnabled(false);
		try {
			if (container != null) {
				tLotCode.setText(container.getLotCode());
				tItemName.setText(container.getItem().getItemName());
				tItemCode.setText(container.getItem().getItemCode());
				spinIdentifier.setSelection(container.getIdentifier()
						.intValue());

				txtPalletLength.setText(container.getItemTotalLength()
						.toString());
				txtItemLength.setText(container.getPerItemAvgLength()
						.toString());

				tItemCount.setText(container.getItemCount().toString());
				String tmp = String.format("%s - %s", container.getLocation()
						.getWarehouse().getWhName(), container.getLocation()
						.getWarehouse().getWarehouseCode().getWhCode());

				UIHelper.select(cbWarehouse, tmp);
				UIHelper.select(cbRow, container.getLocation().getRow());
				tmp = null;

				spinColumn.setSelection(Integer.parseInt(container
						.getLocation().getColumn()));

				spinDepth.setSelection(Integer.parseInt(container.getLocation()
						.getDepth()));
				item = container.getItem();
			} else {
				container = null;
				item = null;
				tItemName.setText("");
				tItemCode.setText("");
				tLotCode.setText("");
				txtItemLength.setText("1.0");
				txtPalletLength.setText("1.0");
				spinIdentifier.setSelection(1);
				tItemCount.setText("");
				cbWarehouse.select(0);
				cbRow.select(0);
				spinColumn.setSelection(0);
				spinDepth.setSelection(0);
			}
		} catch (Exception ex) {
			log.error("Failed to reset form controls.", ex);
		}

		shell.setEnabled(true);
	}

	private void onSave() {
		shell.setEnabled(false);
		boolean shouldClose = false;

		if (validateUserInput()) {
			try {
				Container check = containerMgr.findByLotCodeAndIdentifier(
						lotCode, (long) spinIdentifier.getSelection());

				if (check != null) {
					MessageDialog
							.openInformation(shell, "Ya Existe",
									"Ya existe un pallet con ese identificador y lote.");
				} else {
					boolean update = true;

					if (container == null) {
						container = new Container();
						update = false;
					}

					setContainer();

					if (update) {
						result = containerMgr.update(container);
					} else {
						result = containerMgr.add(container) != null;
					}

					log.debug("Operation result: {}", result);
					shouldClose = true;
				}

			} catch (Exception ex) {
				log.error("Failed to save Container Editor changes.", ex);
			}
		}

		if (shouldClose)
			shell.close();
		else
			shell.setEnabled(true);
	}

	/**
	 * Opens item selection window and selects that item.
	 */
	private void onSelectItem() {
		log.info("Selecting item...");
		ItemView view = null;
		Item tmpItem = null;

		try {
			view = new ItemView(shell, SWT.None);
			tmpItem = view.open();
		} catch (Exception ex) {
			log.error("Exception caught when selecting item.", ex);
		} finally {
			view = null;
		}

		// Only change the item if the user selects another one.
		if (tmpItem != null) {
			item = tmpItem;
			tItemName.setText(item.getItemName());
			tItemCode.setText(item.getItemCode());
		}

		tmpItem = null;
	}

	private void onVerifyDouble(VerifyEvent e) {
		Text text = (Text) e.getSource();
		final String oldS = text.getText();
		StringBuilder newS = new StringBuilder(oldS.substring(0, e.start))
				.append(e.text).append(oldS.substring(e.end));

		try {
			Float.parseFloat(newS.toString());
		} catch (NumberFormatException ex) {
			e.doit = false;
		}

		text = null;
		newS = null;
	}

	private void onVerifyInt(VerifyEvent e) {
		Text text = (Text) e.getSource();
		final String oldS = text.getText();
		StringBuilder newS = new StringBuilder(oldS.substring(0, e.start))
				.append(e.text).append(oldS.substring(e.end));

		try {
			Integer.parseInt(newS.toString());
		} catch (NumberFormatException ex) {
			e.doit = false;
		}

		text = null;
		newS = null;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Fills container data.
	 */
	private void setContainer() {
		container.setCurrentItemLength(Double.parseDouble(txtPalletLength
				.getText()));
		container.setIdentifier((long) spinIdentifier.getSelection());
		container.setItem(item);
		container.setItemCount(Integer.parseInt(tItemCount.getText()));
		container.setItemTotalLength(Double.parseDouble(txtPalletLength
				.getText()));
		Warehouse warehouse = UIHelper.getSelectedWarehouse(cbWarehouse);
		Location location = getLocation(warehouse);
		container.setLocation(location);
		container.setLotCode(tLotCode.getText());
		container.setModCount(0);
		container.setPerItemAvgLength(Double.parseDouble(txtItemLength
				.getText()));
		container.setUnitPrice(0.0);
		container.setDimensions("");

		warehouse = null;
		location = null;
	}

	private void setup() {
		shell.setEnabled(false);

		try {
			gItem.setEnabled(true);
			gNewPallet.setEnabled(true);
			gLocation.setEnabled(true);
			gControls.setEnabled(true);

			if (container == null) {
				UIHelper.fillWarehouseCombo(cbWarehouse, true);
				UIHelper.fillRowCombo(cbRow, true);
			} else {
				tLotCode.setText(container.getLotCode());
				tItemName.setText(container.getItem().getItemName());
				tItemCode.setText(container.getItem().getItemCode());
				spinIdentifier.setSelection(container.getIdentifier()
						.intValue());
				txtPalletLength.setText(container.getItemTotalLength()
						.toString());
				txtItemLength.setText(container.getPerItemAvgLength()
						.toString());
				tItemCount.setText(container.getItemCount().toString());
				UIHelper.fillWarehouseCombo(cbWarehouse, container
						.getLocation().getWarehouse(), true);
				UIHelper.fillRowCombo(cbRow, container.getLocation().getRow(),
						true);
				spinColumn.setSelection(Integer.parseInt(container
						.getLocation().getColumn()));
				spinDepth.setSelection(Integer.parseInt(container.getLocation()
						.getDepth()));
				item = container.getItem();
			}

			shell.setEnabled(true);
		} catch (Exception ex) {
			log.error("Setup failed.", ex);
			MessageDialog.openError(shell, "Error",
					"Falló la configuración inicial.");
			shell.close();
		}
	}

	private void updateItemCount() {
		try {
			double totalLength = Double.parseDouble(txtPalletLength.getText());
			int itemCount = Integer.parseInt(tItemCount.getText());
			log.trace("Pallet Length: {}, Item Length: {}", totalLength,
					itemCount);
			txtItemLength.setText(String.valueOf(totalLength / itemCount));
		} catch (Exception ex) {
			log.error("Exception caught updating item count.", ex);
		}
	}

	private boolean validateUserInput() {
		boolean r = true;
		double selection = Double.parseDouble(txtPalletLength.getText());
		StringBuilder sb = new StringBuilder();

		if (tLotCode.getText().length() <= 0) {
			r &= false;
			sb.append("- Necesita ingresar el número de Lote.\n");
		}

		if (selection < 1) {
			r &= false;
			sb.append("- El Largo Total no puede ser menor a 1.\n");
		}

		selection = Double.parseDouble(txtItemLength.getText());

		if (selection < 1) {
			r &= false;
			sb.append("- El Largo del Rollo no puede ser menor a 1.\n");
		}

		if (cbWarehouse.getSelectionIndex() <= 0) {
			r &= false;
			sb.append("- Debe seleccionar una Bodega.\n");
		}

		if (cbRow.getSelectionIndex() <= 0) {
			r &= false;
			sb.append("- No ha elegido el Rack.\n");
		}

		selection = spinColumn.getSelection();

		if (selection < 0 || selection > 99) {
			r &= false;
			sb.append("- El Cuerpo no puede ser menor a 0 o mayor a 99.\n");
		}

		selection = spinDepth.getSelection();

		if (selection < 0 || selection > 99) {
			r &= false;
			sb.append("- El Estante no puede ser menor a 0 o mayor a 99.\n");
		}

		if (item == null) {
			r &= false;
			sb.append("- Debe seleccionar una Materia Prima / Producto.\n");
		}

		if (sb.length() > 0) {
			MessageDialog.openWarning(shell, "Falta Información",
					"Aún falta información:\n\n" + sb.toString());
		}

		sb = null;

		return r;
	}
}
