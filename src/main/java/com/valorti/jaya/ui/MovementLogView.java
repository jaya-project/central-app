package com.valorti.jaya.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.ui.utils.UIHelper;
import com.valorti.utils.PathUtils;

public class MovementLogView extends Shell {
	private static MovementLogView	shell	= null;

	private Table					table;
	private final Logger			log		= LoggerFactory
													.getLogger(MovementLogView.class);
	private final File				logFile	= new File(
													PathUtils
															.combine(
																	Main.configuration()
																			.getString(
																					"/Application/FolderStructure/LogsFolder"),
																	"rapi_connector.log"));

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public MovementLogView(Display display) {
		super(display, SWT.SHELL_TRIM);
		final Image image = UIHelper.loadTitleIcon(display);
		if (image != null)
			this.setImage(image);

		this.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		if (logFile.exists()) {
			Menu menu = new Menu(this, SWT.BAR);
			setMenuBar(menu);

			MenuItem miFile = new MenuItem(menu, SWT.CASCADE);
			miFile.setText("Archivo");

			Menu menu_1 = new Menu(miFile);
			miFile.setMenu(menu_1);

			MenuItem miQuit = new MenuItem(menu_1, SWT.NONE);
			miQuit.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					shell.close();
				}
			});
			miQuit.setText("Cerrar");

			table = new Table(this, SWT.BORDER | SWT.FULL_SELECTION);
			table.setBounds(10, 10, 764, 393);
			table.setLinesVisible(true);

			final Button btnDeleteLog = new Button(this, SWT.NONE);
			btnDeleteLog
					.setToolTipText("Elimina el registro de errores actual para comenzar uno nuevo.");
			btnDeleteLog.setBounds(699, 409, 75, 25);
			btnDeleteLog.setText("Eliminar");
			btnDeleteLog.addMouseListener(new MouseListener() {
				@Override
				public void mouseDoubleClick(MouseEvent e) {
				}

				@Override
				public void mouseDown(MouseEvent e) {
				}

				@Override
				public void mouseUp(MouseEvent e) {
					int code = new MessageDialog(
							shell,
							"Confirmar",
							null,
							"¿Realmente desea eliminar el registro de errores?",
							MessageDialog.QUESTION,
							new String[] { "Sí", "NO" }, 1).open();

					if (code == 0) {
						try {
							logFile.delete();
							table.removeAll();
							MessageDialog.openInformation(shell,
									"Registro eliminado",
									"El registro fue eliminado correctamente.");
						} catch (SecurityException ex) {
							log.error(
									"Failed to delete log file rapi_connector.log: ",
									ex);
						}
					}
				}
			});
			createContents();
			fillLogData();
		} else {
			MessageDialog.openInformation(this, "No hay mensajes",
					"No hay mensajes que mostrar.");
			close();
		}
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			shell = new MovementLogView(display);

			if (shell != null && !shell.isDisposed()) {
				shell.open();
				shell.layout();

				while (!shell.isDisposed()) {
					if (!display.readAndDispatch()) {
						display.sleep();
					}
				}
			}

			if (!shell.isDisposed())
				shell.dispose();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Registro de Errores en la Copia de Archivos entre Dispositivos");
		setSize(800, 500);
	}

	private void fillLogData() {

		if (logFile.exists()) {
			FileReader freader = null;
			BufferedReader bufReader = null;

			try {
				freader = new FileReader(logFile);
				bufReader = new BufferedReader(freader);
				String line = "";

				while ((line = bufReader.readLine()) != null) {
					if (line != null && line.length() > 0) {
						TableItem ti = new TableItem(table, SWT.NONE);
						ti.setText(line);
					}
				}
			} catch (FileNotFoundException e) {
				MessageDialog.openError(this, "Archivo no Encontrado",
						"No se encontró el archivo logs/rapi_connector.log: "
								+ e.getMessage());
				log.error("Failed to fill log data: ", e);
			} catch (IOException e) {
				log.error("Failed to fill log data: ", e);
			} finally {
				try {
					if (bufReader != null)
						bufReader.close();

					if (freader != null)
						freader.close();
				} catch (IOException ex) {
					log.error("Failed to close a reder: ", ex);
				}

				bufReader = null;
				freader = null;
			}
		} else {

		}
	}
}
