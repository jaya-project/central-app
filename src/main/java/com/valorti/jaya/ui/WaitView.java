package com.valorti.jaya.ui;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;

public class WaitView extends ApplicationWindow {
	private Label	lblMsg;
	private Label	lblTitle;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public WaitView() {
		super(null);
		setShellStyle(SWT.BORDER | SWT.TITLE);
		setBlockOnOpen(false);
	}

	/**
	 * Create contents of the dialog.
	 */
	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(null);

		lblTitle = new Label(composite, SWT.NONE);
		lblTitle.setBounds(0, 0, 235, 30);
		lblTitle.setAlignment(SWT.CENTER);
		lblTitle.setForeground(SWTResourceManager.getColor(255, 228, 181));
		lblTitle.setBackground(SWTResourceManager.getColor(205, 133, 63));
		lblTitle.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.BOLD));
		lblTitle.setText("Operación en Curso");

		lblMsg = new Label(composite, SWT.NONE);
		lblMsg.setBounds(0, 31, 235, 76);
		lblMsg.setAlignment(SWT.CENTER);
		lblMsg.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblMsg.setText("Espere...");

		parent.pack();
		return composite;
	}

	public void setMsgLbl(String txt) {
		lblMsg.setText(txt);
	}

	public void setTitleLbl(String txt) {
		lblTitle.setText(txt);
	}
}
