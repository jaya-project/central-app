package com.valorti.jaya.ui;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.data.formatter.InventorySpreadsheet;
import com.valorti.data.formatter.SpreadsheetFromTable;
import com.valorti.data.formatter.WorkbookBuilder;
import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Inventory;
import com.valorti.jaya.model.InventoryContainer;
import com.valorti.jaya.model.InventoryContainer.Difference;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.InventoryContainerManager;
import com.valorti.jaya.model.manager.InventoryManager;
import com.valorti.ui.utils.UIHelper;

public class InventoryDataView extends Dialog {
	private final Logger					log					= LoggerFactory
																		.getLogger(InventoryDataView.class);
	protected Object						result;
	private Inventory						inventory			= null;
	private final InventoryContainerManager	invContMgr			= new InventoryContainerManager();
	private SpreadsheetFromTable			spreadsheet			= null;

	// Filters
	private final byte						FILTER_SHOW_ULLAGE	= 1;
	private final byte						FILTER_SHOW_SURPLUS	= 2;
	private final byte						FILTER_SHOW_SQUARE	= 4;
	private byte							filter				= FILTER_SHOW_ULLAGE
																		+ FILTER_SHOW_SURPLUS
																		+ FILTER_SHOW_SQUARE;

	// Window Controls
	protected Shell							shell;
	private Table							table;
	private Display							display				= null;
	private Group							grpFilters;
	private Button							btnUpdateData;
	private Button							btnShowUllage;
	private Button							btnShowSurplus;
	private Button							btnShowSquare;
	private Composite						compBottom;
	private Label							lblInfo;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public InventoryDataView(Shell parent, int style, long inventoryId) {
		super(parent, style);
		setText("Información Detallada del Inventario");
		inventory = (Inventory) new InventoryManager().getById(Inventory.class,
				inventoryId);
	}

	private void changeMenuItemState(MenuItem item) {
		Object data = item.getData("name");

		if (data != null) {
			String name = data.toString();

			switch (name) {
				case "addInventoryContainer":
					item.setEnabled(inventory.getEndDate() == null);
					break;

				case "editInventoryContainer":
					item.setEnabled(inventory.getEndDate() == null);
					break;

				case "deleteInventoryContainer":
					item.setEnabled(true);
					break;
			}
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.SHELL_TRIM | SWT.BORDER);
		shell.setSize(800, 500);
		shell.setText("Información del Inventario");
		shell.setLayout(new GridLayout(1, false));

		final Image excelImg = UIHelper.loadExcelIcon(display);

		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if (image != null)
			shell.setImage(image);

		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();

				if (excelImg != null)
					excelImg.dispose();
			}
		});

		ToolBar toolBar = new ToolBar(shell, SWT.FLAT | SWT.RIGHT);

		ToolItem tbitmExportToExcel = new ToolItem(toolBar, SWT.NONE);
		tbitmExportToExcel
				.setToolTipText("Exporta los datos de este inventario a un archivo Excel");
		tbitmExportToExcel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onExportToExcel();
			}
		});

		tbitmExportToExcel.setText("Exportar");

		if (excelImg != null)
			tbitmExportToExcel.setImage(excelImg);

		grpFilters = new Group(shell, SWT.NONE);
		grpFilters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		grpFilters.setText("Filtros");

		btnShowUllage = new Button(grpFilters, SWT.CHECK);
		btnShowUllage.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilterChange();
			}
		});
		btnShowUllage.setSelection(true);
		btnShowUllage.setBounds(10, 20, 138, 16);
		btnShowUllage.setText("Mostrar Mermas");

		btnShowSurplus = new Button(grpFilters, SWT.CHECK);
		btnShowSurplus.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilterChange();
			}
		});
		btnShowSurplus.setSelection(true);
		btnShowSurplus.setBounds(154, 20, 123, 16);
		btnShowSurplus.setText("Mostrar Excedentes");

		btnShowSquare = new Button(grpFilters, SWT.CHECK);
		btnShowSquare.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilterChange();
			}
		});
		btnShowSquare.setSelection(true);
		btnShowSquare.setBounds(283, 20, 180, 16);
		btnShowSquare.setText("Mostrar Elementos Cuadrados");

		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		table.setSelection(new int[] {0});
		table.setSelection(0);
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				log.debug("Row selected?");
			}
		});
		table.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				log.debug("Focused!");
			}
		});
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tcContainer = new TableColumn(table, SWT.CENTER);
		tcContainer.setToolTipText("Identificador del contenedor");
		tcContainer.setWidth(154);
		tcContainer.setText("Pallet (Lote - Identificador)");

		TableColumn tcItem = new TableColumn(table, SWT.NONE);
		tcItem.setWidth(100);
		tcItem.setText("Producto");

		TableColumn tcPerItemLength = new TableColumn(table, SWT.CENTER);
		tcPerItemLength
				.setToolTipText("Cantidad de metros por cada producto dentro del contenedor");
		tcPerItemLength.setWidth(117);
		tcPerItemLength.setText("Mtrs. por Producto");

		TableColumn tcItemCount = new TableColumn(table, SWT.CENTER);
		tcItemCount
				.setToolTipText("Esta es la cantidad de rollos que había antes de realizar el inventario");
		tcItemCount.setWidth(127);
		tcItemCount.setText("Contenido Original");

		TableColumn tcLocation = new TableColumn(table, SWT.CENTER);
		tcLocation.setWidth(68);
		tcLocation.setText("Ubicación");

		TableColumn tcDiffName = new TableColumn(table, SWT.CENTER);
		tcDiffName.setWidth(68);
		tcDiffName.setText("Diferencia");

		TableColumn tcDiffQty = new TableColumn(table, SWT.CENTER);
		tcDiffQty.setToolTipText("Cantidad de rollos de excedente o merma");
		tcDiffQty.setWidth(119);
		tcDiffQty.setText("Rollos de Diferencia");

		TableColumn tcNewTotalLength = new TableColumn(table, SWT.CENTER);
		tcNewTotalLength
				.setToolTipText("Cantidad de metros que quedarán en el pallet al corregir la información. Esta información se calcula así:\r\n\r\nContenido Original +/- Rollos de Diferencia * Metros por Producto.\r\n\r\nLos 'Rollos de Diferencia' se restan o suman dependiendo de si es un 'Excedente' o una 'Merma'");
		tcNewTotalLength.setWidth(119);
		tcNewTotalLength.setText("Metros Restantes");

		TableColumn tcMtrsDiff = new TableColumn(table, SWT.CENTER);
		tcMtrsDiff.setToolTipText("La diferencia en metros.");
		tcMtrsDiff.setWidth(131);
		tcMtrsDiff.setText("Diferencia en Metros");

		Menu tableMenu = new Menu(table);
		tableMenu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				onTableMenuShown(e);
			}
		});
		table.setMenu(tableMenu);

		MenuItem miAddInventoryContainer = new MenuItem(tableMenu, SWT.NONE);
		miAddInventoryContainer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onAddInventoryContainer();
			}
		});
		miAddInventoryContainer.setText("Añadir Elemento");
		miAddInventoryContainer.setData("name", "addInventoryContainer");

		MenuItem miEditInventoryContainer = new MenuItem(tableMenu, SWT.NONE);
		miEditInventoryContainer.setText("Modificar Elemento");
		miEditInventoryContainer.setData("name", "editInventoryContainer");

		miEditInventoryContainer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onEditInventoryContainer();
			}
		});

		compBottom = new Composite(shell, SWT.NONE);
		compBottom.setLayout(new GridLayout(3, false));
		compBottom.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		lblInfo = new Label(compBottom, SWT.NONE);
		lblInfo.setToolTipText("Muestra información dinámica del programa");
		lblInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false,
				1, 1));

		btnUpdateData = new Button(compBottom, SWT.NONE);
		btnUpdateData.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		btnUpdateData
				.setToolTipText("Corrige la información del contenido de los pallets. Tenga en cuenta que si el inventario no se realizó correctamente, esta información causará más incongruencias de las que ya hay.");
		btnUpdateData.setText("Corregir");

		Button btnClose = new Button(compBottom, SWT.NONE);
		btnClose.setToolTipText("Cierra la ventana");
		btnClose.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		btnClose.setText("Cerrar");
		btnClose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
			}
		});
		btnUpdateData.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateContainerData();
			}
		});

		fillTable(filter());
		tbitmExportToExcel.setSelection(false);
	}

	/**
	 * Fills the table with the provided list of objects.
	 * 
	 * @param list
	 *            A list of InventoryContainer objects that will populate the
	 *            table.
	 */
	private void fillTable(List<InventoryContainer> list) {
		if (list != null && list.size() > 0) {
			try {
				if (table.getItemCount() > 0)
					table.removeAll();
				TableItem ti = null;
				Container container = null;
				Double newLength = 0.0;
				Difference diff = null;
				String diffName = "";

				for (InventoryContainer ic : list) {
					int i = 0;
					ti = new TableItem(table, SWT.BORDER);
					container = ic.getContainer();
					ti.setText(i, String.format("%s - %s",
							container.getLotCode(), container.getIdentifier()));
					ti.setText(++i, container.getItem().getItemName());
					ti.setText(++i,
							String.valueOf(container.getPerItemAvgLength()));
					ti.setText(++i, container.getItemCount().toString());
					ti.setText(++i, String.format("%s%s%s", container
							.getLocation().getRow(), container.getLocation()
							.getColumn(), container.getLocation().getDepth()));

					diff = ic.getDifferenceName();
					int bgColor, foreColor;

					switch (diff) {
						case Surplus:
							diffName = "EXCEDENTE";
							bgColor = SWT.COLOR_WHITE;
							foreColor = SWT.COLOR_GREEN;
							newLength = (container.getItemCount() + ic
									.getDifferenceCount())
									* container.getPerItemAvgLength();
							break;

						case Ullage:
							diffName = "MERMA";
							bgColor = SWT.COLOR_WHITE;
							foreColor = SWT.COLOR_RED;
							newLength = (container.getItemCount() - ic
									.getDifferenceCount())
									* container.getPerItemAvgLength();
							break;

						default:
							diffName = "Ninguna";
							bgColor = SWT.COLOR_WHITE;
							foreColor = SWT.COLOR_BLACK;
							newLength = (container.getItemCount())
									* container.getPerItemAvgLength();
							break;
					}

					ti.setText(++i, diffName);
					ti.setBackground(i, display.getSystemColor(bgColor));
					ti.setForeground(i, display.getSystemColor(foreColor));

					ti.setText(++i, ic.getDifferenceCount().toString());

					ti.setText(++i, String.valueOf(newLength));

					ti.setText(
							++i,
							String.valueOf(ic.getDifferenceCount()
									* ic.getContainer().getPerItemAvgLength()));

					ti.setData("object", ic);
				}

				ti = null;
				newLength = null;
				diff = null;
				diffName = null;
				container = null;
				lblInfo.setText("Listado completo.");
			} catch (Exception ex) {
				log.error("Failed to fill inventory data: ", ex);
			}
		} else {
			lblInfo.setText("Sin resultados.");
		}
	}

	private List<InventoryContainer> filter() {
		List<InventoryContainer> list = null;
		try {
			Disjunction con = Restrictions.disjunction();
			String dname = "";

			if (FILTER_SHOW_ULLAGE == (filter & FILTER_SHOW_ULLAGE)) {
				dname = InventoryContainer.Difference.Ullage.toString();
				log.debug("diff name: {}", dname);
				con.add(Restrictions.eq("differenceName", dname));
			}

			if (FILTER_SHOW_SURPLUS == (filter & FILTER_SHOW_SURPLUS)) {
				dname = InventoryContainer.Difference.Surplus.toString();
				log.debug("diff name: {}", dname);
				con.add(Restrictions.eq("differenceName", dname));
			}

			if (FILTER_SHOW_SQUARE == (filter & FILTER_SHOW_SQUARE)) {
				dname = InventoryContainer.Difference.None.toString();
				log.debug("diff name: {}", dname);
				con.add(Restrictions.eq("differenceName", dname));
			}

			if (con != null) {
				log.debug("Conjunction not null!");
				list = invContMgr.list(InventoryContainer.class, con);
			} else {
				log.info("No filter selected; returning full list.");
				list = invContMgr.list(InventoryContainer.class);
			}

			con = null;
		} catch (Exception ex) {
			log.error("Could not filter inventory data.", ex);
		}

		return list;
	}

	/**
	 * 
	 * @return
	 */
	private InventoryContainer getSelectedTableItem() {
		InventoryContainer entity = null;

		if (table.getSelectionCount() > 0) {
			TableItem item = table.getSelection()[0];

			if (item != null) {
				try {
					Object o = item.getData("object");

					if (o != null)
						entity = (InventoryContainer) o;
				} catch (Exception ex) {
					log.error(
							"Exception caught retrieving selected inventory container.",
							ex);
				}
			}
		}

		return entity;
	}

	/**
	 * Handles the add inventory container popup menu click.
	 */
	private void onAddInventoryContainer() {
		shell.setEnabled(false);

		try {
			InventoryContainerEditorView view = new InventoryContainerEditorView(
					shell, SWT.NONE, null, inventory);
			Boolean r = (Boolean) view.open();

			if (r != null) {
				if (r) {
					MessageDialog.openInformation(shell, "Elemento Creado",
							"Cambios guardados sin problemas.");
					fillTable(filter());
				} else {
					MessageDialog
							.openError(
									shell,
									"Falló la Operación",
									"No pudimos crear el elemento. Revise el registro de errores para obtener más información.");
				}
			} else {
				log.info("Adding inventory container item was cancelled by the user.");
			}

			r = null;
			view = null;
		} catch (Exception ex) {
			log.error("Adding inventory container failed.", ex);
			MessageDialog
					.openError(
							shell,
							"Error",
							"Ocurrió un error al procesar la petición. Revise el registro de errores para mayor información.");
		}

		shell.setEnabled(true);
	}

	/**
	 * Handles the edit inventory container popup menu click.
	 */
	private void onEditInventoryContainer() {
		InventoryContainer entity = getSelectedTableItem();

		if (entity != null) {
			try {
				shell.setEnabled(false);
				InventoryContainerEditorView view = new InventoryContainerEditorView(
						shell, SWT.NONE, entity, null);
				Boolean r = (Boolean) view.open();

				if (r != null) {
					if (r) {
						MessageDialog.openInformation(shell,
								"Elemento Modificado",
								"Cambios guardados sin problemas.");
						fillTable(filter());
					} else {
						MessageDialog
								.openError(
										shell,
										"Falló el Proceso",
										"No pudimos guardar los cambios. Revise el registro de errores para obtener más información.");
					}
				} else {
					log.info("Inventory container edition cancelled.");
				}

				r = null;
				view = null;
				fillTable(filter());
			} catch (Exception ex) {
				log.error(
						"Exception caught while processing inventory container edit.",
						ex);
				MessageDialog
						.openError(
								shell,
								"Error",
								"Ocurrió un error al procesar la petición. Resvise el registro de errores para mayor información.");
			} finally {
				shell.setEnabled(true);
			}
		}

		entity = null;
	}

	private void onExportToExcel() {
		if (table.getItemCount() > 0) {
			try {
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String sheetName = String.format(
						"Inventario %s - %s",
						inventory.getStartDate() != null ? df.format(inventory
								.getStartDate()) : "",
						inventory.getEndDate() != null ? df.format(inventory
								.getEndDate()) : "");

				if (spreadsheet == null)
					spreadsheet = new InventorySpreadsheet(table, sheetName,
							inventory);

				FileDialog dlg = new FileDialog(shell, SWT.SAVE);
				dlg.setFilterExtensions(new String[] { "*."
						+ WorkbookBuilder.EXT_MS_XLSX + ";" });
				dlg.setFilterNames(new String[] { "Formato de Excel 2007" });
				dlg.setFilterIndex(0);
				dlg.setFileName(sheetName);
				String fileName = dlg.open();

				if (fileName != null) {
					spreadsheet.setFileExtension(WorkbookBuilder.EXT_MS_XLSX);
					spreadsheet.format();

					if (spreadsheet.saveAs(new File(String.format("%s.%s",
							fileName, WorkbookBuilder.EXT_MS_XLSX)))) {
						MessageDialog.openInformation(shell,
								"Archivo Guardado",
								"El archivo fue guardado sin problemas.");
						spreadsheet = null;
					} else {
						MessageDialog
								.openError(
										shell,
										"Error",
										"No pudimos guardar el archivo. Revise el registro de errores para obtener más información.");
					}
				}

				dlg = null;
				df = null;
				sheetName = null;
				fileName = null;
			} catch (Exception ex) {
				log.error("Failed to export data to spreadsheet.", ex);
			}
		}
	}

	/**
	 * Handles the change of any filter.
	 */
	private void onFilterChange() {
		compBottom.setEnabled(false);
		grpFilters.setEnabled(false);

		filter = 0;
		filter += btnShowUllage.getSelection() ? FILTER_SHOW_ULLAGE : 0;
		filter += btnShowSurplus.getSelection() ? FILTER_SHOW_SURPLUS : 0;
		filter += btnShowSquare.getSelection() ? FILTER_SHOW_SQUARE : 0;
		fillTable(filter());

		compBottom.setEnabled(true);
		grpFilters.setEnabled(true);
	}

	/**
	 * Disables / Enables the menu items depending on the inventory state.
	 * 
	 * @param e
	 *            Event data.
	 */
	private void onTableMenuShown(MenuEvent e) {
		Menu menu = (Menu) e.getSource();
		MenuItem[] items = menu.getItems();

		if (items != null && items.length > 0) {

			for (MenuItem item : items) {
				try {
					changeMenuItemState(item);
				} catch (Exception ex) {
					log.error(
							"Exception caught while changing table menu item state.",
							ex);
				}
			}
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		display = getParent().getDisplay();
		createContents();
		shell.open();
		shell.layout();
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return result;
	}

	/**
	 * Updates the container data with the finished inventory.
	 */
	private void updateContainerData() {
		btnUpdateData.setEnabled(false);
		boolean success = true;

		if (inventory.getEndDate() != null) {
			if (inventory.getDataUpdated() == 0) {
				for (TableItem ti : table.getItems()) {
					InventoryContainer ic = (InventoryContainer) ti
							.getData("object");

					if (ic.getDifferenceName() != InventoryContainer.Difference.None) {
						Container container = ic.getContainer();
						int count = container.getItemCount();

						switch (ic.getDifferenceName()) {
							case Ullage:
								count -= ic.getDifferenceCount();
								break;
							case Surplus:
								count += ic.getDifferenceCount();
								break;
							default:
								log.warn("Unknow difference name: {}", ic
										.getDifferenceName().toString());
								break;
						}

						container.setItemCount(count);

						if (new ContainerManager().update(container)) {
							log.info("Container {} updated!", container);
							success &= true;
						} else {
							log.warn("Failed to update container {}", container);
							success &= false;
						}
					}
				}

				if (success) {
					try {
						inventory.setDataUpdated((byte) 1);

						if (new InventoryManager().update(inventory)) {
							MessageDialog
									.openInformation(shell,
											"Inventario Actualizado",
											"El inventario ha sido actualizado sin problemas.");
						} else {
							MessageDialog
									.openError(
											shell,
											"Falló Actualización",
											"No se pudo actualizar el inventario. Revise el registro de errores para mayor información.");
							btnUpdateData.setEnabled(true);
						}
					} catch (Exception ex) {
						log.error("Failed to modify inventory data: ", ex);
						MessageDialog
								.openError(
										shell,
										"Falló Actualización",
										"No se pudo actualizar el inventario. Revise el registro de errores para mayor información.");
						btnUpdateData.setEnabled(true);
					}
				}
			} else {
				MessageDialog
						.openInformation(
								shell,
								"Información Corregida",
								"Ya se utilizó esta información para corregir el inventario, por lo que no se puede utilizar nuevamente.");
			}
		} else {
			MessageDialog
					.openInformation(
							shell,
							"Inventario en Progreso",
							"No puede corregir la información mientras el inventario está en progreso. Por favor, finalice el inventario y luego corrija la información.");
		}
	}
}
