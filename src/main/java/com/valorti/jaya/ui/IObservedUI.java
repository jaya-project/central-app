package com.valorti.jaya.ui;

import com.valorti.jaya.ui.events.IUserInterfaceEventListener;

public interface IObservedUI {
	public void onUIAction();
	
	public boolean addUIListener(IUserInterfaceEventListener listener);
	
	public boolean removeUIListener(IUserInterfaceEventListener listener);
}
