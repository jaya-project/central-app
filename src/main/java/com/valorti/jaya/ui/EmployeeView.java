package com.valorti.jaya.ui;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.manager.EmployeeManager;
import com.valorti.ui.utils.FillTable;

public class EmployeeView extends TitleAreaDialog {
	private final Logger	log				= LoggerFactory
													.getLogger(EmployeeView.class);
	private Text			tbSearch;
	private Table			table;
	private final byte		FIRST_NAME		= 0x1;
	private final byte		LAST_NAME		= 0x2;
	private final byte		NICKNAME		= 0x3;
	private final byte		FIRST_LAST_NAME	= 0x4;
	private byte			filter			= FIRST_LAST_NAME;

	private boolean			hideAdminUser	= true;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public EmployeeView(Shell parentShell, boolean hideAdminUser) {
		super(parentShell);
		this.hideAdminUser = hideAdminUser;
	}

	private Menu addPopupMenu(Control parent, SelectionListener[] listeners) {
		Menu popup = new Menu(parent);
		MenuItem miNew = new MenuItem(popup, SWT.NONE);
		miNew.setText("Nuevo");
		miNew.setID(1);
		miNew.addSelectionListener(listeners[0]);

		MenuItem miEdit = new MenuItem(popup, SWT.NONE);
		miEdit.setText("Editar");
		miEdit.setID(2);
		miEdit.addSelectionListener(listeners[1]);

		MenuItem miDelete = new MenuItem(popup, SWT.NONE);
		miDelete.setText("Eliminar");
		miDelete.setID(3);
		miDelete.addSelectionListener(listeners[2]);

		return popup;
	}

	private String[] buildSearchExpression(String search) {
		String[] r = new String[2];

		try {
			String[] split = search.split(" ");

			if (split.length == 4) {
				r[0] = String.format("%s %s", split[0], split[1]);
				r[1] = String.format("%s %s", split[2], split[3]);
			} else if (split.length == 3) {
				r[0] = split[0].trim();
				r[1] = String.format("%s %s", split[1].trim(), split[2].trim());
			} else if (split.length == 2) {
				r[0] = split[0];
				r[1] = split[1];
			} else {
				r[0] = split[0];
				r[1] = split[0];
			}
		} catch (Exception ex) {
			log.error("Could not split search string: ", ex);
			r[0] = search;
			r[1] = search;
		}

		return r;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID, "Cerrar", false);
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitleImage(null);
		setTitle("Administrador de Empleados");
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Label lblBuscar = new Label(container, SWT.NONE);
		lblBuscar.setFont(SWTResourceManager
				.getFont("Segoe UI", 10, SWT.NORMAL));
		lblBuscar.setBounds(10, 8, 46, 23);
		lblBuscar.setText("Buscar:");

		tbSearch = new Text(container, SWT.BORDER);
		tbSearch.setBounds(62, 7, 104, 23);

		final Combo filterCombo = new Combo(container, SWT.READ_ONLY);
		filterCombo.setItems(new String[] { "Nombre(s) y Apellido(s)",
				"Nombre(s)", "Apellido(s)", "Apodo" });
		filterCombo.setBounds(198, 7, 156, 23);
		filterCombo.select(0);
		filterCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				switch (filterCombo.getSelectionIndex()) {
					case 0:
						filter = FIRST_LAST_NAME;
						break;

					case 1:
						filter = FIRST_NAME;
						break;

					case 2:
						filter = LAST_NAME;
						break;

					case 3:
						filter = NICKNAME;
						break;
				}
			}
		});

		Label lblEn = new Label(container, SWT.NONE);
		lblEn.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		lblEn.setBounds(172, 8, 20, 15);
		lblEn.setText("en");

		table = new Table(container, SWT.BORDER | SWT.FULL_SELECTION);
		table.addMenuDetectListener(new MenuDetectListener() {

			@Override
			public void menuDetected(MenuDetectEvent e) {
				Table t = (Table) e.getSource();
				int index = t.getSelectionIndex();

				if (index >= 0) {
					t.getMenu().getItem(1).setEnabled(true);
					t.getMenu().getItem(2).setEnabled(true);
				} else {
					t.getMenu().getItem(1).setEnabled(false);
					t.getMenu().getItem(2).setEnabled(false);
				}
			}

		});
		table.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Table t = (Table) e.getSource();
				int index = t.getSelectionIndex();

				if (index >= 0) {
					TableItem ti = t.getItem(index);
					log.debug("Widget Selected: {}", ti.getData("Id"));
				}
			}
		});

		table.setHeaderVisible(true);
		table.setBounds(10, 37, 624, 305);
		table.setMenu(addPopupMenu(table, new SelectionListener[] {
				new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						onRegisterNewEmployee();
					}

				}, new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						onEditEmployee();
					}

				}, new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						onDeleteEmployee();
					}
				} }));

		TableColumn tblclmnNombres = new TableColumn(table, SWT.NONE);
		tblclmnNombres.setWidth(180);
		tblclmnNombres.setText("Nombre(s)");
		tblclmnNombres.setData("getFirstName");

		TableColumn tblclmnApellidos = new TableColumn(table, SWT.NONE);
		tblclmnApellidos.setWidth(180);
		tblclmnApellidos.setText("Apellido(s)");
		tblclmnApellidos.setData("getLastName");

		TableColumn tblclmnApodo = new TableColumn(table, SWT.NONE);
		tblclmnApodo.setWidth(155);
		tblclmnApodo.setText("Login");
		tblclmnApodo.setData("getNickname");

		TableColumn tblclmnRut = new TableColumn(table, SWT.NONE);
		tblclmnRut.setWidth(105);
		tblclmnRut.setText("Rut");
		tblclmnRut.setData("getRut");

		fillEmployees(filterResults(null));

		Button btnFiltrar = new Button(container, SWT.NONE);
		btnFiltrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				fillEmployees(filterResults(tbSearch.getText()));
			}
		});
		btnFiltrar.setBounds(360, 5, 109, 25);
		btnFiltrar.setText("Aplicar");

		Button btnNewEmployee = new Button(container, SWT.NONE);
		btnNewEmployee.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onRegisterNewEmployee();
			}
		});
		btnNewEmployee.setBounds(495, 6, 139, 25);
		btnNewEmployee.setText("Registrar Empleado");

		Label label = new Label(container, SWT.SEPARATOR | SWT.VERTICAL);
		label.setBounds(475, 0, 14, 37);

		return area;
	}

	private void fillEmployees(List<Employee> employees) {
		if (table.getItemCount() > 0) {
			table.removeAll();
		}

		if (employees != null && employees.size() > 0) {
			Main.changeCursor(SWT.CURSOR_WAIT);

			try {
				FillTable<Employee> ft = new FillTable<Employee>(employees,
						table);
				Display.getDefault().syncExec(ft);
			} catch (Exception ex) {
				log.error("Failed to fill employee table: ", ex);
			} finally {
				Main.changeCursor(SWT.CURSOR_ARROW);
			}
		}
	}

	private List<Employee> filterResults(String search) {
		log.debug("Filter Results: [search = \"{}\"]", search);
		EmployeeManager em = new EmployeeManager();
		List<Employee> list = null;
		boolean showMsg = true;

		try {
			Conjunction c = null;

			if (search == null || search.length() <= 0) {
				log.info("Search was empty - retrieving full list.");
				showMsg = false;

				if (hideAdminUser) {
					list = em.list(Employee.class,
							Restrictions.ne("nickname", "admin"));
				} else {
					list = em.list(Employee.class);
				}
			} else {
				c = Restrictions.conjunction();

				if (hideAdminUser) {
					c.add(Restrictions.ne("nickname", "admin"));
				}

				if (filter == FIRST_NAME) {
					log.debug("Filter by first name");
					c.add(Restrictions.like("firstName", "%" + search + "%"));
				} else if (filter == LAST_NAME) {
					log.debug("Filter by last name");
					c.add(Restrictions.like("lastName", "%" + search + "%"));
				} else if (filter == NICKNAME) {
					log.debug("Filter by nickname");
					c.add(Restrictions.like("nickname", "%" + search + "%"));
				} else if (filter == FIRST_LAST_NAME) {
					log.debug("Filter by first and last name");
					String[] sp = buildSearchExpression(search);
					c.add(Restrictions.or(
							Restrictions.like("firstName", "%" + sp[0] + "%"),
							Restrictions.like("lastName", "%" + sp[1] + "%")));
				}

				if (c != null)
					list = em.list(Employee.class, c);
			}
		} catch (Exception ex) {
			log.error("Failed to retrieve employee list: {}", ex);
		}

		if (showMsg) {
			MessageDialog.openInformation(this.getShell(), "No hay Resultados",
					"No se encontró lo que buscaba.");
		}

		return list;
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(650, 500);
	}

	private void onDeleteEmployee() {
		int index = table.getSelectionIndex();

		if (index >= 0) {
			MessageDialog dlg = new MessageDialog(this.getShell(),
					"Confirmación", null,
					"¿Está seguro de que desea eliminar este empleado?",
					MessageDialog.QUESTION, new String[] { "No", "Si" }, 0);
			int rcode = dlg.open();
			log.debug("Return code: {}", rcode);

			if (rcode == 1) {
				Employee employee = (Employee) table.getItem(index).getData(
						"Id");
				log.info("Deleting employee {}", employee);
				EmployeeManager mgr = new EmployeeManager();

				if (mgr.delete(employee)) {
					log.info("Employee deleted.");
					fillEmployees(filterResults(null));
				} else {
					MessageDialog
							.openError(
									this.getShell(),
									"Error",
									"Ocurrió un error intentando eliminar al empleado. Revise el registro de errores para mayor información.");
				}

				mgr = null;
				employee = null;
			}

			dlg = null;
		}
	}

	private void onEditEmployee() {
		int index = table.getSelectionIndex();

		if (index >= 0) {
			Employee employee = (Employee) new EmployeeManager().getById(
					Employee.class,
					((Employee) table.getItem(index).getData("Id")).getId());
			EmployeeEditorView view = new EmployeeEditorView(this.getShell(),
					employee);
			int code = view.open();
			parseEditorResponse(code);
		}
	}

	private void onRegisterNewEmployee() {
		EmployeeEditorView view = new EmployeeEditorView(this.getShell());
		int code = view.open();
		parseEditorResponse(code);
	}

	private void parseEditorResponse(int code) {
		if (code == SWT.OK) {
			MessageDialog.openInformation(this.getShell(), "Cambios Guardados",
					"Los cambios se guardaron sin problemas.");
			fillEmployees(filterResults(null));
		} else if (code == SWT.CANCEL) {
			// Nothing to do...
		} else {
			// Something went wrong...
			MessageDialog
					.openError(
							this.getShell(),
							"Error",
							"Ocurrió un error al guardar los cambios. Revise el registro de errores para mayor información.");
		}
	}
}
