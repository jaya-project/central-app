package com.valorti.jaya.ui;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.HelpEvent;
import org.eclipse.swt.events.HelpListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Capability;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.EmployeeCapabilities;
import com.valorti.jaya.model.manager.CapabilityManager;
import com.valorti.jaya.model.manager.EmployeeManager;
import com.valorti.utils.SessionHelper;

public class EmployeeEditorView extends TitleAreaDialog {
	protected boolean		isEdit		= false;
	private final Logger	log			= LoggerFactory
												.getLogger(EmployeeEditorView.class);
	private Employee		employee	= null;
	private Text			tbFirstName;
	private Text			tbLastName;
	private Text			tbNickname;
	private Text			tbRut;
	private Text			tbPassword;
	private Text			tbPasswordConfirm;
	private Table			tblCapability;

	/**
	 * @wbp.parser.constructor
	 */
	public EmployeeEditorView(Shell parent) {
		super(parent);
		setShellStyle(SWT.BORDER);
	}

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public EmployeeEditorView(Shell parentShell, Employee employee) {
		super(parentShell);
		
		if (employee != null) {
			isEdit = true;
			this.employee = employee;
		} else {
			MessageDialog
					.openError(
							this.getShell(),
							"Datos Nulos",
							"El usuario suministrado no existe o es nulo, por lo que no se puede continuar.");
			setReturnCode(SWT.ABORT);
			close();
		}
	}

	private void addCapabilities(EmployeeManager em) {
		log.debug(employee.toString());
		logEC(employee.getEmployeeCapabilities());

		for (TableItem ti : tblCapability.getItems()) {
			Capability c = (Capability) ti.getData("Capability");

			EmployeeCapabilities ec = new EmployeeCapabilities();
			ec.setCapability(c);
			ec.setEmployee(employee);
			ec.setAccess(6);

			if (ti.getChecked()) {
				if (!employee.getEmployeeCapabilities().contains(ec)) {
					log.debug("Is not contained - trying to add ec.");
					employee.getEmployeeCapabilities().add(ec);
				}
			} else {
				if (employee.getEmployeeCapabilities().contains(ec)) {
					employee.getEmployeeCapabilities().remove(ec);
					log.debug("Is contained - deleting.");
				} else {
					log.debug("Skipped for deletion.");
				}
			}
		}
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.OK_ID, "Guardar",
				true);
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				Button b = (Button) e.getSource();
				b.setEnabled(false);
				onSaveBtnMouseUp();

				if (!b.isDisposed())
					b.setEnabled(true);
			}
		});
		button.setSelection(true);

		Button button_1 = createButton(parent, IDialogConstants.CANCEL_ID,
				"Cancelar", false);
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {

			}
		});
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setMessage("Ingrese la información solicitada y luego presione el botón Guardar.");
		setTitle("Editor de Empleados");
		Composite container = (Composite) super.createDialogArea(parent);
		container.setFont(SWTResourceManager.getFont("Arial", 11, SWT.ITALIC));
		container.setLayout(null);
		container.addHelpListener(new HelpListener() {
			@Override
			public void helpRequested(HelpEvent e) {
				log.debug("Help requested!");
			}
		});

		CheckboxTableViewer checkboxTableViewer = CheckboxTableViewer
				.newCheckList(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblCapability = checkboxTableViewer.getTable();
		tblCapability.setHeaderVisible(true);
		tblCapability.setBounds(242, 32, 394, 142);

		TableViewerColumn tableViewerColumn = new TableViewerColumn(
				checkboxTableViewer, SWT.NONE);
		TableColumn tblclmnPermiso = tableViewerColumn.getColumn();
		tblclmnPermiso.setAlignment(SWT.CENTER);
		tblclmnPermiso.setWidth(390);
		tblclmnPermiso.setText("Permiso");

		fillCapabilityList(tblCapability);

		Label lblNombres = new Label(container, SWT.NONE);
		lblNombres.setLocation(10, 10);
		lblNombres.setSize(64, 20);
		lblNombres.setText("Nombre(s):");

		tbFirstName = new Text(container, SWT.BORDER);
		tbFirstName.setBounds(80, 7, 130, 21);

		Label lblPermisos = new Label(container, SWT.NONE);
		lblPermisos.setLocation(242, 10);
		lblPermisos.setSize(247, 20);
		lblPermisos.setText("Seleccione los permisos para el empleado:");

		Label lblApellidos = new Label(container, SWT.NONE);
		lblApellidos.setLocation(10, 36);
		lblApellidos.setSize(64, 21);
		lblApellidos.setText("Apellido(s):");

		tbLastName = new Text(container, SWT.BORDER);
		tbLastName.setLocation(80, 36);
		tbLastName.setSize(130, 21);

		Label lblApodo = new Label(container, SWT.NONE);
		lblApodo.setLocation(10, 63);
		lblApodo.setSize(64, 21);
		lblApodo.setText("Login:");

		tbNickname = new Text(container, SWT.BORDER);
		tbNickname.setLocation(80, 63);
		tbNickname.setSize(130, 21);

		Label lblRut = new Label(container, SWT.NONE);
		lblRut.setLocation(10, 90);
		lblRut.setSize(64, 21);
		lblRut.setText("Rut:");

		tbRut = new Text(container, SWT.BORDER);
		tbRut.setLocation(80, 90);
		tbRut.setSize(130, 21);

		Label lblPassword = new Label(container, SWT.NONE);
		lblPassword.setLocation(10, 122);
		lblPassword.setSize(64, 21);
		lblPassword.setText("Contraseña:");

		tbPassword = new Text(container, SWT.BORDER | SWT.PASSWORD);
		tbPassword.setLocation(80, 122);
		tbPassword.setSize(130, 21);

		Label label = new Label(container, SWT.SEPARATOR | SWT.VERTICAL);
		label.setBounds(216, 0, 20, 174);

		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setBounds(10, 149, 64, 35);
		lblNewLabel.setText("Repetir\r\nContraseña:");

		tbPasswordConfirm = new Text(container, SWT.BORDER | SWT.PASSWORD);
		tbPasswordConfirm.setBounds(80, 153, 130, 21);

		if (isEdit)
			setData();

		return container;
	}

	private void fillCapabilityList(Table table) {
		CapabilityManager mgr = new CapabilityManager();
		List<Capability> list = mgr.list(Capability.class);

		for (Capability capability : list) {
			TableItem ti = new TableItem(table, SWT.CHECK);
			ti.setData("Capability", capability);
			ti.setText(capability.getDescription());

			if (isEdit) {
				for (EmployeeCapabilities ec : employee
						.getEmployeeCapabilities()) {
					if (ec.getCapability().getId() == capability.getId()) {
						ti.setChecked(true);
						break;
					}
				}
			}
		}
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(652, 333);
	}

	private void logEC(Set<EmployeeCapabilities> list) {
		log.debug("Start logging EC ---");
		for (EmployeeCapabilities ec : list) {
			log.debug("---- {}", ec);
		}
		log.debug("End logging EC ---");
	}

	private boolean onSaveBtnMouseUp() {
		try {
			if (persistEntity()) {
				setReturnCode(SWT.OK);
				this.close();
			}
		} catch (NoSuchAlgorithmException ex) {
			log.error("The algorithm used to encrypt does not exist: ", ex);
			MessageDialog
					.openError(
							this.getShell(),
							"Error",
							"No se encontró el algoritmo para encriptar la contraseña. Contacte a los desarrolladores de la aplicación.");
		}

		return false;
	}

	private boolean persistEntity() throws NoSuchAlgorithmException {
		boolean saved = false;

		if (validateData()) {
			if (!isEdit)
				employee = new Employee();

			employee.setFirstName(tbFirstName.getText());
			employee.setLastName(tbLastName.getText());
			employee.setNickname(tbNickname.getText());
			employee.setRut(tbRut.getText());
			SessionHelper sh = new SessionHelper();
			employee.setSalt(sh.makeSalt());
			byte[] hash = sh.encryptPassword(tbPassword.getText(),
					employee.getSalt());
			employee.setPassword(hash);
			EmployeeManager em = new EmployeeManager();
			String response = "", title = "";
			addCapabilities(em);

			try {
				if (!isEdit) {
					Serializable s = em.add(employee);

					if (s != null) {
						// addCapabilities(em);
						log.info("Employee saved.");
						response = "El empleado fue registrado exitosamente.";
						title = "Creado";
						saved = true;
					} else {
						response = "Falló el registro de empleado.";
						title = "Registro Cancelado";
					}
				} else {
					if (em.update(employee)) {
						// addCapabilities(em);
						log.info("Employee updated.");
						response = "Los cambios fueron guardados.";
						title = "Modificado";
						saved = true;
					} else {
						response = "No se pudieron guardar los cambios.";
						title = "Falló Modificación";
					}
				}

				MessageDialog.openInformation(this.getShell(), title, response);
			} catch (Exception ex) {
				log.error("Failed to save employee: ", ex);
				response = "Ocurrió un error intentando guardar los cambios. Revise el registro de errores para mayor información.";
				title = "Error";
				MessageDialog.openError(this.getShell(), title, response);
			}
		}

		return saved;
	}

	private void setData() {
		tbFirstName.setText(employee.getFirstName());
		tbLastName.setText(employee.getLastName());
		tbNickname.setText(employee.getNickname());
		tbRut.setText(employee.getRut());
	}

	private boolean validateData() {
		EmployeeManager am = new EmployeeManager();

		if (tbFirstName.getText().length() < 3) {
			this.setErrorMessage("Por favor, ingrese un nombre que contenga al menos 3 letras.");
			return false;
		}

		if (tbLastName.getText().length() < 3) {
			this.setErrorMessage("Por favor, ingrese al menos un apellido de al menos 3 letras.");
			return false;
		}

		String password = tbPassword.getText(), passwordConfirm = tbPasswordConfirm
				.getText();

		if (!isEdit) {
			if (password.length() == 0) {
				this.setErrorMessage("La contraseña no puede estar vacía.");
				return false;
			}

			if (password.length() < 6) {
				this.setErrorMessage("La contraseña debe contener al menos 6 caracteres.");
				return false;
			}
		}

		int iCompare = password.compareTo(passwordConfirm);

		if (!isEdit || (isEdit && password.length() > 0)) {
			if (iCompare > 0 || iCompare < 0) {
				this.setErrorMessage("Las contraseñas no coinciden.");
				return false;
			}
		}

		Employee tmp = null;

		if (!isEdit) {
			tmp = am.findByFirstAndLastName(tbFirstName.getText(),
					tbLastName.getText());

			if (tmp != null) {
				this.setErrorMessage("El usuario ya existe! En lugar de crear un nuevo usuario puede modificar el que ya existe.");
				return false;
			}

			if (tbNickname.getText().length() < 3) {
				this.setErrorMessage("El apodo debe contener al menos 3 caracteres.");
				return false;
			}

			tmp = am.findByNickname(tbNickname.getText());

			if (tmp != null) {
				this.setErrorMessage("Ya existe un empleado con ese apodo. Por favor cree uno nuevo.");
				return false;
			}
		} else {
			int compare1 = tbFirstName.getText().compareTo(
					employee.getFirstName());
			int compare2 = tbLastName.getText().compareTo(
					employee.getLastName());

			if (compare1 > 0 || compare1 < 0 && compare2 > 0 || compare2 < 0) {
				tmp = am.findByFirstAndLastName(tbFirstName.getText(),
						tbLastName.getText());

				if (tmp != null) {
					this.setErrorMessage("Ya existe otro empleado con ese mismo nombre y apellidos.");
					return false;
				}
			}

			compare1 = tbNickname.getText().compareTo(employee.getNickname());

			if (compare1 > 0 || compare1 < 0) {
				tmp = am.findByNickname(tbNickname.getText());

				if (tmp != null) {
					this.setErrorMessage("Ya existe un empleado con ese apodo. Por favor, elija otro.");
					return false;
				}
			}
		}

		tmp = null;

		return true;
	}
}
