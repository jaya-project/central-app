package com.valorti.jaya.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.ItemMovement;
import com.valorti.jaya.model.Location;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.ItemMovementManager;
import com.valorti.jaya.model.manager.LocationManager;
import com.valorti.ui.utils.UIHelper;

public class ExtractProductView extends Dialog {
	private Logger						log				= LoggerFactory
																.getLogger(ExtractProductView.class);
	private Container					container		= null;
	private final LocationManager		locationMgr		= new LocationManager();
	private final ItemMovementManager	itemMoveMgr		= new ItemMovementManager();
	private final ContainerManager		containerMgr	= new ContainerManager();

	protected boolean					result;
	protected Shell						shell;
	private Text						tLotCode;
	private Text						tIdentifier;
	private Text						tCurrentItemCount;
	private Text						tCurrentWarehouse;
	private Text						tLocation;
	private Combo						cbRow;
	private Combo						cbWarehouse;
	private Spinner						spinRollsToExtract;
	private Spinner						spinColumn;
	private Spinner						spinDepth;
	private Text						txtRollsLeft;
	private Text						tItemName;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public ExtractProductView(Shell parent, int style, Container container)
			throws NullPointerException {
		super(parent, style);
		result = false;

		if (container != null)
			this.container = container;
		else
			throw new NullPointerException(
					"The container object cannot be null.");
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setText("Extracción de Rollos");
		shell.setSize(342, 503);
		shell.setLayout(new GridLayout(1, false));

		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if (image != null)
			shell.setImage(image);

		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		Group gPallet = new Group(shell, SWT.NONE);
		gPallet.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));
		gPallet.setLayout(new GridLayout(2, false));
		gPallet.setText("1.- Detalles del Pallet");

		Label lblNewLabel = new Label(gPallet, SWT.NONE);
		lblNewLabel.setText("Lote:");

		tLotCode = new Text(gPallet, SWT.BORDER | SWT.READ_ONLY);
		tLotCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel_1 = new Label(gPallet, SWT.NONE);
		lblNewLabel_1.setText("Identificador:");

		tIdentifier = new Text(gPallet, SWT.BORDER | SWT.READ_ONLY);
		tIdentifier.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_9 = new Label(gPallet, SWT.NONE);
		lblNewLabel_9.setText("Ubicación:");

		tLocation = new Text(gPallet, SWT.BORDER | SWT.READ_ONLY);
		tLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel_2 = new Label(gPallet, SWT.NONE);
		lblNewLabel_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_2.setText("Rollos Actuales:");

		tCurrentItemCount = new Text(gPallet, SWT.BORDER | SWT.READ_ONLY);
		tCurrentItemCount.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));

		Label lblNewLabel_3 = new Label(gPallet, SWT.NONE);
		lblNewLabel_3.setText("Bodega Actual:");

		tCurrentWarehouse = new Text(gPallet, SWT.BORDER | SWT.READ_ONLY);
		tCurrentWarehouse.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));

		Label lblNewLabel_12 = new Label(gPallet, SWT.NONE);
		lblNewLabel_12.setText("Materia Prima:");

		tItemName = new Text(gPallet, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL
				| SWT.MULTI);
		GridData gd_tItemName = new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1);
		gd_tItemName.heightHint = 27;
		tItemName.setLayoutData(gd_tItemName);

		Group gMovementInfo = new Group(shell, SWT.NONE);
		gMovementInfo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		gMovementInfo.setLayout(new GridLayout(2, false));
		gMovementInfo.setText("2.- Ingrese la información del movimiento");

		Label lblNewLabel_4 = new Label(gMovementInfo, SWT.NONE);
		lblNewLabel_4.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_4.setText("¿Cuántos rollos sacará?");

		spinRollsToExtract = new Spinner(gMovementInfo, SWT.BORDER);
		spinRollsToExtract.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateCurrentRollCount();
			}
		});
		GridData gd_spinRollsToExtract = new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1);
		gd_spinRollsToExtract.widthHint = 48;
		spinRollsToExtract.setLayoutData(gd_spinRollsToExtract);
		spinRollsToExtract.setMaximum(0);

		Label lblNewLabel_10 = new Label(gMovementInfo, SWT.NONE);
		lblNewLabel_10.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_10.setText("Rollos Restantes:");

		txtRollsLeft = new Text(gMovementInfo, SWT.BORDER | SWT.READ_ONLY);
		txtRollsLeft.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_5 = new Label(gMovementInfo, SWT.NONE);
		lblNewLabel_5.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_5.setText("Bodega de Destino:");

		cbWarehouse = new Combo(gMovementInfo, SWT.READ_ONLY);
		cbWarehouse.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Group gNewWarehouse = new Group(shell, SWT.NONE);
		gNewWarehouse.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		gNewWarehouse.setLayout(new GridLayout(2, false));
		gNewWarehouse
				.setText("3.- (Opcional) Ingrese la bodega dónde quedarán");

		Label lblNewLabel_6 = new Label(gNewWarehouse, SWT.NONE);
		lblNewLabel_6.setText("Rack:");

		cbRow = new Combo(gNewWarehouse, SWT.READ_ONLY);
		cbRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		Label lblNewLabel_7 = new Label(gNewWarehouse, SWT.NONE);
		lblNewLabel_7.setText("Cuerpo:");

		spinColumn = new Spinner(gNewWarehouse, SWT.BORDER);
		spinColumn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinColumn.setMaximum(99);

		Label lblNewLabel_8 = new Label(gNewWarehouse, SWT.NONE);
		lblNewLabel_8.setText("Estante:");

		spinDepth = new Spinner(gNewWarehouse, SWT.BORDER);
		spinDepth.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinDepth.setMaximum(99);

		Group gControls = new Group(shell, SWT.NONE);
		gControls.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));
		gControls.setLayout(new GridLayout(2, false));
		gControls.setText("4.- Guarde los cambios o cierre la ventana.");

		Button btnSave = new Button(gControls, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSave();
			}
		});
		GridData gd_btnSave = new GridData(SWT.CENTER, SWT.CENTER, false,
				false, 1, 1);
		gd_btnSave.widthHint = 125;
		btnSave.setLayoutData(gd_btnSave);
		btnSave.setText("Guardar");

		Button btnReset = new Button(gControls, SWT.NONE);
		GridData gd_btnReset = new GridData(SWT.RIGHT, SWT.CENTER, true, false,
				1, 1);
		gd_btnReset.widthHint = 133;
		btnReset.setLayoutData(gd_btnReset);
		btnReset.setText("Reiniciar");

		setup();
	}

	/**
	 * Retrieves the existing location or creates a new one.
	 * 
	 * @param warehouse
	 *            The warehouse where the item will be.
	 * @return Null if the location does not exist or failed to create it.
	 *         Otherwise the location object is returned.
	 */
	private Location getLocation(Warehouse warehouse) {
		int iCol = spinColumn.getSelection(), iDepth = spinDepth.getSelection();
		String row = cbRow.getItem(cbRow.getSelectionIndex());
		Location location = locationMgr.getLocation(row, iCol, iDepth,
				warehouse);

		if (location == null) {
			log.error("Failed to retrieve or create the new location.");
		}

		row = null;

		return location;
	}

	/**
	 * Handles the save event and stores the container changes.
	 */
	private void onSave() {
		if (validateUserInput()) {
			try {
				Warehouse warehouse = UIHelper
						.getSelectedWarehouse(cbWarehouse);
				container = (Container) containerMgr.getById(Container.class,
						container.getId());
				ItemMovement itemMove = itemMoveMgr
						.findByContainerAndWarehouse(container, warehouse);
				boolean stop = false;

				if (itemMove != null) {
					itemMove.setItemsMoved(itemMove.getItemsMoved()
							+ spinRollsToExtract.getSelection());

					if (cbRow.getSelectionIndex() > 0) {
						itemMove.setLocation(getLocation(warehouse));
					}

					if (itemMoveMgr.update(itemMove)) {
						stop = false;
						log.info("Update to item move successful.");
					} else {
						stop = true;
						log.info("Update to item move failed.");
					}
				} else {
					itemMove = new ItemMovement();
					itemMove.setContainer(container);
					itemMove.setItemsMoved(spinRollsToExtract.getSelection());
					itemMove.setWarehouse(warehouse);

					if (cbRow.getSelectionIndex() > 0) {
						itemMove.setLocation(getLocation(warehouse));
					}

					stop = itemMoveMgr.add(itemMove) == null;
				}

				if (!stop) {
					container.setItemCount(Integer.parseInt(txtRollsLeft
							.getText()));
					container.getItemMovements().add(itemMove);

					if (containerMgr.update(container)) {
						MessageDialog.openInformation(shell,
								"Cambios Guardados",
								"La información se guardó correctamente.");
						result = true;
					} else {
						MessageDialog.openError(shell, "Error",
								"Falló la actualización del pallet.");
					}
				} else {
					MessageDialog
							.openError(shell, "Error",
									"No se pudo guardar la información del movimiento.");
				}
			} catch (Exception ex) {
				log.error("Failed to save data.", ex);
				MessageDialog
						.openError(shell, "Error",
								"Ocurrió un error al guardar la información. Revise el registro de errores.");
			} finally {
				if (result)
					shell.close();
			}
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public boolean open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	private void setup() {
		try {
			tLotCode.setText(container.getLotCode());
			tIdentifier.setText(container.getIdentifier().toString());

			String tmp = String.format("%s%s%s", container.getLocation()
					.getRow(), container.getLocation().getColumn(), container
					.getLocation().getDepth());
			tLocation.setText(tmp);

			tCurrentItemCount.setText(container.getItemCount().toString());
			spinRollsToExtract.setMaximum(container.getItemCount().intValue());
			spinRollsToExtract.setMinimum(1);
			spinRollsToExtract.setSelection(1);

			tmp = String.format("%s - %S", container.getLocation()
					.getWarehouse().getWhName(), container.getLocation()
					.getWarehouse().getWarehouseCode().getWhCode());
			tCurrentWarehouse.setText(tmp);
			tItemName.setText(container.getItem().getItemName());
			txtRollsLeft.setText(String.valueOf(container.getItemCount() - 1));

			UIHelper.fillWarehouseCombo(cbWarehouse, true);
			UIHelper.fillRowCombo(cbRow, true);
		} catch (Exception ex) {
			log.error("Exception caught while setting up the initial data.", ex);
			shell.close();
		}
	}

	private void updateCurrentRollCount() {
		int left = (int) (container.getItemCount() - spinRollsToExtract.getSelection());
		txtRollsLeft.setText(String.valueOf(left));
	}

	private boolean validateUserInput() {
		boolean r = true;
		int selection = spinRollsToExtract.getSelection();
		StringBuilder msg = new StringBuilder();

		if (selection > spinRollsToExtract.getMaximum()) {
			r &= false;
			msg.append("- No puede extraer más rollos de los que contiene el pallet (la cantidad de rollos a extraer es mayor a la cantidad existente en el pallet).\n");
		}

		if (cbWarehouse.getSelectionIndex() <= 0) {
			r &= false;
			msg.append("- Debe seleccionar una Bodega de Destino.\n");
		}

		if (cbRow.getSelectionIndex() > 0) {
			selection = spinColumn.getSelection();

			if (selection < 0 || selection > 99) {
				r &= false;
				msg.append("- El Cuerpo ingresado no es válido (debe ser un número entre 0 y 99, ambos inclusive).");
			}

			selection = spinDepth.getSelection();

			if (selection < 0 || selection > 99) {
				r &= false;
				msg.append("- El Estante ingresado no es válido (debe ser un número entre 0 y 99, ambos inclusive).");
			}
		}

		if (msg.length() > 0) {
			MessageDialog
					.openInformation(
							shell,
							"Información Errónea",
							"Hay errores en la información ingresada. Por favor corrija los siguientes errores:\n\n"
									+ msg.toString());
		}

		msg = null;

		return r;
	}
}
