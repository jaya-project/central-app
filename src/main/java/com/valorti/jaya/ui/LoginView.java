package com.valorti.jaya.ui;

import java.security.NoSuchAlgorithmException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.manager.EmployeeManager;
import com.valorti.ui.utils.UIHelper;
import com.valorti.utils.SessionHelper;

public class LoginView extends Dialog {
	private final Logger			log		= LoggerFactory
													.getLogger(LoginView.class);
	private final EmployeeManager	empMgr	= new EmployeeManager();

	protected Boolean				result;
	protected Shell					shell;
	private Text					txtLogin;
	private Text					txtPassword;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public LoginView(Shell parent, int style) {
		super(parent, style);
		setText("Inicio de Sesión");
		result = false;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.RESIZE | SWT.APPLICATION_MODAL);
		shell.setSize(237, 127);
		shell.setText("Inicio de Sesión");
		shell.setLayout(new GridLayout(2, false));
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event e) {
				onClose(e);
			}
		});

		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		
		if (image != null)
			shell.setImage(image);

		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				true, 1, 1));
		lblNewLabel.setText("Login:");

		txtLogin = new Text(shell, SWT.BORDER);
		txtLogin.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				1, 1));

		Label lblNewLabel_1 = new Label(shell, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		lblNewLabel_1.setText("Password:");

		txtPassword = new Text(shell, SWT.BORDER | SWT.PASSWORD);
		txtPassword.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				true, 1, 1));
		new Label(shell, SWT.NONE);

		Button btnLogin = new Button(shell, SWT.NONE);
		btnLogin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onLogin(e);
			}
		});
		btnLogin.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				true, 1, 1));
		btnLogin.setText("Aceptar");
	}

	private void onClose(Event e) {
		MessageDialog dialog = new MessageDialog(
				shell,
				"Cerrar Programa",
				null,
				"Si cierra esta ventana, y no inicia sesión, el programa se cerrará\n\n¿Está seguro de querer cerrarlo?",
				MessageDialog.CONFIRM, new String[] { "No, no quiero cerrarlo",
						"Sí, cerrar el programa" }, 0);
		int code = dialog.open();
		log.trace("Dialog response code = {}", code);
		e.doit = code == 1;
	}

	private void onLogin(SelectionEvent e) {
		if (validateUserInput()) {
			String user = txtLogin.getText(), pass = txtPassword.getText();
			Employee employee = empMgr.findByNickname(user);

			if (employee != null) {
				SessionHelper helper = new SessionHelper();

				try {
					byte[] hash = helper.encryptPassword(pass,
							employee.getSalt());

					if (!SessionHelper
							.compareHash(employee.getPassword(), hash)) {
						MessageDialog.openError(shell, "Error",
								"La contraseña ingresada no es válida.");
					} else {
						// TODO - Login user and start session.
						log.info("Login successful.");
						result = true;
						shell.close();
					}
				} catch (NoSuchAlgorithmException ex) {
					log.error(
							"Couldn't find the specified encryption algorithm '{}'.",
							helper.getAlgorithm(), ex);
				}
			} else {
				MessageDialog.openError(shell, "Error",
						"El usuario ingresado no es correcto.");
			}
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Boolean open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	private boolean validateUserInput() {
		StringBuilder msg = new StringBuilder();
		boolean r = true;
		String tmp = txtLogin.getText();

		if (tmp.length() <= 0) {
			msg.append("- El 'Login' no puede estar vacío.");
			r &= false;
		}

		tmp = txtPassword.getText();

		if (tmp.length() <= 0) {
			msg.append("- La contraseña ingresada no es válida.");
			r &= false;
		}

		if (msg.length() > 0) {
			MessageDialog.openInformation(
					shell,
					"Falta Información",
					"Se encontraron los siguientes errores:\n\n"
							+ msg.toString());
		}

		tmp = null;
		msg = null;

		return r;
	}
}
