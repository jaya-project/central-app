package com.valorti.jaya.ui;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.ForeignProvider;
import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.manager.ForeignProviderManager;
import com.valorti.jaya.model.manager.ItemManager;
import com.valorti.ui.utils.UIHelper;

public class ItemEditorView extends Dialog {
	private final Logger					log			= LoggerFactory
																.getLogger(ItemEditorView.class);
	protected Object						result		= 0;
	protected Shell							shell;
	private Text							itemName;
	private Text							itemCode;
	private Combo							foreignProvider;
	private final ForeignProviderManager	fpManager	= new ForeignProviderManager();
	private final ItemManager				itmManager	= new ItemManager();

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public ItemEditorView(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	private boolean create() {
		ForeignProvider fp = getSelectedForeignProvider();

		if (fp != null) {
			Item item = new Item();
			item.setForeignProvider(fp);
			item.setItemCode(itemCode.getText());
			item.setItemName(itemName.getText());

			try {
				return itmManager.add(item) != null;
			} catch (Exception ex) {
				log.error("Failed to create item: ", ex);
				MessageDialog
						.openError(
								shell,
								"Error",
								"No pudimos añadir el producto a la base de datos. Revise el registro de errores para obtener más información.");
			}
		} else {
			MessageDialog
					.openError(
							shell,
							"Error",
							"No pudimos crear el objeto del proveedor extranjero (error técnico). Revise el registro de errores.");
		}

		return false;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM);
		shell.setSize(279, 203);
		shell.setText("Editor de Productos");
		
		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if(image != null)
			shell.setImage(image);
		
		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 14,
				SWT.NORMAL));
		lblNewLabel.setBounds(10, 10, 362, 25);
		lblNewLabel.setText("Información del Producto");

		Label lblNewLabel_1 = new Label(shell, SWT.NONE);
		lblNewLabel_1.setBounds(10, 44, 55, 15);
		lblNewLabel_1.setText("Nombre:");

		itemName = new Text(shell, SWT.BORDER);
		itemName.setBounds(117, 41, 143, 21);

		Label lblNewLabel_2 = new Label(shell, SWT.NONE);
		lblNewLabel_2.setBounds(10, 73, 89, 15);
		lblNewLabel_2.setText("Código Softland:");

		itemCode = new Text(shell, SWT.BORDER);
		itemCode.setBounds(117, 70, 143, 21);

		Label lblNewLabel_3 = new Label(shell, SWT.NONE);
		lblNewLabel_3.setBounds(10, 100, 101, 15);
		lblNewLabel_3.setText("Proveedor Alemán:");

		foreignProvider = new Combo(shell, SWT.READ_ONLY);
		foreignProvider.setBounds(117, 97, 143, 23);
		fillForeignProviderCombo();

		Button btnCreate = new Button(shell, SWT.NONE);
		btnCreate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onCreate();
			}
		});
		btnCreate.setBounds(185, 139, 75, 25);
		btnCreate.setText("Crear");

		Button btnCancel = new Button(shell, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onCancel();
			}
		});
		btnCancel.setBounds(10, 139, 75, 25);
		btnCancel.setText("Cancelar");

	}

	private void fillForeignProviderCombo() {
		List<ForeignProvider> list = fpManager.list(ForeignProvider.class);

		for (ForeignProvider fp : list) {
			try {
				foreignProvider.add(String.format("%s - %s", fp.getName(),
						fp.getCode()));
			} catch (Exception ex) {
				log.error("Failed to add foreign provider {} to combo: {}", fp,
						ex);
			}
		}
	}

	private ForeignProvider getSelectedForeignProvider() {
		ForeignProvider fp = null;
		String[] split = null;

		try {
			int index = foreignProvider.getSelectionIndex();
			split = foreignProvider.getItem(index).split("-");
		} catch (Exception ex) {
			log.error("Failed to split foreign provider string: ", ex);
		}

		if (split != null) {
			try {
				fp = fpManager.findByCode(split[1].trim());
			} catch (Exception ex) {
				log.error("Failed to retrieve foreign provider: ", ex);
			}
		}

		return fp;
	}

	private void onCancel() {
		log.info("Item creation cancelled by the user.");
		result = 0;
		shell.close();
	}

	private void onCreate() {
		if (validate()) {
			if (create()) {
				log.info("Item successfully created. Trying to close window.");
				result = 1;
				shell.close();
			}
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return result;
	}

	private boolean validate() {
		log.info("Validating item data.");

		if (itemName.getText().length() <= 0) {
			log.debug("The item name provided is less than 0.");
			MessageDialog.openInformation(shell,
					"Información Errónea",
					"El nombre del producto no puede estar vacío.");
			itemName.setFocus();
			return false;
		} else {
			String name = itemName.getText();

			if (itmManager.findByName(name) != null) {
				log.error("An item with name {} already exists.", name);
				MessageDialog.openError(shell, "Producto Existente",
						"Ya existe un producto con ese nombre.");
				return false;
			}

			name = null;
		}

		if (itemCode.getText().length() <= 0) {
			log.debug("The item code is not valid.");
			MessageDialog.openInformation(shell,
					"Información Errónea",
					"El código Softland no puede estar vacío.");
			itemCode.setFocus();
			return false;
		} else {
			String code = itemCode.getText();

			if (itmManager.findByCode(code) != null) {
				log.error("An item with the code {} already exists.", code);
				MessageDialog
						.openError(shell, "Producto Existente",
								"Ya existe un producto con ese código. Por favor elija otro.");
				return false;
			}

			code = null;
		}

		if (foreignProvider.getSelectionIndex() < 0) {
			log.debug("There is no selected foreign provider");
			MessageDialog.openInformation(shell,
					"Información Errónea",
					"No ha seleccionado un proveedor extranjero.");
			foreignProvider.setFocus();
			return false;
		}

		log.info("Data is valid.");
		return true;
	}
}
