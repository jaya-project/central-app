package com.valorti.jaya.ui;

import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.event.PrintJobEvent;
import javax.print.event.PrintJobListener;

import org.apache.log4j.xml.DOMConfigurator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Decorations;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.hibernate.criterion.Restrictions;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.code128.Code128Constants;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.util.Calendar;
import com.valorti.configuration.XMLConfiguration;
import com.valorti.data.formatter.InletGuideFormatter;
import com.valorti.importer.ImportLotData;
import com.valorti.importer.ImportManager;
import com.valorti.importer.InventoryDataImporter;
import com.valorti.importer.PalletMovementDataImporter;
import com.valorti.importer.ProductExtractionImporter;
import com.valorti.importer.SessionDataImporter;
import com.valorti.jaya.framework.PluginManager;
import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Employee;
import com.valorti.jaya.model.InletGuide;
import com.valorti.jaya.model.Warehouse;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.EmployeeManager;
import com.valorti.jaya.model.manager.WarehouseManager;
import com.valorti.jaya.ui.events.FillCompleteListener;
import com.valorti.jaya.ui.events.IProgressEventListener;
import com.valorti.jaya.ui.events.IUserInterfaceEventListener;
import com.valorti.jaya.ui.events.ProgressEvent;
import com.valorti.ui.utils.FillTable;
import com.valorti.ui.utils.UIHelper;
import com.valorti.utils.HibernateUtils;
import com.valorti.utils.PathUtils;
import com.valorti.utils.WinCEUtils;

public class Main implements IObservedUI {
	private final static Logger					log					= LoggerFactory
																			.getLogger(Main.class);
	private List<Image>							imgList				= new ArrayList<Image>();

	private List<IProgressEventListener>		progressEL			= new ArrayList<IProgressEventListener>();

	private List<IUserInterfaceEventListener>	uiListeners			= new ArrayList<IUserInterfaceEventListener>();

	private static XMLConfiguration				configuration		= null;

	private static WinCEUtils					wince				= null;

	private PluginManager						pluginMgr			= null;

	private final byte							SEARCH_BY_LOT		= 0;

	private final byte							SEARCH_BY_ITEM		= 1;

	private final byte							SEARCH_BY_LOCATION	= 2;

	private final byte							FILTER_HIDE_EMPTY	= 0;

	private byte								filters				= FILTER_HIDE_EMPTY;
	private byte								searchBy			= SEARCH_BY_LOT;

	private static byte							configured			= 0;

	private static Main							self				= null;
	protected static Shell						shell;

	protected static Display					display;
	private Table								tblContainer;
	private Text								tbSearch;
	private Menu								mainMenu;
	private MenuItem							tm_printTag;
	private MenuItem							printersMenu;
	private Button								rbLotCode;
	private Button								rbLocation;
	private Button								rbItem;
	private Button								chkHideEmpty;
	private Button								btnApply;

	private String[]							args				= null;

	private Main(String[] args) {
		this.args = args;
	}

	/**
	 * Changes the cursor Icon.
	 * 
	 * @param cursor
	 *            One of the SWT.CURSOR* values.
	 */
	public static void changeCursor(int cursor) {
		try {
			shell.setCursor(new Cursor(display, cursor));
		} catch (IllegalArgumentException ex) {
			log.error("Failed to change cursor.", ex);
		}
	}

	/**
	 * There is a single configuration instance throughout the entire
	 * application, so this function returns that instance.
	 * 
	 * @return Application configuration object.
	 */
	public static XMLConfiguration configuration() {
		return configuration;
	}

	/**
	 * Configures the application.
	 */
	public static void configure() {
		if (configured == 0) {
			Map<String, String> ini = readIni();
			configureLogger(ini.get("Log4jConfigurationFile"));
			readConfiguration(ini.get("ApplicationConfigurationFile"));
			ini = null;
			// configureRapiScript();
			configured = 1;
		}
	}

	/**
	 * Configures Log4j.
	 */
	private static void configureLogger(String path) {
		if (configured == 0) {
			DOMConfigurator.configure(path);
			log.info("*** Application Starting. ***");
		}
	}

	// --- Listeners ---

	/**
	 * Retrieves the only instance of this class.
	 * 
	 * @return The Main window object.
	 */
	public static Main instance(String[] args) {
		if (self == null) {
			self = new Main(args);
		}

		return self;
	}

	/**
	 * Entry point.
	 * 
	 * @param args
	 *            Program arguments. None of this arguments are used actually.
	 */
	public static void main(String[] args) {
		Main obj = Main.instance(args);
		obj.createContents(Display.getDefault());
		obj.open();
		obj = null;
	}

	/**
	 * Reads the configuration file.
	 */
	private static void readConfiguration(String path) {
		if (configured == 0) {
			log.info("Reading configuration...");
			File conf = null;

			try {
				conf = new File(path);
				configuration = new XMLConfiguration(conf);
				log.trace("TEST: {}", configuration
						.getString("/Application/SpecificConfiguration/Test"));
			} catch (Exception ex) {
				MessageDialog
						.openError(shell, "Error",
								"No pudimos leer el archivo de configuración. Revise el registro de errores.");
				log.error("Couldn't read configuration file [path={}].",
						conf.getAbsolutePath(), ex);
			}
		}
	}

	/**
	 * Reads the inventory-manager.ini file to fetch the log4j and application
	 * configuration files.
	 */
	private static Map<String, String> readIni() {
		if (configured == 0) {
			Map<String, String> values = new HashMap<String, String>();

			// Defaults
			values.put("ApplicationConfigurationFile",
					"configuration/app.xconf");
			values.put("Log4jConfigurationFile", "configuration/log4j.xconf");
			// End Defaults

			File ini = new File("jaya.ini");

			if (ini.exists()) {
				BufferedReader reader = null;

				try {
					reader = new BufferedReader(new FileReader(ini));
					String line = "";
					Pattern pattern = Pattern.compile("^\\[[\\w.\\-+_]+\\]$");
					Matcher matcher = null;

					while ((line = reader.readLine()) != null) {
						matcher = pattern.matcher(line);

						if (!matcher.matches()) {
							String[] split = line.split("=");

							if (split != null && split.length == 2) {
								values.put(split[0].trim(), split[1].trim());
							}
						}
					}

					matcher = null;
					pattern = null;
					line = null;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (IOException e) {
							e.printStackTrace();
						}

						reader = null;
					}
				}
			}

			ini = null;

			return values;
		}

		return null;
	}

	/**
	 * Adds a progress event listener that will be called on start, update and
	 * end of the main window createContents.
	 * 
	 * @param listener
	 *            A listener object that will be called.
	 * @return true if the listener was added. false otherwise.
	 */
	public boolean addProgressEventListener(IProgressEventListener listener) {
		if (!progressEL.contains(listener))
			return progressEL.add(listener);

		return false;
	}

	// --- LISTENERS ---

	// --- EVENT HANDLING PROCESSING ---

	/**
	 * Copies the entries file to the desktop.
	 * 
	 * @return true if the file was moved, false otherwise.
	 */
	private boolean copyEntriesFileToDesktop(String deviceDataFolder,
			String entriesFileName, String desktopDataFolder) {
		boolean result = true;

		try {
			;
			File entriesFile = new File(PathUtils.combine(deviceDataFolder,
					entriesFileName));
			int response = wince.copyFile(entriesFile, desktopDataFolder,
					WinCEUtils.OPTION_REMOVE_AFTER_COPY);
			log.debug("Response code: {}", response);

			if (response != 1) {
				log.error("Failed to copy entries file. Response code was {}:",
						response);
			}
		} catch (Exception ex) {
			log.error(
					"An exception was caught while copying the entries file to the desktop.",
					ex);
		}
		return result;
	}

	/**
	 * Moves the session file from the Windows CE device to the Desktop.
	 */
	private void copySessionFileToDesktop(String deviceDataFolder,
			String sessionFileName, String desktopFolder) {
		log.info("Moving session file to desktop.");

		try {
			File sessionFile = new File(PathUtils.combine(deviceDataFolder,
					sessionFileName));

			int code = wince.copyFile(sessionFile, desktopFolder,
					WinCEUtils.OPTION_REMOVE_AFTER_COPY
							| WinCEUtils.OPTION_OVERWRITE);

			if (code != 1) {
				log.error("Could not move the session file. Response code: {}",
						code);
			}

			sessionFile = null;
		} catch (Exception ex) {
			log.error("Failed to copy session file to desktop.", ex);
		}
	}

	/**
	 * Moves the unfinished inventory XML data.
	 * 
	 * @param file
	 *            The XML file with the inventory data.
	 */
	private void copyUnfinishedInventoryToDevice(File file) {
		log.info("Moving unfinished inventory [{}] to Windows CE device.",
				file.getAbsolutePath());

		if (file != null && file.exists()) {
			try {
				final String devDataFolder = configuration
						.getString("/Application/WinCE/DeviceDataFolder");

				int code = wince.copyFile(file, devDataFolder,
						WinCEUtils.OPTION_COPYTODEVICE
								| WinCEUtils.OPTION_OVERWRITE
								| WinCEUtils.OPTION_REMOVE_AFTER_COPY);

				if (code != 1) {
					StringBuilder sb = new StringBuilder(
							"No se pudo mover el inventario sin finalizar, por lo que tendrá que moverlo manualmenete:\n\n");
					sb.append(
							"1. Asegúrese de que el capturador esta conectado.\n")
							.append("2. Vaya a la carpeta ")
							.append(configuration
									.getString("/Application/FolderStructure/DataFolder"))
							.append(".\n")
							.append("3. Busque el archivo ")
							.append(file.getName())
							.append(" y cópielo (haga clic sobre el archivo y luego presione las teclas Control + C).\n")
							.append("4. Vaya al Equipo (o Mi PC) -> WinCE -> DiskOnChip -> tw")
							.append("5. Peque el archivo (presione las teclas Control + V) en esta carpeta (Equipo\\WinCE")
							.append(devDataFolder);

					parseResponseCode(code, sb.toString());
					sb = null;
				} else {
					log.info("File [{}] successfully moved.",
							file.getAbsolutePath());

					if (file.exists()) {
						try {
							file.delete();
						} catch (SecurityException ex) {
							log.error(
									"Failed to remove unfinished inventories XML file. The software probably does not have permission to delete it. Remove manually [{}]",
									file.getAbsolutePath(), ex);
							MessageDialog
									.openError(
											shell,
											"Error",
											"No pudimos borrar un archivo temporal.\n\n"
													+ "Por favor, antes de continuar, elimine el siguiente archivo: ["
													+ file.getAbsolutePath()
													+ "].");
						}
					}
				}
			} catch (FileNotFoundException ex) {
				log.error("The rapi executable file was not found.", ex);
			}
		}
	}

	/**
	 * Create contents of the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void createContents(Display display) {
		int progress = 0;
		ProgressEvent progressEvent = new ProgressEvent(100, progress,
				"Cargando");
		fireProgressEventStart(progressEvent);

		Main.display = display;

		progress += 5;
		progressEvent.setCurrentValue(progress);
		progressEvent.setMessage("Leyendo configuración");
		fireProgressEventUpdate(progressEvent);

		Main.configure();
		wince = new WinCEUtils(
				configuration
						.getString("/Application/WinCE/RapiWrapper/Executable"));
		File tmp = new File(
				configuration
						.getString("/Application/WinCE/RapiWrapper/ErrorLogFile"));
		boolean exists = tmp.exists();

		if (!exists) {
			try {
				exists = tmp.createNewFile();
			} catch (Exception ex) {
				log.error("Failed to create file rapi wrapper log file [{}].",
						tmp.getAbsolutePath(), ex);
			}
		}

		if (exists)
			wince.setRedirect(java.lang.ProcessBuilder.Redirect.appendTo(tmp));

		progressEvent.setMessage("Cargando plugins...");
		loadPlugins(progressEvent);

		tmp = null;

		progress += 5;
		progressEvent.setCurrentValue(progress);
		progressEvent.setMessage("Generando interfaz de usuario");
		fireProgressEventUpdate(progressEvent);

		shell = new Shell();
		setImg(shell, "title_icon.jpg");

		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				log.debug("Disposing main form.");
				exit();
			}
		});
		shell.setMinimumSize(new Point(800, 500));
		shell.setSize(800, 500);
		shell.setText("Jaya - Gestión de Inventario");
		shell.setLayout(new GridLayout(1, false));

		mainMenu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(mainMenu);

		MenuItem fileMenu = new MenuItem(mainMenu, SWT.CASCADE);
		fileMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

			}
		});
		fileMenu.setText("Archivo");

		Menu fileCascade = new Menu(fileMenu);
		fileMenu.setMenu(fileCascade);

		MenuItem fm_quit = new MenuItem(fileCascade, SWT.NONE);
		setImg(fm_quit, "exit.png");

		fm_quit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
				exit();
			}
		});
		fm_quit.setText("&Salir\tAlt + Q");
		fm_quit.setAccelerator(SWT.ALT + 'Q');

		MenuItem viewMenu = new MenuItem(mainMenu, SWT.CASCADE);
		viewMenu.setText("Ver");

		Menu viewCascade = new Menu(viewMenu);
		viewMenu.setMenu(viewCascade);

		final MenuItem vm_employeeManager = new MenuItem(viewCascade, SWT.NONE);
		vm_employeeManager.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean hideAdmin = true;
				log.trace("Arguments passed length: {}", args.length);

				try {
					if (args.length > 0 && args[0].equals("-showAdminUser")) {
						hideAdmin = false;
						log.trace("Args[0] = {}", args[0]);
					}
				} catch (ArrayIndexOutOfBoundsException ex) {
					log.error("Exception: ", ex);
				}

				EmployeeView view = new EmployeeView(shell, hideAdmin);
				view.open();
			}
		});
		vm_employeeManager.setText("&Administrador de Empleados");
		setImg(vm_employeeManager, "employee_manager.png");

		final MenuItem vm_productMovement = new MenuItem(viewCascade, SWT.NONE);
		vm_productMovement.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ItemMovementView.main(null);
			}
		});
		vm_productMovement.setText("Revisar movimiento de Materias Primas");
		setImg(vm_productMovement, "pallet_movement.png");

		MenuItem vm_inventory = new MenuItem(viewCascade, SWT.NONE);
		vm_inventory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				InventoryView.main(null);
			}
		});
		vm_inventory.setText("Revisar Inventarios");
		setImg(vm_inventory, "inventories.png");

		new MenuItem(viewCascade, SWT.SEPARATOR);

		MenuItem vm_errorMenu = new MenuItem(viewCascade, SWT.CASCADE);
		vm_errorMenu.setText("Registro de &Errores");
		setImg(vm_errorMenu, "error_logs.png");

		Menu vm_errorMenu_cascade = new Menu(vm_errorMenu);
		vm_errorMenu.setMenu(vm_errorMenu_cascade);

		final MenuItem vm_subEm_fileMovement = new MenuItem(
				vm_errorMenu_cascade, SWT.NONE);
		vm_subEm_fileMovement.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				vm_subEm_fileMovement.setEnabled(false);
				MovementLogView.main(null);
				vm_subEm_fileMovement.setEnabled(true);
			}
		});
		vm_subEm_fileMovement.setText("&Traspaso de Archivos");
		setImg(vm_subEm_fileMovement, "movement_logs.png");

		final MenuItem vm_subEm_main = new MenuItem(vm_errorMenu_cascade,
				SWT.NONE);
		vm_subEm_main.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				vm_subEm_main.setEnabled(false);
				InventoryLogView.main(null);
				vm_subEm_main.setEnabled(true);
			}
		});
		vm_subEm_main.setText("&Aplicación Principal");
		setImg(vm_subEm_main, "main_log.png");

		final MenuItem toolsMenu = new MenuItem(mainMenu, SWT.CASCADE);
		toolsMenu.setText("Herramientas");

		Menu toolsCascade = new Menu(toolsMenu);
		toolsMenu.setMenu(toolsCascade);

		final MenuItem tm_firstLoad = new MenuItem(toolsCascade, SWT.NONE);
		tm_firstLoad.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFirstLoadClick(e);
			}
		});
		tm_firstLoad.setText("Primera Carga");

		MenuItem tm_kalleMenu = new MenuItem(toolsCascade, SWT.CASCADE);
		tm_kalleMenu.setText("Kalle");

		Menu tm_kalleMenu_cascade = new Menu(tm_kalleMenu);
		tm_kalleMenu.setMenu(tm_kalleMenu_cascade);

		MenuItem tm_subKm_sqlImport = new MenuItem(tm_kalleMenu_cascade,
				SWT.NONE);
		tm_subKm_sqlImport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onImportFromKalle(e);
			}
		});
		tm_subKm_sqlImport.setText("Importar Todo");

		progress += 10;
		progressEvent.setCurrentValue(progress);
		fireProgressEventUpdate(progressEvent);

		new MenuItem(toolsCascade, SWT.SEPARATOR);

		MenuItem tm_contingencyMenu = new MenuItem(toolsCascade, SWT.CASCADE);
		tm_contingencyMenu.setText("Contingencia");
		setImg(tm_contingencyMenu, "contingency.png");

		Menu tm_contingencyMenu_cascade = new Menu(tm_contingencyMenu);
		tm_contingencyMenu.setMenu(tm_contingencyMenu_cascade);

		MenuItem tm_subCm_itemMgr = new MenuItem(tm_contingencyMenu_cascade,
				SWT.NONE);
		tm_subCm_itemMgr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openItemViewer();
			}
		});
		tm_subCm_itemMgr.setText("Administrador de Productos");
		setImg(tm_subCm_itemMgr, "item_manager.png");

		new MenuItem(toolsCascade, SWT.SEPARATOR);

		final MenuItem tm_importFromDevice = new MenuItem(toolsCascade,
				SWT.NONE);
		tm_importFromDevice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onImportFromCE(e);
			}
		});
		tm_importFromDevice
				.setText("Importar información desde capturador de datos");
		setImg(tm_importFromDevice, "import_from_device.png");

		MenuItem tm_copyDatabaseToDevice = new MenuItem(toolsCascade, SWT.NONE);
		tm_copyDatabaseToDevice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onCopyDatabase(null);
			}
		});
		tm_copyDatabaseToDevice
				.setText("Copiar base de datos al capturador de datos");
		setImg(tm_copyDatabaseToDevice, "copy_db_todevice.png");

		new MenuItem(toolsCascade, SWT.SEPARATOR);

		tm_printTag = new MenuItem(toolsCascade, SWT.NONE);
		tm_printTag.setEnabled(false);
		tm_printTag.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onPrintTagClick(e);
			}
		});
		tm_printTag.setText("Imprimir Etiqueta(s)");
		setImg(tm_printTag, "print_tags.png");

		toolsCascade.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				tm_printTag.setEnabled(tblContainer.getSelectionCount() > 0);
				tm_firstLoad.setEnabled(!Boolean.parseBoolean(configuration
						.getString("/Application/SpecificConfiguration/FirstLoadComplete")));
			}
		});

		printersMenu = new MenuItem(mainMenu, SWT.CASCADE);
		printersMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				log.debug("miPrinters selected!");

			}
		});
		printersMenu.setText("Impresoras");

		Menu printersCascade = new Menu(printersMenu);
		printersMenu.setMenu(printersCascade);

		MenuItem miTest = new MenuItem(mainMenu, SWT.CASCADE);
		miTest.setText("Test");

		Menu menu_3 = new Menu(miTest);
		miTest.setMenu(menu_3);

		MenuItem miTestInletGuideEntryView = new MenuItem(menu_3, SWT.NONE);
		miTestInletGuideEntryView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				InletGuideEntryView view = new InletGuideEntryView(shell,
						SWT.NONE, new ArrayList<Container>());
				view.open();
			}
		});
		miTestInletGuideEntryView.setText("InletGuideEntryView");

		progress += 10;
		progressEvent.setCurrentValue(progress);
		fireProgressEventUpdate(progressEvent);

		MenuItem miTestAddPalletView = new MenuItem(menu_3, SWT.NONE);
		miTestAddPalletView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ContainerEditorView view = new ContainerEditorView(shell,
						SWT.NONE, null);
				view.open();
			}
		});
		miTestAddPalletView.setText("AddPalletView");

		MenuItem miContainerView = new MenuItem(menu_3, SWT.NONE);
		miContainerView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					ContainerView view = new ContainerView(shell, SWT.NONE);
					view.open();
					view = null;
				} catch (Exception ex) {
					log.error("Failed to open ContainerView.", ex);
				}
			}
		});
		miContainerView.setText("ContainerView");

		MenuItem miCopyEntriesFile = new MenuItem(menu_3, SWT.NONE);
		miCopyEntriesFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String desktopPath = configuration
						.getString("/Application/WinCE/TmpDesktopFolder");
				String fileName = configuration
						.getString("/Application/WinCE/Files/File[@id='entries']");
				String devicePath = configuration
						.getString("/Application/WinCE/DeviceDataFolder");
				copyEntriesFileToDesktop(devicePath, fileName, desktopPath);
			}
		});
		miCopyEntriesFile.setText("CopyEntriesFile");

		MenuItem miTestLoginView = new MenuItem(menu_3, SWT.NONE);
		miTestLoginView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				LoginView view = new LoginView(shell, SWT.NONE);
				view.open();
			}
		});
		miTestLoginView.setText("LoginView");

		MenuItem helpMenu = new MenuItem(mainMenu, SWT.CASCADE);
		helpMenu.setEnabled(false);
		helpMenu.setText("Ayuda");

		Menu helpCascade = new Menu(helpMenu);
		helpMenu.setMenu(helpCascade);

		MenuItem hm_manual = new MenuItem(helpCascade, SWT.NONE);
		hm_manual.setText("Manual de Usuario");

		MenuItem hm_about = new MenuItem(helpCascade, SWT.NONE);
		hm_about.setText("Acerca");

		fillPrinterList();

		Group grpFiltros = new Group(shell, SWT.NONE);
		grpFiltros.setLayout(new GridLayout(4, false));
		GridData gd_grpFiltros = new GridData(SWT.FILL, SWT.TOP, true, false,
				1, 1);
		gd_grpFiltros.heightHint = 76;
		gd_grpFiltros.widthHint = 746;
		grpFiltros.setLayoutData(gd_grpFiltros);
		grpFiltros.setText("Reduzca la Lista de Pallets");

		Label lblNewLabel_1 = new Label(grpFiltros, SWT.NONE);
		lblNewLabel_1.setText("¿Qué busca?");

		tbSearch = new Text(grpFiltros, SWT.BORDER);
		tbSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				submitOnReturn(e, btnApply);
			}
		});
		tbSearch.setToolTipText("Ingrese lo que desea encontrar");
		tbSearch.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1));

		btnApply = new Button(grpFiltros, SWT.NONE);
		btnApply.setToolTipText("Realiza la búsqueda en base a lo ingresado y filtros seleccionados.");
		GridData gd_btnApply = new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1);
		gd_btnApply.widthHint = 80;
		btnApply.setLayoutData(gd_btnApply);
		btnApply.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnApply.setEnabled(false);
				onFilterClick(e);
				btnApply.setEnabled(true);
			}
		});
		btnApply.setText("Aplicar");
		new Label(grpFiltros, SWT.NONE);

		Label lblNewLabel = new Label(grpFiltros, SWT.NONE);
		lblNewLabel.setText("Buscar por:");

		rbLotCode = new Button(grpFiltros, SWT.RADIO);
		rbLotCode
				.setToolTipText("Si selecciona esta opción, la búsqueda se realizará por número de lote");
		rbLotCode.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilterChange();
			}
		});
		rbLotCode.setSelection(true);
		rbLotCode.setText("Lote");

		rbLocation = new Button(grpFiltros, SWT.RADIO);
		rbLocation.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilterChange();
			}
		});
		rbLocation
				.setToolTipText("Seleccione esta opción para mostrar todos los pallets que coincidan con la ubicación ingresada");
		rbLocation.setText("Ubicación");

		rbItem = new Button(grpFiltros, SWT.RADIO);
		rbItem.setToolTipText("Si marca esta opción, la búsqueda se realizará por producto");
		rbItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilterChange();
			}
		});
		rbItem.setText("Materia Prima");

		progress += 10;
		progressEvent.setCurrentValue(progress);
		fireProgressEventUpdate(progressEvent);

		Label lblNewLabel_2 = new Label(grpFiltros, SWT.NONE);
		lblNewLabel_2.setText("Opciones:");

		chkHideEmpty = new Button(grpFiltros, SWT.CHECK);
		GridData gd_chkHideEmpty = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_chkHideEmpty.widthHint = 124;
		chkHideEmpty.setLayoutData(gd_chkHideEmpty);
		chkHideEmpty.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilterChange();
			}
		});
		chkHideEmpty.setSelection(true);
		chkHideEmpty
				.setToolTipText("Si esta casilla esta marcada, entonces todos los pallets que no tengan rollos no se mostrarán");
		chkHideEmpty.setText("Ocultar vacíos");
		new Label(grpFiltros, SWT.NONE);
		new Label(grpFiltros, SWT.NONE);

		tblContainer = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.MULTI);
		tblContainer.addMenuDetectListener(new MenuDetectListener() {
			@Override
			public void menuDetected(MenuDetectEvent e) {

			}
		});
		GridData gd_tblContainer = new GridData(SWT.FILL, SWT.FILL, false,
				true, 1, 1);
		gd_tblContainer.heightHint = 155;
		gd_tblContainer.widthHint = 747;
		tblContainer.setLayoutData(gd_tblContainer);
		tblContainer.setHeaderVisible(true);
		tblContainer.setLinesVisible(true);

		TableColumn tcLot = new TableColumn(tblContainer, SWT.NONE);
		tcLot.setToolTipText("Lote, talla, partida o pieza");
		tcLot.setWidth(100);
		tcLot.setText("Lote");
		tcLot.setData("getLotCode");

		TableColumn tcIdentifier = new TableColumn(tblContainer, SWT.NONE);
		tcIdentifier
				.setToolTipText("Identificador del pallet dentro del lote.");
		tcIdentifier.setWidth(100);
		tcIdentifier.setText("Identificador");
		tcIdentifier.setData("getIdentifier");

		TableColumn tcItem = new TableColumn(tblContainer, SWT.NONE);
		tcItem.setToolTipText("Nombre de la materia prima");
		tcItem.setWidth(100);
		tcItem.setText("Materia Prima");
		tcItem.setData("getItem.getItemName");

		TableColumn tcItemCode = new TableColumn(tblContainer, SWT.NONE);
		tcItemCode.setToolTipText("Código de la materia prima");
		tcItemCode.setWidth(146);
		tcItemCode.setText("Cód. Materia Prima");
		tcItemCode.setData("getItem.getItemCode");

		TableColumn tcItemCount = new TableColumn(tblContainer, SWT.NONE);
		tcItemCount
				.setToolTipText("Cantidad de rollos restantes dentro del pallet");
		tcItemCount.setWidth(53);
		tcItemCount.setText("Rollos");
		tcItemCount.setData("getItemCount");

		TableColumn tcCurrentTotalLength = new TableColumn(tblContainer,
				SWT.NONE);
		tcCurrentTotalLength.setWidth(100);
		tcCurrentTotalLength.setText("Mts. Total Actuales");
		tcCurrentTotalLength.setData("getCurrentItemLength");

		TableColumn tcItemTotalLength = new TableColumn(tblContainer, SWT.NONE);
		tcItemTotalLength.setWidth(100);
		tcItemTotalLength.setText("Metros Totales");
		tcItemTotalLength.setData("getItemTotalLength");

		TableColumn tcRow = new TableColumn(tblContainer, SWT.NONE);
		tcRow.setWidth(49);
		tcRow.setText("Rack");
		tcRow.setData("getLocation.getRow");

		TableColumn tcColumn = new TableColumn(tblContainer, SWT.NONE);
		tcColumn.setWidth(56);
		tcColumn.setText("Cuerpo");
		tcColumn.setData("getLocation.getColumn");

		TableColumn tcDepth = new TableColumn(tblContainer, SWT.NONE);
		tcDepth.setWidth(56);
		tcDepth.setText("Estante");
		tcDepth.setData("getLocation.getDepth");

		Menu tblContainerPopup = new Menu(tblContainer);
		tblContainer.setMenu(tblContainerPopup);

		MenuItem miContainer = new MenuItem(tblContainerPopup, SWT.CASCADE);
		miContainer.setText("Pallet");

		Menu menuContainer = new Menu(miContainer);
		miContainer.setMenu(menuContainer);

		MenuItem miNewContainer = new MenuItem(menuContainer, SWT.NONE);
		miNewContainer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onNewContainer();
			}
		});
		miNewContainer.setText("Crear Pallet");

		MenuItem miMoveContainer = new MenuItem(menuContainer, SWT.NONE);
		miMoveContainer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onMovePallet();
			}
		});
		miMoveContainer.setText("Mover");

		MenuItem miEditContainer = new MenuItem(menuContainer, SWT.NONE);
		miEditContainer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onEditContainer();
			}
		});
		miEditContainer.setText("Modificar");

		MenuItem miExtractItems = new MenuItem(menuContainer, SWT.NONE);
		miExtractItems.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onItemExtraction();
			}
		});
		miExtractItems.setText("Extraer Rollos");

		MenuItem miDeleteContainer = new MenuItem(menuContainer, SWT.NONE);
		miDeleteContainer.setEnabled(false);
		miDeleteContainer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onDeleteContainer();
			}
		});
		miDeleteContainer.setText("Eliminar");

		MenuItem separator = new MenuItem(tblContainerPopup, SWT.SEPARATOR);
		separator.setText("Guía");

		MenuItem miCreateInletGuide = new MenuItem(tblContainerPopup, SWT.NONE);
		miCreateInletGuide.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onCreateInletGuide();
			}
		});
		miCreateInletGuide.setText("Generar Guía de Entrada");

		progress += 10;
		progressEvent.setCurrentValue(progress);
		progressEvent.setMessage("Cargando Inventario");
		fireProgressEventUpdate(progressEvent);
		refreshTable();

		progress = 100;
		progressEvent.setCurrentValue(progress);
		fireProgressEventEnd(progressEvent);
	}

	private void setImg(Decorations decor, String imgName) {
		Image img = UIHelper.loadImage(display, imgName);

		if (img != null) {
			decor.setImage(img);

			if (imgList == null)
				imgList = new ArrayList<Image>();

			imgList.add(img);
		}
	}

	private void setImg(MenuItem item, String imgName) {
		Image img = UIHelper.loadImage(display, imgName);

		if (img != null) {
			item.setImage(img);

			if (imgList == null)
				imgList = new ArrayList<Image>();

			imgList.add(img);
		}
	}

	/**
	 * Deletes the specified container.
	 * 
	 * @param container
	 *            A container object to delete.
	 * @return True if it was deleted, false otherwise.
	 */
	private boolean deleteContainer(Container container) {
		ContainerManager cm = new ContainerManager();
		return cm.delete(container);
	}

	/**
	 * Deletes a list of containers.
	 * 
	 * @param containers
	 *            Container list to remove.
	 * @return True if all the containers were removed; False otherwise.
	 */
	private boolean deleteContainer(List<Container> containers) {
		boolean r = true;

		for (Container c : containers) {
			r &= deleteContainer(c);
		}

		return r;
	}

	/**
	 * Utility function to delete variables and dispose anything that wasn't
	 * disposed before.
	 */
	private void exit() {
		try {
			HibernateUtils.close();
			log.debug("Disposing image resources...");

			if (imgList != null && imgList.size() > 0) {
				for (Image img : imgList) {
					if (img != null && !img.isDisposed())
						img.dispose();
				}
			}

			wince = null;
			configuration = null;
			tblContainer = null;
			tbSearch = null;
			mainMenu = null;
			tm_printTag = null;
			printersMenu = null;
			rbLotCode = null;
			rbLocation = null;
			rbItem = null;
			chkHideEmpty = null;
			btnApply = null;
			display = null;
			self = null;
		} catch (Exception ex) {
			log.error("Exception caught on exit: ", ex);
		}
	}

	/**
	 * Fills the table with the list of containers.
	 * 
	 * @param containers
	 *            A list of Container objects.
	 */
	private void fillContainersTable(List<Container> containers) {
		try {
			if (tblContainer.getItemCount() > 0) {
				tblContainer.removeAll();
			}

			FillTable<Container> ft = new FillTable<Container>(containers,
					tblContainer);
			ft.addListener(new FillCompleteListener() {
				@Override
				public void complete() {
				}
			});
			display.asyncExec(ft);
		} catch (Exception ex) {
			log.error("Failed to fill container table: ", ex);
		}
	}

	/**
	 * Fills the printer drop-down menu.
	 */
	private void fillPrinterList() {
		PrintService[] services = PrintServiceLookup.lookupPrintServices(null,
				null);
		// String dflt = configuration.getString("printer.default");
		//
		// if (dflt == null)
		// dflt = "";

		MenuItem menuItem = null;
		Menu menu = printersMenu.getMenu();

		if (services != null && services.length > 0) {
			for (final PrintService service : services) {
				menuItem = new MenuItem(menu, SWT.RADIO);
				menuItem.setText(service.getName());
				menuItem.setData("service", service);
				menuItem.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						/*
						 * configuration.setAttribute("printer.default", "",
						 * service.getName());
						 */
					}
				});

				// if (service.getName().compareTo(dflt) == 0)
				// menuItem.setSelection(true);
			}
		}
	}

	/**
	 * Filters a list of containers based on a filter string.
	 * 
	 * @param filter
	 *            Parameter to search.
	 * @return A sub list of containers if the Filter is not null and its length
	 *         is greater than 0. Otherwise it will return the full list.
	 */
	private List<Container> filterContainers(String filter) {
		List<Container> list = null;
		ContainerManager cm = new ContainerManager();

		try {
			if (filter == null || filter.length() <= 0) {
				list = cm
						.list(Container.class, Restrictions.gt("itemCount", 0));
			} else {
				filter = filter.trim();
				log.debug("Searching for {}", filter);
				boolean isItemCode = filter.matches("^\\d+\\-\\d+$");
				boolean excludeEmpty = (filters & this.FILTER_HIDE_EMPTY) == this.FILTER_HIDE_EMPTY;

				if (searchBy == SEARCH_BY_LOT) {
					log.debug("Searching by lot.");
					list = cm.listByLotCode(filter, excludeEmpty);
				} else if (searchBy == SEARCH_BY_ITEM) {
					log.debug("Searching by item.");
					if (isItemCode)
						list = cm.listByItemCode(filter, excludeEmpty);
					else
						list = cm.listByItemName(filter, excludeEmpty);
				} else if (searchBy == SEARCH_BY_LOCATION) {
					log.debug("Searching by Location.");
					char row = filter.charAt(0);
					String col = filter.substring(1, 3), dep = filter
							.substring(3);
					list = cm.listByLocation(row, col, dep, excludeEmpty);
					dep = col = null;
				}
			}
		} catch (Exception ex) {
			log.error("Failed to filter container data: ", ex);
		}

		return list;
	}

	// Actions for the table popup menu.

	private void fireProgressEventEnd(ProgressEvent e) {
		for (IProgressEventListener listener : progressEL) {
			listener.onFinish(e);
		}
	}

	private void fireProgressEventStart(ProgressEvent e) {
		for (IProgressEventListener listener : progressEL) {
			listener.onStart(e);
		}
	}

	private void fireProgressEventUpdate(ProgressEvent e) {
		for (IProgressEventListener listener : progressEL) {
			listener.onUpdate(e);
		}
	}

	/**
	 * Generates the bar code image when the selected printer is not recognized.
	 * 
	 * @param fname
	 *            File name of the generated image.
	 * @param data
	 *            The data for the bar code.
	 * @return True if the image was generated or False if something goes wrong.
	 */
	private boolean genBarcodeImage(String fname, String data) {
		FileOutputStream out = null;
		boolean r = false;
		final int barcodeResolution = configuration
				.getInteger("/Application/Barcodes/Barcode[@name='inventory-manager']/BarcodeResolution");

		try {
			Code128Bean bean = new Code128Bean();
			bean.doQuietZone(false);
			bean.setModuleWidth(UnitConv.mm2pt(0.125));
			bean.setCodeset(Code128Constants.CODESET_B);
			File f = new File(
					configuration
							.getString("/Application/Barcodes/Barcode[@name='inventory-manager']/OutputFolder"),
					fname);

			if (!f.getParentFile().exists())
				f.getParentFile().mkdirs();

			out = new FileOutputStream(f);
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out,
					"image/png", barcodeResolution,
					BufferedImage.TYPE_BYTE_BINARY, true, 0);
			bean.generateBarcode(canvas, data);
			canvas.getBufferedImage();
			canvas.finish();
			r = true;
		} catch (Exception e) {
			log.error("Failed barcode generation: ", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					log.error("Failed to close FileOutputSteam: ", e);
				}
			}

			out = null;
		}

		return r;
	}

	// --- EVENT HANDLING ---

	// --- Utility Functions ---

	/**
	 * Generates a bar code image from a list of containers.
	 * 
	 * @param list
	 *            Container objects list.
	 * @return True if every image was generated and saved or false if something
	 *         fails.
	 */
	private boolean genBarcodes(List<Container> list) {
		boolean r = true;

		for (Container c : list) {
			String data = getBarCodeData(c);
			String fname = data.replaceAll("#", "_") + ".png";
			r &= genBarcodeImage(fname, data);
		}

		return r;
	}

	// //--- EVENT HANDLING PROCESSING ---
	// --- UTILITY FUNCTIONS ---

	/**
	 * Returns the data for the label.
	 * 
	 * @param container
	 *            Container object from where the data will be extracted.
	 * @return The generated label data.
	 */
	private String getBarCodeData(Container container) {
		return new StringBuilder(container.getLotCode()).append('#')
				.append(container.getIdentifier()).append('#')
				.append(container.getLocation().getRow())
				.append(container.getLocation().getColumn())
				.append(container.getLocation().getDepth()).append('#')
				.append(container.getCurrentItemLength()).toString();
	}

	/**
	 * Retrieves the selected container list from the table.
	 * 
	 * @return A container object list or null if none is selected.
	 */
	private List<Container> getSelectedContainers() {
		List<Container> list = null;

		if (tblContainer.getSelectionCount() > 0) {
			list = new ArrayList<Container>();
			TableItem[] items = tblContainer.getSelection();

			for (TableItem ti : items) {
				Object data = ti.getData("Id");

				if (data != null)
					list.add((Container) data);
			}
		}

		return list;
	}

	/**
	 * Fetches the currently selected printer.
	 * 
	 * @return A service of the printer that is currently selected or null if
	 *         none is selected.
	 */
	private PrintService getSelectedPrinter() {
		for (MenuItem mi : printersMenu.getMenu().getItems()) {
			if (mi.getSelection())
				return (PrintService) mi.getData("service");
		}

		return null;
	}

	/**
	 * Imports the containers from the entriesFile and asks the user to input
	 * inlet guide data and creates the excel file.
	 * 
	 * @param entriesFile
	 *            The XML file with the data to import
	 *            (&lt;Lots&gt;&lt;/Lots&gt;).
	 */
	private void importLotData(File entriesFile) {
		log.debug("Importing lots...");
		ImportLotData importLotData = new ImportLotData(entriesFile);
		InletGuideEntryView igEntryView = null;
		InletGuideFormatter igFormatter = InletGuideFormatter.getInstance();
		InletGuide inletGuide = null;
		StringBuilder failedLots = new StringBuilder();
		StringBuilder lots = new StringBuilder();
		boolean somethingToSave = false;
		String inletGuideFolder = configuration
				.getString("/Application/SpecificConfiguration/InletGuideOutputFolder");

		while (importLotData.hasNext()) {
			try {
				Set<Container> containerSet = importLotData.next();
				igEntryView = new InletGuideEntryView(shell, SWT.NONE,
						containerSet);
				inletGuide = (InletGuide) igEntryView.open();

				if (inletGuide != null) {
					log.debug("Received InletGuide: {}", inletGuide.toString());
					lots.append(inletGuide.getLotCode()).append(", ");

					if (!igFormatter.addRow(inletGuide)) {
						failedLots.append(inletGuide.getLotCode()).append(", ");
					} else {
						if (igFormatter.getCurrentRowCount() >= 29) {
							String fileName = PathUtils
									.generateFileName(null, "ge_", null,
											igFormatter.getFileExtension());
							igFormatter.setFile(new File(PathUtils.combine(
									inletGuideFolder, fileName)));
							fileName = null;

							if (!igFormatter.save()) {
								failedLots.append(inletGuide.getLotCode())
										.append(", ");
							}
						}
					}

					somethingToSave = true;
				} else {
					log.info("User skipped the process of creating the inlet guide.");
				}
			} catch (Exception ex) {
				log.error("Exception caught while importing lot data. Error:",
						ex);
			}
		}

		if (somethingToSave) {
			String add = "Hay";

			try {
				if (!igFormatter.isSaved()) {
					String fileName = PathUtils.generateFileName(null, "ge_",
							null, igFormatter.getFileExtension());
					igFormatter.setFile(new File(PathUtils.combine(
							inletGuideFolder, fileName)));
					fileName = null;

					if (!igFormatter.save()) {
						MessageDialog
								.openWarning(
										shell,
										"Falló la Creación del Archivo",
										"No pudimos crear el archivo excel, por lo que tendrá "
												+ "que generarlo nuevamente más tarde.\n\n"
												+ "Anote los siguientes números de lote: "
												+ lots.toString().replaceAll(
														"(,\\s*)$", ""));
						add = "Adicionalmente hay";
					}
				}

				if (failedLots.length() > 0) {
					MessageDialog
							.openWarning(
									shell,
									"Lotes Fallidos",
									add
											+ " lotes que no se pudieron añadir, pero podrá generarlos más tarde manualmente. Anote los siguientes lotes:\n\n"
											+ failedLots.toString().replaceAll(
													"(,\\s*)$", ""));
				}
			} catch (Exception ex) {
				log.error("Exception caught.", ex);
			}

			add = null;
		}

		failedLots = null;
		lots = null;
		inletGuide = null;
		igFormatter = null;
		igEntryView = null;
		importLotData = null;
	}

	/**
	 * Imports session data from the specified file.
	 * 
	 * @param sessionFile
	 *            The file that contains the data.
	 */
	private void importSessionData(File sessionFile) {
		SessionDataImporter sessionDataImporter = new SessionDataImporter(
				sessionFile);

		try {
			sessionDataImporter.run();
		} catch (Exception ex) {
			log.error("Exception importing session data", ex);
		}

		if (sessionDataImporter.getUnparsedLines().size() > 0) {
			MessageDialog
					.openError(
							shell,
							"Error",
							"No se pudieron procesar una o más líneas del archivo de sesión. "
									+ "Revise el registro de erroes para mayor información.");
		}

		sessionDataImporter = null;
	}

	/**
	 * Loads plugins. It MUST be called after configure.
	 */
	private void loadPlugins(ProgressEvent pe) {
		String pluginsPath = configuration
				.getString("/Application/FolderStructure/PluginsFolder");
		pluginMgr = PluginManager.instance();
		pluginMgr.configure(pluginsPath);
		pluginMgr.loadPlugins(pe);
	}

	/**
	 * Copies the database to the Windows CE Device.
	 */
	private void onCopyDatabase(WaitView wait) {
		shell.setEnabled(false);
		boolean stop = false;
		String dbFilePath = configuration
				.getString("/Application/Databases/Database[@id='sqlite']/Path");

		try {
			log.info("Closing hibernate factory to free database file...");
			HibernateUtils.close();
			log.info("Success!");
		} catch (Exception ex) {
			log.error("Failed to close hibernate factory.", ex);
			stop = true;
		}

		if (!stop) {
			boolean closeWaitView = false;

			if (wait == null) {
				wait = new WaitView();
				wait.open();
				closeWaitView = true;
			}

			wait.getShell().setActive();
			wait.setTitleLbl("Copia de Archivos");
			wait.setMsgLbl("Copiando base de datos...");
			int moved = 0;

			try {
				File tmp = new File(dbFilePath);
				moved = wince.copyFile(tmp, "\\DiskOnChip\\tw",
						WinCEUtils.OPTION_COPYTODEVICE
								| WinCEUtils.OPTION_OVERWRITE);
				tmp = null;
			} catch (FileNotFoundException ex) {
				log.error("The rapi wrapper executable was not found.", ex);
			}

			log.debug("Return code: {}", moved);

			if (closeWaitView)
				wait.close();

			if (moved == 1) {
				MessageDialog.openInformation(shell, "Copia Completada",
						"La copia de la base de datos finalizó correctamente.");
			} else {
				File tmp = new File(dbFilePath);
				StringBuilder sb = new StringBuilder(
						"No se pudo copiar la base de datos.\n\n");
				sb.append(
						"Si no es la primera vez que ocurre, siga las siguientes instrucciones:\n\n")
						.append("1. Cierre el programa.\n")
						.append("2. Vaya a la carpeta [")
						.append(tmp.getAbsoluteFile().getParent())
						.append("]\n")
						.append("3. Copie el archivo [")
						.append(tmp.getName())
						.append("]\n")
						.append("4. Vaya al capturador a través de Equipo -> WinCE -> DiskOnChip -> tw y pegue el archivo ahí.");
				MessageDialog.openError(shell, "Copia Fallida", sb.toString());
				sb = null;
				tmp = null;
			}

			try {
				log.info("Trying to reconfigure Hibernate...");
				HibernateUtils.getSessionFactory();
				log.info("Success!");
			} catch (Exception ex) {
				log.error("Failed to reconfigure Hibernate.", ex);
			}

			wait = null;
		}

		shell.setEnabled(true);
	}

	/**
	 * Generates inlet guide files for the selected lots.
	 */
	private void onCreateInletGuide() {
		List<Container> contList = getSelectedContainers();
		String outputFolder = configuration
				.getString("/Application/SpecificConfiguration/InletGuideOutputFolder");

		if (contList != null) {
			ContainerManager cmgr = new ContainerManager();
			List<InletGuide> list = new ArrayList<InletGuide>();
			InletGuide ig = null;
			InletGuideEntryView view = null;
			InletGuideFormatter format = InletGuideFormatter.getInstance();
			String fileName = PathUtils.generateFileName(null, "ge_", null,
					format.getFileExtension());
			format.setFile(new File(PathUtils.combine(outputFolder, fileName)));
			fileName = null;

			MessageDialog
					.openInformation(
							shell,
							"Información",
							"Si alguno de los lotes no tiene información de la guía de entrada, aparecerá una ventana donde tendrá que ingresar esta información y luego se generará el archivo Excel.");
			boolean shouldSave = false;

			for (Container container : contList) {
				try {
					log.debug("Generating inlet guide for lot {}.",
							container.getLotCode());
					view = new InletGuideEntryView(shell, SWT.NONE,
							cmgr.listByLotCode(container.getLotCode(), false));
					ig = (InletGuide) view.open();

					if (ig != null && !list.contains(ig)) {
						if (ig != null) {
							if (format.addRow(ig)) {
								if (format.getCurrentRowCount() >= 29) {
									if (!format.save()) {
										MessageDialog
												.openError(shell, "Error",
														"Falló la creación del archivo Excel.");
									}
								}
								shouldSave = true;
							} else {
								log.error("Failed to add row to inlet guide file.");
							}
						}
					}
				} catch (Exception ex) {
					log.error(
							"Exception caught while generating inlet guide files.",
							ex);
				}
			}

			cmgr = null;
			list = null;
			ig = null;
			view = null;

			if (shouldSave) {
				if (!format.isSaved()) {
					if (!format.save()) {
						MessageDialog.openError(shell, "Error",
								"Falló la creación del archivo Excel.");
					}
				}
			}

			MessageDialog.openInformation(shell, "Terminado",
					"El proceso ha finalizado.");
		}
	}

	/**
	 * Handles the delete button click
	 */
	private void onDeleteContainer() {
		List<Container> containers = getSelectedContainers();
		if (containers != null && containers.size() > 0) {
			int confirm = new MessageDialog(shell, "Confirmar", null,
					"¿Está seguro de querer eliminar este Pallet?",
					MessageDialog.QUESTION, new String[] { "Sí", "NO" }, 0)
					.open();

			if (confirm == 0) {
				if (deleteContainer(containers)) {
					MessageDialog
							.openInformation(shell, "Elementos eliminados",
									"Los contenendores fueron eliminados sin problemas.");
				} else {
					MessageDialog
							.openError(
									shell,
									"Falló el borrado",
									"No se pudieron eliminar los contenedores. Para mayor información revise el registro de errores.");
				}
			} else {
				log.debug("Delete container cancelled by the user.");
			}

			refreshTable();
		}
	}

	/**
	 * Iterates through the selected containers and opens the container editor
	 * window for each of one.
	 */
	private void onEditContainer() {
		shell.setEnabled(false);
		List<Container> list = getSelectedContainers();

		if (list != null && list.size() > 0) {
			// Only show the dialog if there are many containers.
			boolean showDialog = list.size() > 3, refreshTable = false;
			ContainerEditorView view = null;

			for (Container container : list) {
				try {
					view = new ContainerEditorView(shell, SWT.NONE, container);
					Boolean response = (Boolean) view.open();

					if (response != null) {
						if (!response) {
							MessageDialog
									.openError(shell, "Error",
											"No pudimos guardar la información del pallet.");
						} else {
							MessageDialog
									.openInformation(shell,
											"Pallet Modificado",
											"La modificación se ha realizado sin inconvenientes.");
							refreshTable = true;
						}
					}

					response = null;
				} catch (Exception ex) {
					log.error("Exception caught while editing container.", ex);
				}

				view = null;
			}

			if (showDialog) {
				MessageDialog.openInformation(shell, "Proceso Terminado",
						"Terminó el proceso de modificación de los Pallets.");
			}

			if (refreshTable)
				refreshTable();
		}

		list = null;
		shell.setEnabled(true);
	}

	private void onFilterChange() {
		filters = 0;
		searchBy = SEARCH_BY_LOT;

		if (rbItem.getSelection())
			searchBy = SEARCH_BY_ITEM;

		if (rbLocation.getSelection())
			searchBy = SEARCH_BY_LOCATION;

		filters += chkHideEmpty.getSelection() ? FILTER_HIDE_EMPTY : 0;
		log.debug("onFilterChange: search by = {}, filters = {}", searchBy,
				filters);
	}

	/**
	 * Handles the click on the filter button.
	 * 
	 * @param e
	 *            Click event.
	 */
	private void onFilterClick(SelectionEvent e) {
		List<Container> list = filterContainers(tbSearch.getText());

		if (list != null && list.size() > 0) {
			fillContainersTable(list);
		} else {
			MessageDialog.openInformation(shell, "Sin Resultados.",
					"La búsqueda no arrojó resultados.");
		}
	}

	/**
	 * Processes the initial database load.
	 * 
	 * @param e
	 *            Not required.
	 */
	private void onFirstLoadClick(SelectionEvent e) {
		FileDialog fileDialog = new FileDialog(shell, SWT.SINGLE);
		fileDialog.setFilterExtensions(new String[] { "*.csv" });
		fileDialog.setFilterNames(new String[] { "Documento CSV (.csv)" });
		String file = fileDialog.open();

		if (file != null) {
			log.debug("Selected file: {}", file);

			try {
				Employee employee = new EmployeeManager()
						.findByNickname("admin");
				Warehouse warehouse = new WarehouseManager()
						.findByName("materias primas");
				ImportManager im = new ImportManager();
				List<String> list = im.firstImport(new File(file), warehouse,
						employee);

				if (list != null && list.size() > 0) {
					log.info("These are the lines that could not be parsed:");
					StringBuilder sb = new StringBuilder(
							"Las siguientes líneas no se pudieron procesar y tendrá que ingresarlas manualmente:")
							.append('\n').append('\n');

					for (String line : list) {
						log.info("Line: {}", line);
						sb.append(line).append('\n');
					}

					configuration
							.setValue(
									"/Application/SpecificConfiguration/FirstLoadComplete",
									true);
					MessageDialog.openInformation(shell,
							"Importado con Errores", sb.toString());
				} else {
					MessageDialog.openInformation(shell, "Proceso Finalizado",
							"Información importada sin problemas.");
				}

				employee = null;
				warehouse = null;
				im = null;
				list = null;
			} catch (Exception ex) {
				MessageDialog
						.openError(
								shell,
								"Error",
								"Ocurrió un error intentando importar los datos. Revise el registro de errores.");
				log.error("Failed to import data: ", ex);
			}
		}
	}

	/**
	 * Processes the import of data from the Windows CE device.
	 * 
	 * @param e
	 *            Event data.
	 */
	private void onImportFromCE(SelectionEvent e) {
		shell.setEnabled(false);

		final String deviceDataFolder = configuration
				.getString("/Application/WinCE/DeviceDataFolder");
		final String desktopTmpFolder = configuration
				.getString("/Application/WinCE/TmpDesktopFolder");
		final String entriesFileName = configuration
				.getString("/Application/WinCE/Files/File[@id='entries']");
		final String sessionFileName = configuration
				.getString("/Application/WinCE/Files/File[@id='session']");
		final File entriesFile = new File(PathUtils.combine(desktopTmpFolder,
				entriesFileName));
		final File sessionFile = new File(PathUtils.combine(desktopTmpFolder,
				sessionFileName));

		copyEntriesFileToDesktop(deviceDataFolder, entriesFileName,
				desktopTmpFolder);
		copySessionFileToDesktop(deviceDataFolder, sessionFileName,
				desktopTmpFolder);

		boolean moveEntriesFile = true;
		WaitView waitView = new WaitView();
		waitView.open();
		waitView.setTitleLbl("Importando Datos");
		waitView.setMsgLbl("Importando información desde capturador...");
		File tmpFile = null;

		if (entriesFile.exists()) {
			waitView.setMsgLbl("Importando movimientos de productos...");
			log.debug("ProductExtractionImporter ***");
			ProductExtractionImporter productExtractionImporter = new ProductExtractionImporter(
					entriesFile);

			try {
				while (productExtractionImporter.hasNext()) {
					try {
						productExtractionImporter.next();
					} catch (Exception ex) {
						log.error("Exception importing product extractions.",
								ex);
						moveEntriesFile &= false;
					}
				}
			} catch (Exception ex) {
				log.error("Exception caught: ", ex);
			} finally {
				productExtractionImporter = null;
			}

			waitView.setMsgLbl("Importando movimientos de pallets...");
			log.debug("PalletMovementDataImporter ****");
			PalletMovementDataImporter palletMovementDI = new PalletMovementDataImporter(
					entriesFile);

			try {
				Boolean success = true;

				while (palletMovementDI.hasNext()) {
					Boolean tmp = palletMovementDI.next();

					if (tmp != null) {
						success &= tmp;
					}
				}

				if (!success) {
					MessageDialog
							.openInformation(
									shell,
									"Error",
									"Ocurrió un error importando los movimientos de pallets. Revise el registro de errores para más información.");
					waitView.getShell().setActive();
				}
			} catch (Exception ex) {
				log.error("Exception importing pallet movement.", ex);
				moveEntriesFile &= false;
			} finally {
				palletMovementDI = null;
			}

			waitView.setMsgLbl("Importando datos de inventario...");
			InventoryDataImporter inventoryDI = new InventoryDataImporter(
					entriesFile);

			try {
				tmpFile = new File(PathUtils.combine(configuration
						.getString("/Application/FolderStructure/DataFolder"),
						entriesFileName));

				while (inventoryDI.hasNext()) {
					try {
						inventoryDI.next();
					} catch (Exception ex) {
						log.error("Error caught parsing inventory data.", ex);
					}
				}

				int code = inventoryDI.storeDocument(tmpFile.getAbsolutePath());

				if (code == -1) {
					MessageDialog
							.openError(
									shell,
									"Error",
									"No pudimos crear el archivo con el inventario. Revise el registro de errores para obtener más información.");
					log.warn("Failed to create the entries.xml file back.");
					waitView.getShell().setActive();
				} else if (code == 1) {
					copyUnfinishedInventoryToDevice(tmpFile);
				}
			} catch (Exception ex) {
				log.error("Exception importing inventory data.", ex);
				moveEntriesFile &= false;
			} finally {
				inventoryDI = null;
			}

			tmpFile = null;
			waitView.setMsgLbl("Importando lotes...");
			importLotData(entriesFile);

			if (sessionFile.exists()) {
				waitView.setMsgLbl("Importando datos de sesión...");
				importSessionData(sessionFile);
			} else {
				waitView.setMsgLbl("No se encontró el archivo de sesion - se omite.");
			}

			waitView.setMsgLbl("Leyendo configuración...");
			tmpFile = new File(PathUtils.combine(configuration
					.getString("/Application/WinCE/ProcessedFilesFolder")));

			if (!tmpFile.exists()) {
				waitView.setMsgLbl("Creando estructura de carpetas...");

				if (!tmpFile.mkdirs()) {
					MessageDialog.openError(
							shell,
							"Error de Escritura",
							"No pudimos crear la carpeta "
									+ tmpFile.getAbsolutePath()
									+ ". Será necesario crearla manualmente.");
					log.error("Failed to create processed directory structure. Entries file copy is pending.");
					waitView.getShell().setActive();
					return;
				}
			}

			File newPath = null;

			// This doesn't actually deletes the file, instead, we try to move
			// them to the processed folder.
			if (moveEntriesFile) {
				if (entriesFile.exists()) {
					log.info("Trying to move entries file");
					newPath = new File(PathUtils.combine(
							tmpFile.getAbsolutePath(), "proc_"
									+ Calendar.getInstance().getTimeInMillis()
									+ ".xml"));
					waitView.setMsgLbl("Moviendo archivo XML a procesados...");

					// TODO - Ask the user if the file should be deleted if
					// it already exists.

					if (!newPath.exists()) {
						if (!entriesFile.renameTo(newPath)) {
							MessageDialog
									.openError(shell, "Error de Copia",
											"Falló la copia hacia la carpeta de los archivos procesados.");
							waitView.getShell().setActive();
						} else {
							log.info("Entry file moved to {}.",
									newPath.getAbsolutePath());
						}
					} else {
						log.error("The proccessed file ({}) already exists.",
								newPath.getAbsolutePath());
					}
				} else {
					log.warn("The entries file ({}) does not exist! Weird...",
							entriesFile.getAbsolutePath());
				}
			}

			if (sessionFile.exists()) {
				log.debug("Trying to delete session file...");
				sessionFile.delete();
				log.debug("Success!");
			}

			newPath = null;

			MessageDialog.openInformation(shell, "Proceso Terminado",
					"Terminó la importación de datos desde el capturador.");
			waitView.getShell().setActive();
			onCopyDatabase(waitView);
		} else {
			MessageDialog
					.openError(
							shell,
							"Falta Un Archivo",
							"No se encontró el archivo XML que contiene la información.\n"
									+ "Quizás falló la copia desde el capturador de datos.\n"
									+ "Asegúrese de que el archivo está en "
									+ entriesFile.getAbsolutePath());
		}

		waitView.close();
		waitView = null;
		tmpFile = null;
		shell.setEnabled(true);
		refreshTable();
	}

	/**
	 * Handles the import from the SQL Server database of the "client".
	 * 
	 * @param e
	 *            Event data.
	 */
	private void onImportFromKalle(SelectionEvent e) {
		log.debug("Trying to import all data from client server...");
		shell.setEnabled(false);
		WaitView view = new WaitView();
		view.open();
		ImportManager mgr = new ImportManager();
		log.debug("Importing foreign providers.");

		try {
			view.setMsgLbl("Importando proveedores extranjeros...");
			mgr.importForeignProviders();
		} catch (Exception ex) {
			MessageDialog
					.openError(
							shell,
							"Error de Importación",
							"No pudimos importar los datos de los proveedores extranjeros. Por favor revise el registro de errores.");
			log.error("Failed to import Foreign Provider data: ", ex);
		}

		log.debug("Importing Items.");

		try {
			view.setMsgLbl("Importando materias primas...");
			mgr.importItems();
		} catch (Exception ex) {
			MessageDialog
					.openError(
							shell,
							"Error de Importación",
							"No pudimos importar los datos de las materias primas. Por favor revise el registro de errores.");
			log.error("Failed to import Item data: ", ex);
		}

		view.close();
		shell.setEnabled(true);
	}

	/**
	 * Process the item extraction from the selected pallets.
	 */
	private void onItemExtraction() {
		List<Container> list = getSelectedContainers();

		if (list != null) {
			ExtractProductView view = null;
			boolean refresh = false;

			for (Container container : list) {
				view = new ExtractProductView(shell, SWT.NONE, container);

				if (!view.open()) {
					log.warn("Product extraction failed!");
				} else {
					refresh = true;
				}
			}

			if (refresh)
				this.refreshTable();
		}
	}

	/**
	 * Opens the container movement window.
	 */
	private void onMovePallet() {
		List<Container> list = getSelectedContainers();

		if (list != null) {
			MoveContainerView view = null;
			boolean refresh = false;

			for (Container container : list) {
				view = new MoveContainerView(shell, SWT.NONE, container);
				if (!view.open())
					log.warn("Pallet movement failed!");
				else
					refresh = true;
			}

			if (refresh)
				this.refreshTable();
		}
	}

	/**
	 * Handles the creation of a new container.
	 */
	private void onNewContainer() {
		shell.setEnabled(false);

		try {
			ContainerEditorView view = new ContainerEditorView(shell, SWT.NONE,
					null);
			Boolean response = (Boolean) view.open();

			if (response != null) {
				if (response) {
					MessageDialog.openInformation(shell, "Pallet Creado",
							"Pallet creado sin problemas.");
					refreshTable();
				} else {
					MessageDialog
							.openError(
									shell,
									"Error",
									"No se pudo crear el pallet.\n\nRevise el registro de errores para mayor información.");
				}
			}

			response = null;
			view = null;
		} catch (Exception ex) {
			log.error("Exception caught creating a new container.", ex);
			MessageDialog
					.openError(
							shell,
							"Error de Interfaz",
							"No pudimos abrir el editor de pallets.\n\nRevise el registro de errores para mayor información.");
		} finally {
			shell.setEnabled(true);
		}
	}

	/***
	 * Handles the click on the print label button.
	 * 
	 * @param e
	 *            Event data.
	 */
	private void onPrintTagClick(SelectionEvent e) {
		changeCursor(SWT.CURSOR_WAIT);
		mainMenu.setEnabled(false);

		if (tblContainer.getSelectionCount() > 0) {
			TableItem[] items = tblContainer.getSelection();
			List<Container> containers = new ArrayList<Container>();

			for (TableItem item : items) {
				if (item != null)
					containers.add((Container) item.getData("Id"));
			}

			PrintService printer = getSelectedPrinter();

			// if (printer == null) {
			// MessageDialog
			// .openInformation(
			// shell,
			// "Seleccione Impresora",
			// "Aún no ha seleccionado una impresora. Haga clic sobre Impresoras y elija una de impresora de la lista.");
			// } else {
			// // TODO - Load printer plugin.
			// }

			if (printer != null
					&& printer.getName()
							.compareToIgnoreCase("bixolon slp-t400") == 0) {
				if (printTag(containers)) {
					MessageDialog
							.openInformation(
									shell,
									"Imprimiendo etiquetas",
									"Está comenzando la impresión de etiquetas (debería ver el ícono de la impresora en la bandeja del sistema).");
				} else {
					MessageDialog
							.openError(shell,
									"Falló la impresión de etiquetas",
									"Una o más etiquetas no se han podido imprimir. Revise el registro de errores.");
				}
			} else {
				String barcodeFolder = configuration
						.getString("/Application/Barcodes/Barcode[@name='inventory-manager']/OutputFolder");
				if (genBarcodes(containers)) {
					MessageDialog dialog = new MessageDialog(
							shell,
							"Imágenes Guardadas",
							null,
							String.format(
									"Los códigos de barra fueron generados y guardados en la carpeta %s.",
									barcodeFolder), MessageDialog.INFORMATION,
							new String[] { "Cerrar", "Abrir Carpeta" }, 0);
					int result = dialog.open();

					if (result == 1) {
						File tmp = new File(barcodeFolder);

						if (tmp != null && tmp.exists() && tmp.isDirectory()) {
							try {
								Desktop.getDesktop().open(tmp);
							} catch (IOException ex) {
								log.error("Failed to open File Explorer.", ex);
							}
						}

						tmp = null;
					}

					dialog = null;
					barcodeFolder = null;
				} else {
					MessageDialog
							.openError(
									shell,
									"Error",
									"Falló la generación de códigos de barra. Revise el registro de errores para mayor información.");
				}
			}
		}

		mainMenu.setEnabled(true);
		changeCursor(SWT.CURSOR_ARROW);
	}

	/**
	 * Open the window.
	 */
	public void open() {
		shell.open();
		shell.setActive();
		shell.layout();

		// LoginView login = new LoginView(shell, SWT.ON_TOP);
		//
		// if (login.open()) {
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		// } else {
		// shell.close();
		// }
		//
		// login = null;
	}

	/**
	 * Opens the a window that lets the user view and edit Item objects.
	 */
	private void openItemViewer() {
		try {
			ItemView view = new ItemView(shell, SWT.None);
			view.open();
		} catch (Exception ex) {
			log.error("Failed to open item viewer.", ex);
			MessageDialog
					.openInformation(
							shell,
							"Error en la Interfaz",
							"No pudimos abrir la ventana para ver los Productos. Revise el registro de errores para mayor información.");
		}
	}

	/**
	 * Parses the Rapi Wrapper response code.
	 * 
	 * @param code
	 *            The code returned by the wrapper.
	 * @param msg
	 *            An optional message that will added to the emerging message.
	 */
	private void parseResponseCode(int code, String msg) {
		switch (code) {
			case -1:
			case 0:
				if (msg != null)
					msg += "\n\nAyuda: revise el registro de errores para mayor información.";
				else
					msg = "\n\nAyuda: revise el registro de errores para mayor información.";

				MessageDialog.openError(shell, "Error", msg);
				break;

			case 2:
			case 3:
				if (msg != null)
					msg += "\n\nAyuda: asegúrese de que el equipo está conectado y aparece en el explorador de Windows (Equipo o Mi PC)";
				else
					msg = "\n\nAyuda: asegúrese de que el equipo está conectado y aparece en el explorador de Windows (Equipo o Mi PC)";

				MessageDialog.openError(shell, "Error", msg);
				break;

			case 4:
				if (msg != null)
					msg += "\n\nAyuda: quizás el archivo no existe en la ruta especificada.";
				else
					msg = "\n\nAyuda: quizás el archivo no existe en la ruta especificada.";
				MessageDialog.openError(shell, "Error", msg);
				break;

			case 5:
				if (msg != null)
					msg += "\n\nAyuda: el archivo ya existe y no debería sobre-escribirse.";
				else
					msg = "\n\nAyuda: el archivo ya existe y no debería sobre-escribirse.";
				MessageDialog.openError(shell, "Error", msg);
				break;
		}
	}

	/**
	 * Prints a single label with the specified data. Only for the Bixolon
	 * printer.
	 * 
	 * @param data
	 *            Data for the barcode label.
	 * @return True on success, False on failure.
	 */
	private boolean print(String data) {
		if (data.length() > 0) {
			PrintService service = getSelectedPrinter();

			if (service != null) {
				InputStream inputStream = null;

				try {
					String head = configuration
							.getString("/Application/Printers/Printer[@name='Bixolon-SLP-T400']/Command/Head");
					String cmd = String.format("%s%s", head, data);
					// String cmd = data;
					log.debug("Unparsed command: {}", cmd);
					cmd = cmd.replaceAll("\\{RN\\}", "\r\n");
					cmd = cmd.replaceAll("\\{R\\}", "\r");
					log.debug("Prased command:\n{}", cmd);
					DocPrintJob job = service.createPrintJob();
					inputStream = new ByteArrayInputStream(cmd.getBytes());
					DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
					Doc doc = new SimpleDoc(inputStream, flavor, null);
					job.print(doc, null);
					job.addPrintJobListener(new PrintJobListener() {

						@Override
						public void printDataTransferCompleted(PrintJobEvent pje) {
							log.debug("Print job transfer complete!");
						}

						@Override
						public void printJobCanceled(PrintJobEvent pje) {
							log.debug("Print job cancelled!");
						}

						@Override
						public void printJobCompleted(PrintJobEvent pje) {
							log.debug("Print Job Completed: {}", pje
									.getPrintJob().toString());
						}

						@Override
						public void printJobFailed(PrintJobEvent pje) {
							log.debug("Print job failed: {}", pje.getPrintJob());
						}

						@Override
						public void printJobNoMoreEvents(PrintJobEvent pje) {
							log.debug("NO MORE PRINT JOB EVENTS");
						}

						@Override
						public void printJobRequiresAttention(PrintJobEvent pje) {
							log.debug("PRINT JOB REQUIRES ATTENTION!");
						}

					});

					job = null;
					doc = null;
					cmd = "";

					return true;
				} catch (Exception ex) {
					log.error("Failed to execute command [{}]: ", data, ex);
				} finally {
					try {
						if (inputStream != null)
							inputStream.close();
					} catch (IOException ex) {
						log.error("Failed to close printer input stream: ", ex);
					}

					inputStream = null;
				}
			} else {
				log.error("Printer service was null!");
				MessageDialog
						.openInformation(
								shell,
								"Falta la impresora",
								"Al parecer no ha elegido una impresora. Vaya al menu Impresoras y seleccione una de las impresoras.");
			}
		} else {
			log.info("Empty printing data. Nothing done.");
		}

		return false;
	}

	/**
	 * Prints a list of labels. This only sends the jobs to the printer, which
	 * means that it doesn't check if they were actually printed.
	 * 
	 * @param list
	 *            Container objects to generate the data that will be used on
	 *            the label.
	 * @return True if every tag was sent to the printer or False otherwise.
	 */
	private boolean printTag(List<Container> list) {
		StringBuilder data = new StringBuilder();
		int printCount = 0;

		for (Container container : list) {
			data.append(configuration
					.getString(
							"/Application/Printers/Printer[@name='Bixolon-SLP-T400']/Command/Body")
					.replace("{DATA}", getBarCodeData(container))
					.replace("{LABEL}", container.getItem().getItemName()));
			printCount++;
		}

		String tmp = data.toString();
		char c = 'G';

		if (printCount > 1)
			c = 'C';

		tmp = tmp.replace("{CONTINUOUS}", String.valueOf(c));

		return print(tmp);
	}

	/**
	 * Shortcut method to refresh the container table.
	 */
	private void refreshTable() {
		this.fillContainersTable(this.filterContainers(null));
	}

	/**
	 * Removes the listener from the list only if it exists.
	 * 
	 * @param listener
	 *            The listener to remove.
	 * @return true if the listener exists and is not null. false otherwise.
	 */
	public boolean removeProgressEventListener(IProgressEventListener listener) {
		if (listener != null)
			return progressEL.remove(listener);

		return false;
	}

	private void submitOnReturn(KeyEvent e, Control control) {
		log.debug("submitOnReturn({}, {})", e, control);
		log.trace("e.char == swt.cr: {}", e.character == SWT.CR);

		if (e.character == SWT.CR) {
			control.notifyListeners(SWT.Selection, null);
		}
	}

	@Override
	public void onUIAction() {
		if (uiListeners.size() > 0) {
			for (IUserInterfaceEventListener listener : uiListeners) {
				listener.eventDetected();
			}
		}
	}

	@Override
	public boolean addUIListener(IUserInterfaceEventListener listener) {
		return uiListeners.add(listener);
	}

	@Override
	public boolean removeUIListener(IUserInterfaceEventListener listener) {
		return uiListeners.remove(listener);
	}
}