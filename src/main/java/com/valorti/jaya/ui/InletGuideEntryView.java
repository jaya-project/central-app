package com.valorti.jaya.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.util.Calendar;
import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.InletGuide;
import com.valorti.jaya.model.Provider;
import com.valorti.jaya.model.manager.ProviderManager;
import com.valorti.ui.utils.UIHelper;
import com.valorti.utils.DateUtils;
import com.valorti.utils.StringUtils;

public class InletGuideEntryView extends Dialog {
	private InletGuide				inletGuide;
	private final ProviderManager	providerMgr			= new ProviderManager();
	private int						entryConceptType	= -1;
	private final Logger			log					= LoggerFactory
																.getLogger(InletGuideEntryView.class);

	private Set<Container>			containerSet		= null;

	protected Shell					shell;
	private Text					tDescription;
	private Text					tCostCenterCode;
	private Combo					cbProvider;
	private Composite				form;
	private Group					gEntryConcept;
	private Text					tLotCode;
	private Text					tPostingCostCenterCode;
	private Text					tProductCode;
	private Spinner					spinUnitPrice;
	private Spinner					spinTotalLength;
	private Spinner					spinFolioNumber;
	private DateTime				dtGenerationDate;
	private Combo					cbWarehouseEntryConcept;
	private Spinner					spinTotal;
	private Spinner					spinPurchaseOrderNo;
	private DateTime				dtPurchaseDate;
	private ScrolledComposite		scrolledComposite;
	private Label					lblNewLabel_8;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 * @param containers
	 *            The list of container objects that will be linked to the
	 *            InletGuide.
	 */
	public InletGuideEntryView(Shell parent, int style,
			List<Container> containerList) {
		this(parent, style, new HashSet<Container>((containerList != null
				&& containerList.size() > 0 ? containerList
				: new ArrayList<Container>())));
	}

	/**
	 * @wbp.parser.constructor
	 * @param parent
	 * @param style
	 * @param containers
	 *            A set of container objects that will be linked to the Inlet
	 *            Guide.
	 */
	public InletGuideEntryView(Shell parent, int style,
			Set<Container> containers) {
		super(parent, style);

		if (containers != null && containers.size() > 0) {
			containerSet = containers;
			setText("Generar Guía de Entrada");
		} else {
			if (!log.isDebugEnabled()) {
				MessageDialog
						.openInformation(
								parent,
								"No hay Pallets",
								"La lista de pallets está vacía, por lo que no se puede generar una guía de entrada.");
			}
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setMinimumSize(new Point(560, 500));
		shell.setSize(600, 500);
		shell.setText("Ingreso de Guía de Entrada");
		shell.setLayout(new GridLayout(1, false));

		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if (image != null)
			shell.setImage(image);

		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		scrolledComposite = new ScrolledComposite(shell, SWT.BORDER
				| SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		form = new Composite(scrolledComposite, SWT.NONE);
		scrolledComposite.setContent(form);
		form.setLayout(new GridLayout(1, false));

		lblNewLabel_8 = new Label(form, SWT.NONE);
		GridData gd_lblNewLabel_8 = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_lblNewLabel_8.heightHint = 32;
		lblNewLabel_8.setLayoutData(gd_lblNewLabel_8);
		lblNewLabel_8.setFont(SWTResourceManager.getFont("Segoe UI", 12,
				SWT.NORMAL));
		lblNewLabel_8
				.setText("Ingrese la información de la guía de entrada y luego haga clic sobre Guardar.");

		Group gBasicInfo = new Group(form, SWT.NONE);
		gBasicInfo.setLayout(new GridLayout(2, false));
		GridData gd_gBasicInfo = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gd_gBasicInfo.widthHint = 547;
		gd_gBasicInfo.heightHint = 403;
		gBasicInfo.setLayoutData(gd_gBasicInfo);
		gBasicInfo.setText("1.- Información Básica");

		Label lblNewLabel = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel.setText("Lote");

		tLotCode = new Text(gBasicInfo, SWT.BORDER | SWT.READ_ONLY);
		tLotCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel_2 = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel_2.setText("Largo Total:");

		spinTotalLength = new Spinner(gBasicInfo, SWT.BORDER | SWT.READ_ONLY);
		spinTotalLength.setEnabled(false);
		spinTotalLength.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onTotalActorChange(e);
			}
		});
		spinTotalLength.setPageIncrement(1000);
		spinTotalLength.setMaximum(99999999);
		spinTotalLength.setMinimum(1);
		spinTotalLength.setSelection(1);
		spinTotalLength.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));

		Label lblNewLabel_4 = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel_4.setText("Código de Producto:");

		tProductCode = new Text(gBasicInfo, SWT.BORDER | SWT.READ_ONLY);
		tProductCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label label = new Label(gBasicInfo, SWT.NONE);
		label.setText("Número de Folio:");

		spinFolioNumber = new Spinner(gBasicInfo, SWT.BORDER);
		spinFolioNumber.setPageIncrement(100);
		spinFolioNumber.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinFolioNumber.setMaximum(9999999);

		Label label_5 = new Label(gBasicInfo, SWT.NONE);
		label_5.setText("Fecha de Generación:");

		dtGenerationDate = new DateTime(gBasicInfo, SWT.BORDER | SWT.DROP_DOWN);
		dtGenerationDate
				.setToolTipText("Fecha en la que se genrará la guía de entrada.");
		dtGenerationDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label label_4 = new Label(gBasicInfo, SWT.NONE);
		label_4.setText("Proveedor:");

		cbProvider = new Combo(gBasicInfo, SWT.READ_ONLY);
		cbProvider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label label_3 = new Label(gBasicInfo, SWT.NONE);
		label_3.setText("Descripción:");

		tDescription = new Text(gBasicInfo, SWT.BORDER | SWT.V_SCROLL
				| SWT.MULTI);
		GridData gd_tDescription = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gd_tDescription.heightHint = 40;
		tDescription.setLayoutData(gd_tDescription);

		Label label_2 = new Label(gBasicInfo, SWT.NONE);
		label_2.setText("Código Centro Costos:");

		tCostCenterCode = new Text(gBasicInfo, SWT.BORDER | SWT.READ_ONLY);
		tCostCenterCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		tCostCenterCode.setText("10-10-10");

		Label lblNewLabel_3 = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel_3.setText("Cuenta de Consumo:");

		tPostingCostCenterCode = new Text(gBasicInfo, SWT.BORDER
				| SWT.READ_ONLY);
		tPostingCostCenterCode.setText("10-10-10");
		tPostingCostCenterCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));

		Label lblNewLabel_1 = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel_1.setText("Precio Unitario:");

		spinUnitPrice = new Spinner(gBasicInfo, SWT.BORDER);
		spinUnitPrice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onTotalActorChange(e);
			}
		});
		spinUnitPrice.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinUnitPrice.setMaximum(99999999);

		Label lblNewLabel_5 = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel_5.setText("Total Final:");

		spinTotal = new Spinner(gBasicInfo, SWT.BORDER);
		spinTotal.setPageIncrement(1000);
		spinTotal.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinTotal.setMaximum(99999999);

		Label lblNewLabel_7 = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel_7.setText("Fecha de Compra:");

		dtPurchaseDate = new DateTime(gBasicInfo, SWT.BORDER | SWT.DROP_DOWN);
		dtPurchaseDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));

		Label lblNewLabel_6 = new Label(gBasicInfo, SWT.NONE);
		lblNewLabel_6.setText("No. Orden de Compra (interno):");

		spinPurchaseOrderNo = new Spinner(gBasicInfo, SWT.BORDER);
		spinPurchaseOrderNo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				false, false, 1, 1));
		spinPurchaseOrderNo.setMaximum(99999999);

		Label label_1 = new Label(gBasicInfo, SWT.NONE);
		label_1.setText("Concepto de Entrada a Bodega:");

		cbWarehouseEntryConcept = new Combo(gBasicInfo, SWT.READ_ONLY);
		cbWarehouseEntryConcept.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false, 1, 1));
		cbWarehouseEntryConcept.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onWarehouseEntryConceptChange(e);
			}
		});
		cbWarehouseEntryConcept
				.setItems(new String[] { "Compra con Guía de Despacho - 01",
						"Compra con Factura - 02" });

		gEntryConcept = new Group(form, SWT.NONE);
		gEntryConcept.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		gEntryConcept.setLayout(new GridLayout(2, false));
		gEntryConcept.setText("");

		Group gControls = new Group(form, SWT.NONE);
		gControls.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));
		gControls.setLayout(new GridLayout(2, false));
		gControls.setText("Controles");

		Button btnSave = new Button(gControls, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSave(e);
			}
		});
		btnSave.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1,
				1));
		btnSave.setToolTipText("Sólo guarda la información en la base de datos, pero no genera el archivo.");
		btnSave.setText("Guardar");

		Button btnReset = new Button(gControls, SWT.NONE);
		btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onReset();
			}
		});
		btnReset.setToolTipText("Borra todos los campos y deja el formulario tal como estaba al comienzo.");
		btnReset.setText("Borrar Formulario");
		scrolledComposite.setContent(form);
		scrolledComposite
				.setMinSize(form.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		// Setup form controls
		fillData();
	}

	/**
	 * Fills the read-only data on the form.
	 */
	private void fillData() {
		log.debug("fillData()");
		fillProviderComboBox();

		if (containerSet != null && containerSet.size() > 0) {
			Iterator<Container> it = this.containerSet.iterator();

			if (it.hasNext()) {
				Double length = 0.0;
				Container container = null;

				for (; it.hasNext();) {
					container = it.next();
					length += container.getItemTotalLength();
				}

				tLotCode.setText(container.getLotCode());
				spinTotalLength.setSelection(length.intValue());
				tProductCode.setText(container.getItem().getItemCode());
			}
		}
	}

	// Event handlers

	/**
	 * Fills the inlet guide object data after data has been validated.
	 */
	private void fillInletGuideObject() {
		if (inletGuide == null)
			inletGuide = new InletGuide();

		try {
			Calendar calendar = Calendar.getInstance();

			inletGuide.setLotCode(tLotCode.getText());
			inletGuide.setCostCenterCode(tCostCenterCode.getText());
			inletGuide.setFolioNumber((long) spinFolioNumber.getSelection());

			calendar.set(dtGenerationDate.getYear(),
					dtGenerationDate.getMonth(), dtGenerationDate.getDay());
			inletGuide.setGenerationDate(calendar.getTime());
			calendar.clear();

			inletGuide.setIntakeAccount("");
			inletGuide.setiProductionOrderNumber(0L);
			inletGuide.setiPurchaseOrderNumber((long) spinPurchaseOrderNo
					.getSelection());
			inletGuide.setiWorkOrderNumber(0L);

			inletGuide.setLotCode(tLotCode.getText());
			inletGuide.setPostingCostCenterCode(tPostingCostCenterCode
					.getText());
			inletGuide.setProvider(getSelectedProvider());

			calendar.set(dtPurchaseDate.getYear(), dtPurchaseDate.getMonth(),
					dtPurchaseDate.getDay());
			inletGuide.setPurchaseDate(calendar.getTime());
			calendar.clear();

			inletGuide.setPurchaseTotal((long) spinTotal.getSelection());
			inletGuide.setSerialNumber("");
			inletGuide.setSourceWarehouseCode("");

			inletGuide.setWhEntryConcept(StringUtils
					.getValueFromSplit(cbWarehouseEntryConcept
							.getItem(cbWarehouseEntryConcept
									.getSelectionIndex())));

			switch (entryConceptType) {
				case 0:
					inletGuide.seteWaybillNumber(Long
							.parseLong(getEntryConceptControlValue(
									"waybillNumber").toString()));
					inletGuide
							.seteWaybillDate((Date) getEntryConceptControlValue("waybillDate"));
					break;

				case 1:
					inletGuide
							.seteBillNumber(Long
									.parseLong(getEntryConceptControlValue(
											"billNumber").toString()));
					inletGuide.setBillType(Long
							.parseLong(getEntryConceptControlValue("billType")
									.toString()));
					inletGuide.seteBillSubtype(StringUtils.getValueFromSplit(
							getEntryConceptControlValue("billSubtype")
									.toString()).charAt(0));
					inletGuide
							.seteBillDate((Date) getEntryConceptControlValue("billDate"));
					break;
			}

			inletGuide.setContainers(containerSet);
		} catch (Exception ex) {
			log.error("Exception caught trying to create inlet guide object.",
					ex);
			MessageDialog.openError(shell, "Error",
					"Falló la creación del objeto. Intente nuevamente.");
		}
	}

	/**
	 * Fills the provider combo box.
	 */
	private void fillProviderComboBox() {
		if (this.cbProvider.getItemCount() <= 0) {
			List<Provider> providerList = this.providerMgr.list(Provider.class);

			if (providerList.size() > 0) {
				int i = 0;

				for (Provider provider : providerList) {
					try {
						this.cbProvider.add(String.format("%s - %s",
								provider.getProviderName(),
								provider.getProviderCode()));
						this.cbProvider.setData("p" + i, provider.getId());
						i++;
					} catch (Exception ex) {
						log.error(
								"Exception caught while filling provider combo box.",
								ex);
					}
				}
			} else {
				MessageDialog.openError(this.shell, "No hay Proveedores",
						"La lista de proveedores está vacía.");
			}
		}
	}

	/**
	 * Retrieves the value of the specified control.
	 * 
	 * @param controlName
	 *            Name of the control which value is required.
	 * @return The value required or null if the control does not exist.
	 */
	private Object getEntryConceptControlValue(String controlName) {
		Object object = null;

		try {
			Control[] children = gEntryConcept.getChildren();
			Calendar calendar = Calendar.getInstance();

			for (Control control : children) {
				Object name = control.getData("name");
				if (name != null) {
					if (name.toString().compareTo(controlName) == 0) {
						if (control instanceof Text) {
							object = ((Text) control).getText();
						} else if (control instanceof DateTime) {
							DateTime tmp = (DateTime) control;
							calendar.set(tmp.getYear(), tmp.getMonth(),
									tmp.getDay());
							object = calendar.getTime();
							calendar.clear();
							tmp = null;
						} else if (control instanceof Combo) {
							Combo tmp = (Combo) control;
							object = StringUtils.getValueFromSplit(tmp
									.getItem(tmp.getSelectionIndex()));
						} else {
							log.debug("Control {} skipped.", control.toString());
						}
					}
				}
			}
		} catch (Exception ex) {
			log.error("Failed to retrieve control[{}] value. Reason:",
					controlName, ex);
		}

		return object;
	}

	/**
	 * Retrieves the selected provider from the combo box.
	 * 
	 * @return A Provider object if the selectionIndex is >= 0 or null if it
	 *         isn't.
	 */
	private Provider getSelectedProvider() {
		Provider provider = null;
		int index = this.cbProvider.getSelectionIndex();

		if (index >= 0) {
			try {
				long id = Long.parseLong(this.cbProvider.getData("p" + index)
						.toString());
				log.debug("Trying to retrieve provider with id {}.", id);
				provider = (Provider) this.providerMgr.getById(Provider.class,
						id);
			} catch (Exception ex) {
				log.error("Failed to retrieve selected provider. Exception:",
						ex);
			}
		}

		return provider;
	}

	// Utility functions

	/**
	 * Resets the form data (except the read only or disabled controls).
	 */
	private void onReset() {
		spinFolioNumber.setSelection(1);
		Calendar calendar = Calendar.getInstance();
		dtGenerationDate.setDate(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));
		cbProvider.select(-1);
		tDescription.setText("");
		spinUnitPrice.setSelection(0);
		spinTotal.setSelection(0);
		dtPurchaseDate.setDate(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));
		spinPurchaseOrderNo.setSelection(0);
		cbWarehouseEntryConcept.select(-1);
		entryConceptType = -1;
		removeEntryConceptControls();
		scrolledComposite.setMinSize(form.computeSize(SWT.DEFAULT, SWT.DEFAULT,
				true));
		form.layout();
	}

	private void onSave(SelectionEvent e) {
		shell.setEnabled(false);
		boolean close = false;

		try {
			if (validateData()) {
				log.info("Data is valid. Trying to store inlet guide to database.");
				fillInletGuideObject();
				// MessageDialog.openInformation(shell, "Datos Guardados",
				// "Los datos fueron guardados correctamente.");
				close = true;
			}
		} catch (Exception ex) {
			log.error("Exception caught handling onSave event.", ex);
		} finally {
			shell.setEnabled(true);

			if (close) {
				log.debug("Closing dialog.");
				shell.close();
			}
		}
	}

	private void onTotalActorChange(SelectionEvent e) {
		spinTotal.setSelection(spinTotalLength.getSelection()
				* spinUnitPrice.getSelection());
	}

	private void onWarehouseEntryConceptChange(SelectionEvent e) {
		Combo combo = (Combo) e.getSource();
		int index = combo.getSelectionIndex();

		if (index >= 0) {
			form.setEnabled(false);
			setEntryConceptControls(index);
			form.setEnabled(true);
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return inletGuide;
	}

	/**
	 * Removes the Entry Concept Controls from the group.
	 */
	private void removeEntryConceptControls() {
		if (gEntryConcept.getChildren().length > 0) {
			log.debug("Removing controls.");

			for (Control c : gEntryConcept.getChildren()) {
				try {
					c.dispose();
				} catch (Exception ex) {
					log.error("Failed to remove control.", ex);
				}
			}
		}
	}

	/**
	 * Loads the desired controls based on the selection of the warehouse entry
	 * concept combo.
	 * 
	 * @param index
	 *            The index of the selected element.
	 */
	private void setEntryConceptControls(int index) {
		log.debug(
				"Loading warehouse entry concept controls. Index value = {}.",
				index);

		if (entryConceptType != index) {
			removeEntryConceptControls();
			entryConceptType = index;
			Text text = null;
			DateTime dateTime = null;
			Label label = null;
			Combo combo = null;
			boolean grabHSpace = true, grabVSpace = false;

			switch (index) {
				case 0:
					gEntryConcept.setText("2.- Compra con Guía de Despacho");
					label = new Label(gEntryConcept, SWT.NONE);
					label.setText("Número de Guía de Despacho:");
					text = new Text(gEntryConcept, SWT.BORDER);
					text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
							grabHSpace, grabVSpace, 1, 1));
					text.setData("name", "waybillNumber");
					text.setData("fieldName", "Número de la Guía de despacho");

					label = new Label(gEntryConcept, SWT.NONE);
					label.setText("Fecha en la Guía de Despacho:");
					dateTime = new DateTime(gEntryConcept, SWT.BORDER
							| SWT.DROP_DOWN);
					dateTime.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
							grabHSpace, grabVSpace, 1, 1));
					dateTime.setData("name", "waybillDate");
					dateTime.setData("fieldName",
							"Fecha en la Guía de Despacho");
					break;

				case 1:
					gEntryConcept.setText("2.- Compra con Factura");
					label = new Label(gEntryConcept, SWT.NONE);
					label.setText("Número de Factura:");
					text = new Text(gEntryConcept, SWT.BORDER);
					text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
							grabHSpace, grabVSpace, 1, 1));
					text.setData("name", "billNumber");
					text.setData("fieldName", "Número de Factura");

					label = new Label(gEntryConcept, SWT.NONE);
					label.setText("Tipo de Factura:");
					combo = new Combo(gEntryConcept, SWT.READ_ONLY);
					combo.setItems(new String[] { "Factura - 30",
							"Factura Venta Bienes y Servicios - 32",
							"Factura Electrónica - 33",
							"Factura no Afecta o Exenta Electrónica - 34",
							"Factura de Exportación - 101",
							"Factura a Turista - 109" });
					combo.setLayoutData(new GridData(SWT.FILL, SWT.LEFT,
							grabHSpace, grabVSpace, 1, 1));
					combo.setData("name", "billType");
					combo.setData("fieldName", "Tipo de Factura");
					combo.select(0);

					label = new Label(gEntryConcept, SWT.NONE);
					label.setText("Subtipo de Factura:");
					combo = new Combo(gEntryConcept, SWT.READ_ONLY);
					combo.setItems(new String[] { "Afecto - A", "Exento - E",
							"Exportación - X", "Electrónica - T",
							"Electrónica Exenta - S", "Liquidación - L",
							"Liquidación Factura - N",
							"Liquidación Electrónica - O" });
					combo.setLayoutData(new GridData(SWT.FILL, SWT.LEFT,
							grabHSpace, grabVSpace, 1, 1));
					combo.setData("name", "billSubtype");
					combo.setData("fieldName", "Subtipo de Factura");
					combo.select(-1);

					label = new Label(gEntryConcept, SWT.NONE);
					label.setText("Fecha de la Factura:");
					dateTime = new DateTime(gEntryConcept, SWT.BORDER
							| SWT.DROP_DOWN);
					dateTime.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
							grabHSpace, grabVSpace, 1, 1));
					dateTime.setData("name", "billDate");
					dateTime.setData("fieldName", "Fecha de la Factura");
					break;

				default:
					log.warn("A call to setEntryConceptControls was made without a valid index.");
					break;
			}

			label = null;
			text = null;
			dateTime = null;
			combo = null;

			scrolledComposite.setMinSize(form.computeSize(SWT.DEFAULT,
					SWT.DEFAULT, true));
			form.layout();
		}
	}

	/**
	 * Validates the data on the form.
	 * 
	 * @return True if the data is valid, false if not.
	 */
	private boolean validateData() {
		boolean r = true;
		Calendar cCompare = Calendar.getInstance(), cCurrent = Calendar
				.getInstance();
		StringBuilder msg = new StringBuilder();
		int dateCompare;

		if (spinFolioNumber.getSelection() <= 0) {
			if (!MessageDialog.openConfirm(shell, "Confirmar",
					"Puede dejar el NÚMERO DE FOLIO vacío, "
							+ "pero esto implica que tendrá que llenar "
							+ "el campo manualmente en el archivo Excel.\n\n"
							+ "¿Está seguro (OK) de que desea dejarlo en 0?")) {
				r &= false;
				msg.append("- Debe ingresar el \"número de folio\" (número correlativo provisto por Softland).\n");
			}
		}

		cCompare.set(dtGenerationDate.getYear(), dtGenerationDate.getMonth(),
				dtGenerationDate.getDay());
		dateCompare = DateUtils.compareOnlyDate(cCompare, cCurrent);
		cCompare.clear();
		log.debug("Generation date VS Current date: {}", dateCompare);

		if (dateCompare > 0) {
			r &= false;
			msg.append("- La Fecha de Generación no puede ser mayor a la fecha actual.\n");
		}

		if (cbProvider.getSelectionIndex() < 0) {
			r &= false;
			msg.append("- Debe seleccionar un Proveedor.\n");
		}

		cCompare.set(dtPurchaseDate.getYear(), dtPurchaseDate.getMonth(),
				dtPurchaseDate.getDay());
		dateCompare = DateUtils.compareOnlyDate(cCompare, cCurrent);
		log.debug("Purchase date VS Current date: {}", dateCompare);

		if (dateCompare > 0) {
			r &= false;
			msg.append("- La Fecha de Compra no puede ser superior a la fecha actual.\n");
		}

		if (entryConceptType < 0) {
			r &= false;
			msg.append("- Debe elegir un Concepto de Entrada a Bodega.\n");
		}

		Control[] controls = gEntryConcept.getChildren();

		for (Control control : controls) {
			try {
				Object fieldName = control.getData("fieldName");
				log.debug("FieldName = {}; Name = {}", fieldName,
						control.getData("name"));

				if (fieldName != null) {
					if (control instanceof DateTime) {
						DateTime tmp = (DateTime) control;
						cCompare.set(tmp.getYear(), tmp.getMonth(),
								tmp.getDay());
						dateCompare = DateUtils.compareOnlyDate(cCompare,
								cCurrent);
						cCompare.clear();

						if (dateCompare > 0) {
							r &= false;
							msg.append("- La "
									+ fieldName
									+ " no puede ser superior a la fecha actual.\n");
						}

						tmp = null;
					} else if (control instanceof Text) {
						Text tmp = (Text) control;

						if (tmp.getText().length() <= 0) {
							r &= false;
							msg.append("- El " + fieldName
									+ " no puede estar vacío.\n");
						}

						tmp = null;
					} else if (control instanceof Combo) {
						Combo tmp = (Combo) control;

						if (tmp.getSelectionIndex() < 0) {
							r &= false;
							msg.append("- No ha seleccionado ningún "
									+ fieldName + ".\n");
						}

						tmp = null;
					}
				} else {
					log.debug("Control doesn't contain a fieldName.");
				}
			} catch (Exception ex) {
				log.error(
						"Exception caught while validating EntryConceptData.",
						ex);
			}
		}

		if (msg.length() > 0) {
			MessageDialog.openInformation(
					shell,
					"Falta Información",
					"Se encontraron los siguientes errores:\n\n"
							+ msg.toString()
							+ "\nCorríjalos y vuelva a intentarlo.");
		}

		return r;
	}
}
