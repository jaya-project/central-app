package com.valorti.jaya.ui;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.wb.swt.SWTResourceManager;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Inventory;
import com.valorti.jaya.model.InventoryContainer;
import com.valorti.jaya.model.manager.InventoryManager;
import com.valorti.jaya.ui.events.FillCompleteListener;
import com.valorti.ui.utils.FillTable;
import com.valorti.ui.utils.UIHelper;

public class InventoryView extends Shell {
	private final Logger			log				= LoggerFactory
															.getLogger(InventoryView.class);

	private final InventoryManager	inventoryMgr	= new InventoryManager();

	private Table					table;
	private DateTime				dtEndDate;
	private DateTime				dtStartDate;
	private Button					btnInclusive;
	private Label					lblInfo;

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public InventoryView(final Display display) {
		super(display, SWT.SHELL_TRIM);
		final InventoryView _this = this;
		
		final Image image = UIHelper.loadTitleIcon(display);
		if(image != null)
			this.setImage(image);
		
		this.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		setLayout(new GridLayout(1, false));

		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 11,
				SWT.NORMAL));
		lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 1, 1));
		lblNewLabel
				.setText("Puede buscar un inventario realizado anteriormente basándose en la fecha de inicio y término (o ambas).");

		Group filter = new Group(this, SWT.NONE);
		filter.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		filter.setLayout(new GridLayout(5, false));
		GridData gd_filter = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_filter.heightHint = 59;
		gd_filter.widthHint = 65;
		filter.setLayoutData(gd_filter);
		filter.setText("Filtros");

		Label lblNewLabel_1 = new Label(filter, SWT.NONE);
		lblNewLabel_1.setText("Fecha de Inicio:");

		dtStartDate = new DateTime(filter, SWT.BORDER | SWT.DROP_DOWN
				| SWT.SHORT);
		dtStartDate.setToolTipText("Fecha en la que comenzó el inventario");

		Label lblNewLabel_2 = new Label(filter, SWT.NONE);
		lblNewLabel_2.setText("Fecha de término:");

		dtEndDate = new DateTime(filter, SWT.BORDER | SWT.DROP_DOWN | SWT.SHORT);
		dtEndDate.setToolTipText("Fecha en la que se finalizó el inventario");

		Button btnApplyFilter = new Button(filter, SWT.NONE);
		btnApplyFilter.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilter();
			}
		});
		btnApplyFilter.setText("Aplicar");

		Label lblNewLabel_3 = new Label(filter, SWT.NONE);
		lblNewLabel_3.setText("Opciones:");

		btnInclusive = new Button(filter, SWT.CHECK);
		btnInclusive.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		btnInclusive.setSelection(true);
		btnInclusive
				.setToolTipText("Marque esta casilla para filtrar los resultados entre ambas fechas, incluyendo las fechas seleccionadas.");
		btnInclusive.setText("Incluir ambas fechas");
		new Label(filter, SWT.NONE);
		new Label(filter, SWT.NONE);
		new Label(filter, SWT.NONE);

		table = new Table(this, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_table.heightHint = 367;
		table.setLayoutData(gd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				log.trace("Double clic on table!");
				onViewInventoryData();
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
		});

		TableColumn tcInventoryStart = new TableColumn(table, SWT.NONE);
		tcInventoryStart.setWidth(251);
		tcInventoryStart.setText("Inicio");
		tcInventoryStart.setData("getStartDate");

		TableColumn tcInventoryEnd = new TableColumn(table, SWT.NONE);
		tcInventoryEnd.setWidth(334);
		tcInventoryEnd.setText("Fin");
		tcInventoryEnd.setData("getEndDate");

		Menu ctxMenu = new Menu(table);
		table.setMenu(ctxMenu);

		MenuItem miViewInventoryData = new MenuItem(ctxMenu, SWT.NONE);
		miViewInventoryData.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onViewInventoryData();
			}
		});
		miViewInventoryData.setText("Ver este inventario");

		MenuItem miStartInventory = new MenuItem(ctxMenu, SWT.NONE);
		miStartInventory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onStartInventory();
			}
		});
		miStartInventory.setText("Comenzar Inventario");

		MenuItem miEndInventory = new MenuItem(ctxMenu, SWT.NONE);
		miEndInventory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onEndInventory();
			}
		});
		miEndInventory.setText("Terminar Inventario");

		Composite compBottom = new Composite(this, SWT.NONE);
		compBottom.setLayout(new GridLayout(2, false));
		compBottom.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));

		lblInfo = new Label(compBottom, SWT.NONE);
		lblInfo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Button btnClose = new Button(compBottom, SWT.NONE);
		btnClose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_this.close();
			}
		});
		btnClose.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false,
				1, 1));
		btnClose.setText("Cerrar");

		createContents();
		fillInventoryTable(filter(null, null, true));
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			InventoryView shell = new InventoryView(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}

			if (!shell.isDisposed())
				shell.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Lista de Inventarios");
		setSize(800, 500);
	}

	private void fillInventoryTable(List<Inventory> list) {
		if (list != null && list.size() > 0) {
			try {
				if (table.getItemCount() > 0)
					table.removeAll();

				FillTable<Inventory> ft = new FillTable<Inventory>(list, table);
				ft.addListener(new FillCompleteListener() {
					@Override
					public void complete() {
						lblInfo.setText("Listado completo.");
					}
				});

				this.getDisplay().asyncExec(ft);
			} catch (Exception ex) {
				log.error("Exception caught while filling inventory table.", ex);
			}
		} else {
			lblInfo.setText("No hay resultados.");
		}
	}

	/**
	 * Filters the list of inventories.
	 * 
	 * @param start
	 *            Start date of the Inventory we are looking for.
	 * @param end
	 *            End date of the Inventory we are looking for.
	 * @param includeBoth
	 *            Should both dates be included on the search?
	 * @return The filtered list of inventories or null if there are none.
	 */
	private List<Inventory> filter(Date start, Date end, boolean includeBoth) {
		List<Inventory> list = null;

		try {
			if (start == null && end == null) {
				list = inventoryMgr.list(Inventory.class);
			} else {
				Conjunction c = Restrictions.conjunction();

				if (includeBoth && start != null && end != null) {
					c.add(Restrictions.and(Restrictions.ge("startDate", start),
							Restrictions.le("endDate", end)));
				} else {
					c.add(Restrictions.and(Restrictions.gt("startDate", start),
							Restrictions.lt("endDate", end)));
				}

				list = inventoryMgr.list(Inventory.class, c);
			}
		} catch (Exception ex) {
			log.error("Failed to filter inventory data: ", ex);
		}

		return list;
	}

	private Inventory getSelectedInventory() {
		Inventory inventory = null;

		if (table.getSelectionCount() > 0) {
			Object o = table.getItem(table.getSelectionIndex()).getData("Id");

			if (o != null)
				inventory = (Inventory) o;
		}

		return inventory;
	}

	private void onEndInventory() {
		Inventory inventory = getSelectedInventory();

		if (inventory.getEndDate() == null) {
			MessageDialog dlg = new MessageDialog(
					getShell(),
					"Confirmar Cierr de Inventario",
					null,
					"¿Está seguro de que quiere cerrar el inventario?\n\nUna vez que el inventario haya sido cerrado ya no podrá añadir más elementos al mismo.",
					MessageDialog.CONFIRM, new String[] { "Sí, estoy seguro",
							"No, no estoy seguro" }, 1);
			int result = dlg.open();

			if (result != 1 && result != SWT.DEFAULT) {
				inventory.setEndDate(Calendar.getInstance().getTime());

				if (inventoryMgr.update(inventory)) {
					MessageDialog.openInformation(getShell(),
							"Cambios Guardados",
							"El inventario se ha cerrado correctamente.");
				} else {
					MessageDialog
							.openError(
									getShell(),
									"Error",
									"No se pudo cerrar el inventario. Revise el registro de errores para obtener más información.");
				}
			}

			dlg = null;
		}

		inventory = null;
	}

	/**
	 * Process the apply filter button click.
	 */
	private void onFilter() {
		this.setEnabled(false);
		Date startDate = null, endDate = null;

		Calendar c = Calendar.getInstance();
		c.set(dtStartDate.getYear(), dtStartDate.getMonth(),
				dtStartDate.getDay());
		startDate = c.getTime();

		c.set(dtEndDate.getYear(), dtEndDate.getMonth(), dtEndDate.getDay());
		endDate = c.getTime();

		fillInventoryTable(filter(startDate, endDate,
				btnInclusive.getSelection()));

		this.setEnabled(true);
	}

	/**
	 * Starts a new Inventory if there is not one started yet. Asks the user if
	 * the inventory should be ended, removed or do nothing.
	 */
	private void onStartInventory() {
		this.setEnabled(false);

		try {
			boolean refreshTable = false;
			Inventory inventory = inventoryMgr.findUnfinishedInventory();

			if (inventory != null) {
				int doNothing = 0, endInventory = 1;

				StringBuilder msg = new StringBuilder(
						"Ya ha comenzado un inventario, por lo que no puede comenzar uno nuevo a no ser de que lo cancele.\n\n");
				msg.append("¿Qué desea hacer?\n\n")
						.append("\"Cancelar Inventario\" implica perder toda posible información recopilada hasta este moemento,\n")
						.append("\"Terminar Inventario\" finalizará el inventario en curso, impidiendo que se pueda añadir más información al mismo,\n")
						.append("\"No hacer nada\" significa que el inventario seguirá abierto y no se modificará nada.");

				MessageDialog dialog = new MessageDialog(getShell(),
						"Inventario Comenzado", null, msg.toString(),
						MessageDialog.QUESTION, new String[] { "No hacer nada",
								"Terminar Inventario", "Cancelar Inventario" },
						doNothing);
				msg = null;
				int response = dialog.open();

				if (response == doNothing || response == SWT.DEFAULT) {
					log.info("User chooses to leave inventory open.");
				} else if (response == endInventory) {
					log.info("User chooses to end current inventory.");
					inventory.setEndDate(Calendar.getInstance().getTime());

					if (inventoryMgr.update(inventory)) {
						log.info("The inventory {} has ended at {}",
								inventory.getId(), inventory.getEndDate());
						MessageDialog
								.openInformation(getShell(),
										"Inventario Finalizado",
										"El inventario se ha finalizado sin problemas.");
						inventory = null;
						refreshTable = true;
					} else {
						MessageDialog
								.openError(
										getShell(),
										"Falló la Actualización",
										"No pudimos terminar el inventario. Revise el registro de errores para mayor información.");
					}
				} else {
					log.info("User chooses to remove inventory.");
					Set<InventoryContainer> set = inventory
							.getInventoryContainers();

					boolean failed = false;

					if (set != null && set.size() > 0) {
						log.debug("Removing InventoryContainer items...");

						for (Iterator<InventoryContainer> it = set.iterator(); it
								.hasNext();) {
							try {
								failed &= !inventoryMgr.delete(it.next());
							} catch (NullPointerException ex) {
								log.error(
										"One of the inventory container elements was null.",
										ex);
							}
						}
					}

					if (!failed) {
						if (inventoryMgr.delete(inventory)) {
							MessageDialog
									.openInformation(getShell(),
											"Inventario Eliminado",
											"Toda la información del inventario ha sido eliminada.");
							inventory = null;
							refreshTable = true;
						} else {
							MessageDialog.openInformation(getShell(), "Error",
									"No se pudo eliminar el inventario.");
							refreshTable = false;
						}
					}
				}
			}

			if (inventory == null) {
				inventory = new Inventory();
				inventory.setStartDate(Calendar.getInstance().getTime());
				inventory.setDataUpdated((byte) 0);

				if (inventoryMgr.add(inventory) != null) {
					log.info("Inventory started at {}",
							inventory.getStartDate());
					MessageDialog.openInformation(getShell(),
							"Inventario Comenzado",
							"El nuevo inventario ha comenzado.");
					refreshTable = true;
				} else {
					MessageDialog
							.openError(
									getShell(),
									"Falló el Inicio",
									"No pudimos comenzar el inventario. Revise el registro de errores para mayor información.");
				}
			} else {
				refreshTable = false;
			}

			if (refreshTable) {
				fillInventoryTable(filter(null, null, false));
			}
		} catch (Exception ex) {
			log.error("Exception caught starting a new inventory.", ex);
			MessageDialog
					.openError(
							getShell(),
							"Error",
							"Ocurrió un error procesando la información. Revise el registro de errores para obtener más información.");
		} finally {
			this.setEnabled(true);
		}
	}

	/**
	 * Opens the inventory data view.
	 */
	private void onViewInventoryData() {
		Inventory inventory = getSelectedInventory();

		if (inventory != null) {
			InventoryDataView view = new InventoryDataView(this, SWT.BORDER,
					inventory.getId());
			view.open();
		}
	}
}
