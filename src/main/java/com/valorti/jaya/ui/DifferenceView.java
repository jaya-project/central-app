package com.valorti.jaya.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.InventoryContainer;
import com.valorti.ui.utils.UIHelper;

public class DifferenceView extends Dialog {
	private final Logger		log			= LoggerFactory
													.getLogger(DifferenceView.class);
	private int					itemCount	= 0;
	private InventoryContainer	entity		= null;

	protected Object			result;
	protected Shell				shell;
	private Text				tDiff;
	private Spinner				spinDiffCount;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public DifferenceView(Shell parent, int style, int itemCount) {
		super(parent, style);
		setText("Diferencia");

		this.itemCount = itemCount;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setSize(207, 183);
		shell.setText("Diferencia");
		shell.setLayout(new GridLayout(1, false));

		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if (image != null)
			shell.setImage(image);

		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setText("¿Cuántos rollos quedan en el pallet?");

		spinDiffCount = new Spinner(shell, SWT.BORDER);
		spinDiffCount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onDiffCountChange();
			}
		});
		spinDiffCount.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinDiffCount.setMaximum(999999999);

		tDiff = new Text(shell, SWT.BORDER | SWT.READ_ONLY | SWT.MULTI);
		GridData gd_tDiff = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		gd_tDiff.heightHint = 64;
		tDiff.setLayoutData(gd_tDiff);

		Button btnSave = new Button(shell, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSave();
			}
		});
		btnSave.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1));
		btnSave.setText("Guardar");
	}

	private void onDiffCountChange() {
		int tmp = spinDiffCount.getSelection() - itemCount;
		log.debug("Difference count: {}", tmp);
		StringBuilder sb = new StringBuilder();

		if (tmp > 0) {
			sb.append("Hay un EXCEDENTE de ").append(Math.abs(tmp))
					.append(" rollos.");
		} else if (tmp < 0) {
			sb.append("Hay una MERMA de ").append(Math.abs(tmp))
					.append(" rollos.");
		} else {
			sb.append("No hay diferencia.");
		}

		tDiff.setText(sb.toString());
		sb = null;
	}

	private void onSave() {
		try {
			int tmp = spinDiffCount.getSelection() - itemCount;
			int abs = Math.abs(tmp);
			entity = new InventoryContainer();
			entity.setDifferenceCount(abs);
			StringBuilder sb = new StringBuilder("There is ");

			if (tmp > 0) {
				sb.append("a ")
						.append(InventoryContainer.Difference.Surplus
								.toString()).append(" of ").append(abs)
						.append(" items.");
				entity.setDifferenceName(InventoryContainer.Difference.Surplus);
			} else if (tmp < 0) {
				sb.append("an ")
						.append(InventoryContainer.Difference.Ullage.toString())
						.append(" of ").append(abs).append(" items.");
				entity.setDifferenceName(InventoryContainer.Difference.Ullage);
			} else {
				sb.append("no difference.");
				entity.setDifferenceName(InventoryContainer.Difference.None);
			}

			entity.setComments(sb.toString());
			result = entity;
			entity = null;
			sb = null;
			shell.close();
		} catch (Exception ex) {
			log.error("Failed to set return value.", ex);
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return result;
	}
}
