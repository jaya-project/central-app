package com.valorti.jaya.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.valorti.ui.utils.UIHelper;
import com.valorti.utils.PathUtils;

public class InventoryLogView extends Shell {
	private final File				logFile	= new File(
													PathUtils
															.combine(Main
																	.configuration()
																	.getString(
																			"/Application/FolderStructure/LogsFolder"), "rapi_connector.log"));

	private Table					table;
	private static InventoryLogView	shell;

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public InventoryLogView(Display display) {
		super(display, SWT.SHELL_TRIM);

		if (logFile.exists()) {
			final Image image = UIHelper.loadTitleIcon(this.getDisplay());
			if (image != null)
				this.setImage(image);

			this.addDisposeListener(new DisposeListener() {
				@Override
				public void widgetDisposed(DisposeEvent e) {
					if (image != null)
						image.dispose();
				}
			});

			setLayout(new GridLayout(1, false));
			table = new Table(this, SWT.BORDER | SWT.FULL_SELECTION);
			GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
					1);
			gd_table.widthHint = 751;
			gd_table.heightHint = 399;
			table.setLayoutData(gd_table);
			table.setLinesVisible(true);

			Button btnClose = new Button(this, SWT.NONE);
			btnClose.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true,
					false, 1, 1));
			btnClose.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseUp(MouseEvent e) {
					shell.close();
				}
			});
			btnClose.setText("Cerrar");
			createContents();
			fillLogData();
		} else {
			MessageDialog.openInformation(
					shell,
					"Falta Archivo",
					"No hemos encontrado el archivo "
							+ logFile.getAbsolutePath());
		}
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			shell = new InventoryLogView(display);
			if (shell != null && !shell.isDisposed()) {
				shell.open();
				shell.layout();
				while (!shell.isDisposed()) {
					if (!display.readAndDispatch()) {
						display.sleep();
					}
				}
			}

			if (!shell.isDisposed())
				shell.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Registro de Errores de la Aplicación Principal");
		setSize(800, 500);

	}

	private void fillLogData() {
		FileReader freader = null;
		BufferedReader bufReader = null;

		try {
			freader = new FileReader(logFile);
			bufReader = new BufferedReader(freader);
			String line = "";

			while ((line = bufReader.readLine()) != null) {
				if (line != null && line.length() > 0) {
					TableItem ti = new TableItem(table, SWT.NONE);
					ti.setText(line);
				}
			}
		} catch (FileNotFoundException e) {
			MessageDialog.openError(
					this,
					"Archivo no Encontrado",
					"No se encontró el archivo logs/rapi_connector.log: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			MessageDialog.openError(
					shell,
					"Error de lectura",
					"Ocurrió un error al leer el archivo de registros: "
							+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (bufReader != null)
					bufReader.close();

				if (freader != null)
					freader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			bufReader = null;
			freader = null;
		}
	}
}
