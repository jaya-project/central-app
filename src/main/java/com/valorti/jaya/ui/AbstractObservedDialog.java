package com.valorti.jaya.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Shell;

import com.valorti.jaya.ui.events.IUserInterfaceEventListener;

public class AbstractObservedDialog extends Dialog implements IObservedUI {
	// List of event listeners
	protected List<IUserInterfaceEventListener>	uiListeners;

	public AbstractObservedDialog(Shell parent) {
		super(parent);
		uiListeners = new ArrayList<IUserInterfaceEventListener>();
	}

	public AbstractObservedDialog(Shell parent, int style) {
		super(parent, style);
	}

	@Override
	public void onUIAction() {
		if (uiListeners.size() > 0) {
			for (IUserInterfaceEventListener listener : uiListeners) {
				listener.eventDetected();
			}
		}
	}

	@Override
	public boolean addUIListener(IUserInterfaceEventListener listener) {
		return uiListeners.add(listener);
	}

	@Override
	public boolean removeUIListener(IUserInterfaceEventListener listener) {
		return uiListeners.remove(listener);
	}
}
