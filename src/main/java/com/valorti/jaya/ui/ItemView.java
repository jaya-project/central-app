package com.valorti.jaya.ui;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Item;
import com.valorti.jaya.model.manager.ItemManager;
import com.valorti.ui.utils.FillTable;
import com.valorti.ui.utils.UIHelper;

public class ItemView extends Dialog {
	private Item				result;
	private Table				table;
	private final Logger		log		= LoggerFactory
												.getLogger(ItemView.class);
	private Text				tSearch;
	private final ItemManager	itemMgr	= new ItemManager();

	private Shell				shell;

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public ItemView(Shell parent, int style) {
		super(parent, SWT.SHELL_TRIM);
		setText("Listado de Materias Primas");
		result = null;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setSize(800, 500);
		setText("Listado de Materias Primas");
		shell.setLayout(new GridLayout(1, false));
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event e) {
				onClose();
			}
		});

		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if(image != null)
			shell.setImage(image);
		
		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		Group gFilterControls = new Group(shell, SWT.NONE);
		gFilterControls.setText("Filtrar Resultados");
		gFilterControls.setLayout(new GridLayout(1, false));
		GridData gd_gFilterControls = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_gFilterControls.heightHint = 66;
		gFilterControls.setLayoutData(gd_gFilterControls);

		Composite compDescription = new Composite(gFilterControls, SWT.NONE);
		compDescription.setLayout(new GridLayout(1, false));
		GridData gd_compDescription = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gd_compDescription.heightHint = 22;
		compDescription.setLayoutData(gd_compDescription);

		Label lblNewLabel = new Label(compDescription, SWT.NONE);
		lblNewLabel
				.setText("Escriba el nombre o código de la materia prima y luego haga clic sobre Buscar.");

		Composite compSearch = new Composite(gFilterControls, SWT.NONE);
		compSearch.setLayout(new GridLayout(3, false));
		GridData gd_compSearch = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_compSearch.heightHint = 31;
		compSearch.setLayoutData(gd_compSearch);

		Label lblNewLabel_1 = new Label(compSearch, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_1.setText("Búsqueda:");

		tSearch = new Text(compSearch, SWT.BORDER);
		tSearch.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Button btnSearch = new Button(compSearch, SWT.NONE);
		btnSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onFilter();
			}
		});
		btnSearch.setText("Buscar");

		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(194);
		tblclmnNewColumn.setText("Nombre Materia Prima");
		tblclmnNewColumn.setData("getItemName");

		TableColumn tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_1.setWidth(205);
		tblclmnNewColumn_1.setText("Código Materia Prima (Softland)");
		tblclmnNewColumn_1.setData("getItemCode");

		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onNewItemClick();
			}
		});
		btnNewButton.setText("Nuevo");

		fillTable(filter(null));
	}

	private void fillTable(List<Item> itemList) {
		shell.setEnabled(false);

		if (itemList != null && itemList.size() > 0) {
			try {
				if (table.getItemCount() > 0) {
					table.clearAll();
				}

				FillTable<Item> ft = new FillTable<Item>(itemList, table);
				shell.getDisplay().asyncExec(ft);
			} catch (Exception ex) {
				log.error("Failed to fill item view table: ", ex);
			}
		} else {
			log.info("Item list was empty.");
		}

		shell.setEnabled(true);
	}

	private List<Item> filter(String search) {
		List<Item> list = null;
		log.trace("Filtering list: {}", search);

		try {
			if (search != null && search.length() > 0) {
				if (search.matches("^\\d+\\-\\d+$")) {
					log.debug("Searching by item code.");
					list = itemMgr.list(Item.class,
							Restrictions.eq("itemCode", search));
				} else {
					log.debug("Searching by item name.");
					list = itemMgr.list(Item.class,
							Restrictions.like("itemName", "%" + search + "%"));
				}
			} else {
				log.debug("Retrieving full list.");
				list = itemMgr.list(Item.class);
			}
		} catch (Exception ex) {
			log.error("Failed to filter item list.", ex);
		}

		return list;
	}

	private void onClose() {
		setResult();
	}

	private void onFilter() {
		fillTable(filter(tSearch.getText()));
	}

	private void onNewItemClick() {
		ItemEditorView view = new ItemEditorView(shell, SWT.None);
		int code = (int) view.open();

		if (code == 1) {
			MessageDialog.openInformation(shell, "Producto Creado",
					"Se añadió el producto sin problemas.");
			refreshTable();
		} else if (code < 0) {
			MessageDialog
					.openError(shell, "Error",
							"No se pudo crear el producto. Revise el registro de errores.");
		}

		view = null;
	}

	public Item open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return result;
	}

	private void refreshTable() {
		fillTable(filter(null));
	}

	private void setResult() {
		try {
			if (table.getItemCount() > 0 && table.getSelectionCount() > 0) {
				TableItem[] items = table.getSelection();
				Object data = items[0].getData("Id");
				result = (Item) data;
			}
		} catch (Exception ex) {
			log.error("Failed to set result object.", ex);
		}
	}
}
