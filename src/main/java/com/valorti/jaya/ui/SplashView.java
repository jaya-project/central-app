package com.valorti.jaya.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.valorti.jaya.ui.events.IProgressEventListener;
import com.valorti.jaya.ui.events.ProgressEvent;
import com.valorti.utils.PathUtils;

public class SplashView {
	private boolean	ready	= false;

	// private ProgressBar bar = null;

	public SplashView() {
	}

	/**
	 * @wbp.parser.entryPoint
	 */

	public void start(final Display display, String[] args) {
		final Image image = new Image(display, PathUtils.combine(
				PathUtils.APP_PATH, "data", "img", "splash_screen.png"));
		final Shell splash = new Shell(SWT.ON_TOP);
		final Main main = Main.instance(args);

		FormLayout layout = new FormLayout();
		splash.setLayout(layout);

		// bar = new ProgressBar(splash, SWT.NONE);
		// FormData progressData = new FormData();
		// progressData.right = new FormAttachment(100, 0);
		// progressData.bottom = new FormAttachment(100, 0);
		// bar.setLayoutData(progressData);

		final Label message = new Label(splash, SWT.NONE);
		FormData labelData = new FormData();
		labelData.left = new FormAttachment(0, 0);
		labelData.right = new FormAttachment(50, 0);
		labelData.bottom = new FormAttachment(100, 0);
		message.setLayoutData(labelData);

		Label label = new Label(splash, SWT.NONE);
		label.setImage(image);

		labelData = new FormData();
		labelData.right = new FormAttachment(100, 0);
		labelData.bottom = new FormAttachment(100, 0);
		label.setLayoutData(labelData);

		splash.pack();

		Rectangle splashRect = splash.getBounds();
		Rectangle displayRect = display.getBounds();
		int x = (displayRect.width - splashRect.width) / 2;
		int y = (displayRect.height - splashRect.height) / 2;
		splash.setLocation(x, y);
		splash.open();

		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				main.addProgressEventListener(new IProgressEventListener() {
					@Override
					public void onFinish(ProgressEvent event) {
						// bar.setSelection(event.getCurrentValue());
						message.setText(event.getMessage());
					}

					@Override
					public void onStart(ProgressEvent event) {
						// bar.setMinimum(event.getInitialValue());
						// bar.setMaximum(event.getMaxValue());
						message.setText(event.getMessage());
					}

					@Override
					public void onUpdate(ProgressEvent event) {
						// bar.setSelection(event.getCurrentValue());
						message.setText(event.getMessage());
					}
				});

				main.createContents(display);
				image.dispose();
				splash.close();
				ready = true;
			}
		});

		while (!ready) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		splashRect = null;
		displayRect = null;
		labelData = null;
		layout = null;
		label = null;
		// bar = null;
		main.open();
	}
}