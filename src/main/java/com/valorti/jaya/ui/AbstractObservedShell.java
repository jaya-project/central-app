package com.valorti.jaya.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.valorti.jaya.ui.events.IUserInterfaceEventListener;

public abstract class AbstractObservedShell extends Shell implements
		IObservedUI {
	// List of event listeners
	protected List<IUserInterfaceEventListener>	uiListeners;

	public AbstractObservedShell() {
		uiListeners = new ArrayList<IUserInterfaceEventListener>();
	}

	public AbstractObservedShell(int style) {
		super(style);
	}

	public AbstractObservedShell(Display display) {
		super(display);
	}

	public AbstractObservedShell(Shell parent) {
		super(parent);
	}

	public AbstractObservedShell(Display display, int style) {
		super(display, style);
	}

	public AbstractObservedShell(Shell parent, int style) {
		super(parent, style);
	}

	@Override
	public void onUIAction() {
		if (uiListeners.size() > 0) {
			for (IUserInterfaceEventListener listener : uiListeners) {
				listener.eventDetected();
			}
		}
	}

	@Override
	public boolean addUIListener(IUserInterfaceEventListener listener) {
		return uiListeners.add(listener);
	}

	@Override
	public boolean removeUIListener(IUserInterfaceEventListener listener) {
		return uiListeners.remove(listener);
	}
}
