package com.valorti.jaya.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Location;
import com.valorti.jaya.model.manager.ContainerManager;
import com.valorti.jaya.model.manager.LocationManager;
import com.valorti.ui.utils.UIHelper;

public class MoveContainerView extends Dialog {
	private static final Logger		log				= LoggerFactory
															.getLogger(MoveContainerView.class);
	private Container				container;

	private final ContainerManager	containerMgr	= new ContainerManager();
	private final LocationManager	locationMgr		= new LocationManager();

	protected boolean				result;
	protected Shell					shell;
	private Text					tIdentifier;
	private Text					tLocation;
	private Text					tWarehouse;
	private Combo					cbRow;
	private Spinner					spinColumn;
	private Spinner					spinDepth;
	private Text					tLotCode;

	/**
	 * 
	 * @param parent
	 * @param style
	 * @param container
	 *            The container that will be moved.
	 * @throws NullPointerException
	 *             if the container is null.
	 */
	public MoveContainerView(Shell parent, int style, Container container)
			throws NullPointerException {
		super(parent, style);
		setText("Mover Pallet");
		result = false;

		if (container != null) {
			this.container = container;
		} else {
			throw new NullPointerException();
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setSize(300, 330);
		shell.setText(getText());
		shell.setLayout(new GridLayout(1, false));
		
		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if(image != null)
			shell.setImage(image);
		
		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});

		Group gOriginalLocation = new Group(shell, SWT.NONE);
		gOriginalLocation.setLayout(new GridLayout(2, false));
		GridData gd_gOriginalLocation = new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1);
		gd_gOriginalLocation.heightHint = 108;
		gOriginalLocation.setLayoutData(gd_gOriginalLocation);
		gOriginalLocation.setText("1.- Revise la ubicación original");

		Label lblNewLabel_7 = new Label(gOriginalLocation, SWT.NONE);
		lblNewLabel_7.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_7.setText("Lote:");

		tLotCode = new Text(gOriginalLocation, SWT.BORDER | SWT.READ_ONLY);
		tLotCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel_1 = new Label(gOriginalLocation, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_1.setText("Identificador:");

		tIdentifier = new Text(gOriginalLocation, SWT.BORDER | SWT.READ_ONLY);
		tIdentifier.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_2 = new Label(gOriginalLocation, SWT.NONE);
		lblNewLabel_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_2.setText("Ubicación:");

		tLocation = new Text(gOriginalLocation, SWT.BORDER | SWT.READ_ONLY);
		tLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel_3 = new Label(gOriginalLocation, SWT.NONE);
		lblNewLabel_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_3.setText("Bodega:");

		tWarehouse = new Text(gOriginalLocation, SWT.BORDER | SWT.READ_ONLY);
		tWarehouse.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Group gNewLocation = new Group(shell, SWT.NONE);
		gNewLocation.setLayout(new GridLayout(2, false));
		GridData gd_gNewLocation = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gd_gNewLocation.heightHint = 86;
		gNewLocation.setLayoutData(gd_gNewLocation);
		gNewLocation.setText("2.- Ingrese la nueva ubicación");

		Label lblNewLabel_4 = new Label(gNewLocation, SWT.NONE);
		lblNewLabel_4.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_4.setText("Rack:");

		cbRow = new Combo(gNewLocation, SWT.READ_ONLY);
		cbRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		Label lblNewLabel_5 = new Label(gNewLocation, SWT.NONE);
		lblNewLabel_5.setText("Cuerpo:");

		spinColumn = new Spinner(gNewLocation, SWT.BORDER);
		spinColumn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinColumn.setMaximum(99);

		Label lblNewLabel_6 = new Label(gNewLocation, SWT.NONE);
		lblNewLabel_6.setText("Estante:");

		spinDepth = new Spinner(gNewLocation, SWT.BORDER);
		spinDepth.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		spinDepth.setMaximum(99);

		Group gControls = new Group(shell, SWT.NONE);
		gControls.setLayout(new GridLayout(1, false));
		GridData gd_gControls = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_gControls.heightHint = 33;
		gControls.setLayoutData(gd_gControls);
		gControls.setText("3.- Guarde los cambios o cierre la ventana");

		Button btnSave = new Button(gControls, SWT.NONE);
		btnSave.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false,
				1, 1));
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSave();
			}
		});
		btnSave.setText("Guardar");

		// Fills initial data.
		setup();
	}

	/**
	 * Retrieves the existing location or creates a new one.
	 * 
	 * @return Null if the location does not exist or failed to create it.
	 *         Otherwise the location object is returned.
	 */
	private Location getLocation() {
		int iCol = spinColumn.getSelection(), iDepth = spinDepth.getSelection();
		String row = cbRow.getItem(cbRow.getSelectionIndex());

		Location location = locationMgr.getLocation(row, iCol, iDepth,
				container.getLocation().getWarehouse());

		if (location == null) {
			log.error("Failed to retrieve or create the new location.");
		}

		row = null;

		return location;
	}

	private void onSave() {
		shell.setEnabled(false);

		if (validateUserInput()) {
			boolean shouldClose = false;

			try {
				Location location = getLocation();

				if (location != null) {
					container.setLocation(location);
					if (containerMgr.update(container)) {
						MessageDialog.openInformation(shell,
								"Cambios Guardados",
								"Cambios guardados sin problemas.");
						shouldClose = true;
						result = true;
					} else {
						MessageDialog
								.openError(shell, "Error",
										"No pudimos guardar los cambios. Revise el registro de errores.");
					}
				} else {
					MessageDialog
							.openError(
									shell,
									"Error",
									"No se encontró la ubicación y no se pudo crear. Revise el registro de errores para mayor información.");
				}
			} catch (Exception ex) {
				log.error("Exception caught while saving the data.", ex);
			} finally {
				shell.setEnabled(true);

				if (shouldClose)
					shell.close();
			}
		}
	}

	// Event Handlers

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public boolean open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	// // --- EVENT HANDLERS ---

	// --- Utility Functions ---

	/**
	 * Fills initial data.
	 */
	private void setup() {
		try {
			tLotCode.setText(container.getLotCode());
			tIdentifier.setText(container.getIdentifier().toString());
			String tmp = String.format("%s%s%s", container.getLocation()
					.getRow(), container.getLocation().getColumn(), container
					.getLocation().getDepth());
			tLocation.setText(tmp);
			tmp = String.format("%s - %s", container.getLocation()
					.getWarehouse().getWhName(), container.getLocation()
					.getWarehouse().getWarehouseCode().getWhCode());
			tWarehouse.setText(tmp);
			UIHelper.fillRowCombo(cbRow, true);
			tmp = null;
		} catch (Exception ex) {
			log.error("Failed to fill initial data.", ex);
			MessageDialog
					.openError(
							shell,
							"Error",
							"Ocurrió un error llenando la información inicial. Revise el registro de errores para obtener más información.");
		}
	}

	private boolean validateUserInput() {
		boolean r = true;
		StringBuilder msg = new StringBuilder();

		if (cbRow.getSelectionIndex() <= 0) {
			r &= false;
			msg.append("- No ha seleccionado ningún rack.");
		}

		if (spinColumn.getSelection() < 0 || spinColumn.getSelection() > 99) {
			r &= false;
			msg.append("- El cuerpo seleccionado no es válido (debe estar entre 0 y 99, ambos inclusive).");
		}

		if (spinColumn.getSelection() < 0 || spinColumn.getSelection() > 99) {
			r &= false;
			msg.append("- El Estante seleccionado no es válido (debe estar entre 0 y 99, ambos inclusive).");
		}

		if (msg.length() > 0) {
			MessageDialog
					.openInformation(
							shell,
							"Falta Información",
							"La siguiente información es errónea:\n\n"
									+ msg.toString());
		}

		return r;
	}
}
