package com.valorti.jaya.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.model.Container;
import com.valorti.jaya.model.Inventory;
import com.valorti.jaya.model.InventoryContainer;
import com.valorti.jaya.model.manager.InventoryContainerManager;
import com.valorti.ui.utils.UIHelper;

public class InventoryContainerEditorView extends Dialog {

	private final Logger					log			= LoggerFactory
																.getLogger(InventoryContainerEditorView.class);
	private final InventoryContainerManager	invContMgr	= new InventoryContainerManager();

	private Inventory						inventory	= null;
	private InventoryContainer				entity		= null,
			icResult = null;
	private Container						container	= null;

	private boolean							isEdit		= false;

	protected Object						result		= false;
	protected Shell							shell;
	private Text							tLotCode;
	private Text							tIdentifier;
	private Button							btnSelectContainer;
	private Label							lblNewLabel_2;
	private Text							tLocation;
	private Group							gControls;
	private Button							btnSave;
	private Button							btnReset;
	private Text							tItemCount;
	private Group							gAboutContainerContent;
	private Label							lblNewLabel_4;
	private Composite						compDiff;
	private Button							btnDifference;
	private Button							btnSquare;
	private Label							lblDiffMsg;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public InventoryContainerEditorView(Shell parent, int style,
			InventoryContainer entity, Inventory inventory) {
		super(parent, style);
		result = null;
		this.entity = entity;
		this.inventory = inventory;

		if (this.entity != null) {
			isEdit = true;
			setText("Modificar Elemento de Inventario");
			container = entity.getContainer();
		} else {
			setText("Añadir Elemento al Inventario");
			isEdit = false;
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MIN);
		shell.setSize(402, 409);
		shell.setText("Editor de Elementos de Inventario");
		shell.setLayout(new GridLayout(1, false));
		
		final Image image = UIHelper.loadTitleIcon(shell.getDisplay());
		if(image != null)
			shell.setImage(image);
		
		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (image != null)
					image.dispose();
			}
		});
		
		Group gContainerInfo = new Group(shell, SWT.NONE);
		gContainerInfo.setLayout(new GridLayout(2, false));
		GridData gd_gContainerInfo = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gd_gContainerInfo.heightHint = 132;
		gContainerInfo.setLayoutData(gd_gContainerInfo);
		gContainerInfo.setText("Contenido del Pallet");

		Label lblNewLabel_1 = new Label(gContainerInfo, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_1.setText("Lote:");

		tLotCode = new Text(gContainerInfo, SWT.BORDER | SWT.READ_ONLY);
		tLotCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel = new Label(gContainerInfo, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel.setText("Identificador:");

		tIdentifier = new Text(gContainerInfo, SWT.BORDER | SWT.READ_ONLY);
		tIdentifier.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		lblNewLabel_2 = new Label(gContainerInfo, SWT.NONE);
		lblNewLabel_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_2.setText("Ubicación:");

		tLocation = new Text(gContainerInfo, SWT.BORDER | SWT.READ_ONLY);
		tLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel_3 = new Label(gContainerInfo, SWT.NONE);
		lblNewLabel_3.setText("Contenido Actual:");

		tItemCount = new Text(gContainerInfo, SWT.BORDER | SWT.READ_ONLY);
		tItemCount.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		new Label(gContainerInfo, SWT.NONE);

		btnSelectContainer = new Button(gContainerInfo, SWT.NONE);
		btnSelectContainer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSelectContainer();
			}
		});
		btnSelectContainer.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
				false, false, 1, 1));

		gAboutContainerContent = new Group(shell, SWT.NONE);
		gAboutContainerContent.setLayout(new GridLayout(1, false));
		GridData gd_gAboutContainerContent = new GridData(SWT.FILL, SWT.CENTER,
				false, false, 1, 1);
		gd_gAboutContainerContent.heightHint = 119;
		gAboutContainerContent.setLayoutData(gd_gAboutContainerContent);
		gAboutContainerContent.setText("Respecto al contenido del Pallet:");

		lblNewLabel_4 = new Label(gAboutContainerContent, SWT.NONE);
		GridData gd_lblNewLabel_4 = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_lblNewLabel_4.widthHint = 374;
		gd_lblNewLabel_4.heightHint = 35;
		lblNewLabel_4.setLayoutData(gd_lblNewLabel_4);
		lblNewLabel_4
				.setText("¿Hay alguna diferencia entre lo que dice el \"Contenido Actual\" y lo que\r\nhay dentro del pallet?");

		compDiff = new Composite(gAboutContainerContent, SWT.NONE);
		compDiff.setLayout(new GridLayout(2, false));
		compDiff.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1));

		btnDifference = new Button(compDiff, SWT.NONE);
		btnDifference.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onDifference();
			}
		});
		GridData gd_btnDifference = new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 1, 1);
		gd_btnDifference.widthHint = 80;
		btnDifference.setLayoutData(gd_btnDifference);
		btnDifference
				.setToolTipText("Sí, hay una diferencia entre la cantidad de pallets mostrada en la pantalla y la cantidad de rollos que hay en el pallet");
		btnDifference.setText("Sí");

		btnSquare = new Button(compDiff, SWT.NONE);
		btnSquare.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSquare();
			}
		});
		btnSquare
				.setToolTipText("No, no hay diferencia entre el contenido mostrado más arriba con la cantidad de rollos en el pallet");
		GridData gd_btnSquare = new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnSquare.widthHint = 91;
		btnSquare.setLayoutData(gd_btnSquare);
		btnSquare.setText("No");

		lblDiffMsg = new Label(gAboutContainerContent, SWT.NONE);
		lblDiffMsg.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				true, 1, 1));

		gControls = new Group(shell, SWT.NONE);
		gControls.setLayout(new GridLayout(2, false));
		GridData gd_gControls = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_gControls.heightHint = 30;
		gControls.setLayoutData(gd_gControls);
		gControls.setText("Controles");

		btnSave = new Button(gControls, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSave();
			}
		});
		GridData gd_btnSave = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_btnSave.widthHint = 136;
		btnSave.setLayoutData(gd_btnSave);
		btnSave.setText("Guardar");

		btnReset = new Button(gControls, SWT.NONE);
		btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onReset();
			}
		});
		GridData gd_btnReset = new GridData(SWT.RIGHT, SWT.CENTER, true, false,
				1, 1);
		gd_btnReset.widthHint = 148;
		btnReset.setLayoutData(gd_btnReset);
		btnReset.setText("Reiniciar");

		setup();
	}

	private void fillContainerData(Container c) {
		tLotCode.setText(c.getLotCode());
		tIdentifier.setText(c.getIdentifier().toString());
		StringBuilder sb = new StringBuilder(c.getLocation().getRow());
		sb.append(c.getLocation().getColumn()).append(
				c.getLocation().getDepth());
		tLocation.setText(sb.toString());
		tItemCount.setText(c.getItemCount().toString());
		container = c;
		sb = null;
	}

	private void onDifference() {
		if (container != null) {
			try {
				DifferenceView view = new DifferenceView(shell, SWT.None,
						Integer.parseInt(tItemCount.getText()));
				icResult = (InventoryContainer) view.open();
				view = null;

				if (icResult != null) {
					setDiffMsg(icResult);
				}
			} catch (Exception ex) {
				log.error("Failed to open difference view window.", ex);
			}
		} else {
			MessageDialog.openInformation(shell, "Falta el Pallet",
					"No ha seleccionado un pallet aún.");
			icResult = null;
		}
	}

	// --- Helper Functions ---

	private void onReset() {
		tLotCode.setText("");
		tIdentifier.setText("");
		tItemCount.setText("");
		tLocation.setText("");

		if (isEdit) {
			fillContainerData(entity.getContainer());
			setDiffMsg(entity);
		}

		icResult = null;
	}

	private void onSave() {
		if (!invContMgr.objectExists(inventory, container)) {
			if (validateUserInput()) {
				try {
					if (!isEdit) {
						entity = new InventoryContainer();
						entity.setInventory(inventory);
					}

					entity.setContainer(container);
					entity.setComments(icResult.getComments());
					entity.setDifferenceCount(icResult.getDifferenceCount());
					entity.setDifferenceName(icResult.getDifferenceName());
					boolean close = false;

					if (isEdit) {
						if (invContMgr.update(entity)) {
							result = true;
							close = true;
						} else {
							result = null;
						}
					} else {
						if (invContMgr.add(entity) != null) {
							result = true;
							close = true;
						} else {
							result = null;
						}
					}

					if (close) {
						icResult = null;
						container = null;
						entity = null;
						shell.close();
					}
				} catch (Exception ex) {
					log.error("Failed to store InventoryContainer data.", ex);
					MessageDialog
							.openError(
									shell,
									"Error",
									"Ocurrió un error al guardar los datos. Revise el registro de errores para mayor información.");
				}
			}
		} else {
			MessageDialog
					.openInformation(
							shell,
							"Elemento Existente",
							"El pallet ya esta asociado a este inventario. Si desea modificarlo, cierre esta ventana, "
									+ "búsquelo en la lista, luego haga clic con el botón derecho sobre el elemento y por último "
									+ "haga clic sobre Mdificar Elemento.");
		}
	}

	private void onSelectContainer() {
		try {
			ContainerView view = new ContainerView(shell, SWT.NONE);
			Container tmp = (Container) view.open();

			if (tmp != null) {
				fillContainerData(tmp);
			}

			tmp = null;
			view = null;
		} catch (Exception ex) {
			log.error("Failed to open container selection window.", ex);
			MessageDialog
					.openError(
							shell,
							"Error",
							"Ocurrió un error al mostrar la ventana de selección de Pallets. Revise el registro de errores para obtener más información.");
		}
	}

	private void onSquare() {
		if (container != null) {
			icResult = new InventoryContainer();
			icResult.setComments("There is no difference.");
			icResult.setDifferenceCount(0);
			icResult.setDifferenceName(InventoryContainer.Difference.None);
			setDiffMsg(icResult);
		} else {
			MessageDialog.openInformation(shell, "Falta el Pallet",
					"No ha seleccionado un pallet aún.");
			icResult = null;
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	private void setDiffMsg(InventoryContainer ic) {
		StringBuilder sb = new StringBuilder();

		switch (ic.getDifferenceName()) {
			case None:
				sb.append("No hay diferencia.");
				break;
			case Surplus:
				sb.append("Hay un EXCEDENTE de ")
						.append(ic.getDifferenceCount()).append(" rollos.");
				break;
			case Ullage:
				sb.append("Hay una MERMA de ").append(ic.getDifferenceCount())
						.append(" rollos.");
				break;
		}

		lblDiffMsg.setText(sb.toString());
		sb = null;
	}

	/**
	 * Sets initial data.
	 */
	private void setup() {
		if (isEdit) {
			fillContainerData(container);
			setDiffMsg(entity);
			btnSelectContainer.setText("Cambiar");
		} else {
			btnSelectContainer.setText("Seleccionar");
		}
	}

	private boolean validateUserInput() {
		boolean r = true;
		StringBuilder sb = new StringBuilder();

		if (container == null) {
			sb.append("- No ha seleccionado un pallet.\n");
			r &= false;
		}

		if (icResult == null) {
			sb.append("- No nos ha dicho si el contenido del pallet es correcto o hay una diferencia.\n");
			r &= false;
		}

		if (sb.length() > 0) {
			MessageDialog
					.openInformation(
							shell,
							"Falta Información",
							"Falta información para poder continuar.\nCorrija lo siguiente y luego vuelva a intentarlo:\n\n"
									+ sb.toString());
		}

		sb = null;

		return r;
	}
}
