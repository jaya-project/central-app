package com.valorti.sql;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.valorti.utils.PathUtils;

public class SqlServerHelper extends AbstractSqlHelper {
	public SqlServerHelper(String connectionString) {
		super(connectionString);
		setAuthLibPath();
		preparedIndexStart = 1;
	}

	@Override
	public Connection connect() {
		log.debug("Trying to establish a connection to SQL Server...");
		Connection c = null;

		try {
			c = DriverManager.getConnection(connectionString);
		} catch (SQLException e) {
			log.error("Failed: ", e);
		}

		log.debug("Done!");

		return c;
	}

	@Override
	public long exists(String idColumnName, String table, String where,
			Object... params) {
		long r = 0;
		Connection c = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			c = connect();

			if (c != null) {
				boolean retrieveId = true;

				if (idColumnName == null || idColumnName.length() <= 0) {
					idColumnName = "*";
					retrieveId = false;
				}

				String query = new StringBuilder("SELECT ")
						.append(idColumnName).append(" FROM ").append(table)
						.append(" WHERE ").append(where).toString();

				log.debug("Exists query: {}", query);

				if (params.length > 0) {
					ps = prepare(c.prepareStatement(query), params);
					rs = ps.executeQuery();
				} else {
					rs = c.createStatement().executeQuery(query);
				}

				log.debug("Exists() row fetch size: {}", rs.getFetchSize());

				if (rs != null) {
					if (retrieveId)
						r = rs.getLong(0);
					else
						r = 1;
				}
			}
		} catch (SQLException ex) {
			log.error("Failed to check existence: ", ex);
			r = -1;
		} finally {
			closePreparedStatement(ps);
			closeResultSet(rs);
			close(c);
		}

		ps = null;
		rs = null;
		c = null;

		return r;
	}

	public String limit(String query, String orderBy, int offset) {
		return limit(query, orderBy, offset, 0);
	}

	public String limit(String query, String orderBy, int offset, int count) {
		StringBuilder q = new StringBuilder();
		log.debug("Limit: [query={}, orderBy={}, offset={}, count={}]", query,
				orderBy, offset, count);

		if (offset >= 0 && count >= 1) {
			q.append("SELECT * FROM (");
			q.append(
					query.replaceFirst("SELECT ",
							"SELECT ROW_NUMBER() OVER(ORDER BY " + orderBy
									+ ") AS RowNum, "))
					.append(") Rows WHERE Rows.RowNum BETWEEN ").append(offset)
					.append(" + 1 AND ").append(offset).append(" + ")
					.append(count);
		}

		if (offset >= 0 && count <= 0) {
			q.append("SELECT * FROM (");
			q.append(
					query.replaceFirst("SELECT ",
							"SELECT ROW_NUMBER() OVER(ORDER BY " + orderBy
									+ ") AS RowNum, "))
					.append(") Rows WHERE Rows.RowNum > ").append(offset);
		}

		if (offset < 0 && count <= 0) {
			q.append(query);
		}

		return q.toString();
	}

	@Override
	public void loadDriver() {
		log.info("Trying to load MS SQL Server driver...");

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			log.error("Could not find sqlserver driver class: ", e);
		}
	}

	@Override
	public List<Map<String, Object>> query(String query, Object... params) {
		log.debug("Quering MSSQL Server: {}", query);
		Connection c = connect();
		List<Map<String, Object>> list = null;

		try {
			ResultSet rs = null;

			if (c != null && !c.isClosed()) {
				if (params.length > 0) {
					rs = prepare(c.prepareStatement(query), params)
							.executeQuery();
				} else {
					rs = c.prepareCall(query).executeQuery();
				}

				if (rs != null) {
					list = this.remapResultSet(rs);
				}

				rs = null;
			}
		} catch (Exception e) {
			log.error("Failed to execute query: ", e);
		} finally {
			close(c);
		}

		return list;
	}

	@Override
	public void resetAutoincrement(String table)
			throws UnimplementedMethodException {
		throw new UnimplementedMethodException("");
	}

	@Override
	public long rowId(Connection c) {
		long id = -1L;

		try {
			if (c != null) {
				id = c.prepareCall("SELECT SCOPE_IDENTITY() AS rowId")
						.executeQuery().getLong("rowId");
			}
		} catch (SQLException ex) {
			log.error("Failed to retrieve row id.", ex);
		} finally {
			close(c);
		}

		return id;
	}

	private void setAuthLibPath() {
		log.info("Trying to set java.library.path...");

		try {
			String libpath = PathUtils.combine(PathUtils.APP_PATH, "data",
					"lib", "x64");
			StringBuilder path = new StringBuilder(System.getProperty(
					"java.library.path").replaceAll(";|\\.$", ""));
			path.append(';');
			path.append(libpath);
			System.setProperty("java.library.path", path.toString());
			path = null;

			Field fieldSysPath;
			fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
			fieldSysPath.setAccessible(true);
			fieldSysPath.set(null, null);
			log.debug("java.library.path: {}",
					System.getProperty("java.library.path"));
			log.info("java.library.path set!");
		} catch (SecurityException e) {
			log.error("Can't access \"sys_paths\" field: ", e);
		} catch (NoSuchFieldException e) {
			log.error("Could not find the field \"sys_paths\": ", e);
		} catch (IllegalArgumentException e) {
			log.error("Call to Field.set failed: ", e);
		} catch (IllegalAccessException e) {
			log.error("Call to Field.set failed: ", e);
		}
	}
}
