package com.valorti.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractSqlHelper implements ISqlHelper {
	public class UnimplementedMethodException extends Exception {
		/**
		 * 
		 */
		private static final long	serialVersionUID	= 8784296697710957336L;

		public UnimplementedMethodException(String string) {
			super(string);
		}
	}
	protected final Logger		log					= LoggerFactory
															.getLogger(AbstractSqlHelper.class);
	protected String			connectionString;
	protected int				preparedIndexStart	= 1;

	protected static Connection	connection			= null;

	public AbstractSqlHelper(String connectionString) {
		this.connectionString = connectionString;
		loadDriver();
	}

	@Override
	public void close(Connection connection) {
		try {
			if (connection != null) {
				log.debug("Closing connection.");
				connection.close();
				log.debug("Connection closed.");
			}
		} catch (SQLException e) {
			log.error("Failed to close connection to SQL server: ", e);
		}
	}

	@Override
	public void closePreparedStatement(PreparedStatement ps) {
		try {
			if (ps != null) {
				log.debug("Closing prepared statement.");
				ps.clearParameters();
				ps.clearBatch();
				ps.close();
			}
		} catch (SQLException ex) {
			log.error("Failed to close Prepared Statement: ", ex);
		}
	}

	@Override
	public void closeResultSet(ResultSet rs) {
		try {
			if (rs != null) {
				log.debug("Closing result set.");
				rs.clearWarnings();
				rs.close();
			}
		} catch (SQLException ex) {
			log.error("Failed to close result set: ", ex);
		}
	}

	@Override
	public void closeStatement(Statement stmt) {
		try {
			if (stmt != null) {
				log.debug("Closing SQL Statement.");
				stmt.clearBatch();
				stmt.close();
			}
		} catch (Exception ex) {
			log.error("Failed to close a Statement: ", ex);
		}
	}

	/**
	 * Loads the required database driver.
	 */
	abstract void loadDriver();

	/**
	 * Parses the params and prepares the statement.
	 * 
	 * @param ps
	 *            - A prepared statement Object.
	 * @param params
	 *            - Variable length of parameters.
	 * @return A prepared statement.
	 * @throws SQLException
	 */
	protected PreparedStatement prepare(PreparedStatement ps, Object... params)
			throws SQLException {
		log.info("Preparing statements; Param count = {}", params.length);

		int i = preparedIndexStart;
		for (Object o : params) {
			// log.debug("Parameters: [Index={}, Value={}]", i, o);
			ps.setObject(i, o);
			i++;
		}

		return ps;
	}

	/**
	 * To abstract the queries, the result set is re-mapped so that we can close
	 * the connection after executing the query.
	 * 
	 * @param set
	 *            - ResultSet object.
	 * @return - A list if the re-mapping was successful or null on exception.
	 */
	protected List<Map<String, Object>> remapResultSet(ResultSet set) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		try {
			log.debug("Is result set null?: {}", set == null);

			while (set.next()) {
				Map<String, Object> row = new HashMap<String, Object>();
				ResultSetMetaData meta = set.getMetaData();
				int columnCount = meta.getColumnCount();

				for (int i = 1; i <= columnCount; i++) {
					row.put(meta.getColumnName(i), set.getObject(i));
				}

				list.add(row);
				row = null;
			}

		} catch (SQLException ex) {
			log.error("Failed to remap result set: ", ex);
		}

		return list;
	}
}
