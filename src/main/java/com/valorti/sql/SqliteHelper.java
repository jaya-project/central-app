package com.valorti.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqliteHelper extends AbstractSqlHelper {
	private final Logger	log	= LoggerFactory.getLogger(SqliteHelper.class);

	public SqliteHelper(String url) {
		super(url);
		loadDriver();
		preparedIndexStart = 1;
	}

	@Override
	public Connection connect() {
		Connection c = null;
		log.info("Trying to establish a connection to SQLite Database.");

		try {
			c = DriverManager.getConnection(connectionString);
		} catch (SQLException ex) {
			log.error("Connection to SQLite failed: ", ex);
		}

		log.info("Connection stablished.");

		return c;
	}

	public boolean execute(String query) {
		log.debug("Execute: {}", query);
		boolean r = false;
		Connection c = connect();
		Statement s = null;

		try {
			if (c != null) {
				log.debug("SqliteHelper.query({})", query);
				s = c.createStatement();

				if (s != null) {
					r = s.execute(query);
				}
			}
		} catch (SQLException ex) {
			log.error("Failed to execute query: ", ex);
		} finally {
			closeStatement(s);
			close(c);

			s = null;
			c = null;
		}

		return r;
	}

	@Override
	public long exists(String columnId, String table, String where,
			Object... params) {
		long r = 0;
		Connection c = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			c = connect();
			if (c != null) {
				boolean retrieveId = true;

				if (columnId == null || columnId.length() <= 0) {
					columnId = "*";
					retrieveId = false;
				}

				String query = new StringBuilder("SELECT ").append(columnId)
						.append(" FROM ").append(table).append(" WHERE ")
						.append(where).append(" LIMIT 1").toString();

				if (params.length > 0) {
					ps = prepare(c.prepareStatement(query), params);
					rs = ps.executeQuery();
				} else {
					rs = c.createStatement().executeQuery(query);
				}

				if (rs != null) {
					if (retrieveId) {
						if (rs.next())
							r = rs.getLong(1);
					} else {
						if (!rs.next())
							r = 1;
					}
				}
			}
		} catch (SQLException ex) {
			log.error("Failed to check existence: ", ex);
			r = -1;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
			close(c);

			rs = null;
			ps = null;
			c = null;
		}

		return r;
	}

	/**
	 * Empties the specified table by deleting all rows.
	 * 
	 * @param tableName
	 *            - The name of the table to clear.
	 * @return Number of affected rows.
	 */
	public int flushTable(String tableName) {
		Connection c = null;
		Statement s = null;
		int r = 0;

		try {
			c = connect();

			if (c != null) {
				StringBuilder sb = new StringBuilder("DELETE FROM ");
				sb.append(tableName);
				s = c.createStatement();

				if (s != null) {
					r = s.executeUpdate(sb.toString());
				}
			}
		} catch (SQLException ex) {
			log.error("Failed to flush table {}: ", tableName, ex);
			r = -1;
		} finally {
			closeStatement(s);
			close(c);

			s = null;
			c = null;
		}

		return r;
	}

	/**
	 * Inserts a row in the database.
	 * 
	 * @param query
	 *            - The insert statement.
	 * @return The last inserted row id on success or 0 on failure.
	 */
	public long insert(String table, String columns, String bindValues,
			Object... params) {
		Connection c = null;
		long r = 0;

		try {
			c = connect();

			if (c != null) {
				StringBuilder query = new StringBuilder("INSERT INTO ")
						.append(table);

				if (columns != null && columns.length() > 0) {
					query.append('(').append(columns).append(')');
				}

				query.append(" VALUES (").append(bindValues).append(')');
				String sql = query.toString();
				log.debug("Insert query: {}", sql);

				if (params.length > 0) {
					r = prepare(c.prepareStatement(sql), params)
							.executeUpdate();
				} else {
					r = c.prepareCall(sql).executeUpdate();
				}
			}
		} catch (SQLException ex) {
			log.error("Failed to insert data: ", ex);
			r = -1;
		} finally {
			if (r > 0)
				r = rowId(c);

			close(c);
			c = null;
		}

		return r;
	}

	@Override
	public void loadDriver() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException ex) {
			log.error("Could not find SQLite Driver: ", ex);
		}
	}

	@Override
	public List<Map<String, Object>> query(String query, Object... params) {
		List<Map<String, Object>> list = null;
		log.debug("Query: {}", query);

		if (query.length() > 0) {
			Connection c = null;
			ResultSet rs = null;
			PreparedStatement ps = null;

			try {
				c = connect();
				log.debug("Is connection open?: {}", c.isClosed());

				if (c != null && !c.isClosed()) {
					if (params.length > 0) {
						ps = prepare(c.prepareStatement(query), params);
						rs = ps.executeQuery();
					} else {
						rs = c.prepareCall(query).executeQuery();
					}

					if (rs != null) {
						list = this.remapResultSet(rs);
					} else {
						log.debug("NULL result set.");
					}
				} else {
					log.error("The connection was either null or closed: {}", c);
				}
			} catch (SQLException ex) {
				log.error("Failed to query the database: ", ex);
			} finally {
				closeStatement(ps);
				closeResultSet(rs);
				close(c);

				ps = null;
				rs = null;
				c = null;
			}
		}

		return list;
	}

	@Override
	public void resetAutoincrement(String table) {
		Connection c = null;
		Statement s = null;

		try {
			c = connect();

			if (c != null) {
				s = c.createStatement();
				int r = s.executeUpdate(new StringBuilder(
						"DELETE FROM sqlite_sequence WHERE name = '").append(
						table).toString());

				if (r > 0) {
					log.info("Auto increment counter successfully reset.");
				} else {
					log.warn("Could not reset auto increment counter. This may be because it was already reset or the DB engine did not return any modified rows.");
				}
			}
		} catch (SQLException ex) {
			log.error(
					"Failed to reset SQLite auto increment counter on table {}: ",
					ex);
		} finally {
			closeStatement(s);
			close(c);

			s = null;
			c = null;
		}
	}

	@Override
	public long rowId(Connection c) {
		long id = -1L;

		try {
			if (c != null) {
				id = c.createStatement()
						.executeQuery("SELECT last_insert_rowid() AS rowId")
						.getLong("rowId");
				log.debug("Last insert row id: {}", id);
			}
		} catch (SQLException ex) {
			log.error("Failed to retrieve row id: ", ex);
		}

		return id;
	}
}
