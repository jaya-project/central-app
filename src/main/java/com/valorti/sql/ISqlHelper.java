package com.valorti.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import com.valorti.sql.AbstractSqlHelper.UnimplementedMethodException;

public interface ISqlHelper {
	/**
	 * Tries to close the connection.
	 * 
	 * @param connection
	 *            - The Connection established to the database.
	 */
	public void close(Connection connection);

	/**
	 * Closes a prepared statement.
	 * 
	 * @param ps
	 *            - The prepared statement to close.
	 */
	public void closePreparedStatement(PreparedStatement ps);

	/**
	 * Closes a result set.
	 * 
	 * @param rs
	 *            - The result set to close.
	 */
	public void closeResultSet(ResultSet rs);

	/**
	 * Closes a statement.
	 * 
	 * @param stmt
	 *            - The statement to close.
	 */
	public void closeStatement(Statement stmt);

	/**
	 * Establish a connection to the database.
	 * 
	 * @return The connection on success or null on failure.
	 */
	public Connection connect();

	/**
	 * Checks if a row exists with the specified condition and retrieves the
	 * specified column(s).
	 * 
	 * @param idColumnName
	 *            - The name of the id column (primary key or any number column
	 *            that is needed).
	 * 
	 * @param table
	 *            - Table to check existence.
	 * @param where
	 *            - Where condition.
	 * @return 0 if there was an error, -1 if the row did not exist or the id
	 *         otherwise. If the idColumnName is not specified, then all columns
	 *         are retrieved. In this case, the return value is 1 if the row
	 *         exists, zero if not and -1 on error.
	 */
	public long exists(String idColumnName, String table, String where,
			Object... params);

	public List<Map<String, Object>> query(String query, Object... params);

	/**
	 * Resets the auto-increment value of the table.
	 * 
	 * @param table
	 *            - The table to reset the auto-increment value count.
	 * @throws UnimplementedMethodException
	 *             If a class implementing this interface is read only, this
	 *             method will be un-implemented.
	 */
	public void resetAutoincrement(String table)
			throws UnimplementedMethodException;

	/**
	 * Retrieves the last inserted row id.
	 * 
	 * @return The last inserted row id or -1L on error.
	 */
	public long rowId(Connection c);
}
