package com.valorti.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.Path;

public class PathUtils {

	public static final String	APP_PATH	= new File("").getAbsolutePath();
	public static final String	CONFIG_DIR	= "configuration";
	public static char			separator	= Path.SEPARATOR;

	private PathUtils() {
	}

	/**
	 * Combines various strings into one path.
	 * 
	 * @param basePath
	 *            - The path that will hold all the pieces.
	 * @param paths
	 *            - Pieces that will be joined to the path.
	 * @return The combined path.
	 */
	public static String combine(String basePath, String... paths) {
		StringBuilder path = new StringBuilder();
		path.append(basePath);

		if (paths.length > 0) {
			for (String s : paths) {
				String tmp = StringUtils.stripStart(
						StringUtils.stripEnd(s, "/\\"), "/\\");

				if (tmp.length() > 0) {
					path.append(separator);
					path.append(tmp);
				}
			}
		}

		return path.toString().trim();
	}

	/**
	 * Builds the full path to the relativeBase.
	 * 
	 * @param relativeBase
	 *            - The base path relative to the application root.
	 * @param paths
	 *            - Pieces to glue into the path.
	 * @return String - The combined path.
	 */
	public static String combineFull(String relativeBase, String... paths) {
		return combine(combine(APP_PATH, relativeBase), paths);
	}

	/**
	 * Generates a file name with the format prefix + SimpleDateFormat + sufix.
	 * This function only generates the file name without extension.
	 * 
	 * @param format
	 *            Date format to append to the name. If set to null, then the
	 *            default format is used: dd_MM_yyyy_HH_mm_ss
	 *            (20_08_2014_20_32_03).
	 * @param prefix
	 *            The prefix for the file name. Defaults to an empty string.
	 * @param sufix
	 *            Prefix to add at the end of the name and before the extension.
	 *            Defaults to an empty string.
	 * 
	 * @param extension
	 *            The extension of the file. If set to null or empty, the .txt
	 *            extension will be used.
	 * @return A string representing the generated file name.
	 */
	public static String generateFileName(SimpleDateFormat format,
			String prefix, String sufix, String extension) {
		StringBuilder s = new StringBuilder();

		if (prefix != null && prefix.length() > 0)
			s.append(prefix);

		if (format != null)
			s.append(format.format(Calendar.getInstance().getTime()));
		else
			s.append(new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss")
					.format(Calendar.getInstance().getTime()));

		if (sufix != null && sufix.length() > 0) {
			s.append(sufix);
		}

		if (extension != null && extension.length() > 0) {
			s.append('.').append(extension.replaceFirst("\\.", ""));
		} else
			s.append(".txt");

		return s.toString();
	}
}