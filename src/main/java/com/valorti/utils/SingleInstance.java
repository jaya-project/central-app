package com.valorti.utils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;

public class SingleInstance {
	private String		appName	= "";
	private File		file	= null;
	private FileLock	lock	= null;
	private FileChannel	channel	= null;

	public SingleInstance(String appName) {
		this.appName = appName;
	}

	public boolean isActive() {
		try {
			file = new File(String.format("%s.lock", appName));
			channel = new RandomAccessFile(file, "rw").getChannel();

			try {
				lock = channel.tryLock();
			} catch (OverlappingFileLockException ex) {
				release();
				return true;
			}

			if (lock == null) {
				release();
				return true;
			}

			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					release();
					delete();
				}
			});

			return false;
		} catch (Exception ex) {
			release();
			delete();
			return true;
		}
	}

	public void release() {
		try {
			lock.release();
		} catch (ClosedChannelException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				channel.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public void delete() {
		if (file != null && file.exists()) {
			try {
				Files.delete(file.toPath());
			} catch (SecurityException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} else {
			System.out.println("File was null or does not exist ["
					+ (file != null ? file.getAbsolutePath() : "null") + "]");
		}
	}
}
