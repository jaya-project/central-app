package com.valorti.utils;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import com.valorti.jaya.ui.Main;

public class SessionHelper {
	private String	algorithm;

	private int		maxSaltLength	= Main.configuration()
											.getInteger(
													"/Application/Session/MaxSaltLength");

	public SessionHelper() {
		algorithm = Main.configuration().getString(
				"/Application/Session/PasswordEncryption");
	}

	public SessionHelper(String algorithm) {
		this.algorithm = algorithm;
	}

	public static boolean compareHash(byte[] hash, byte[] hash1) {
		if (hash.length != hash1.length)
			return false;

		for (int i = 0; i < hash.length; i++) {
			if (hash[i] != hash1[i])
				return false;
		}

		return true;
	}

	/**
	 * 
	 * @param hash
	 * @return
	 */
	public static String humanReadableHash(byte[] hash) {
		return Base64.encodeBase64String(hash);
	}

	public byte[] encryptPassword(byte[] passwordBytes, byte[] saltBytes)
			throws NoSuchAlgorithmException {
		byte[] hash = new byte[passwordBytes.length + saltBytes.length];

		int i = 0;
		int j = 0;

		for (; j < passwordBytes.length; j++) {
			hash[i++] = passwordBytes[j];
		}

		for (j = 0; j < saltBytes.length; j++) {
			hash[i++] = saltBytes[j];
		}

		MessageDigest md = MessageDigest.getInstance(algorithm);
		hash = md.digest(hash);
		md = null;

		return hash;
	}

	public byte[] encryptPassword(String password, byte[] saltBytes)
			throws NoSuchAlgorithmException {
		byte[] passBytes = password.getBytes(Charset.forName("UTF-8"));
		return encryptPassword(passBytes, saltBytes);
	}

	/**
	 * Encrypts a password with algorithm specified.
	 * 
	 * @param password
	 *            - The password that will be encrypted.
	 * @param salt
	 *            - Random byte sequence to append to the hash.
	 * @return The generated password hash.
	 * @throws NoSuchAlgorithmException
	 */
	public byte[] encryptPassword(String password, String salt)
			throws NoSuchAlgorithmException {
		byte[] saltBytes = salt.getBytes(Charset.forName("UTF-8"));
		byte[] passBytes = password.getBytes(Charset.forName("UTF-8"));
		return encryptPassword(passBytes, saltBytes);
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public byte[] makeSalt() {
		return makeSalt(maxSaltLength);
	}

	/**
	 * Creates a random number sequence of bytes.
	 * 
	 * @param saltLength
	 *            - How many bytes to generate.
	 * @return The generated salt.
	 */
	public byte[] makeSalt(int saltLength) {
		if (saltLength <= 0 || saltLength > maxSaltLength)
			saltLength = maxSaltLength;

		Random r = new SecureRandom();
		byte[] salt = new byte[saltLength];
		r.nextBytes(salt);
		r = null;

		return salt;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
}
