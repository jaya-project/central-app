package com.valorti.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XmlUtils {
	private static final Logger	log	= LoggerFactory.getLogger(XmlUtils.class);

	private XmlUtils() {
	}

	public static Calendar getCalendar(Element e, String n, DateFormat df,
			Locale locale) {
		String v = getString(e, n);

		if (v.length() > 0) {
			Calendar c = Calendar.getInstance(locale);

			try {
				c.setTime(df.parse(v));
			} catch (ParseException ex) {
				log.error("Failed to convert string to Calendar: ", ex);
			}

			return c;
		}

		return Calendar.getInstance();
	}

	public static Date getDate(Element e, String nodeName, DateFormat format) {
		String v = getString(e, nodeName);

		if (v.length() > 0) {
			try {
				return format.parse(v);
			} catch (ParseException ex) {
				log.error("Failed to parse date: ", ex);
			}
		}

		return null;
	}

	public static Double getDouble(Element e, String node) {
		String v = getString(e, node);

		if (v.length() > 0) {
			v = v.replaceAll(",", ".");
			return Double.parseDouble(v);
		}

		return 0.0;
	}

	public static Integer getInt(Element el, String nodeName) {
		String v = getString(el, nodeName);

		if (v.length() > 0) {
			return Integer.parseInt(v);
		}

		return 0;
	}

	public static Long getLong(Element e, String node) {
		String v = getString(e, node);

		if (v.length() > 0) {
			return Long.parseLong(v);
		}

		return 0L;
	}

	public static String getString(Element e, String nodeName) {
		if (e != null) {
			String s = e.selectSingleNode(nodeName).getStringValue();

			if (s != null)
				return s;
		} else {
			log.debug("getString element was null.");
		}

		return "";
	}
}
