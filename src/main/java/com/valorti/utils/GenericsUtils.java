package com.valorti.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GenericsUtils {

	private GenericsUtils() {
	}

	/**
	 * Casts every object in the list to the specified class.
	 * 
	 * @param collection
	 *            The object collection to cast.
	 * @param clazz
	 *            The class to cast every object in the collection.
	 * @return A list of type clazz or null if the collection was null or empty
	 *         or clazz was null.
	 */
	public static <T> List<T> castList(Collection<?> collection,
			Class<? extends T> clazz) {
		if (collection != null && collection.size() > 0 && clazz != null) {
			List<T> list = new ArrayList<T>(collection.size());

			for (Object object : collection) {
				list.add(clazz.cast(object));
			}
			return list;
		}

		return null;
	}
}
