package com.valorti.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.ui.Main;

public class WinCEUtils {
	private final Logger		log							= LoggerFactory
																	.getLogger(WinCEUtils.class);
	/**
	 * If this flag is set, then the file on the other end will be overwritten,
	 * otherwise it will be skipped.
	 */
	public static final short	OPTION_OVERWRITE			= 0x000001;

	/**
	 * If this option is set the file(s) will be moved to the device. If not
	 * set, then the file(s) will be moved to the desktop.
	 */
	public static final short	OPTION_COPYTODEVICE			= 0x000010;

	/**
	 * Tells the software to delete the file after it's been moved.
	 */
	public static final short	OPTION_REMOVE_AFTER_COPY	= 0x000100;

	private File				log4netConfPath				= null;
	private String				binaryPath					= "";
	private Redirect			redirect					= null;

	/**
	 * @param binaryPath
	 *            Path to the RapiWrapper executable.
	 */
	public WinCEUtils(String binaryPath) {
		log4netConfPath = new File(Main.configuration().getString(
				"/Application/WinCE/RapiWrapper/ConfigurationFile"));
		this.binaryPath = binaryPath;
	}

	/**
	 * Copies a single file to or from a Windows CE device.
	 * 
	 * @param file
	 *            The file that will be copied.
	 * @param destFolder
	 *            The folder where the file will be saved. If the flag
	 *            OPTION_COPYTODEVICE is set, then this is the folder on the
	 *            device, whereas if not set, this is a folder on the desktop.
	 * @param options
	 *            One or more of the WinCEUtils constants OPTION_*.
	 * @return One of the following integers:
	 *         <ul>
	 *         <li>-1 = on any uncaught error or exception.</li>
	 *         <li>0 = The program didn't do anything.</li>
	 *         <li>1 = Success</li>
	 *         <li>2 = The devices was not found.</li>
	 *         <li>3 = The device is not connected.</li>
	 *         <li>4 = One of the files was not found.</li>
	 *         <li>5 = If the overwrite option is not set and the file already
	 *         exists (only applies for the copy from device to desktop).</li>
	 *         </ul>
	 * @throws FileNotFoundException
	 *             If the executable file doesn't exists.
	 */
	public int copyFile(File file, String destFolder, int options)
			throws FileNotFoundException {
		List<String> args = new ArrayList<String>();
		args.add("-df");
		args.add(destFolder);
		args.add("-sf");
		args.add(file.getParent());

		if ((options & WinCEUtils.OPTION_COPYTODEVICE) == WinCEUtils.OPTION_COPYTODEVICE) {
			log.trace("Copying file {} to DEVICE.", file.getName());
			args.add(String.format("-copyToDevice=%s", file.getName()));
		} else {
			log.trace("Copying file {} to DESKTOP.", file.getName());
			args.add(String.format("-copyToDesktop=%s", file.getName()));
		}

		if ((options & WinCEUtils.OPTION_OVERWRITE) == WinCEUtils.OPTION_OVERWRITE) {
			log.trace("Overwriting existings files on destination [{}].", destFolder);
			args.add("-o");
		} else {
			log.trace("Existing files on destination [{}] will not be overwritten.", destFolder);
		}

		if ((options & WinCEUtils.OPTION_REMOVE_AFTER_COPY) == WinCEUtils.OPTION_REMOVE_AFTER_COPY) {
			log.trace("Removing original file after copy.");
			args.add("-r");
		} else {
			log.trace("Original file will be kept.");
		}

		return exec(args);
	}

	/**
	 * Copies a single file to or from a Windows CE device.
	 * 
	 * @param file
	 *            The file that will be copied.
	 * @param destFolder
	 *            The folder where the file will be saved. If the flag
	 *            OPTION_COPYTODEVICE is set, then this is the folder on the
	 *            device, whereas if not set, this is a folder on the desktop.
	 * @param options
	 *            One or more of the WinCEUtils constants OPTION_*.
	 * @return See {@link WinCEUtils#copyFile(File, String, int)} for the return
	 *         code description.
	 * @throws FileNotFoundException
	 *             If the executable file doesn't exists.
	 */
	public int copyFile(String file, String destFolder, int options)
			throws FileNotFoundException {
		return copyFile(new File(file), destFolder, options);
	}

	/**
	 * Copies a list of files to or from a Windows CE device.
	 * 
	 * @param files
	 *            The list of files to copy.
	 * @param destFolder
	 *            Destination folder on the device or the desktop, depending if
	 *            the OPTION_COPYTODEVICE is set or not.
	 * @param options
	 *            One or more of the OPTION_* constants.
	 * @return A map with the file and response code for that specific file.
	 * @throws FileNotFoundException
	 *             If the executable file was not found.
	 */
	public Map<File, Integer> copyFiles(List<File> files, String destFolder,
			int options) throws FileNotFoundException {
		Map<File, Integer> returnCodes = new HashMap<File, Integer>();

		if (files != null && files.size() > 0) {
			for (File file : files) {
				returnCodes.put(file, copyFile(file, destFolder, options));
			}
		}

		return returnCodes;
	}

	/**
	 * Copies a single file from a Windows CE Device to Desktop.
	 * 
	 * @param file
	 *            the file to copy.
	 * @param destFolder
	 *            Destination folder on the device.
	 * @param overwrite
	 *            Pass true to overwrite the file if it exists.
	 * @param deleteAfterCopy
	 *            Pass true to delete original file after copy.
	 * @return See {@link WinCEUtils#copyFile(File, String, int)} for the return
	 *         code description.
	 * @throws FileNotFoundException
	 *             If the executable file is not found.
	 */
	public int copyFileToDesktop(File file, String destFolder,
			boolean overwrite, boolean deleteAfterCopy)
			throws FileNotFoundException {
		int options = -1;

		if (overwrite)
			options |= OPTION_OVERWRITE;

		if (deleteAfterCopy)
			options |= OPTION_REMOVE_AFTER_COPY;

		return copyFile(file, destFolder, options);
	}

	/**
	 * Copies a single file to a Windows CE device.
	 * 
	 * @param file
	 *            the file to copy.
	 * @param destFolder
	 *            Destination folder on the device.
	 * @param overwrite
	 *            Pass true to overwrite the file if it exists.
	 * @param deleteAfterCopy
	 *            Pass true to delete original file after copy.
	 * @return See {@link WinCEUtils#copyFile(File, String, int)} for the return
	 *         code description.
	 * @throws FileNotFoundException
	 *             If the executable file is not found.
	 */
	public int copyFileToDevice(File file, String destFolder,
			boolean overwrite, boolean deleteAfterCopy)
			throws FileNotFoundException {
		int opts = OPTION_COPYTODEVICE;

		if (overwrite)
			opts = OPTION_OVERWRITE;

		if (deleteAfterCopy)
			opts |= OPTION_REMOVE_AFTER_COPY;

		return copyFile(file, destFolder, opts);
	}

	/**
	 * Executes the RAPI wrapper binary file with the specified arguments.
	 * 
	 * @param args
	 *            The list of arguments to use.
	 * @return See {@link WinCEUtils#copyFile(File, String, int)} for the return
	 *         codes.
	 * @throws FileNotFoundException
	 *             See {@link WinCEUtils#copyFile(File, String, int)} for the
	 *             description.
	 */
	private int exec(List<String> args) throws FileNotFoundException {
		int rcode = 0;
		File exeFile = new File(binaryPath);

		if (exeFile.exists()) {
			args.add(0, exeFile.getAbsolutePath());
			args.add(1, "-c");
			args.add(2, log4netConfPath.getAbsolutePath());
			ProcessBuilder task = null;
			Process process = null;

			try {
				task = new ProcessBuilder(args);

				if (log.isTraceEnabled())
					log.trace("Arguments: {}", task.command());

				if (redirect != null) {
					task.redirectErrorStream(true);
					task.redirectOutput(redirect);
				}

				process = task.start();
				rcode = process.waitFor();
			} catch (Exception ex) {
				log.error("Failed to execute rapi wrapper.", ex);
				rcode = -1;
			} finally {
				task = null;

				if (process != null)
					process.destroy();

				process = null;
			}
		} else {
			throw new FileNotFoundException("The rapi wrapper (" + binaryPath
					+ ") binary file was not found.");
		}

		exeFile = null;

		return rcode;
	}

	/**
	 * @return the redirect
	 */
	public Redirect getRedirect() {
		return redirect;
	}

	/**
	 * Removes a single file from the Windows CE device.
	 * 
	 * @param file
	 *            The file that will be removed. Use an absolute path.
	 * @return See {@link WinCEUtils#copyFile(File, String, int)} for the return
	 *         codes.
	 * @throws FileNotFoundException
	 *             See {@link WinCEUtils#copyFile(File, String, int)} for a
	 *             description.
	 */
	public int removeFile(String file) throws FileNotFoundException {
		List<String> args = new ArrayList<String>();
		args.add(String.format("-rm=%s", file));
		return exec(args);
	}

	/**
	 * @param redirect
	 *            The redirect to be used to output any errors or output from
	 *            the executing binary.
	 */
	public void setRedirect(Redirect redirect) {
		this.redirect = redirect;
	}
}
