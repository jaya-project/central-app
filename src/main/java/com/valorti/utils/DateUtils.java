package com.valorti.utils;

import com.ibm.icu.util.Calendar;

public class DateUtils {

	public DateUtils() {
		// TODO Auto-generated constructor stub
	}

	public static int compareOnlyDate(Calendar c1, Calendar c2) {
		if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
				&& c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
				&& c1.get(Calendar.DAY_OF_MONTH) == c2
						.get(Calendar.DAY_OF_MONTH))
			return 0;

		if (c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR)
				&& c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH)
				&& c1.get(Calendar.DAY_OF_MONTH) > c2
						.get(Calendar.DAY_OF_MONTH)) {
			return 1;
		}

		return -1;
	}
}
