package com.valorti.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExpressionEngine {
	private final Logger	log			= LoggerFactory
												.getLogger(ExpressionEngine.class);

	private int				exprCount	= 0;
	private int				currExpr	= 0;

	protected List<String>	expressions	= new ArrayList<String>();

	public ExpressionEngine() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Evaluates a string and performs successive calls to the base object.
	 * 
	 * @param baseObj
	 *            The base object where the calls will be made.
	 * @param expression
	 *            Expression to evaluate.
	 * @return The result object or null if there is an exception.
	 */
	public Object eval(Object baseObj, String expression) {
		Object result = null;

		if (baseObj != null && expression != null && expression.length() > 0) {
			prepare(expression);
		} else {
			throw new IllegalArgumentException(
					"The base object and expression cannot be null or empty.");
		}

		return result;
	}

	private boolean prepare(String expr) {
		log.trace("Expression: {}", expr);
		String patternStr = null;
		Pattern pattern = null;
		Matcher matcher = null;

		try {
			patternStr = "(\\+|\\-|\\*|/)";
			pattern = Pattern.compile(patternStr, Pattern.MULTILINE);
			log.trace("Pattern: {}", pattern.pattern());
			matcher = pattern.matcher(expr);

			while (matcher.find()) {
				String group = matcher.group();
				log.trace("Group: {}", group);
				String[] split = expr.split("\\" + group + "");

				for (String s : split) {
					log.trace("Split: {}", s);
				}
			}

			if (expressions.size() <= 0) {
				expressions.add(expr);
			}

			return true;
		} catch (Exception ex) {
			log.error("Exception parsing expresion [{}].", expr, ex);
		} finally {
			patternStr = null;
			pattern = null;
			matcher = null;
		}

		return false;
	}
}
