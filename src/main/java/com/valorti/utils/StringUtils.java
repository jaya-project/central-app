package com.valorti.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtils {
	private static final Logger	log	= LoggerFactory
											.getLogger(StringUtils.class);

	public StringUtils() {
		// TODO Auto-generated constructor stub
	}

	public static Date getDate(String s, DateFormat format) {
		if (s != null && s.length() > 0) {
			try {
				return format.parse(s);
			} catch (ParseException e) {
				log.error("Failed to parse date: [format={}, string={}]", s,
						format, e);
			}
		}

		return null;
	}

	public static String getValueFromSplit(String value) {
		return getValueFromSplit(value, "-", 1);
	}

	public static String getValueFromSplit(String value, int returnIndex) {
		return getValueFromSplit(value, "-", returnIndex);
	}

	/**
	 * Splits the value specified by the separator and returns the returnIndex
	 * on the array.
	 * 
	 * @param value
	 *            The String value of a Combo box (it could be any other value).
	 * @param separator
	 *            A splitter String for the value.
	 * @param returnIndex
	 *            The index value on the array to be returned.
	 * @return A String with the value specified by the index. If the return
	 *         index is < 0, the returned String will be the first element on
	 *         the array; if the returnIndex is > than the length of the split
	 *         array, then it will return the last element; this is assuming
	 *         that the array length is greater than 0.
	 */
	public static String getValueFromSplit(String value, String separator,
			int returnIndex) {
		String out = "";
		String[] split = value.split(separator);

		if (split.length > 0) {
			if (returnIndex >= split.length)
				returnIndex = split.length - 1;

			if (returnIndex < 0)
				returnIndex = 0;

			out = split[returnIndex];
		}

		split = null;
		return out.trim();
	}

	public static String encodeURIComponent(String s, String encoding) {
		String result;

		try {
			result = URLEncoder.encode(s, encoding).replaceAll("\\+", "%20")
					.replaceAll("\\%21", "!").replaceAll("\\%27", "'")
					.replaceAll("\\%28", "(").replaceAll("\\%29", ")")
					.replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException ex) {
			log.error("Failed to encode URI component.", ex);
			result = s;
		}

		log.trace("Resulting URI: {}", result);

		return result;
	}
}
