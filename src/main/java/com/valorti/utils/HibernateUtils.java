package com.valorti.utils;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.registry.selector.spi.StrategySelectionException;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.valorti.jaya.ui.Main;

public class HibernateUtils {
	private static ServiceRegistry	serviceRegistry	= null;
	private static SessionFactory	factory	= null;
	private static final Logger		log				= LoggerFactory
															.getLogger(HibernateUtils.class);

	public static void close() {
		try {
			if (factory != null) {
				factory.close();
			} else {
				log.debug("Hibernate factory already closed.");
			}
		} catch (HibernateException ex) {
			log.error("Failed to close hibernate session factory.", ex);
		}

		serviceRegistry = null;
		factory = null;
	}

	private static Configuration configure(Configuration c) {
		try {
			c.setProperty(
					"hibernate.connection.url",
					Main.configuration()
							.getString(
									"/Application/DomainModels/DomainModel[@id='hibernate-sqlite']/ConnectionURL"));
			c.setProperty(
					"hibernate.show_sql",
					Main.configuration()
							.getString(
									"/Application/DomainModels/DomainModel[@id='hibernate-sqlite']/ShowSql"));
			c.setProperty(
					"hibernate.format_sql",
					Main.configuration()
							.getString(
									"/Application/DomainModels/DomainModel[@id='hibernate-sqlite']/FormatSql"));
			c.setProperty(
					"hibernate.connection.pool_size",
					Main.configuration()
							.getString(
									"/Application/DomainModels/DomainModel[@id='hibernate-sqlite']/PoolSize"));
		} catch (StrategySelectionException e) {
			log.error("Failed to select strategy: ", e);
		} catch (Exception e) {
			log.error("Could not configure hibernate! Cause: ", e);
		}

		return c;
	}

	public static SessionFactory getSessionFactory() {
		if (factory == null) {
			try {
				Configuration configuration = new Configuration();
				configuration.configure("/hibernate.cfg.xml");
				configuration = configure(configuration);
				serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				factory = configuration
						.buildSessionFactory(serviceRegistry);
			} catch (Throwable e) {
				log.error("Could not initialize hibernate! Cause: ", e);
			}
		}

		return factory;
	}
}
