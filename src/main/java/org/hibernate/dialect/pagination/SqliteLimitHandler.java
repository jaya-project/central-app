package org.hibernate.dialect.pagination;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.hibernate.engine.spi.RowSelection;

public class SqliteLimitHandler extends AbstractLimitHandler {
	public SqliteLimitHandler(String sql, RowSelection selection) {
		super(sql, selection);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hibernate.dialect.pagination.AbstractLimitHandler#bindLimitParameters
	 * (java.sql.PreparedStatement, int)
	 */
	@Override
	protected int bindLimitParameters(PreparedStatement statement, int index)
			throws SQLException {
		return super.bindLimitParameters(statement, index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.dialect.pagination.AbstractLimitHandler#
	 * bindLimitParametersFirst()
	 */
	@Override
	public boolean bindLimitParametersFirst() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.dialect.pagination.AbstractLimitHandler#
	 * bindLimitParametersInReverseOrder()
	 */
	@Override
	public boolean bindLimitParametersInReverseOrder() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hibernate.dialect.pagination.AbstractLimitHandler#forceLimitUsage()
	 */
	@Override
	public boolean forceLimitUsage() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hibernate.dialect.pagination.AbstractLimitHandler#supportsLimit()
	 */
	@Override
	public boolean supportsLimit() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hibernate.dialect.pagination.AbstractLimitHandler#supportsLimitOffset
	 * ()
	 */
	@Override
	public boolean supportsLimitOffset() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hibernate.dialect.pagination.AbstractLimitHandler#supportsVariableLimit
	 * ()
	 */
	@Override
	public boolean supportsVariableLimit() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hibernate.dialect.pagination.AbstractLimitHandler#useMaxForLimit()
	 */
	@Override
	public boolean useMaxForLimit() {
		return false;
	}
}